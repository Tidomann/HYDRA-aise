#![allow(dead_code)]
#![allow(non_snake_case)]
/*use std::ffi::CString;

use std::slice;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use bincode;
use byteorder::{WriteBytesExt, LE};
use directories::UserDirs;
use parking_lot::Mutex;
use serde::{self, Deserialize, Deserializer, Serialize, Serializer};

use crate::ai::{self, has_resources};
use crate::feature_disabled;
use block_alloc::RETURNllocSet;



use list::ListIter;
use order::{self, OrderId};
use rng::Rng;

use idle_tactics;
use swap_retain::SwapRetain;*/
use config::{self, Config, DataType, UnitsDatEntry, WeaponsDatEntry};
use data_extender::{self, FlingyValues, SpriteValues, ImageValues};
use globals::{
    Globals,HydraEffect
};
use bw_dat::{UnitId};
use std::ptr::null_mut;
use bw;
use ai;
use list::ListIter;
use game::Game;
use aiscript;
use unit::{self,Unit};
use order::{self, OrderId};
use std::ptr;
//use unit_search::UnitSearch;

/*
// 00441270 = SpiderMine_FindTarget?(), esi Unit *mine
pub unsafe fn spidermine_find_target_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit,)->*mut bw::Unit)->*mut bw::Unit {
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(unit);
				return 0;
			},
		};
		let mut flags = (*unit.0).flags;
		if flags & 0x8000 != 0 {
			return 0;
		}
		let mut range = bw::get_target_acquisition_range(unit.0);
		return bw::find_nearest_unit_box((32 * range) as u16, unit.0, bw::spidermine_enemy_proc() as u32, unit.0) // (int (__fastcall *)(_DWORD, _DWORD))SpiderMine_EnemyProc
	}
*/

//45d0d0
//Bugged!
pub unsafe fn order_building_morph_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)){
		orig(unit);
	
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(unit);
				return;
			},
		};
		
		let player = unit.player();
		let v1 = unit.current_train_id();
		let v3 = unit.id().gas_cost();
		let queue = unit.current_train_id();
		bw::player_build_minecost[player as usize] = unit.id().mineral_cost();
		bw::player_build_gascost[player as usize] = unit.id().gas_cost();
		let mut ignore = false;
		if bw::check_supply_for_building(player as u32, queue.0 as u32, 1) != 1 {
			ignore = true;
			bw_print!("Ignore");
		}
		let mut jump = false;	
		let mut msg = 0;
		let mut v6 = 0;
		if !ignore {
			let game = Game::get();
			let v2 = bw::player_list[player as usize].race;		
			if game.minerals(player) < bw::player_build_minecost[player as usize] {
				msg = 850;
				v6 =(*unit.0).order_flags;
				jump = true;
			}
			if game.gas(player) < bw::player_build_gascost[player as usize] {
				msg = 851;
				v6 = (*unit.0).invisibility_effects;
				jump = true;
			}		
		}
		if jump || ignore {
			if !ignore {
				bw::show_info_msg(player as u32, msg, v6 as u32);//player,message,sound
			}
			bw::append_order(0,null_mut(),228,bw::units_dat_return_order[unit.id().0 as usize] as u32,unit.0,0);
			return;
		}
		let mut v7 = unit.player();
		bw::player_ore[v7 as usize] -= bw::player_build_minecost[v7 as usize];
		bw::player_gas[v7 as usize] -= bw::player_build_gascost[v7 as usize];
		(*unit.0).flags &= !0x1;
		if unit.is_disabled() || unit.id().is_building() {
			(*unit.0).buttons = 236;
		}
		bw::set_construction_graphic(1,unit.0);
		let mut v9 = (*unit.0).order;
		(*unit.0).remaining_build_time = unit.id().build_time() as u16;
		(*unit.0).order_flags |= 0x1;
		bw_print!("Reach");
		if v9 != 0 {//order is not Die
			loop {
				let v10 = (*unit.0).order_queue_end;
				if v10==null_mut() {
					break;
				}
				let v11 = (*v10).order_id;
				if bw::orders_dat_can_interrupt[v11 as usize] == 0 && v11 != 45 {
					break;
				}
				bw::remove_order_from_queue(v10, unit.0);
			}
			let _ = bw::perform_another_order(0x2D,null_mut(),unit.0,228,0,null_mut());
		}
		bw::do_next_queued_order(unit.0);
		for img in ListIter((*(*unit.0).sprite).first_image){
			let mut play_anim = true;
			if (*img).image_id!=200 {
				bw::set_iscript_anim(img, 13);
			}
		}
		bw::update_ui();
	}
	


// 0045D410 = CancelBuildingMorph(), eax Unit *unit -- https://pastebin.com/raw/DFzQLr1m
pub unsafe fn cancel_building_morph_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(unit);
				return;
			},
		};
		let v2 = unit.current_train_id();
		let v3 = unit.player();
		let v4 = 3*bw::units_dat_gas_cost[v2.0 as usize];
		let game = Game::get();
		(*game.0).minerals[v3 as usize] += ((3*bw::units_dat_ore_cost[v2.0 as usize])/4) as u32;
		(*game.0).gas[v3 as usize] += v4 as u32/4;
		(*unit.0).build_queue[(*unit.0).current_build_slot as usize] = 228;
		let v5 = unit.hitpoints();
		(*unit.0).flags |= 0x1;
		bw::modify_unit_counters2_func(-1, unit.0, 1);
		(*unit.0).flags &= 0xffff_fffe;
		bw::finish_unit_pre(unit.0);
		bw::finish_unit(unit.0);
		bw::set_hp(unit.0, v5 as u32);
		let mut play_sound = true;
		let config = config::config();
		if config.hydra_misc {
			match unit.id().0 {
				158|161|182|183|184=>{
					play_sound = false;
				},
				_=>{},
			}
		}		
		if play_sound {
			bw::playsound_unit(1,0,4,unit.0);
		}
		let mut img = (*(*unit.0).sprite).first_image;
		while img != null_mut(){
			bw::play_image_iscript(img, 15);
			img = (*img).next;
		}
	
		/*
		let mut queue = unit.current_train_id();
		let mut player = unit.player();
		let mut hp = unit.hitpoints();
		let game = Game::get();
		(*game.0).minerals[player as usize] += (3*bw::units_dat_ore_cost[queue.0 as usize]/4) as u32;
		(*game.0).gas[player as usize] += (3*bw::units_dat_gas_cost[queue.0 as usize]/4) as u32;
		(*unit.0).build_queue[(*unit.0).current_build_slot as usize] = 228;
		(*unit.0).flags |= 0x1;
		bw::modify_unit_counters2_func(-1, unit.0, 1);
		(*unit.0).flags &= 0xFFFF_FFFE;
		bw::finish_unit_pre(unit.0);
		bw::finish_unit(unit.0);
		bw::set_hp(unit.0, hp as u32);
		let mut play_sound = true;
		let config = config::config();
		if config.hydra_misc {
			match unit.id().0 {
				158|161|182|183|184=>{
					play_sound = false;
				},
				_=>{},
			}
		}
		if play_sound {
			bw::playsound_unit(1,0,4,unit.0);
		}
		for img in ListIter((*(*unit.0).sprite).first_image){
			let mut play_iscript = true;
			match (*img).image_id {
				200=>{
					play_iscript=false;
				},
				_=>{},
			}
			if play_iscript {
				bw::play_image_iscript(img, 15);
			}
		}
		*/
	}

/*
// 0046E1C0 = CheckUnitDatRequirements(), arg 1 player_id, eax unit_id, esi Unit *parent -- https://pastebin.com/raw/kAtNKcqJ
pub unsafe fn check_unit_dat_requirements_hook(
	player: u32,
	unitid: u32,
	parent: *mut bw::Unit,
	orig: &dyn Fn(u32, u32, *mut bw::Unit,)->i32,)->i32 {
		let unit = match Unit::from_ptr(parent) {
			Some(s) => s,
			None => {
				return orig(player, unitid, parent);
			},
		};
		let mut loword |= unitid >> 0x0000_ffff; // loword, 0x0000_ffff might be wrong
		bw::last_error = 0;
		if unitid >= 0xE4 {
			bw::last_error = 16;
			return 0;
		}
		if player != (*parent.0).player {
			bw::last_error = 1;
			return 0;
		}
		if (*parent.0).flags & 1 == 0 { // probably can do is_complete(parent.0)?
			bw::last_error = 20;
			return 0;
		}
		if bw::unit_is_frozen(parent.0) == 1 {
			bw::last_error = 10;
			return 0;
		}
		if bw::unit_availability[228 * player + loword as u16] == 0 {
			bw::last_error = 2;
			return 0;
		}
		if bw::is_constructing_building(parent.0, 4) != 0 {
			// label 86
			bw::last_error = 3;
			return 0;
		}
		if bw::unit_req_offset[loword] == 0xffff {
			bw::gen_unit_req_offsets();
		}
		let mut index = bw::unit_req_offset[loword] as u16;
		if bw::unit_req_offset[loword] == 0 {
			// label 93
			bw::last_error = 23;
			return 0;
		} 
		let mut v26 = 1;
		let mut v25 = 1;
		let mut table |= bw::requirement_table[index] >> 0x0000_ffff; // loword, 0x0000_ffff might be wrong
		let mut v27 = 0;
		if table as u16 == -1 {
			return 1;
		}
		// label 18
		let mut v8 = 0;
		let mut v28 = 0;
		let mut v10 = 0;
		while 2 {
			table = table as u16;
			match table {
				reqOpcode_Current_unit_is => { // how do I define this?
					let mut v22 = bw::requirement_table[index += 1];
					if bw::completed_unit_count[0][player + 12 * v22] != 0 {
						v26 = 0;
					}
					if v22 == (*parent.0).type {
						v27 = 0;
						v25 = 0;
						v10 = v8 + 1;
						// goto label 72
					}
					if v25 != 0 {
						v27 = 1;
					}
					// goto label 73
				}
				reqOpcode_Must_have_ => {
					let mut v23 = bw::requirement_table[index += 1] as u16;
					let mut v24 = bw::all_unit_count[0].count[player + 12 * v23] + bw::completed_unit_count[0][player + 12 * v23];
					// goto label 71
				}
				reqOpcode_Must_have_addon => {
					if bw::cheat_flags & 0x2000 {
						index += 1;
						v10 = v8 + 1;
					}
					else {
						let mut v11 = (*parent.0).unit_specific;
						if v11 == 0 || (index += 1, v11.unit_id != bw::requirement_table[index]) {
							if v27 == 0 {
								bw::last_error = 4;
							}
							return -1;
						}
						v10 = v8 + 1;
					}
					// goto label 72
				}
				reqOpcode_Is_not_lifted_off => {
					if (*parent.0).flags & 2 == 0 {
						bw::last_error = 7;
						return 0;
					}
					v10 = v8 + 1;
					// goto label 72
				}
				reqOpcode_Is_not_training_or_morphing => {
					let mut v12 |= bw::is_training_or_building_addon(parent.0) >> 0x00_ff;
					if v12 != 0
					|| (((*parent.0).unit_specific+0x8) >> 0x00_ff) != 44
					|| BYTE1((*parent.0).unit_specific) != 61
					{ // no clue how to do byte1, but it's checking if the structure is upgrading/researching
						// goto label 87
					}
					v29 ++ 1;
					// goto label 73
				}
				reqOpcode_Is_not_constructing_addon => {
					let mut rusted |= bw::is_building_addon(parent.0) >> 0x00_ff;
					let mut rusted2 |= bw::is_building_protoss(parent.0) >> 0x00_ff;
					if rusted != 0 || rusted2 != 0 {
						// goto label 87
					}
					v10 = goodprogram + 1; // goodprogram is a random variable depending on how many times you disassemble the function, v15 in pastebin link
					// goto label 72
				}
				reqOpcode_Is_not_researching => {
					if (((*parent.0).unit_specific + 0x8) >> 0x00_ff) != 44 { // checks if currently researched tech is out of techdata array
						// goto label 87
					}
					v10 = v8 + 1;
					// goto label 72
				}
				reqOpcode_Is_not_upgrading => {
					if BYTE1((*parent.0).unit_specific+0x8) != 61 {
						// goto label 87
					}
					v10 = v8 + 1;
					// goto label 72
				}
				reqOpcode_Does_not_have_addon_attached => {
					if (*parent.0).unit_specific != 0 {
						if v24 == 0 {
							bw::last_error = 4;
						}
						return 0;
					}
					v10 = v8 + 1;
					// label 72
					v25 = v10;
					// label 73
					if bw::requirement_table[index += 1] == -255 {
						let mut table2 |= bw::requirement_table[index];
						v8 = v25;
						v6 += 1;
						continue;
					}
					if v25 == 0 && v23 != 0 {
						bw::last_error = 8;
						return -1;
					}
					table2 |= bw::requirement_table[index];
					v23 = 1;
					if table2 as u16 != -1 {
						bw::last_error = 25;
						return -1;
					}
					return 1;
				}
				reqOpcode_Has_hangar_space => {
					let mut v14 = -1;
					let mut carrier |= bw::is_carrier(parent.0) >> 0x00_ff;
					if carrier == 1 {
						let mut v16 = BYTE1((*parent.0).unit_specific + bw::get_train_queue_size(v13, parent.0 as u32));
					}
					else {
						let mut reaver |= bw::is_reaver(parent.0) >> 0x00_ff;
						if reaver != 0 {
							// goto label 86
						}
						let mut v16 = bw::get_train_queue_size(v17, parent.0 as u32);
					}
					let mut v14 |= ((*parent.0).unit_specific >> 0x00_ff) + v16;
					// label 51
					if v14 >= bw::get_hangar_capacity(parent.0) {
						// goto label 86
					}
					v25 += 1;
					// goto label 73
				}
				reqOpcode_Does_not_have_a_loaded_nuke => {
					if (*parent.0).unit_specific + 0x10 != 0 {
						// goto label 86
					}
					v10 = v8 + 1;
					// goto label 72
				}
				reqOpcode_Is_not_burrowed => {
					if (*parent.0).flags & 0x10 == 1 {
						// label 87
						bw::last_error = 5;
						return 0;
					}
					v10 = v8 + 1;
					// goto label 72
				}
				reqOpcode_Grey => {
					bw::last_error = 21;
					return -1;
				}
				reqOpcode_Blank => {
					// goto label 93
				}
				reqOpcode_Must_be_Brood_War => {
					if is_bw == 0 {
						bw::last_error = 26;
						return 0;
					}
					v10 = v8 + 1;
					// goto label 72
				}
				reqOpcode_Is_researched => {
					let mut table69 = bw::requirement_table[index += 1];
					if bw::is_tech_researched(player, v9) == 0 && bw::cheat_flags & 0x1000 == 0 {
						bw::last_error = 0;
						return -1;
					}
					v25 += 1;
					// goto label 73
				}
				reqOpcode_Is_burrowed => {
					if (*parent.0).flags & 0x10 == 0 {
						bw::last_error = 5;
						return -1;
					}
					v10 = v8 + 1;
					// goto label 72
				}
				_ => {
					if bw::cheat_flags & 0x2000 != 0 {
						v8 += 1;
					}
					let mut v24 = bw::completed_unit_count[0][player + 12 * table];
					// label 71
					// goto label 72
				}
			}
		}
	}
*/
// 00473150 = UpdateNydusPlacementState(), eax player, arg 1 x_tile, arg 2 y_tile, arg 3 size_wh, arg 4 placement_state_entry, arg 5 check_vision
/*pub unsafe fn update_nydus_placement_state_hook(
	player: u8,
	argX: u16,
	argY: u16,
	size: u32,
	entry: u32,
	visFlag: u32,
	orig: &dyn Fn(u8, u16, u16, u32, u32, u32)->u32)->u32 {
	
		let mut playerType = bw::player_list[player as usize].ty;
		let mut playerFlag: u32 = 0; // v18
		let mut playerFlagB: u32 = 0; // v17
		if playerType == 2 {
			playerFlag = 1 << (player + 8);
		}
		else {
			playerFlag = 0;
		}
		if playerType == 2 {
			playerFlagB = 1 << player;
		}
		else {
			playerFlagB = 0;
		}
		let game = Game::get();
		let mut tileVar = *(*bw::tile_flags).offset((argX + ((*game.0).map_width_tiles * argY)) as isize); // v7
		let mut sizeVar = 0; // v8
		let mut v9 = 0;
		let mut v10 = 0;
		let mut v11 = 0;
		let mut v12 = 0;
		let mut v13 = 0;
		let mut width = 0; // v15
		let mut v16: u32 = 0;
		let mut v19 = 0;
		let mut v20: u8 = 0;
		let mut current_block = 0;
		let mut globals = Globals::get("nydus_placement_hook");
		let transfusion = aiscript::get_new_upgrade_level_upg(game,&mut globals.upgrades,74,player);
		drop(globals);
		bw_print!("Try...");
		let mut height = (size & 0xffff0000)>>16;
		let mut width = size & 0x0000ffff;
		let creep_flag = match visFlag {
		  0=>0x400000,
		  _=>0x40000000,
		};
		let mut ret_result = 0;
		let mut placement_status = 0;
		
		let mut offset = 0;
		let mut xoffset = 0;
		for i in 0..width {
			for j in 0..height {
				
				let x_tile = argX+i as u16;
				let y_tile = argY+j as u16;
				tileVar = *(*bw::tile_flags).offset((x_tile + ((*game.0).map_width_tiles * y_tile)) as isize);
				placement_status = 0;
				let offset = j+(i*height);
				let current_placement_state = 6*(offset+(8*entry));
				bw::building_placement_state[current_placement_state as usize] = 0;
				
				if visFlag!=0 && tileVar & playerFlagB != 0 {
					//bw_print!("P10");
					placement_status = 10;
				}
				else if visFlag==1 && tileVar & playerFlagB !=0 {
					//bw_print!("P9");
					placement_status = 9;
//					continue;
				}
				else {
					if tileVar & creep_flag == 0 && transfusion==0 {
						//bw_print!("P5");
						placement_status = 5;
					}
					//bw_print!("P0");
				}
				if ret_result == 0 {
					ret_result = placement_status;
				}
				bw::building_placement_state[current_placement_state as usize] = placement_status;
				//offset+=1;
			}
			//offset+=1;
			
		}
		return ret_result as u32;
}
*/

/*
pub unsafe fn update_nydus_placement_state_hook(
	player: u8,
	argX: u16,
	argY: u16,
	size: u32,
	entry: u32,
	visFlag: u32,
	orig: &dyn Fn(u8, u16, u16, u32, u32, u32)->u32)->u32 {
	
		let mut playerType = bw::player_list[player as usize].ty;
		let mut playerFlag: u32 = 0; // v18
		let mut playerFlagB: u32 = 0; // v17
		if playerType == 2 {
			playerFlag = 1 << (player + 8);
		}
		else {
			playerFlag = 0;
		}
		if playerType == 2 {
			playerFlagB = 1 << player;
		}
		else {
			playerFlagB = 0;
		}
		let game = Game::get();
		let mut tileVar = *(*bw::tile_flags).offset((argX + ((*game.0).map_width_tiles * argY)) as isize); // v7
		let mut sizeVar = 0; // v8
		let mut v9 = 0;
		let mut v10 = 0;
		let mut v11 = 0;
		let mut v12 = 0;
		let mut v13 = 0;
		let mut width = 0; // v15
		let mut v16: u32 = 0;
		let mut v19 = 0;
		let mut v20: u8 = 0;
		let mut globals = Globals::get("nydus_placement_hook");
		let transfusion = aiscript::get_new_upgrade_level(game, &mut globals, 74,player);
		if (size & 0xffff0000)>>16 > 0 {
			sizeVar = (size & 0x000ffff) as u16;
			v9 = 0;
			width = 4 * ((*game.0).map_width_tiles - sizeVar);
			loop {
				v19 = 0;
				if sizeVar > 0 {
					break;
				}
				'label21: loop {
					// label 21
					sizeVar = (sizeVar as u16 + width) as u16;
					v16 += 1;
					v9 = v16 as u16;
					if v16 as u32 >= (size & 0xffff0000)>>16 {
						return v20 as u32;
					}
					v10 = 6 * (v9 + 8 * entry as u16);
					v11 = 0;
					loop {
						v12 = tileVar;
						v13 = 0;
						let mut wait_to_label20 = false;
						let mut wait_to_label19 = false;
						'label19: loop {
							
							if tileVar & playerFlag != 0 || transfusion>0 || wait_to_label19 {
								if !wait_to_label19 {
									v13 = 10;
								}
								v20 = v13;
								wait_to_label20 = true;
							}
							if !wait_to_label20 && (visFlag == 1 && v12 & playerFlagB != 0) {
								v13 = 9;
							}
							else {
								if !wait_to_label20 {
									let flag_cmp = match visFlag {
									  0=>0x400000,
									  _=>0x40000000,
									};
									if v12 & flag_cmp != 0 {
										continue 'label19;
									}
								}
								sizeVar = (size & 0x0000ffff) as u16;
								bw::building_placement_state[(v10 + v11) as usize] = v13;
								tileVar += 1;
								v19 += 1;
								v11 = v19 as u16;
								if v19 as u16 >= (size & 0x0000ffff) as u16 {
									continue 'label21;
								}
							}
							break 'label19;
						}
					}
					break;
				}
			}
		}
		return 0;
	}
*/


/*
// 0048B2D0 = BulletState_Bounce(), edi Bullet *bullet -- https://pastebin.com/raw/DPkb5FGX
pub unsafe fn bullet_state_bounce_hook(
	bullet: *mut bw::Bullet,
	orig: &dyn Fn(*mut bw::Bullet)) {
		// need to initialize unit and bullet structs
		let mut target = (*bullet.0).target;
		let mut bounce = (*bullet.0).remaining_bounces;
		let mut bounceTarget: = 0;
		let mut i = 0;
		let mut j = 0;
		if target != null_mut() {
			if (*bullet.0).hit_flags & 1 == 0 {
				bw::change_move_pos(bullet.0, (*target.0).sprite.position + 2, (*target.0).sprite.position);
			}
		}
		bw::progress_bullet_movement(bullet.0);
		bounceTarget = bw::choose_bounce_target(*bullet.0);
		if (*bullet.0).move_target == (*bullet.0).position && (*bullet.0).move_target + 2 == (*bullet.0).position + 2 {
			bounce = (*bullet.0).remaining_bounces - 1;
			(*bullet.0).remaining_bounces = bounce;
			if bounce != 0 && (*bullet.0).next_bounce_target == (*bullet.0).target {
				i = (*bullet.0).sprite.main_image;
				for i in 0..0 {
					bw::set_iscript_anim(i.0, 0xD); // Special state 1
					i = (*i.0).next;
				}
			/*  for ( i = a1->sprite->overlay; i; i = i->next )
			playImageIscript(i, ISCRIPT_Anim_SpecialState1);
			a1->targetUnit = v3;*/
			(*bullet.0).target = bounceTarget;
			}
			else {
				(*bullet.0).order_state = 5;
				if bounceTarget != null_mut() {
					(*bullet.0).order_target_pos = (*bounceTarget.0).sprite.position;
					(*bullet.0).order_target_pos + 2 = (*bounceTarget.0).sprite.position + 2;
				}
				else {
					(*bullet.0).target = 0;
				}
				let mut sprite = (*bullet.0).sprite;
				(*bullet.0).order_signal |= 0 >> 0x00_ff; // LOBYTE(a1->field_4E) = 0;
				(*bullet.0).order_fow_unit = 0xE4; // None
				j = (*sprite.0).main_image;
				for j in 0..0 {
					bw::set_iscript_anim(j.0, 1); // Death
					j = (*j.0).next;
				}
			}
		}
	}
*/
/*
// 0048B1E0 = ChooseBounceTarget(), eax Bullet *bullet -- https://pastebin.com/raw/x7Uv8wCF
pub unsafe fn choose_bounce_target_hook(
	bullet: *mut bw::Bullet,
	orig: &dyn Fn(*mut bw::Bullet,)->*mut Unit,)->*mut Unit {
		let mut source = (*bullet.0).source_unit;
		if source == null_mut() {
			return 0;
		}
		let mut bounceTarget = (*bullet.0).next_bounce_unit;
		bw::bullet_bounce_missile_source = source.0;
		let mut target = (*bullet.0).target;
		let mut sprite = (*bullet.0).sprite;
		bw::bullet_bounce_missile_target = target.0;
		target |= ((*sprite.0).position + 2) >> 0x0000_ffff;
		sprite |= (*sprite.0).position >> 0x0000_ffff;
		bw::bullet_bounce_missile_target_next = bounceTarget.0;
		let mut box = 0; // dwBox a1; // [esp+0h] [ebp-8h] // not sure how to define this one
		box.x = sprite as u16 - 96;
		box.w = sprite as u16 + 96;
		box.y = target as u16 - 96;
		box.h = target as u16 + 85;
		return bw::for_each_unit_borders_in_area(box, bw::find_next_bounce_target(bounceTarget.0), 0); // find_next_bounce_target doesn't have an argument passed in pseudocode
	}
*/
// 0x00428810 BTNSCOND_HasNuke -- https://pastebin.com/raw/ugbxggxK
pub unsafe fn btnscond_has_nuke_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)->i32)->i32 {
		let mut nuke = bw::first_player_unit[(*unit).player as usize];
		if nuke == null_mut() {
			return -1;
		}
		let nuke_u = Unit::from_ptr(nuke).unwrap();
		while nuke_u.id().0 != 108 || nuke_u.has_nuke() {
			nuke = (*nuke).next_player_unit;
			if nuke == null_mut(){
				return -1;
			}
		}
		return 1;
	}

/*
// 0x00423A40 BTNSACT_UseNuke -- https://pastebin.com/raw/CJ6gpZit
pub unsafe fn btnsact_use_nuke_hook(
	this: u32,
	orig: &dyn Fn(u32),){
		let mut orderUnit = 0x7E;
		let mut orderGround = 0x7E;
		let mut orderObstruct = 0x7E;
		let mut netTable1 = bw::network_tbl < 0x3E;
		let mut netTable2 = bw::network_tbl == 62;
		bw::is_targeting = 1;
		let mut string: char = 0;
		if netTable1 == 1 || netTable2 == 1 {
			string = "";
		}
		else {
			string = (bw::network_tbl + bw::network_tbl[63]) as char;
		}
		bw::unk_nuke_message(this, string as u32);
		bw::reset_game_screen_event_handlers();
		bw::unk_nuke = 2;
		bw::orders_buttonset = 229;
		bw::update_current_buttonset();
	}

// 0x004231D0 CMDACT_CancelNuke -- https://pastebin.com/raw/M6FWsvpT
pub unsafe fn cmdact_cancel_nuke_hook(
	orig: &dyn Fn()->u8)->u8 {
		bw::send_command(46, 1);
	}
	*/
	

// 0042E7A0 = displayIdlePortrait -- https://pastebin.com/raw/0Tfmn4k2


// 00442DA0 = ?(), ecx Unit *other, edx Unit *self -- https://pastebin.com/raw/WLuGKQLC
/*
pub unsafe fn pick_attack_target(
	target: *mut bw::Unit,
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit, *mut bw::Unit,)->u32,)->u32 {
	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => {
			return orig(target, unit);
		},
	};
	let target = match Unit::from_ptr(target) {
		Some(s) => s,
		None => {
			return orig(target, unit.0);
		},
	};
	let spriteTgt = (*target.0).sprite;
	let config = config::config();
	if let Some(spriteTgt) = target.sprite(){
		if unit.0 != target.0 {
			if (1 << ((*unit.0).player as u8)) & (*spriteTgt).visibility_mask != 0 {
				let canAttack = bw::can_attack_unit(1, unit.0, target.0);
				if canAttack != 0 {
					let mut are_enemies = bw::are_enemies(unit.0 as u32, target.0 as u32);
					if config.hydra_misc && (*unit.0).is_blind != 0 {
						are_enemies = 1;
					}
					if are_enemies != 0 {
						if *bw::min_range_px == 0 || (bw::is_in_area(unit.0, *bw::min_range_px, target.0) == 0 && 
												bw::is_in_area(unit.0, *bw::max_range_px, target.0) != 0) {
							
							let mut sub = (*unit.0).subunit;
							if bw::issubunit((*unit.0).subunit) == 0 {
								sub = unit.0;
							}
							let id = Unit::from_ptr(sub).unwrap().id();
							if (*sub).flags & 0x10000 == 0 {
								if let Some(w_id) = id.ground_weapon(){
									if bw::check_firing_angle(sub, (*spriteTgt).position.x as u32, 
										(*spriteTgt).position.y as u32, w_id.0 as u8) == 0 {
										return 0;
									}								
								}
							}
							if bw::is_unk_target(unit.0, target.0) == 0 {
								let mut threatLevel = bw::get_threat_level(unit.0, target.0);
								bw::add_unit_to_possible_targets(threatLevel, target.0);
							}
						}
					}
				}
			}
		}
	}
	return 0;
}
*/
/*
// 004593A0 = CmdBtn_DrawTooltip(), arg 1 Dialog *dlg, eax Control *button -- https://pastebin.com/raw/cjRjtdAa
pub unsafe fn cmdbtn_draw_tooltip_hook(
	bin1: *mut bw::Control,
	bin2: *mut bw::Control,
	orig: &dyn Fn(*mut bw::Control, *mut bw::Control)) {
		if bin1 == bin2 {
/*			bw::hide_tooltip();
			bw::tooltip_minerals = 0;
			bw::tooltip_gas = 0;
			bw::tooltip_energy_p = 0;
			bw::tooltip_energy_t = 0;
			bw::tooltip_energy_z = 0;
			bw::tooltip_supply_p = 0;
			bw::tooltip_supply_t = 0;
			bw::tooltip_supply_z = 0;
			bw::tooltip_upgrade_repeat = 0;*/
			bw::clear_cmdbtn_tooltip(); // originally inlined
			return;
		}
		let mut bin1Var = bin1;
		let mut bin2Var = bin2;
		let mut field3E = *bin1.next + 0x3E;
		if bin1Var == null_mut() {
			if field3E == null_mut() {
				return;
			}
			let mut flags = *field3E.flags;
			if flags & 1 == 0 { // Update image event
				*field3E.flags =| 1;
				bw::mark_control_dirty(field3E);
			}
			field3E = 0;
			//Label 43
			bw::clear_cmdbtn_tooltip();
			return;
		}
		if *bin1.flags & 8 == 0 {
			bw::clear_cmdbtn_tooltip();
			return;
		}
		let mut val = *bin1.val;
		let mut string = bw::get_stat_txt_str(val[8]);
		let mut stringArr = string[1];
		if v6 & 2 != 0 {
			field3E = bin1;
			bw::set_tooltip_resources(0, 0, 0, 0, 0, 0, 0, 0);
			let mut valArr = val[9];
			bw::tooltip_upgrade_repeat = 0;
			if valArr != null_mut() {
				let mut valArr2 = valArr;
				if stringArr == 2 {
					valArr2 = bw::get_upgrade_level((*bw::active_sel.0).player as u8, var[7]) as u8 + 8;
				}
				let mut txtString = bw::get_stat_txt_str(valArr2);
				bw::draw_cmdbtn_tooltip_text(txtString as u32);
			}
			else {
				bw::clear_cmdbtn_tooltip();
			}
		}
		else {
			bw::highlight(field3E);
			let mut valArr3 = valArr - 1;
			let mut ttMinerals: u16 = 0;
			let mut ttGas: u16 = 0;
			let mut ttUpgRep: u16 = 0;
			let mut ttEnergyP: u16 = 0;
			let mut ttEnergyT: u16 = 0;
			let mut ttEnergyZ: u16 = 0;
			let mut ttSupplyP: u16 = 0;
			let mut ttSupplyT: u16 = 0;
			let mut ttSupplyZ: u16 = 0;
			// SWITCH valArr3
			match valArr3 {
				1 => {
					let mut local = val[7];
					let mut groupFlags = bw::units_dat_group_flags[local];
					let mut lobyte |= ((bw::units_dat_flags[local] & 0x400) != 0) + 1 << 0x00_ff;
					let mut reqSupply1: u16 = 0;
					let mut reqSupply2: u16 = 0;
					let mut reqSupply3: u16 = 0;
					if groupFlags & 4 == 1 {
						reqSupply1 = bw::units_dat_supply_req[local];
					}
					let mut math1 = reqSupply1 * lobyte as u8;
					if groupFlags & 2 == 1 {
						reqSupply2 = bw::units_dat_supply_req[local];
					}
					let mut math2 = reqSupply2 * lobyte as u8;
					if groupFlags & 1 == 1 {
						reqSupply3 = bw::units_dat_supply_req[local];
					}
					let mut math3 = reqSupply3 * lobyte as u8;
					ttSupplyZ = (math3 * lobyte as u8) >> 1;
					ttMinerals = bw::units_dat_ore_cost[local];
					ttSupplyT = math2 >> 1;
					ttSupplyP = math1 >> 1;
					ttGas = bw::units_dat_gas_cost[local];
					//label 36
					ttEnergyP = 0; 
					ttEnergyT = 0;
					ttEnergyZ = 0;
					//label 37
					bw::set_tooltip_resources(ttMinerals, ttEnergyP, ttGas, ttEnergyT, ttEnergyZ, ttSupplyP, ttSupplyT, ttSupplyZ);
					//label 38
					ttUpgRep = 0;
					bw::draw_cmdbtn_tooltip_text(string);
				}
				2 => {
					let mut localUnit = bw::active_sel;
					let mut localPlayer = (*localUnit.0).player;
					let mut fourteen: u8 = val + 14 as u8;
					let mut upgGas = bw::get_next_upgrade_gas_cost(localPlayer, fourteen);
					let mut wtf |= (*localUnit.0).player << 0x00_ff;
					let mut upgOre = bw::get_next_upgrade_ore_cost(wtf, fourteen);
					bw::set_tooltip_resources(upgOre, 0, upgGas, 0, 0, 0, 0, 0);
					let mut local = val[7];
					if bw::upgrades_dat_repeat_count[local] <= 1 {
						//label 38
						ttUpgRep = 0;
						bw::draw_cmdbtn_tooltip_text(string);
					}
					ttUpgRep = bw::get_upgrade_level((*bw::active_sel.0).player as u8, local) + 1 as u8;
					bw::draw_cmdbtn_tooltip_text((string as u32) + 2);
				}
				3 => {
					let mut local = val[7];
					let mut race = bw::get_race(bw::active_sel.0);
					let mut energyCost: u16 = 0;
					if race != null_mut() {
						if race == 1 {
							ttGas = bw::tech_dat_energy_cost[local];
							//label 32
							ttEnergyP = 0;
							ttSupplyZ = 0;
							ttSupplyT = 0;
							ttSupplyP = 0;
							ttEnergyZ = energyCost;
							ttEnergyT = ttGas;
							ttGas = 0;
							ttMinerals = 0;
							//label 37
							bw::set_tooltip_resources(ttMinerals, ttEnergyP, ttGas, ttEnergyT, ttEnergyZ, ttSupplyP, ttSupplyT, ttSupplyZ);
							//label 38
							ttUpgRep = 0;
							bw::draw_cmdbtn_tooltip_text(string);
						}
					}
					else {
						energyCost = bw::tech_dat_energy_cost[local];
					}
					ttGas = 0;
					if race == 2 {
						ttSupplyZ = 0;
						ttSupplyT = 0;
						ttSupplyP = 0;
						ttEnergyP = bw::tech_dat_energy_cost[local];
						ttEnergyZ = energyCost;
						ttEnergyT = 0;
						ttMinerals = 0;
						//label 37
						bw::set_tooltip_resources(ttMinerals, ttEnergyP, ttGas, ttEnergyT, ttEnergyZ, ttSupplyP, ttSupplyT, ttSupplyZ);
						//label 38
						ttUpgRep = 0;
						bw::draw_cmdbtn_tooltip_text(string);
					}
					//label 32
					ttEnergyP = 0;
					ttSupplyZ = 0;
					ttSupplyT = 0;
					ttSupplyP = 0;
					ttEnergyZ = energyCost;
					ttEnergyT = ttGas;
					ttGas = 0;
					ttMinerals = 0;
					//label 37
					bw::set_tooltip_resources(ttMinerals, ttEnergyP, ttGas, ttEnergyT, ttEnergyZ, ttSupplyP, ttSupplyT, ttSupplyZ);
					//label 38
					ttUpgRep = 0;
					bw::draw_cmdbtn_tooltip_text(string);
				}
				4 => {
					let mut local = val[7];
					ttGas = bw::tech_dat_gas_cost[local];
					ttMinerals = bw::tech_dat_ore_cost[local];
					//label 35
					ttSupplyP = 0;
					ttSupplyT = 0;
					ttSupplyZ = 0;
					//label 36
					ttEnergyP = 0; 
					ttEnergyT = 0;
					ttEnergyZ = 0;
					//label 37
					bw::set_tooltip_resources(ttMinerals, ttEnergyP, ttGas, ttEnergyT, ttEnergyZ, ttSupplyP, ttSupplyT, ttSupplyZ);
					//label 38
					ttUpgRep = 0;
					bw::draw_cmdbtn_tooltip_text(string);
				}
				5 => {
					let mut local = val[7];
					ttGas = bw::units_dat_gas_cost[local];
					ttMinerals = bw::units_dat_ore_cost[local];
					//label 35
					ttSupplyP = 0;
					ttSupplyT = 0;
					ttSupplyZ = 0;
					//label 36
					ttEnergyP = 0; 
					ttEnergyT = 0;
					ttEnergyZ = 0;
					//label 37
					bw::set_tooltip_resources(ttMinerals, ttEnergyP, ttGas, ttEnergyT, ttEnergyZ, ttSupplyP, ttSupplyT, ttSupplyZ);
					//label 38
					ttUpgRep = 0;
					bw::draw_cmdbtn_tooltip_text(string);
				}
				_ => {
					ttMinerals = 0;
					ttGas = 0;
					//label 35
					ttSupplyP = 0;
					ttSupplyT = 0;
					ttSupplyZ = 0;
					//label 36
					ttEnergyP = 0; 
					ttEnergyT = 0;
					ttEnergyZ = 0;
					//label 37
					bw::set_tooltip_resources(ttMinerals, ttEnergyP, ttGas, ttEnergyT, ttEnergyZ, ttSupplyP, ttSupplyT, ttSupplyZ);
					//label 38
					ttUpgRep = 0;
					bw::draw_cmdbtn_tooltip_text(string);
				}
			}
		}
	}
*/
/*
// 00459150 = DrawCmdBtnTooltipText(), ecx char *string -- https://pastebin.com/raw/tfc0XjEt
pub unsafe fn draw_cmdbtn_tooltip_text_hook(
	text: u32,
	orig: &dyn Fn(u32,)->u8)->u8 {
		let textVar = text;
		let tooltip: u8 = 0;
		if bw::tooltip_upgrade_repeat != 1 {
			if textVar != null_mut() {
				return bw::draw_tooltip_text(textVar as u8);
			}
			return bw::hide_tooltip();
		}
		if textVar != null_mut() {
			return bw::hide_tooltip();
		}
		if bw::stat_txt_tbl_buffer > 794 {
			tooltip = bw::stat_txt_tbl_buffer as u8 + bw::stat_txt_tbl_buffer[795];
		}
		else {
			tooltip = "";
		}
		bw::snprintf(dest, 128, "%s\n%s %d", text, tooltip, bw::tooltip_upgrade_repeat);
		return bw::draw_tooltip_text(dest);
	}
)
*/

/*
// 004348C0 = Ai_TryProgressSpendingQueue_Worker(), arg 1 AiTown *town, arg 2 Unit *worker
pub unsafe fn ai_try_progress_spending_queue_worker_hook(
	town: *mut bw::AiTown,
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut AiTown, *mut Unit,)->u32,)->u32 {
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(town, unit);
			},
		};
		let mut unitCopy = unit.0;
		let mut player = (*unitCopy.0).player;
        let ai = bw::player_ai(player);
		if ai.request_count == 0 || ai.requests + 1 != 0x3 { // 0x19 = New build type, 0x3 = Construct morph
			return 0;
		}
		let mut unitAi = (*unit.0).ai;
		if ai.requests + 4 != (*unitAi.0).town && ai.requests + 1 != 0x6D { // 0x1C = Town main 0x19 = New build type, 0x6D = Supply Depot
			return 0;
		}
		if (*unit.0).unk_A >= 3 {
			return 0;
		}
		let mut buildType = ai.requests + 1;
		if bw::are_same_race(buildType, unit.0) == 0 ||
		bw::check_unit_dat_requirements(buildType, unitCopy.0, player) != 1 ||
		bw::Ai_DoesHaveResourcesForUnit(buildType, player) == 0 {
			return 0;
		}
		let mut townCopy = town;
		let mut townPlayer = townCopy.player;
		if buildType != bw::get_gas_building_id(townPlayer) {
			if buildType == 0x7D { // Bunker
				if bw::get_bunker_position(v9, pos) == 0 { // neither of these variables are defined in the function, according to IDA
					let mut townPosY = townCopy.position + 2;
					let mut townPosX = townCopy.position;
				}
				let mut region = bw::get_region2(townPosX, townPosY);
				bw::do_area_fixups_for_enemy(townPlayer, region, 1);
				if ai.currently_building_bunkers == 0 {
					if bw::choose_placement_position(unit.0, 0x7D, pos, &pos, 64) == 0 { // 0x7D = Bunker
						ai.currently_building_bunkers = 1;
						return bw::ai_finish_unit(unit.0);
					}
					region = bw::get_region2(townPosX, townPosY);
					let ai_region = bw::ai_regions(player);
					let localAiRegion = bw::ai_region_var[region];
					if (*localAiRegion.0).state == 0 {
						bw::change_ai_region_state(4, localAiRegion.0);
						let mut pathing = bw::pathing();
						(*localAiRegion.0).flags |= 0x41;
						let mut buildTypeLoword |= buildType & 0x0000_ffff;
						if pathing.regions[region as u16].unk != 0x1FFD {
							(*localAiRegion.0).needed_ground_strength = 1000;
						}
						// GOTO LABEL_44
						// GOTO LABEL_45
					}
					// LABEL 43
					let mut buildTypeLoword |= buildType & 0x0000_ffff;
					// LABEL 44
					// GOTO LABEL_45
				}
			}
			else if buildType == 0x7C { // Missile Turret
				if bw::get_turret_position(v9, unit.0 as u32) == 0 { // actually checking position of u32; v9 is not defined in function
					let mut townPosYCopy = town.position + 2;
					unitCopy |= town.position & 0x0000_ffff;
					unitCopy |= townPosYCopy & 0x0000_ffff << 16;
				}
				if 
			}
		}

	}
*/
/*
// 0046DD80 = CanUseTech(), arg 1 player, edi tech, eax Unit *unit (-1 = Disabled, 0 = Can't) -- https://pastebin.com/raw/2yDZSDsv
pub unsafe fn can_use_tech_hook(
	unit: *mut bw::Unit,
	arg2: i16,
	arg3: i32,
	orig: &dyn Fn(*mut bw::Unit, i16, i32)->i32,)->i32 {
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit, i16, i32);
			},
		};
		let mut result: i32 = 0;
		let mut v3 = unit;
		let mut error = 0;
		let mut v5: i32 = 0;
		if arg2 < 44 {
			if bw::techdata_dat_req_offsets[arg2 as usize] == 0xFFFF { // end of sublist
				bw::init_tech_use_req_offsets();
			}
			if arg3 == (*v3.0).player {
				v5 = (*v3.0).flags;
				if v5 & 1 <= 1 { // completed
					if bw::is_disabled(v3.0) == 1 {
						error = 10;
						result = 0;
					}
					else if v5 & 0x40000000 <= 1 { // hallucination
						error = 22;
						result = 0;
					}
					else if bw::is_tech_available(arg3, arg2 as u16) == 1 || bw::cheat_flags & 0x1000 { // medieval man
						result = bw::techdata_dat_req_offsets[arg2 as usize] as i32;
						if result != 0 {
							let mut rustbrain = bw::use_tech_tree;
							result = bw::check_dat_req_func(result, v3, arg2, arg3, rustbrain as u32);
						}
						else {
							error = 23;
						}
					}
					else {
						error = 2;
						result = 0;
					}
				}
				else {
					error = 20;
					result = 0;
				}
			}
			else {
				error = 1;
				result = 0;
			}
		}
		else {
			error = 17;
			result = 0;
		}
		return result;
	}
*/
/*
pub unsafe fn BTNSCOND_HasTech_hook(
	arg1: u8,
	arg2: i32,
	unit: *mut bw::Unit,
	orig: &dyn Fn(u8, i32, *mut bw::Unit)->i32,)->i32 {
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(arg1, arg2, unit);
			},
		};
		return bw::can_use_tech(unit.0, arg1 as i16, arg2);
	}
*/
/*
pub unsafe fn send_lowercase_hotkey_event_hook(
	eventId: u16,
	arg2: u32,
	dialog: *mut Dialog,
	orig: &dyn Fn(eventId, arg2, dialog)->i32,)->i32 {

	}
*/
/*
pub unsafe fn right_click_action_hook (
	a1: i32,
	a2: i32,
	a3: u32,
	a4: i32,
	a5: u8,
	orig: &dyn Fn(i32,i32,u32,i32,u8)->*mut bw::Unit)->*mut bw::Unit {
		let game = Game::get();
		let mut v6 = a2;
		let mut v18: i32 = 0;
		v18 |= a1 & 0x0000_ffff;
		v18 |= (a2 & 0x0000_ffff) << 16;
		bw::prepare_formation_movement(v18,1);
		let mut a3m = a3;
		let mut a4m = a4;
		if a3 < 0 {
			a4 = 0xE4;
		}
		else if a4 != 0xE4 {
			let v21: i32 = (a1 / 32) + ((*game.0).map_width_tiles as i32 * (a2 / 32));
			let flags = *(*bw::tile_flags).offset(v21 as isize);
			if ((1 << *bw::command_user) & flags) > 0 {
				a3m = bw::find_fow_unit(a2 as i16, a1 as i16, a4 as u16) as u32;
				a4m = 0xE4;
			}
		}
		*(&bw::map_height_pixels + 2) = 0;
		let mut result = bw::get_next_commanded_unit();
		let mut v10: u8 = 0;
		let mut i = result;
		while result != null_mut() {
			let mut v9 = (*i.0).unit_id;
			if v9 == 0x67 && (*i.0).flags & 0x10 { // Lurker, burrowed
				v10 = 3; // Order Player Guard
			}
			else {
				let mut v11 = bw::units_dat_rclick_action[v9];
				if v11 != null_mut() {
					if (*i.0).flags & 2 { // Landed building
						if bw::has_rally(i.0) == 1 {
							v11 = 2; // Order Guard
						}
					}
				}
				v10 = v11;
			}
			if (*i.0).flags & 0x4000_000 { // Hallucination
				if v10 == 4 || v10 == 5 { // Order Turret Guard || Order Bunker Guard
					v10 = 1; // Order Stop
					}
				}
			let mut a4a: u32 = a3;
			bw::unk_order[0] = 0;
			let mut v12 = bw::get_right_click_order[v10](v6, &a4a as i32, a4); // no idea how to format this in rust
			v18 |= v12 & 0x00_ff; // lobyte v18 = v12, probably wrong syntax
			if v12 != 0x17 { // Order Nothing
				if v12 == 0x27 { // Order Rally Point 1
					if bw::has_rally(i.0) == 1 {
						let mut v13 = a3;
						if a3 == null_mut() {
							v13 = i;
						}
						(*i.0).rally_unit = v13 as i32;
						(*i.0).rally_point |= (*v13.0).sprite.position.x & 0x0000_ffff;
						(*i.0).rally_point |= ((*v13.0).sprite.position.y & 0x0000_ffff) << 16;
					}
				}
				else if i != a3 {
					if v12 == 40 {
						if bw::has_rally(i.0) {
							(*i.0).rally_unit = 0;
							(*i.0).rally_point |= v5 & 0x0000_ffff;
							(*i.0).rally_point |= (v6 & 0x0000_ffff) << 16;
						}
					}
					else {
						if v12 == 8 {
							let mut v14 = (*i.0).subunit;
							if v14 != null_mut() {
								bw::targeted_order_unit(a4a,(*i.0.).subunit,a5,*bw::units_dat_attack_order[(*v14.0).unit_id];
								v6 = a2;
							}
							v18 |= (*bw::units_dat_attack_order[(*i.0.).unit_id as i16] & 0x00_ff);
							v12 = v18;
						}
						if can_issue_order(i.0,v12,*bw::command_user) == 1 {
							if a4a != null_mut() {
								bw::targeted_order_unit(a4a,1.0,a5,v18);
								if bw::unk_order[0] != 23 {
									bw::targeted_order_unit(a4a,i.0,a5,*bw::unk_order[0]);
								}
							}
							else {
								v15 |= v6 & 0x0000_ffff;
								v15 |= (a1 & 0x0000_ffff) << 16;
								if a4 == 228 {
									bw::get_formation_movement_target(i.0,&v15 as i32);
								}
								else {
									v16 = v15;
								}
								v17 = a5 != 0;
								bw::targeted_order_ground(*bw::unk_order[0],a4,i.0,v17,v16,SHIWORD(v16));
								if bw::unk_order[0] != 23 {
									bw::targeted_order_ground(*bw::unk_order[0],a4,i.0,v17,v16,SHIWORD(v16));
								}
							}
							v6 = a2;
							(*i.0).order_flags |= 2u;
						}
						v5 = a1;
						}
					}
				}
			result = bw::get_next_commanded_unit();
		}
	return result;
	}
*/
/*
pub unsafe fn get_default_placement_position_hook (
	unit_id: u32,
	placement_data: u8,
	unit: *mut bw::Unit,
	pos: u32,
	orig: &dyn Fn(u32,u8,*mut Unit,u32)->u32,)->u32 {
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit_id, placement_data, unit.0, pos);
			},
		};
		let mut result : u32 = 0;
		let mut v24 = Vec::with_capacity(4096);
		ptr::copy(placement_data, v24.as_mut_ptr(), sizeof(v24));

		let mut unitPointer = bw::first_player_unit[unit.0];
		while unitPointer != null_mut() {
			if (*unitPointer.0).sprite != null_mut() && (*unitPointer.0).order != null_mut() {
				let mut v23 = 0;
				match (*unitPointer.0).unit_id {
					0x6F | 0x7D | 0x8F =>
					{
						v23 = 1;
						bw::reserve_static_building_on_placemap(unitPointer, placement_data, unit_id as u16, pos, v23);
						break;
					}
					0x71 | 0x7C | 0x9B | 0xA0 | 0xA2 =>
					{
						v23 = 2;
						bw::reserve_static_building_on_placemap(unitPointer, placement_data, unit_id as u16, pos, v23);
						break;
					}
					_ => {
						continue;
					}
				}
			}
			unitPointer = (*unitPointer.0).next_player_unit;
		}

		match unit_id {
			0x6F | 0x71 | 0x7C | 0x7D | 0x9B | 0xA0 | 0xA2 => {
				let mut v6 : u32 = 0;
				let mut desiredPos : Point = { x: 0, y: 0 };
				loop {
					let mut v25 : u32 = 0;
					let mut v7 : u8 = 0;
					let mut a3a = Vec::with_capacity(placement_data & [64*6]);
					let mut v8 : i32 = 1;
					let mut v26 : u32 = 64;
					
					v25 = v6 << 6;
					v7 = &placement_data[64*v6];
					loop { 
						if v7 != null_mut() {
							let mut v9 : i32 = v8 - 2;
							let mut v10 : i32 = v6 -1;
							if (v8 - 2) < 0
							||	v9 >= 64
							||	v10 < 0
							||	v10 >= 64 {
								//label 58//
								v6 = *(&desiredPos as *u32);
								*a3a = 0;
							}

							if let mut v11 : bool = (v24[64 * v10 + v9] == 0) {
								v6 = *(&desiredPos as *u32);
								*a3a = 0;
							};
							
							let mut v12 : i32 = v8 - 1;
							if v8 - 1 < 0
								|| v12 >= 64
								|| v10 < 0
								|| v10 >= 64 {
								//label 58
								v6 = *(&desiredPos as *u32);
								*a3a = 0;
							}

							LOBYTE(v11) = v24[64 * v10 + v12] == (_BYTE)v11;
							v13 = v11;

							if v11
							|| v8 < 0
							|| v8 >= 64
							|| v10 < 0
							|| v10 >= 64 {
								//label 58//
								v6 = desiredPos as u32;
								*a3a = 0;
							}

							if ( v13
								|| (v6 = desiredPos as u32, desiredPos as i32 < 0)
								|| desiredPos as i32 >= 64
								|| (LOBYTE(v13) = v24[v25 + v9] == 0, v13)
								|| !v24[v25 + v8]
								|| (v14 = *(_DWORD *)&desiredPos + 1, *(_DWORD *)&desiredPos + 1 < 0)
								|| v14 >= 64
								|| (v15 = v24[64 * v14 + v9] == 0) != 0
								|| v14 < 0
								|| v14 >= 64
								|| (LOBYTE(v15) = v24[64 * v14 + v12] == 0, (v16 = v15) != 0)
								|| v14 < 0
								|| v14 >= 64
								|| (LOBYTE(v16) = v24[64 * v14 + v8] == 0, v16) )
							{
								//label 58//
								v6 = desiredPos as u32;
								*a3a = 0;
							}

						}
						++v8;
						v7 = a3a + 1;
						v17 = v26 == 1;
						++a3a;
						--v26;

						if !v17 { break; }
					}
					desiredPos = (dwPosition)++v6;
					if v6 < 0x40 { break; }
				}	
				v18 = 0;
				v19 = a2;
				LABEL_45:
			// LABEL_45:
				result = 0;
				while ( !v19[result] )
				{
					if ( ++result >= 0x40 )
					{
					++v18;
					v19 += 64;
					if ( v18 < 0x40 )
						goto LABEL_45;
					qmemcpy((void *)a2, v24, 0x1000u);
					return result;
					}
				}
				break;
			}
			_ => {
				v21 = a2;
				v22 = 0;
			//LABEL_51:
				result = 0;
				while ( !v21[result] )
				{
					if ( ++result >= 0x40 )
					{
					++v22;
					v21 += 64;
					if ( v22 < 0x40 )
						goto LABEL_51;
					std::memcpy((void *)a2, v24, 0x1000u);
					return result;
					}
				}
				break;
			}
		}

		return result;
	}
*/
/*
pub unsafe fn order_entries_hook (
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit);
			},
		};
		let mut v1 = unit;
		let mut uOrder = (*v1.0).order;
		match uOrder {
			0x30 => {
				bw::order_protoss_build_self(v1.0);
				return;
			}
			0xAE => {
				bw::order_warp_in(v1.0);
				return;
			}
			0 => {
				bw::order_die(v1.0);
				return;
			}
			0x81 => {
				let _ = bw::order_nuke_track(v1.0);
				return;
			}
			_ => {
				if v1.is_disabled() {
					let _ = bw::focus_unit(v1.0);
					return;
				}
				let mut uFlags = (*v1.0).flags;
				if uFlags & 0x2000 == 0 && uFlags & 0x8000 != 0 {
					let _ = bw::focus_unit(v1.0);
				}
				match uOrder {
					0x4 => {
						bw::order_subunit_guard(v1.0);
					}
					0x16 => {
						bw::order_subunit_attack(v1.0);
					}
					0x1A => {
						bw::order_drone_build(v1.0);
					}
					0x1E => {
						bw::order_scv_build(v1.0);
					}
					0x1F => {
						bw::order_probe_build(v1.0);
					}
					0x21 => {
						bw::order_construct_building(v1.0);
					}
					0x22 => {
						bw::order_repair(v1.0);
					}
					0x29 => {
						bw::order_zerg_birth(v1.0);
					}
					0x2A => {
						bw::order_unit_morph(v1.0);
					}
					0x2C => {
						bw::order_terran_build_self(v1.0);
					}
					0x2D => {
						bw::order_zerg_build_self(v1.0);
					}
					0x41 => {
						bw::order_scarab(v1.0);
					}
					0x42 => {
						bw::order_recharge_shields(v1.0);
					}
					0x47 => {
						bw::order_land(v1.0);
					}
					0x48 => {
						bw::order_liftoff(v1.0,0);
					}
					0x4B => {
						bw::order_tech_func(v1.0);
					}
					0x4C => {
						bw::order_upgrade_func(v1.0);
					}
					0x58 => {
						bw::order_ore_interrupt(v1.0);
					}
					0x59 => {
						bw::order_harvest_wtf(v1.0);
					}
					0x5B => {
						bw::order_harvest_interrupt(v1.0);
					}
					0x62 => {
						bw::order_siege_mode(v1.0);
					}
					0x63 => {
						bw::order_tank_mode(v1.0);
					}
					0x69 => {
						bw::order_warping_archon(v1.0);
					}
					0x6A => {
						bw::order_complete_archon_summon(v1.0);
					}
					0x7C => {
						bw::order_nuke_train(v1.0);
					}
					0x82 => {
						bw::order_cloak_nearby_units(v1.0);
					}
					0x96 => {
						bw::order_reset_collision1(v1.0);
					}
					0x97 => {
						bw::order_reset_harvester_collision(v1.0);
					}
					0x9B => {
						bw::order_ctf_cop2(v1.0);
					}
					0xA5 => {
						bw::kill_unit(v1.0);
					}
					0xA6 => {
						bw::order_critter_idle(v1.0);
					}
					0xB0 => {
						bw::order_heal(v1.0);
					}
					0xB1 => {
						bw::order_heal_move(v1.0);
					}
					0xB2 => {
						bw::order_medic_holdpos(v1.0);
					}
					0xB3 => {
						bw::order_heal2(v1.0);
					}
					0xB7 => {
						bw::order_warping_dark_archon(v1.0);
					}
					_ => {},
				}
				if (*v1.0).order_wait > 0 {
					return;
				}
			}
		}
		(*v1.0).order_wait = 0;
		bw::focus_unit2(v1.0);
		(*v1.0).secondary_order_wait = 0;
		match uOrder {
			0x6C | 0x88 => {
				if (*v1.0).order_state != 0 {
					let u = (*v1.0).order_queue_begin;
					if u == null_mut() {
						return;
					}
					bw::do_next_queued_order(v1.0);
					return;
				}
				if (*v1.0).order_state == 0 {
					//bw::halt(v1.0);
				}
				(*v1.0).order_state = 1;
				bw::do_next_queued_order(v1.0);
				return;
			}
			0x17 => {
				bw::do_next_queued_order(v1.0);
				return;
			}
			0x9f => {
				bw::order_ai_patrol(v1.0);
				return;
			}
			0xB => {
				let u = (*v1.0).target;
				if u == null_mut() {
					bw::order_attack_fixed_range(v1.0,bw::get_auto_target(v1.0));
					return;
				}
				else if u != null_mut() {
					bw::order_attack_fixed_range(v1.0,(*v1.0).target);
					return;
				}
			}
			0xE => {
				bw::order_attack_move(v1.0);
				return;
			}
			0x9D => {
				bw::order_ai_attack_move(v1.0);
				return;
			}
			0xA => {
				let u = (*v1.0).target;
				if u == null_mut() {
					bw::order_attack_unit(v1.0,bw::get_auto_target(v1.0));
					return;
				}
				else if u != null_mut() {
					bw::order_attack_unit(v1.0,(*v1.0).target);
					return;
				}
			}
			0x43 => {
				bw::order_recharge_shields2(v1.0);
				return;
			}
			0xF => {
				let u = (*v1.0).target;
				if u == null_mut() {
					bw::order_infest_mine1(v1.0,bw::get_auto_target(v1.0));
					return;
				}
				else if u != null_mut() {
					bw::order_infest_mine1(v1.0,(*v1.0).target);
					return;
				}
			}
			0x74 => {
				bw::order_burrow(v1.0);
				return;
			}
			0x75 => {
				bw::order_zerg_unkown(v1.0);
				return;
			}
			0x32 | 0x38 | 0x39 => {
				bw::order_carrier(v1.0);
				return;
			}
			0x37 => {
				bw::order_carrier_ignore(v1.0);
				return;
			}
			0x35 => {
				bw::order_carrier_attack1(v1.0);
				return;
			}
			0x34 => {
				bw::order_carrier_idle(v1.0);
				return;
			}
			0x94 => {
				bw::order_hallucination(v1.0);
				return;
			}
			0x7 => {
				bw::order_reaver_stop(v1.0);
				return;
			}
			0x9C => {
				bw::order_computer_script(v1.0);
				return;
			}
			0xA3 => {
				bw::order_computer_return(v1.0);
				return;
			}
			0x99 => {
				bw::order_cop_create_flag(v1.0);
				return;
			}
			0x9A => {
				bw::order_ctf_cop1(v1.0);
				return;
			}
			0x76 => {
				bw::order_unburrow(v1.0);
				return;
			}
			0x8d => {
				bw::order_defensive_matrix(v1.0);
				return;
			}
			0xA8 => {
				bw::order_open_door(v1.0);
				return;
			}
			0xA9 => {
				bw::order_close_door(v1.0);
				return;
			}
			0xAD => {
				bw::order_disable_doodad(v1.0);
				return;
			}
			0xAC => {
				bw::order_unk_iscript(v1.0);
				return;
			}
			0x46 => {
				bw::order_drone_land(v1.0);
				return;
			}
			0x19 => {
				bw::order_drone_start_build(v1.0);
				return;
			}
			0x2F => {
				bw::order_enter_nydus(v1.0);
				return;
			}
			0x5C => {
				bw::order_enter_transport(v1.0);
				return;
			}
			0x5E => {
				bw::order_transport_idle_open(v1.0);
				return;
			}
			0x11 => {
				bw::order_unused_powerup(v1.0);
				return;
			}
			0x5F => {
				bw::order_pickup3(v1.0);
				return;
			}
			0x9E => {
				bw::order_harass_move(v1.0);
				return;
			}
			0xA7 => {
				bw::order_trap_attack(v1.0);
				return;
			}
			0xAA => {
				let u = (*v1.0).target;
				if u == null_mut() {
					bw::order_trap_hide(v1.0,bw::get_auto_target(v1.0));
					return;
				}
				else if u != null_mut() {
					bw::order_trap_hide(v1.0,(*v1.0).target);
					return;
				}
			}
			0xAB => {
				bw::order_returntoidle_unk(v1.0);
				return;
			}
			0x31 => {
				bw::order_follow(v1.0);
				return;
			}
			0x2 => {
				bw::order_guard(v1.0);
				return;
			}
			0x3 => {
				bw::order_player_guard(v1.0);
				return;
			}
			0xA0 => {
				bw::order_computer_guard(v1.0);
				return;
			}
			0x45 | 0x51 => {
				bw::order_move_to_harvest(v1.0);
				return;
			}
			0x52 => {
				bw::order_enter_gas(v1.0);
				return;
			}
			0x53 => {
				bw::order_harvest_gas(v1.0);
				return;
			}
			0x55 => {
				bw::order_move_to_minerals(v1.0);
				return;
			}
			0x56 => {
				bw::order_wait_minerals(v1.0);
				return;
			}
			0x57 => {
				bw::order_harvest_minerals(v1.0);
				return;
			}
			0x6B => {
				bw::order_hold_position(v1.0);
				return;
			}
			0x6 | 0x33 => {
				bw::order_move(v1.0);
				return;
			}
			0x65 => {
				bw::order_init_creep_growth(v1.0);
				return;
			}
			0xA4 => {
				bw::order_init_pylon(v1.0);
				return;
			}
			0x4D => {
				bw::order_larva(v1.0);
				return;
			}
			0x4A => {
				bw::order_lifting_off(v1.0);
				return;
			}
			0x70 => {
				bw::order_move_unload(v1.0);
				return;
			}
			0x60 => {
				bw::order_pickup4(v1.0);
				return;
			}
			0xA2 => {
				bw::order_neutral(v1.0);
				return;
			}
			0x80 => {
				bw::order_nuke_ground(v1.0);
				return;
			}
			0x7D => {
				bw::order_nuke_launch(v1.0);
				return;
			}
			0x7E => {
				bw::order_nuke_attack(v1.0);
				return;
			}
			0x7F => {
				bw::order_nuke_ground2(v1.0);
				return;
			}
			0x2E => {
				bw::order_nydus_exit(v1.0);
				return;
			}
			0x98 => {
				bw::order_patrol(v1.0);
				return;
			}
			0x5D => {
				bw::order_transport_idle(v1.0);
				return;
			}
			0x24 => {
				bw::order_place_addon(v1.0,(*v1.0).player); // not sure if second arg is really used? hexrays does a few operations with it
				return;
			}
			0x84 => {
				bw::order_place_mine(v1.0);
				return;
			}
			0x61 => {
				bw::order_powerup_idle(v1.0);
				return;
			}
			0x20 => {
				bw::order_protoss_prebuild(v1.0);
				return;
			}
			0x1B => {
				bw::order_infest(v1.0);
				return;
			}
			0x1D => {
				let u = (*v1.0).target;
				if u == null_mut() {
					bw::order_infest_mine4(v1.0,bw::get_auto_target(v1.0));
					return;
				}
				else if u != null_mut() {
					bw::order_infest_mine4(v1.0,(*v1.0).target);
					return;
				}
			}
			0x85 => {
				bw::order_right_click_action(v1.0);
				return;
			}
			0xA1 => {
				bw::order_rescue_passive(v1.0);
				return;
			}
			0x45 => {
				bw::order_interceptor_return(v1.0);
				return;
			}
			0x54 | 0x5A => {
				bw::order_return_resources(v1.0);
				return;
			}
			0x87 => {
				bw::order_sap_location(v1.0);
				return;
			}
			0x86 => {
				bw::order_sap_unit(v1.0);
				return;
			}
			0x8B => {
				bw::order_place_scanner(v1.0);
				return;
			}
			0x8C => {
				bw::order_zerg_birth_kill(v1.0);
				return;
			}
			0x15 => {
				bw::order_stay_in_range(v1.0);
				return;
			}
			0x1 => {
				bw::order_returntoidle(v1.0);
				return;
			}
			0x67 => {
				bw::order_stop_creep_growth(v1.0);
				return;
			}
			0x40 => {
				bw::order_interceptor(v1.0);
				return;
			}
			0x89 => {
				bw::order_recall(v1.0);
				return;
			}
			0x12 => {
				bw::order_building_attack(v1.0);
				return;
			}
			0x13 => {
				bw::order_tower_attack(v1.0);
				return;
			}
			0x3A | 0x3D | 0x3E => {
				bw::order_reaver(v1.0);
				return;
			}
			0x3B => {
				bw::order_reaver_attack_unk(v1.0);
				return;
			}
			0x6F => {
				bw::order_unload(v1.0);
				return;
			}
			0x14 => {
				bw::order_spider_mine(v1.0);
				//iquare - must be replaced to jump-like interface, check other functions as well
				return;
			}
			0x64 => {
				bw::order_watch_target(v1.0);
				return;
			}
			0x2B => {
				bw::order_building_morph(v1.0);
				return;
			}
			0x71 | 0x73 | 0x77 | 0x78 | 0x79 | 0x7A | 0x8E | 0x8F | 0x90 | 0x91 | 0x92 | 0x93 | 0xB4 | 0xB5 | 0xB9 | 0xBA => {
				bw::order_spell(v1.0);
				return;
			}
			0x9 | 0x1C | 0x23 | 0x36 | 0x50 | 0x72 => {
				bw::order_obscured(v1.0);
				return;
			}
			0xAF => {
				bw::order_medic_idle(v1.0);
				return;
			}
			0xB6 => {
				bw::order_mind_control(v1.0);
				return;
			}
			0xB8 => {
				bw::order_feedback(v1.0);
				return;
			}
			0xBB => {
				bw::order_junk_yard_dog(v1.0);
				return;
			}
			_ => {
				return;
			}
		}
	}
*/
pub unsafe fn unburrow_hook(
    unit: *mut bw::Unit,
    orig: &dyn Fn(*mut bw::Unit)){
    let unit = match Unit::from_ptr(unit) {
        Some(s) => s,
        None => {
            return orig(unit);
        },
    };
    let mut v2 = unit;
    let v1 = (*v2.0).order;
    let mut v3 = (*v2.0).order_queue_end;
    let mut v4 = 0;
    if v1 == 0x74 || v1 == 0x75 || (*v2.0).unit_id == 0x67 {
        if (*v2.0).unit_id == 0x67 {
            (*v2.0).flags |= 0x1000; // No orders allowed
        }
        (*v2.0).order_flags |= 1;
            loop {
				v3 = (*v2.0).order_queue_end;
				if v3 == null_mut() {
					break;
				}
                v4 = (*v3).order_id;
                if bw::orders_dat_can_interrupt[v4 as usize] == 0 && v4 != 0x76 {
                    break;
                }
                let _ = bw::remove_order_from_queue(v3, v2.0);
                //
            }
        let _ = bw::perform_another_order(0x76,null_mut(),v2.0,228,0,null_mut());
        bw::do_next_queued_order(v2.0);
        bw::force_order_done(v2.0);
    }
}

	
pub unsafe fn remove_from_ai_structs(
	unit: *mut bw::Unit,
	was_killed: u32,
	orig: &dyn Fn(*mut bw::Unit,u32)->u32)->u32{
		if unit != null_mut(){
			if (*unit).sprite == null_mut(){
				debug!("BUG: Failed to remove, null sprite detected (safety check)");
				return 1;
			}
		}
		orig(unit,was_killed)
	}
	
	
pub unsafe fn increase_upgrade_level(
	unused: u32,
	upgrade_id: u32,
	upgrade_level: u32,
	player_id: u32,
	orig: &dyn Fn(u32,u32,u32,u32)->u32)->u32{
		let game = Game::get();
		let mut globals = Globals::get("increase_upgrade_level");
		if player_id > 11 {
			debug!("BUG: Failed to remove, player {} detected (safety check)",player_id);
			return 1;
		}
		
		if upgrade_id > 255 {
			debug!("BUG: Failed to remove, upgrade {} detected (safety check)",upgrade_id);
			return 1;
		}
		let v4 = aiscript::get_new_upgrade_level(game,&mut globals,upgrade_id as u16,player_id as u8);
		if upgrade_level as u8 > v4 {
			aiscript::set_new_upgrade_level_current(game,&mut globals,upgrade_id as u16,player_id as u8,upgrade_level as u8);
		}
		return 1;
	}
	
	/*
	

pub enum WeaponsDatEntry {
	Dmg_Upgrade,
	Flingy,
}

pub struct WeaponDataSetting {
	pub entry: WeaponsDatEntry,
	pub value: u32,
	pub index: usize,
}
pub struct FlingyDataSetting {
	pub entry: FlingyValues,
	pub value: u32,
	pub index: usize,
}
	*/
	
pub unsafe fn get_upgrade_level_hook(
	player: u32,
	upgrade_id: u32,
	orig: &Fn(u32,u32)->u32)->u32{
		//orig(player,upgrade_id)
		let game = Game::get();
		let mut globals = Globals::get("get_upgrade_level_hook");
		let result = aiscript::get_new_upgrade_level_upg(game,&mut globals.upgrades,upgrade_id as u16,player as u8);
		result as u32
	}
	
use windows;	
use std::ffi::CStr;	

pub unsafe fn building_vision_sync_hook(orig: &Fn()->u32)->u32{
	let result = orig();
	result
}
pub unsafe fn load_game_data(
	a1: u32,
	a2: *const u8,
	filename: *const libc::c_char,
	orig: &Fn(u32,*const u8,*const libc::c_char)->u32)->u32{
		let result = orig(a1,a2,filename);//eax,edi,u32
		let string = CStr::from_ptr(filename).to_str().unwrap();
		let mut active_patcher = ::PATCHER.lock().unwrap();
		let mut exe = active_patcher.patch_exe(0x00400000);	
		let config = config::config();
		if string == "arr\\units.dat" {
			if config.extended_data {
				data_extender::init_unit_ext(&mut exe);
			}
			debug!("Do data rewrite");
			for e in config.units_dat_rewrite.iter(){
//				let msg = format!("{:?} {:?} {}",e.entry,e.index,e.value);
//				windows::message_box("Aiscript extension plugin", &msg);
				if config.extended_data && e.entry!=UnitsDatEntry::ArmorUpgrade && 
				e.entry!=UnitsDatEntry::Construction &&
				e.entry!=UnitsDatEntry::Portrait {
					if e.entry != UnitsDatEntry::Flingy {
						data_extender::set_unit_data_e(e.entry.clone(),e.index,e.value);
					}
					else {
						bw::gptp_set_flingy_id(e.index as u32,e.value as u32);
					}
					
					
				}
				else {
					match e.entry {
						UnitsDatEntry::Flingy=>bw::units_dat_flingy_old[e.index]=e.value as u8,
						UnitsDatEntry::ArmorUpgrade=>bw::units_dat_armor_upg[e.index]=e.value as u8,		
						UnitsDatEntry::SndReady=>bw::units_dat_snd_ready[e.index]=e.value as u16,
						UnitsDatEntry::SndYesFirst=>bw::units_dat_snd_yes_begin[e.index]=e.value as u16,
						UnitsDatEntry::SndYesLast=>bw::units_dat_snd_yes_end[e.index]=e.value as u16,
						UnitsDatEntry::SndWhatFirst=>bw::units_dat_snd_what_begin[e.index]=e.value as u16,
						UnitsDatEntry::SndWhatLast=>bw::units_dat_snd_what_end[e.index]=e.value as u16,
						UnitsDatEntry::SndPissedFirst=>bw::units_dat_snd_annoyed_begin[e.index]=e.value as u16,
						UnitsDatEntry::SndPissedLast=>bw::units_dat_snd_annoyed_end[e.index]=e.value as u16,
						UnitsDatEntry::Construction=>bw::units_dat_construction_image[e.index]=e.value as u32,
						UnitsDatEntry::Portrait=>bw::units_dat_portrait[e.index]=e.value as u16,
						_=>(),
					}
				}
			}
		}
		/*
		if string == "arr\\weapons.dat" {
			for e in config.weapons_dat_rewrite.iter(){
				match e.entry {
					WeaponsDatEntry::Dmg_Upgrade=>bw::weapons_dat_dmg_upg[e.index]=e.value as u8,
					WeaponsDatEntry::Flingy=>bw::weapons_dat_flingy[e.index]=e.value,
				}
			}
		}
		*/
		if string == "arr\\flingy.dat" {
			if config.extended_data {
				data_extender::init_flingy_ext(&mut exe);
			}
			for e in config.flingy_dat_rewrite.iter(){
				if config.extended_data {
					data_extender::set_flingy_data_e(e.entry,e.index,e.value);
				}
				else {
					match e.entry {
						FlingyValues::TopSpeed=>bw::flingy_dat_top_speed[e.index]=e.value,
						FlingyValues::Acceleration=>bw::flingy_dat_acceleration[e.index]=e.value as u16,
						FlingyValues::TurnRadius=>bw::flingy_dat_turn_radius[e.index]=e.value as u8,							
						FlingyValues::MovementType=>bw::flingy_dat_movement_type[e.index]=e.value as u8,
						FlingyValues::SpriteId=>bw::flingy_dat_sprite_id[e.index]=e.value as u16,
						FlingyValues::HaltDistance=>bw::flingy_dat_halt_distance[e.index]=e.value,							
						FlingyValues::IscriptMask=>bw::flingy_dat_iscript_mask[e.index]=e.value as u8,
						_=>(),
					}
				}
			}
		}
		if string == "arr\\sprites.dat" {
			debug!("Sprites!");
			
			//
			// sprites.dat rewrite
			//
			/*if config.extended_data {
				data_extender::init_sprite_ext(&mut exe);
			}
			
			for e in config.sprite_dat_rewrite.iter(){
				if config.extended_data {
					data_extender::set_sprite_data_e(e.entry,e.index,e.value);
				}
				else {
					match e.entry {
						SpriteValues::IncludeInVision => bw::sprites_dat_vision[e.index]=e.value as u8,
						SpriteValues::StartAsVisible => bw::sprites_dat_as_visible[e.index]=e.value as u8,
						SpriteValues::ImageId => bw::sprites_dat_image_id[e.index]=e.value as u16,
						SpriteValues::SelectionCircle => bw::sprites_dat_selection_circle[e.index]=e.value as u8,
						SpriteValues::ImagePos => bw::sprites_dat_selection_imagepos[e.index]=e.value as u8,
						SpriteValues::HpBar => bw::sprites_dat_healthbar[e.index]=e.value as u8,
						_=>(),
					}
				}
			}*/
		}
		if string == "arr\\images.dat" {
			//debug!("Images!");
			if config.extended_data {
				data_extender::set_image_entries(&mut exe);
			}
			for e in config.image_dat_rewrite.iter(){
				if config.extended_data {
					data_extender::set_image_data_e(e.entry,e.index,e.value);
				}
				else {
					match e.entry {
						ImageValues::GrpId => bw::images_dat_grp_id[e.index]=e.value as u32,
						ImageValues::GraphicTurns => bw::images_dat_graphic_turns[e.index]=e.value as u8,
						ImageValues::Clickable => bw::images_dat_clickable[e.index]=e.value as u8,
						ImageValues::UseFullIscript => bw::images_dat_use_full_iscript[e.index]=e.value as u8,
						ImageValues::DrawIfCloaked => bw::images_dat_draw_if_cloaked[e.index]=e.value as u8,
						ImageValues::DrawFunc => bw::images_dat_draw_func[e.index]=e.value as u8,
						ImageValues::DrawRemap => bw::images_dat_draw_remap[e.index]=e.value as u8,
						ImageValues::IscriptId => bw::images_dat_iscript_id[e.index]=e.value as u32,
						ImageValues::OverlayShield => bw::images_dat_overlay_shield[e.index]=e.value as u32,
						ImageValues::OverlayAttack => bw::images_dat_overlay_attack[e.index]=e.value as u32,
						ImageValues::OverlayDamage => bw::images_dat_overlay_damage[e.index]=e.value as u32,
						ImageValues::OverlaySpecial => bw::images_dat_overlay_special[e.index]=e.value as u32,
						ImageValues::OverlayLanding => bw::images_dat_overlay_landing[e.index]=e.value as u32,
						ImageValues::OverlayLiftoff => bw::images_dat_overlay_liftoff[e.index]=e.value as u32,
						_=>(),
					}
				}
			}
		}
		result
	}
	//
//	0x004D2E80 => load_game_data(@eax u32,@edi *const u8, u32)->u32; 


/*
pub unsafe fn init_game(	
	orig: &dyn Fn()->u32,)->u32{
		let result = orig();
		
		let mut active_patcher = ::PATCHER.lock().unwrap();
		let mut exe = active_patcher.patch_exe(0x00400000);	
		data_extender::init_flingy_ext(&mut exe);
		//panic!("test");
		
		result
	}
*/

pub unsafe fn single_player_init_game_hook(
	orig: &dyn Fn()->u32,)->u32{
		let result = orig();
		for i in 0..8 {
			if bw::player_list[i as usize].ty == 6 || bw::player_list[i as usize].ty == 0 {
				bw::player_list[i as usize].ty = 1;
				debug!("Set player {} as computer",i);
			}
		}
		
		result
		/*
		let mut v1 = 0;
		if *bw::is_multiplayer != 0 {
			return 0;
		}
		if *bw::custom_singleplayer != 0 {
			if (*bw::game_template).victory_conditions == 0x1 	||
				(*bw::game_template).starting_units == 0x1 		||
				(*bw::game_template).unk_tournament {
					bw::clear_melee_computer_slots();
					bw::player_list[*bw::local_nation_id as usize].race = *bw::selected_singleplayer_race;
					v1 = 0;
					if bw::selected_singleplayer_races[v1 as usize] != 7 { // 7 = inactive
						while v1 < 8 {
							bw::set_slot_to_computer(bw::selected_singleplayer_races[v1 as usize] as u32);
							v1 += 1;
						}
					}
			}
		}
		//let first_human = bw::GetFirstFreeHumanPlayerId();
		return 1;*/
			
	}
//
//	0x0046F0F0 => multi_select_units_hook(*mut *mut Unit, *mut *mut Unit, *mut Unit)->u32;
	
pub unsafe fn multi_select_units_hook(
	input: *mut *mut bw::Unit,
	buffer: *mut *mut bw::Unit,
	clicked: *mut bw::Unit,
	orig: &dyn Fn(*mut *mut bw::Unit,*mut *mut bw::Unit,*mut bw::Unit)->u32)->u32{
		orig(input,buffer,clicked)
		//*buffer.offset(1 as isize) = null_mut();
		//return 0;
		/*
		let mut v3 = clicked;
		let v4 = 0;
		let v17 = 0;
		let v16 = 0;
		let mut v12 = null_mut();
		if clicked != null_mut(){
			*buffer.offset(0 as isize) = clicked;
			v17 = 1;
		}
		let mut input_offset = 0;
		let mut v5 = *input;
		if v5 != null_mut() {
			'outer: loop {
				'label30wait: loop {
					if v5 != v3 {
						let unit = Unit::from_ptr(v5).unwrap();
						if (*unit.0).subunit != null_mut(){
							if unit.id().is_subunit(){
								unit.0 = (*unit.0).subunit;
							}
							v6 = v17;
							while v6 != 0 {
								v6 -= 1;
								v7 = *buffer.offset(v6);
								if v7 == unit.0 {
									break 'label30wait;
								}
							}
						}
						//
						if bw::player_visions & (*unit.sprite().unwrap()).visibility_mask != 0
						{
						  let v8 = (*unit.0).flags;
						  if !unit.is_invisible() || bw::player_visions & detection_status != 0
						  {
							let v9 = unit.id();
							if bw::is_unselectable_func(v9.0)==0
							{
							  let v10 = bw::is_multi_selectable(v5);
							  let mut limit = 12;//new
							  let v11 = unit.player();
							  if v10==0 || *bw::is_replay==1 || v11!=*bw::local_nation_id
							  {
								v16 = v5;
							  }
							  else
							  {
								let mut v11 = clicked;
								if clicked != null_mut()
								{
								  if (*clicked).player != v11 {
									break 'label30wait;
								  }
								  if (*clicked).unit_id != v9 {
									break 'label30wait;
								  }
								  let v13 = v8 ^ (*clicked).flags;
								  if v13 & 0x10 != 0 {
									break 'label30wait;
								  }
								  let v14 = unit.burrowed();
								  if !v14
								  {
									if v13 & 0x100 != 0 {
										//jump to block 30;
									}
								  }
								  if v13 & 0x40000000 != 0 {
									break 'label30wait;
								  } //hallucination
								  v12 = clicked;
								}
								if v17 < limit {
									*buffer.offset[v17 as isize] = v5;
									v17+=1;
								}
								else {
									 bw::replace_lower_draw_order_unit(a2, v5, v11,v16);
								}
							}
						}
					}
				  }
				  break;
			    }
			   //block30
			  input_offset+=1;
			  v5 = *input.offset(input_offset);
			  if v5 == null_mut()
			  {
				v4 = v16;
				break 'outer;
			  }
			}
		  }
		  let result = v17;
		  if v17 != 0
		  {
			if v4 != 0
			{
			  if v3 != 0 {
				*buffer = v3;
			  }
			  else {
				*buffer = v4;
			  }
			  result = 1;
			}
			else if ( v3 )
			{
			  *buffer = v3;
			  result = 1;
			}
		  }
		  return result;
	*/
	}
	
/*
pub unsafe fn box_selection_hook(
	area: *mut bw::Rect,
	orig: &dyn Fn(*mut bw::Rect),){//a1 is area
	
	//
	//orig(area);
	
	let alt = bw::keystate[16];
	let mut buffer = [null_mut();12];//0x30
	let v3 = bw::find_units_rect(area);
	let v1 = bw::multi_select_units_func(v3,&mut buffer[0] as *mut *mut bw::Unit, null_mut());
	let v4 = bw::position_search_results[bw::position_search_results[0] as usize];
	bw::position_search_results[0] -= 1;
	let unitsInBoundsCurrentIndex = v4;
	if v1 != 0 {
		if alt!=0 && bw::client_select3[0]!=null_mut() {
			let mut buffer2 = bw::client_select3.clone();
			let v5 = bw::add_to_selection_buffer(&mut buffer2[0] as *mut *mut bw::Unit,&mut buffer[0] as *mut *mut bw::Unit,v1,null_mut());
			bw::select_units(&mut buffer2[0] as *mut *mut bw::Unit,v5,1,1);
			return;
		}
		let v6 = buffer;
		let v7 = bw::unit_to_index(buffer[0]);
		let result_select = bw::try_select_recent_hotkey_group(v7); 
		if v1!=1 || bw::keystate[18]==0 || result_select!=0 {
		
		} 
		
	
	}


		if ( v1 != 1 || !VK_Array[18] || (v6 = thisU, v7 = CUnitToUnitID(thisU), !selectSingleUnitFromID(v7)) )
		{
		  selectMultipleUnitsFromUnitList(v1, &thisU, 1, 1);
		  if ( v1 != 1 )
			return;
		  v6 = thisU;
		}
		if ( v6 )
		{
		  if ( (unsigned __int8)v6->owner == g_ActiveNationID )
		  {
			LOBYTE(v8) = unitIsFactoryUnit(v6);
			if ( v8 )
			  sub_468670(v6);
		  }
		}
	  }	
	//
	
	}
	*/
	
pub unsafe fn azazel_energy_restore(
	caster: *mut bw::Unit,
	player: u8,
	unit_list: Vec<Unit>){
	for u in unit_list {
		let max_energy = bw::get_max_energy(u.0);
		(*u.0).energy = cmp::min(max_energy as u16,(*u.0).energy.saturating_add(6400));
		if u.0==bw::player_selections[*bw::active_player_id as usize][0]{
			bw::update_ui();
		}
	}	
}
pub unsafe fn order_defensive_matrix_hook(
	caster: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)){	
		//orig(caster);
		let sight_range = bw::get_sight_range(caster,1);
		let mut v2: u16 = 0;
		if bw::spell_order(6,32*sight_range,&mut v2 as *mut u16,880,caster)!=0{//880 is Invalid target string
			if *bw::cheat_flags & 0x800 == 0 {
				(*caster).energy -= v2;
				
			}
			let _ = bw::create_defensive_matrix((*caster).target);
			bw::playsound_unit(1,0,349,caster);
			let mut globals = Globals::get("order_defensive_matrix_hook");
			let game = Game::get();
			let mut damageHp=false;
			//globals.ngs.clear_blind_timer(Unit::from_ptr((*caster).target).unwrap());
			globals.ngs.set_matrix_caster(Unit::from_ptr(caster).unwrap(),Unit::from_ptr((*caster).target).unwrap());
			let restore_energy_for = globals.ngs.get_energy_targets(Unit::from_ptr(caster).unwrap());
			if globals.ngs.get_generic_timer(caster,63)>0 {//energy decay - enthropy gauntlets
				damageHp = true;
			}
			drop(globals);
			azazel_energy_restore(caster,*bw::active_player_id as u8,restore_energy_for);
			if damageHp {
				bw::damage_unit_func(caster,(*caster).player as u32,0,caster,v2 as u32);
			}
			if (*caster).order_queue_begin != null_mut(){
				(*caster).order_flags |= 0x1;
				bw::do_next_queued_order(caster);
			}
			else if (*caster).ai!=null_mut(){
				bw::issue_order_targ_nothing(caster,0x9c);
			}
			else {
				bw::issue_order_targ_nothing(caster, UnitId((*caster).unit_id).return_to_idle());
			}
		}
	}
	
	
pub unsafe fn end_invisibility_hook(
	unit: *mut bw::Unit,
	sound: u32,
	orig: &dyn Fn(*mut bw::Unit,u32)){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit,sound);
			},
		};
	let mut v2 = unit;
	if unit.is_burrowed() {
			bw::play_sound_from_source(0 as u32,1 as u32,sound,v2.0);
			
	}
	if *bw::is_replay == 0 &&
	   unit.player() == *bw::local_nation_id as u8{
			*bw::force_button_refresh = 1;
			*bw::portrait_redraw = 1;
			*bw::status_screen_redraw = 1;
			*bw::control_under_mouse = 0;
			*bw::control_under_mouse_val = 0;
	}
	if unit.is_burrowed() {
		if let Some(sprite) = unit.sprite(){
			bw::remove_cloak_draw_funcs(sprite);
			bw::show_images_hidden_by_cloak(sprite);
			(*unit.0).flags &= 0xFFFFFCFF;
		}
	}
	else {
		bw::update_sprite_images_for_cloak((*v2.0).sprite);
		(*v2.0).flags = (*v2.0).flags & 0xFFFF_FEFF | 0x200; // USFlag_Cloaked
		let v3 = (*v2.0).subunit;
		if v3 != null_mut() {
			bw::update_sprite_images_for_cloak((*v3).sprite);
			(*v3).flags = (*v3).flags & 0xFFFF_FEFF | 0x200; // USFlag_Cloaked
			}
		}
	}

//ecx, edx, ebp+8
//signed int __fastcall BTNSCOND_TerranBasic(int a1, int a2, CUNIT *unitMem)
pub unsafe fn btnscond_terran_basic_hook(
	unit: *mut bw::Unit,
	player_id: u32,
	ecx: u32,
	orig: &dyn Fn(*mut bw::Unit,u32,u32)->u32)->u32{

	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => {
			return orig(unit,player_id,ecx);
		},
	};
	let v3 = player_id;
	if *bw::client_selection_amount != 1 {
		return 0;
	}
	let mut itr = 0;
	while bw::client_selection[itr as usize]!=null_mut() || (*bw::client_selection[itr as usize]).order == 0x21 {
		
		itr+=1;
		if itr == 12 {
			break;
		}
	}
	if	bw::check_unit_dat_requirements(player_id,0x6a,unit.0)!=0 || // command center
		bw::check_unit_dat_requirements(player_id,0x6d,unit.0)!=0 || // supply depot
		bw::check_unit_dat_requirements(player_id,0x6e,unit.0)!=0 || // refinery
		bw::check_unit_dat_requirements(player_id,0x6f,unit.0)!=0 || // barracks
		bw::check_unit_dat_requirements(player_id,0x7a,unit.0)!=0 || // engineering bay
		bw::check_unit_dat_requirements(player_id,0x7c,unit.0)!=0 || // missile turret
		bw::check_unit_dat_requirements(player_id,0x7d,unit.0)!=0 // bunker
	{ // units dat req checks
		return 1;
	}  
	return 0;
}
//let globals = Globals::get("is_tile_blocked_by_hook");
//let assume_pushing_units = globals.ai_mode[(*builder).player as usize].move_from_baselayout;
/*
pub unsafe fn 	(
	builder: *mut bw::Unit,
	x_tile: u32,
	y_tile: u32,
	dont_ignore_reacting: bool,
	also_invisible: bool,
	units: *mut *mut bw::Unit,
	orig: &Fn(*mut bw::Unit,u32,u32,bool,bool,*mut *mut bw::Unit)->u32,)->u32 {

  let v8 = units;
  let mut v6 = (*units);//first unit?
  let mut v7 = builder;
  
  let globals = Globals::get("is_tile_blocked_by_hook");
  let assume_pushing_units = globals.ai_mode[(*builder).player as usize].move_from_baselayout;
  if *units == null_mut(){
    return 0; 
  }
  loop
  {
    let mut v10 = 0;
    if v7 != null_mut()
    {
      let v9 = (*v6).flags;
      if v9 & 0x300 != 0
      {
        if !( (*v6).detection_status & (1 << (*v7).player) != 0)
		 {
          v10 = 1;
	     }
      }
    }
	//
	let mut jump_to_label_21 = false;
    if !dont_ignore_reacting && ((*v6).flags & 0x20000 != 0) {
		jump_to_label_21 = true;
	}
	if !jump_to_label_21 {	
		if v6 == v7 {
			jump_to_label_21 = true;
		}
		if !jump_to_label_21 {			
			if (*v6).flags & 6 != 0 && !assume_pushing_units {
				jump_to_label_21 = true;
			}
			if !jump_to_label_21 {	
				let v11 = (*v6).unit_id;
				if v11 == 202 || v11 == 105 || v10==1 && !also_invisible || ((*(*v6).sprite).flags & 0x20 != 0){ //dswarm, dweb
					jump_to_label_21 = true;
				}
				if !jump_to_label_21 {
					if bw::unit_positions_x[(*v6).position_search_left as usize].key < ((32 * x_tile) + 32) as u32	
					   && bw::unit_positions_x[(*v6).position_search_right as usize].key > (32 * x_tile) as u32
					   && bw::unit_positions_y[(*v6).position_search_top as usize].key < ((32 * y_tile) + 32) as u32
					   && bw::unit_positions_y[(*v6).position_search_bottom as usize].key > (32 * y_tile) as u32
					{
						if v10 !=0 {
							return 1;
						}
						else {
							return 4;
						}
					}
					v7 = builder;				
				}
			}
		}
	}
	//label21
	v6 = (*v6).next;
	if v6 == null_mut() {
		return 0;
	}
  }
}
*/

unsafe fn excavation_condition(
	game: Game,
	config: &Config,
	unit_id: u16,
	x_tile: u32,
	y_tile: u32,
	)->bool {
		
		if config.overlord {
			let map_width = (*game.0).map_width_tiles as u16;
			//if unit_id == 176 {
			if unit_id == 119 {
				//bw_print!("Excavated");
				let offset = x_tile as u16 + ((y_tile as u16)*map_width);
				let tile_id = *(*bw::tile_ids).offset(offset as isize) as u32;
				for e in config.excavation_settings.sets[(*game.0).tileset as usize].iter() {
					//bw_print!("tile_id {} < {}, {}, xy {} {}",e.min,e.max,tile_id,x_tile,y_tile);
					if tile_id >= e.min && tile_id<=e.max {
						return true;
					}
				}
				//bw_print!("No tiles");
				return false;
			}
		}
		false
	}
pub unsafe fn create_unit(
	y: u32,
	player: u32,
	x: u32,
	unit_id: u32,
	orig: &dyn Fn(u32,u32,u32,u32)->*mut bw::Unit)->*mut bw::Unit{
	
		let result = orig(y,player,x,unit_id);
		let config = config::config();
		if config.overlord {
			if result != null_mut(){
				if (*result).unit_id==98 {
					let sprite = (*result).sprite;
					let image = (*sprite).main_image;
					bw_print!("Y Offset {}",(*image).y_off);
				}
			}
		}
		result
	}
	
use std::cmp;
pub unsafe fn maelstrom_proc(
	target: *mut bw::Unit,
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit,*mut bw::Unit)->u32,)->u32{
		let config=config::config();
		if !config.hydra_malediction {
			return orig(target,unit);
		}
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(target,unit);
			},
		};
		let target = match Unit::from_ptr(target) {
			Some(s) => s,
			None => {
				return orig(target,unit.0);
			},
		};
		if target.is_hallucination(){
			bw::kill_unit(target.0);
			return 0;
		}
		else {
			if target.0!=unit.0 && !target.is_invincible(){
				if !target.id().is_building() && !target.burrowed(){
					if target.id().is_organic(){
//						bw::set_maelstrom_timer(22,target.0);
						bw::set_maelstrom_timer(12,target.0);
					}
					//malediction
					let game = Game::get();
					let mut globals = Globals::get("maelstrom_proc");
					//let level = aiscript::get_new_upgrade_level_upg(game,&mut globals.upgrades,116,unit.player());
					globals.ngs.set_generic_value(target,10,1);
					//new maelstrom visual code
					globals.ngs.set_maelstrom_offset(target.0,0);
					
					drop(globals);
					//if level!=0 {
						if (*target.0).energy > 0 && target.id().is_spellcaster() {//caster
							let mut leftover = cmp::min(12800,(*target.0).energy) as i32;
							(*target.0).energy -= leftover as u16;
							//savant status don't checks
							if target.id().has_shields(){
								if leftover > (*target.0).shields as i32 {
									leftover -= (*target.0).shields;
									(*target.0).shields = 0;
								}
								else {
									(*target.0).shields -= leftover;
									leftover = 0;
								}
							}
							let mut overlay = 0;
							if target.id().has_medium_overlay(){
								overlay = 1;
							}
							else if target.id().has_large_overlay(){
								overlay = 2;
							}
							//bw::create_lone_sprite(500+overlay,target.position().x as u32,0,target.position().y as u32);
							bw::damage_unit_func(unit.0,unit.player() as u32,0,target.0,leftover as u32);
							if (*target.0).order ==0 || (*target.0).sprite==null_mut(){
								bw::create_sprite_overlay(500+overlay,target.0);
							}
							else {
								bw::add_overlay_above_main(0,0,0,(*target.0).sprite, 979+overlay);
							}
						}	
					//}
				}
			}
			return 0;
		}
	}


struct Dimensions {
	pub height: u16,
	pub width: u16,
}	

impl From<u32> for Dimensions {
	fn from(val: u32) -> Self {
		Dimensions { height: ((val &0xffff0000)>>16) as u16, width: (val & 0x0000ffff) as u16 }
	}
}

impl Into<u32> for Dimensions {
	fn into(self) -> u32 {
		self.width as u32 | ((self.height as u32) << 16)
	}
}

//0043E670 = AiRegion_TrainAttackForce?(), eax AiRegion *region
//    Some offensive state?
pub unsafe fn train_attack_force (
    region: *mut bw::AiRegion,
    orig: &dyn Fn(*mut bw::AiRegion)->u32,)->u32 {
// 1

	let v2 = (*region).player;
	let ai = bw::player_ai(v2 as u32);
	let flags = (*ai).flags;
	let config = config::config();
	let globals = Globals::get("train_attack_force");
	let length = match config.extended_attacks {
		true=>globals.attack_extender[v2 as usize].unit_ids.len(),
		false=>63,
	};
	if length == 0 {//new safety check
		return 0;
	}
	drop(globals);
	let mut buffer: Vec<u32> = vec![0;length];
	let mut result = bw::get_missing_attack_force_units_func(region,&mut buffer[0] as *mut u32, ((flags >> 5)&0x1)==0);
	let v10 = result;
	let game = Game::get();
	let v5 = (*ai).previous_building_hit_second;
	let v12 = (*game.0).elapsed_seconds.saturating_sub(v5);	
	let mut condition = false;
	condition = v12>=30;
	if !condition {
		condition = (v12<30);
	}
	let v11;
	if flags & 0x20 !=0 || (v5!=0 && condition) {
		v11 = 60;
	}
	else {
		v11 = 100;
	}
	let mut v6 = 0;
	let mut v12 = 0;
	if result != 0 {
		loop {
			let v7 = buffer[v6];
			if v7 < 228 && bw::ai_add_to_region_military(v7, v11, region)==0 {
				let v8 = (*region).player;
				(*ai).flags |= 0x40;
				bw::add_cost_to_ai_needs(1,v7,v8 as u32);
				bw::add_spending_request_func(60, region as *mut libc::c_void, v7 as u16, 1, v8);
				bw::add_base_cost_to_ai_needs(60, region, v8 as u32, v7);
				v6 = v12;
			}
			result = v10;
			v6 += 1;
			v12 = v6;
			if v6 as u32 >= v10 {
				break;
			}
		}
	}
	return result;
}


//0043EEC0 = AiRegion_GroupingForAttack(), arg 1 AiRegion *region
pub unsafe fn grouping_for_attack (
    region: *mut bw::AiRegion,
    orig: &dyn Fn(*mut bw::AiRegion)) {
	
	bw::ai_recalculate_region_strength(region);
	bw::ai_target_enemy_in_region(region);
	let mut a2: u32=0;//has_ground_military
	let mut a3: u32=0;//cant_attack_ground
	let mut a4: u32=0;//cant_attack_air
	let config = config::config();
	let globals = Globals::get("grouping_for_attack");
	let length = match config.extended_attacks {
		true=>globals.attack_extender[(*region).player as usize].unit_ids.len(),
		false=>63,
	};
	drop(globals);
	if length == 0 {//new safety check
		return;
	}
	let mut buffer: Vec<u32> = vec![0;length];	
	if (*region).flags & 0x20 !=0 && bw::move_military_to_defend(region)!=0{
		return;
	} 
	bw::ai_get_region_military_types(&mut a2 as *mut u32,&mut a3 as *mut u32,&mut a4 as *mut u32,region);
	let mut v1 = region;
	let pathing = bw::pathing();
	if a2!=0 && (*pathing).regions[(*region).id as usize].unk == 0x1ffd || bw::ai_can_travel_to_region(region,region)!=0{//0x1ffd - unwalkable
		bw::change_ai_region_state(region,0);
		let v2 = bw::ai_pickattacktarget_nobuildings(region);
		if v2 != null_mut() {
			let region_group = bw::ai_regions((*region).player as u32);
			let v3 = ai::ai_region(region_group, bw::Point{x: (*(*v2).sprite).position.x, y: (*(*v2).sprite).position.y }).unwrap();
			bw::change_ai_region_state(v3,9);
			bw::ai_move_military_to_otherregion(region,v3,0,0,1);
		}
		else {
			bw::func_3dd20(region);
		}
		return;
	} 
	
    if !(bw::is_attack_timed_out((*region).player as u32)!=0) {
        let v4 = bw::get_missing_attack_force_units_func(region, &mut buffer[0] as *mut u32, true);
        let mut v5 = 0;
		let mut current_block;
        if v4 == 0 {
            current_block = true;
        } else { 
			current_block = false; 
		}
        loop  {
            match current_block {
                true => { 
					if bw::has_forces_moving_to_attack((*region).player as u32) != 0 { 
						return; 
					} 
					break; 
				}
                _ => {
                    if buffer[v5] >= 0xe4 {
                        v5 += 1;
                        if v5 as u32 >= v4 {
                            current_block = true;
                        } else { 
							current_block = false; 
						}
                    } else { 
						return; 
					}
                }
            }
        }
    }
	let v6 = (*region).target_region_id;
    if v6 != 0
    {
		let regions = bw::ai_regions((*region).player as u32);
		let v7 = regions.offset(v6 as isize);
		bw::change_ai_region_state(v7,1);
		let v8 = v7;
		v1 = region;
		bw::ai_move_military_to_otherregion(region,v8,0,0,1);
    }
	bw::change_ai_region_state(v1,0);
}

// 00438050 = GetMissingAttackForceUnits(), arg 1 AiRegion *region, arg 2 dword *out_unit_ids, arg 3 only_accept_completed -- https://pastebin.com/raw/vUSUGniF
// resolves 00438061
//also needed to hook: 0043E670 () and 0043EEC0 

pub unsafe fn get_missing_attack_force_units_hook (
    region: *mut bw::AiRegion,
    attacker_ids_result: *mut u32,
    campaign_bool: bool,
    orig: &dyn Fn(*mut bw::AiRegion,*mut u32,bool)->u32,)->u32 {
	let player = (*region).player;
	let ai = bw::player_ai(player as u32);
	let mut globals = Globals::get("get_missing_attack_force_units_hook");
	let config = config::config();
	/*
	let ids = match config.extended_attacks {
		true=>globals.attack_extender[player as usize].unit_ids.iter().filter(|x| **x!=0).map(|x| x-1).collect::<Vec<_>>(),
		false=>(*ai).attack_force.iter().filter(|x| **x!=0).map(|x| x-1).collect::<Vec<_>>(),
	};*/
	let mut last = 0;
	match config.extended_attacks {
		true=>{
			for (i, uid) in globals.attack_extender[player as usize].unit_ids.iter_mut().filter(|x| **x!=0).enumerate(){
				*attacker_ids_result.offset(i as isize) = (*uid-1).into();
				last = i as u32;
			}				
		},
		false=>{
			for (i, uid) in (*ai).attack_force.iter_mut().filter(|x| **x!=0).enumerate(){
				*attacker_ids_result.offset(i as isize) = (*uid-1).into();
				last = i as u32;
			}	
		},
	}
	
	drop(globals);
	for u in unit::active_player_units(player){
		if u.sprite().is_some() && u.order()!=order::id::DIE{
			let id = if u.id().0 == 0x1e { 0x5 } else { u.id().0 };
			let mut id = UnitId(id);
			let mut unit_count = 1;
			if !u.is_completed() || u.order().0==0x96 || u.order().0==0x6a  {
				if campaign_bool {
					//do same on label 36 goto
					continue;
				}
				let is_egg = config.eggs.single_list.clone().iter_mut().find(|x| x.0 == id.0).is_some();
				if is_egg {
				//???
					id = u.current_train_id();
//					id = UnitId((*u.building_ai().unwrap()).train_queue_types[(*u.0).current_build_slot as usize] as u16);//unsafe
					if id.two_in_egg(){
						unit_count = 2;
					}
				}
			}
			if u.id().ai_flags() & 0x1 == 0 {
				let mut ai_condition = false;
				if let Some(military_ai) = u.military_ai(){
					if (*military_ai).region == region {
						ai_condition = true;
					}
				}
				if !u.is_completed() || u.order().0==0x96 || u.order().0==0x6a || ai_condition {
					if unit_count != 0 {						
						let mut index = last.saturating_sub(1);
						loop {
							let label_35: bool;
							let mut itr = 0;
							if last != 0 {
								loop {
									if !(id.0 != *attacker_ids_result.offset(itr as isize) as u16) {
										label_35 = false;
										break ;
									}
									itr += 1;
									if itr >= last { 
										label_35 = true;
										break; 
									}
								}
								match label_35 {
									true => { }
									_ => { 
										*attacker_ids_result.offset(itr as isize) = 228;
										if itr == index { 
											last -= 1; 
											index -= 1; 
										} 
									}
								}
							}
							unit_count -= 1;
							if unit_count == 0 { 
								break; 
							}
						}
					}
				}	
			}
		}
	}
	return last;
/*
        if ( v11 )
        {
          v15 = i - 1;
          do
          {
            v16 = 0;
            if ( i )
            {
              while ( v10 != a2[v16] )
              {
                if ( ++v16 >= i )
                  goto LABEL_35;
              }
              a2[v16] = UNITID_None;
              if ( v16 == v15 )
              {
                --i;
                --v15;
              }
            }
			LABEL_35:
            --v11;
          }
          while ( v11 );
        }
      }
    }
    goto LABEL_36;
  }
  return i;
*/	
	
	}

// 0043ABB0 = Ai_AttackTo?(), arg 1 player, arg 2 x, arg 3 y, arg 4 bool always_overwrite_last_attack, arg 5 allow_fallback_air_only -- https://pastebin.com/raw/CnpBsRsj
// resolves 0043AC54, 0043AC5B, and 0043B1C4
pub unsafe fn ai_attack_to_hook (
    player: u32,
    x: i32,
    y: i32,
    overwrite_last: bool,
    air_fallback_only: bool,
    orig: &dyn Fn(u32,i32,i32,bool,bool)->u32,)->u32 {
        let mut regionList: [u8;2500] = [0;2500];
        //let mut v10: i32 = 0;
        let ai = bw::player_ai(player);
        if (*ai).attack_failed == 1 {
            return 1;
        }
        let grouping_region = (*ai).attack_grouping_region;
        let regions = bw::ai_regions(player);
        if grouping_region != 0 {
			let region = regions.offset(grouping_region as isize);
            return ((*region).state != 8) as u32;//state of previous airegion is not Moving to attack
        }
		let region_offset = &mut regionList[0] as *mut u8;
        bw::ai_get_own_regions(region_offset, player);
        bw::ai_get_enemy_regions(player, region_offset);
        while bw::ai_get_neighbor_region_types(region_offset) != 0 {
            bw::ai_finish_region_access(region_offset);
        }
		let globals = Globals::get("ai_attack_to");
		let config = config::config();
		let mut air_attack = match config.extended_attacks {
			true=>globals.attack_extender[player as usize].unit_ids.len()!=0,
			false=>(*ai).attack_force[0]!=0,
		};
		if air_attack {
			let find = match config.extended_attacks {
				true=>globals.attack_extender[player as usize].unit_ids.iter()/*.take_while(|x| **x!=0)*/.map(|x| x-1).find(|x| !UnitId(*x).is_air()),
				false=>(*ai).attack_force.iter().take_while(|x| **x!=0).map(|x| x-1).find(|x| !UnitId(*x).is_air()),				
			};
			if find.is_some(){
				air_attack = false;
			}
		}
		drop(globals);
		(*ai).flags |= 0x100;//send detectors
        if bw::ai_set_attack_prep_region(player, region_offset, x, y, overwrite_last, air_attack, false) == 0 && 
			bw::ai_set_attack_prep_region(player, region_offset, x, y,overwrite_last, air_attack, true) == 0
		{
            if !air_fallback_only || air_attack  {
                return 1;
            }
            if bw::ai_set_attack_prep_region(player, region_offset,  x, y, overwrite_last, true, false) == 0 && 
				bw::ai_set_attack_prep_region(player, region_offset,  x, y, overwrite_last, true, false) == 0 {
                (*ai).attack_failed = 1;
                return 1;
            }
        }
        return 0;
    }






// 00437F20 = Ai_HasUnitsForAttackForce(), edx player -- https://pastebin.com/raw/NwriVgR5
// resolves 00437F31
pub unsafe fn has_units_for_attack_force_hook (
    player: u32,
    orig: &dyn Fn(u32)->u32,)->u32 {
	let ai = bw::player_ai(player);		
	let config = config::config();
	let globals = Globals::get("has_units_for_attack_force_hook");
	let unit_ids = match config.extended_attacks {
		true=>globals.attack_extender[player as usize].unit_ids.iter().filter(|x| **x!=0).map(|x| x-1).collect::<Vec<_>>(),
		false=>(*ai).attack_force.iter().filter(|x| **x!=0).map(|x| x-1).collect::<Vec<_>>(),
	};
						
	drop(globals);
	if unit_ids.len()==0 {
		return 0;
	}
	for u in unit::active_player_units(player as u8).filter(|u| u.sprite().is_some() && u.unit_ai().is_some() && !u.id().is_subunit() && u.is_completed())
						.filter(|u| !u.id().unknown_ai_flag() && u.order().0!=0x96 && u.order().0!=0x6a 
								&& bw::orders_dat_use_weapon_targeting[u.order().0 as usize]==0)
						//archon, reset collision
	{		
		let id = if u.id().0 == 0x1e { 0x5 } else { u.id().0 };//epic siege tank hardcode
		let index = unit_ids.iter().position(|x| *x==id);
		if let Some(index) = index {
			return 1;
		}
	}		
    return 0;
}



// 0043E050 = Ai_SuicideMission(), arg 1 player, eax bool random_suicide -- https://pastebin.com/raw/LcNKeAF7
// resolves 0043E1EE
/*
pub unsafe fn ai_suicide_mission_hook(
    player: u32,
	random: u8,
    orig: &dyn Fn(player,random)) {
    let ai = bw::player_ai(player);
    if random
    {
		for unit in unit::active_player_units(player as u8) {
			if unit.sprite().is_none()|| unit.order()==order::id::DIE || unit.is_subunit(){
				continue;
			}
			if unit.id().0 == 14 || unit.id() == unit::id::LARVA {//nuke or larva
				continue;
			}
			if (*unit.sprite().unwrap()).flags & 0x20 != 0 {
				continue;
			}
			if bw::is_threat(null_mut(), unit.0) {
				continue;
			}
			bw::set_suicide_target(unit);	
		}
    }
	else if (*ai).strategic_suicide_mission_cooldown == 0 {
		let mut target_region: *mut bw::Region = null_mut();
		for unit in unit::active_player_units(player as u8) {
			if unit.sprite().is_none()|| unit.order()==order::id::DIE || unit.is_subunit(){
				continue;
			}
			if ((*unit.0).flags & 0x20 != 0) && (*ai).flags & 0x20 == 0 {
				continue;
			}
			if unit.unit_ai().is_none(){
				continue;
			}
			match (*(*unit.0).ai).ai_type {
				2|3=>{
					continue;
				},
				1=>{
					(*(*unit.0).ai).parent = null_mut();
					let region = bw::get_ai_region(unit.0);
					bw::add_military_ai(unit.0,region,1);
				}
			}
			/*
            if (unit->ai->type == 1)
            {
                GuardAi *ai = (GuardAi *)unit->ai;
                ai->parent = nullptr;
                ai->list.Change(bw::first_guard_ai[player], needed_guards[player]);
                Region *region = GetAiRegion(unit);
                AddMilitaryAi(unit, region, true);
            }
            Assert(unit->ai->type == 4);
            if (IsInAttack(unit))
                continue;
            if (target_region == nullptr)
            {
                memset(bw::player_ai[player].attack_force, 0, 0x40 * 2);
                bw::player_ai[player].attack_grouping_region = 0;
                bw::Ai_EndAllMovingToAttack(player);
                const auto &pos = unit->sprite->position;
                auto error = bw::Ai_AttackTo(player, pos.x, pos.y, 0, 1);
                if (error != 0)
                    continue;
                bw::player_ai[player].strategic_suicide = 0x18;
                target_region = bw::player_ai_regions[player] +
                    (bw::player_ai[player].attack_grouping_region - 1);
            }
            RemoveUnitAi(unit, 0);
            AddMilitaryAi(unit, target_region, true);
        }
    }	*/
}*/

// 00447230 = Ai_AddToAttackForce(), edi unit_id, eax count, ecx player -- https://pastebin.com/raw/jz3ZLuH3
// resolves 0044724F, 0044725F, and 0044727B
pub unsafe fn ai_attack_add(
    unit_id: u32,
	count: u32,
	player: u32,
    orig: &dyn Fn(u32, u32, u32)) {
		let mut globals = Globals::get("ai_attack_add_hook");
		if player < 8 && unit_id < 0xe4 {
			aiscript::add_to_attack_force(&mut globals,player as u8,UnitId(unit_id as u16),count);
		}
		
		
	/*
        let mut counter: u32 = result;
        let mut plOffset: u32 = 0;
        let mut attackGroups: i16 = 0;
        let mut v7: i16 = 0;
        let ai = player_ai(player);
        let mut atkForce = (*ai).attack_force;

        if player < 8 && result < 63 && unit_id < 0xE4 {
            plOffset = 628 * player;
            let mut i: u32 = 0;
            if atkForce != null_mut() {
                atkGroups = (*ai).attackForce;
                while i < 63 {
                    atkGroups += 1;
                    i += 1;
                    if atkGroups == null_mut() {
                        //label 8
                        if counter != 0 {
                            v7 = (*ai).attack_force[plOffset + i];
                            while counter != 0 {
                                counter -= 1;
                                if i >= 63 {
                                    break;
                                }
                                v7 = unit_id + 1;
                                i += 1;
                                v7 += 1;
                            }
                        }
                    }
                }
            }
            else {
                if counter != 0 {
                    v7 = (*ai).attack_force[plOffset + i];
                    while counter != 0 {
                        counter -= 1;
                        if i >= 63 {
                            break;
                        }
                        v7 = unit_id + 1;
                        i += 1;
                        v7 += 1;
                    }
                }
            }
        }*/
    }

// 00447040 = Ai_RemoveFromAttackForce(), eax unit_id -- https://pastebin.com/raw/9cWaCGku
// resolves 00447059 and 0044707C
pub unsafe fn ai_remove_from_attack(
    unit_id: u32,
    player: u32,
    orig: &dyn Fn(u32, u32)) {
		let mut globals = Globals::get("ai_remove_from_attack");
		let config = config::config();
        let unit_id = if unit_id == 0x1e {
            5
        } else {
            unit_id as u16
        };
		let ai = bw::player_ai(player);
		if config.extended_attacks {
			for val in globals.attack_extender[player as usize].unit_ids.iter_mut(){
				if *val == unit_id + 1 {
					*val = 0xe5;
					return;
				}
			}
		}
		else {
			for val in (*ai).attack_force.iter_mut() {
				if *val == unit_id + 1 {
					*val = 0xe5;
					return;
				}
			}			
		}
    }

pub unsafe fn maptileflag_placement_hook(
	x_tile: u32,
	y_tile: u32,
	player: u8,
	size_wh: u32,
	placement_state_entry: u32,
	check_vision: u32,
	unit_id: u16,//eax
	orig: &dyn Fn(u32,u32,u8,u32,u32,u32,u16)->u32,)->u32{
//		let result = orig(x_tile,y_tile,player,size_wh,placement_state_entry,check_vision,unit_id);
//		bw_print!("R: {}",result);
//		result
	//let size = Dimensions::from(size_wh);

    let game = Game::get();
	let config = config::config();
	let map_width = (*game.0).map_width_tiles as i16;
	let map_height = (*game.0).map_height_tiles as i16;
	if x_tile as i16 > map_width || y_tile as i16 > map_height {
		return orig(x_tile,y_tile,player,size_wh,placement_state_entry,check_vision,unit_id);
	}
	if player>=0xc {
		panic!("Incorrect player id {} in maptileflags placement hook",player);
	}
	
  //a1 - unit_id
  //a2 - x_tile
  //a3 - y_tile
  //a4 - player
  //a5 - size_wh
  //a6 - placement_state_entry
  //a7 - check_vision
  
  let mut v17 = 0;
  if unit_id >= 194 && unit_id<=199 {
	v17 = 1;
  }
  let id = UnitId(unit_id as u16);
  let v7 = bw::player_list[player as usize].ty;
  let v20 = match v7==2{
	true=>1<<(player+8),//fog
	false=>0,
  };
  let v19 = match v7==2{
	true=>1<<player,//vision
	false=>0,
  };
  
//  debug!("x y wdth {} {} {}",x_tile,y_tile,map_width);
  //let mut v8 = *(*bw::tile_flags).offset((x_tile as i16 + (y_tile as i16)*map_width) as isize);
 
  let v8 = *(*bw::tile_flags).offset((x_tile as u16 + (y_tile as u16)*map_width as u16) as isize);
  let mut v18 = 0x20800000;//mixed walkability + unbuildable
  if id.group_flags() & 0x1 == 0 {
	if check_vision != 0 {
		v18 |= 0x40000000;//local creep (player sees due to the fog)
	}
	else {
		v18 |= 0x400000;//real creep
	}
  }
  let mut v22 = 0;
  let mut v21 = 0;
  
  let width = size_wh & 0x0000ffff;//width //v10
  let height = (size_wh & 0xffff0000)>>16;//height
  if height <= 0 {
    return 0;
  }
  let mut v9 = 0;
//  let mut offset = 0;//old
 // let mut y_offset = 0;//new
  let mut v11 = 0;
  //let mut v8_ptr = (*bw::tile_flags).offset((x_tile as i16 + (y_tile as i16)*map_width) as isize);
  let mut v8_ptr = (*bw::tile_flags).offset((x_tile as u16 + (y_tile as u16)*map_width as u16) as isize);
//  debug!("Maptileflags x{} y{} p{}, wh{}",x_tile,y_tile,player,size_wh);

  loop {
    v11 = 0;
    if width > 0
    {
	  let v15 = 6*(v9+(8*placement_state_entry));
	  loop 
	  {
		let v12 = *v8_ptr;
		let mut v13 = 0;
		//CUSTOM EXCAVATION	
		let mut excav_check = true;
		let excav = excavation_condition(game,&config,unit_id,v11+x_tile,v21+y_tile);
		//if unit_id==176 && !excav {
		let config = config::config();
		if unit_id==119 && config.overlord && !excav {
			excav_check = false;
		}
		//CUSTOM EXCAVATION		
		if v12 & v20 != 0 {//fog flags
			v13 = 10;
		}
        else if  ((check_vision==0 && (v12 & v19 != 0 )) || ((v12 & v18 == 0 || excav) && excav_check) || v17!=0 /*(v17!=0 && v12 < 0)*/) 
					 //vision,fog
        {
          if id.is_town_hall()
          {
			if bw::check_resource_blocking(check_vision,(v11+x_tile) as u16,(v21+y_tile) as u16)==1
			{			 
              v13 = 2;
			}
          }
        }
        else
        {
          v13 = 4;
        }
		v8_ptr = v8_ptr.offset(1);
		bw::building_placement_state[(v11+v15) as usize]=v13;
		v11 += 1;
		if v22 <= v13 {
			v22 = v13;
		}
//		width = size_wh & 0x0000_ffff;	  
		if v11 >= width {
			break;
		}
      }
    }
    v9 = v21 + 1;
    v21 = v9;
	v8_ptr = v8_ptr.offset((map_width-width as i16) as isize);
	//v9 += 1;
//    offset += (map_width - width as i16);//old
//	y_offset += 1;//new
	//end external loop
	if v9 >= height {
		break;
	}
	
  }
  return v22 as u32;
}


//attacks


pub unsafe fn is_tile_blocked_by_hook(
	builder: *mut bw::Unit,
	x_tile: u32,
	y_tile: u32,
	dont_ignore_reacting: bool,
	also_invisible: bool,
	units: *mut *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit,u32,u32,bool,bool,*mut *mut bw::Unit)->u32,)->u32 {

   //let analysis = orig(builder,x_tile,y_tile,dont_ignore_reacting,also_invisible,units);
	
  //debug!("Init...{:p}",units);
  if units == null_mut(){
	return orig(builder,x_tile,y_tile,dont_ignore_reacting,also_invisible,units);
  }
  let mut v8 = units;
 // debug!("Set to units {:p}",units);
  
  let mut v6 = *units;//first unit?
   //debug!("Set to builder {:p}",builder);
   if builder == null_mut(){
	//debug!("return orig");
	return orig(builder,x_tile,y_tile,dont_ignore_reacting,also_invisible,units);
  }
  let mut v7: *mut bw::Unit = builder;
 // debug!("is_tile_blocked_by_hook {}",(*builder).player);
  
  
  //let globals = Globals::get("is_tile_blocked_by_hook");
//  debug!("borrowed (disabled)... {}",(*builder).player);
  
  //let assume_pushing_units = globals.ai_mode[(*builder).player as usize].move_from_baselayout;
 // debug!("set assume");
 // drop(globals);
  let globals = Globals::get("is_tile_blocked_by_hook");
  let assume_pushing_units = globals.ai_mode[(*builder).player as usize].move_from_baselayout.clone();
  drop(globals);
 
  //debug!("dropped");
  let xpos = x_tile as i16 * 32;
  let ypos = y_tile as i16 * 32;
  //debug!("xpos/ypos");
  let game = Game::get();
  let map_width = (*game.0).map_width_tiles as u16;
  //debug!("*v8");
  if *v8 == null_mut(){
    return 0; 
  };
  loop
  {
	'inner: loop {
		let mut invisible = false;
		if v7 != null_mut() {
			 let v9 = (*v6).flags;
			 if v9 & 0x300 != 0 {
				//
				if  ((1 << (*v7).player) & (*v6).detection_status)==0 {
					invisible = true;
				}
			 }
		}
		if !dont_ignore_reacting && (*v6).flags & 0x20000 != 0 {//moves-reacts flag
			break 'inner;
		}
		
		if v6==v7 {
			break 'inner;
		}
		if (*v6).flags & 0x6 != 0 {
			break 'inner;
		}
		let v11 = (*v6).unit_id;
		if v11==202 || v11==105 || (invisible && !also_invisible) || (*(*v6).sprite).flags&0x20 != 0 {
			break 'inner;
		} 
		//new code
		if (*v6).flags & 0x10 != 0 {//burrow status
			break 'inner;
		}
		//new unit mover
		if assume_pushing_units {
			if aiscript::block_check(Unit::from_ptr(v6).unwrap(),Unit::from_ptr(builder).unwrap(),assume_pushing_units){
				break 'inner;
			}
		}
		
		if bw::unit_positions_x[(*v6).position_search_left as usize].key < ((32 * x_tile) + 32) as u32
					   && bw::unit_positions_x[(*v6).position_search_right as usize].key > (32 * x_tile) as u32
					   && bw::unit_positions_y[(*v6).position_search_top as usize].key < ((32 * y_tile) + 32) as u32
					   && bw::unit_positions_y[(*v6).position_search_bottom as usize].key > (32 * y_tile) as u32
		{
			if invisible {
				return 1;
			}
			else {
				return 4;
			}
		}
		v7 = builder;
		break 'inner;
	}
//	v6 = (*v6).next;//OLD
	 //debug!("offset...");
	v8 = v8.offset(1);//NEW
	v6 = *v8;//NEW
	


	if v6 == null_mut() {
		return 0;
	}	
  }
  /*
  if *units == null_mut(){
	if analysis != 0 {
		bw_print!("TileBlockMismatch {} {}",0,analysis); 
	}
    return 0; 
  }
  loop
  {
    let mut v10 = 0;
    if v7 != null_mut()
    {
      let v9 = (*v6).flags;
      if v9 & 0x300 != 0//0x100 begin invisibility, 0x200 invisibility done 
      {
        if !( (*v6).detection_status & (1 << (*v7).player) != 0)
		 {
          v10 = 1;
	     }
      }
    }
	//
    if (!(!dont_ignore_reacting && ((*v6).flags & 0x20000 != 0))) || assume_pushing_units {//reacts-moves flag
		if v6 != v7 {
			if (*v6).flags & 6 == 0 {//not landed building, not in air
				let v11 = (*v6).unit_id;
				if !(v11 == 202 || v11 == 105 || v10==1 && !also_invisible || ((*(*v6).sprite).flags & 0x20 != 0)){ //dswarm, dweb, hidden flag
					if bw::unit_positions_x[(*v6).position_search_left as usize].key < ((32 * x_tile) + 32) as u32
					   && bw::unit_positions_x[(*v6).position_search_right as usize].key > (32 * x_tile) as u32
					   && bw::unit_positions_y[(*v6).position_search_top as usize].key < ((32 * y_tile) + 32) as u32
					   && bw::unit_positions_y[(*v6).position_search_bottom as usize].key > (32 * y_tile) as u32
					{
						if v10 !=0 {
							if analysis != 1 {
								bw_print!("TileBlockMismatch {} {}",1,analysis); 
							}
							return 1;
						}
						else {
							if analysis != 4 {
								bw_print!("TileBlockMismatch {} {}",4,analysis); 
							}
							return 4;
						}
					}
					v7 = builder;
				}
			}
		}
	}
	v6 = (*v6).next;
	if v6 == null_mut() {
		if analysis != 0 {
			bw_print!("TileBlockMismatch {} {}",0,analysis); 
		}
		return 0;
	}
  }*/
}

pub unsafe fn ai_make_detector_behavior(
	unit: *mut bw::Unit,
	player_id: u32,
	orig: &dyn Fn(*mut bw::Unit,u32)->u32,)->u32{
		let player = bw::player_ai(player_id);
		let req = (*player).requests[0 as usize];
		orig(unit,player_id)
	}

pub unsafe fn btnscond_terran_adv_hook(
	unit: *mut bw::Unit,
	player_id: u32,
	ecx: u32,
	orig: &dyn Fn(*mut bw::Unit,u32,u32)->u32)->u32{


	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => {
			return orig(unit,player_id,ecx);
		},
	};
	let v3 = player_id;
	if *bw::client_selection_amount != 1 {
		return 0;
	}
	let mut itr = 0;
	while bw::client_selection[itr as usize]!=null_mut() || (*bw::client_selection[itr as usize]).order == 0x21 {	
		itr+=1;
		if itr == 12 {
			break;
		}
	}
	if 	bw::check_unit_dat_requirements(player_id,0x71,unit.0)!=0 || // factory
		bw::check_unit_dat_requirements(player_id,0x71,unit.0)!=0 || // starport
		bw::check_unit_dat_requirements(player_id,0x71,unit.0)!=0 || // science facility
		bw::check_unit_dat_requirements(player_id,0x71,unit.0)!=0 // armory
		{
		return 1;
	}
	return 0;
}

pub unsafe fn ai_train_unit_hook(
    unit_id: u16,
    ai_request_type: u32,
    value: *mut libc::c_void,
    parent: *mut bw::Unit,
    orig: &dyn Fn(u16,u32,*mut libc::c_void,*mut bw::Unit)->u32)->u32{
        let parent = match Unit::from_ptr(parent) {
            Some(s) => s,
            None => {
                return orig(unit_id,ai_request_type,value,parent);
            },
        };
		let queue_slot = bw::get_empty_build_queue_slot(parent.0);
		if queue_slot == 5{
			return 0;
		}
		let build_unit_bool = bw::prepare_build_unit(unit_id as u32,parent.0);
		if build_unit_bool == 0 {
			return 0;
		}
		if let Some(building_ai) = parent.building_ai(){
			(*building_ai).train_queue_types[queue_slot as usize] = ai_request_type as u8;
			(*building_ai).train_queue_values[queue_slot as usize] = value;
		}
		return 1;
	}

pub unsafe fn set_build_hp_gain_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(unit);
				return;
			},
		};

			if unit.id().build_time() == 0{
				//blockA
				(*unit.0).build_hp_gain = 1;
				//blockB
				return;
			}
			let hp_difference = unit.id().hitpoints() - unit.id().hitpoints() / 10;
			let time_cost = unit.id().build_time();
			let calc = (hp_difference as u32+time_cost-1) as f64/time_cost as f64;
			let set_build_hp_gain_calc = calc;
			if (set_build_hp_gain_calc) as u16 != 0{
				(*unit.0).build_hp_gain = (set_build_hp_gain_calc) as u16;
				//blockB
				return;
			}
			else{
				//blockA
				(*unit.0).build_hp_gain = 1;
				//blockB
				return;
			}
	}





pub unsafe fn ai_workerai(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
	
	
	orig(unit);
	
	/*
	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => {
			return;
		},
	};			
	let game = Game::get();
	if let Some(worker_ai) = unit.worker_ai(){
		if (*worker_ai).wait_timer != 0 {
			(*worker_ai).wait_timer -= 1;
			return;
		}
		let town = (*worker_ai).town;
		if town == null_mut(){
			debug!("Incorrect worker bug - no AI town");
			return;
			//there's no such check in original hook, but I call it anyway, just in case
		}
		(*worker_ai).last_update_second = (*game.0).elapsed_seconds;
		if !(unit.id().is_zerg() || unit.id().is_protoss()){
			if unit.id().is_terran(){
				let ai_repair = bw::ai_repairsomething(unit.0, town);
				if ai_repair != 0 {
					//jump to blockD;
				}
			}
		}
		//blockC
		if bw::ai_tryprogressspendingqueue_worker(town, unit.0) != 0 {
			//jump to blockD;
		}
		if bw::is_resource_depot((*town).main_building) == 0 {
			//jump to blockD;
		}
		//
		let res = (*worker_ai).target_resource;
		let target = unit.current_harvest_target();
		let mut gas: *mut bw::Unit = null_mut();
		if res == 1 {
			if target != null_mut() && bw::is_mineral_field(target)!=0 {
				if bw::is_mineral_field(target)!=0{
					bw::ai_harvest(null_mut(),unit.0,target);
					return;			
				}
			}
			let mut res: *mut bw::Unit = null_mut();
			if (*town).mineral != null_mut(){
				let used_res = unit.ai_used_resource();
				if used_res != 0 {
					let sprite = (*unit.0).sprite;
					let y = (*sprite).position.y;
					let count = (*town).worker_count;
					let mut index: i32 = match (*town).worker_count <=  0xc {
						true=>0,
						false=>-1,
					};
					index += 1;
					let pos_xy = (*sprite).position.x as u32 + ((*sprite).position.y<<16) as u32;
					res = bw::find_free_resource(unit.0,pos_xy,index as u32);//res is EDI
					if res == null_mut() {
						res = (*town).mineral;
					}
				}
			}
			gas = bw::get_town_gas(3,1,town);
			if gas == null_mut(){
				gas = bw::get_town_gas(2,1,town);
			}
			bw::ai_harvest(gas,unit.0,res);//?
			return;			
		}
		if res == 2 {
			gas = bw::get_town_gas(3,1,town);//eax
			//jump to blockK
		}
		gas = (*town).gas_buildings[0];
		if (*worker_ai).target_resource == 3 {
			let mut gas_building_count = 0;
			if gas!=null_mut() {
				if bw::get_gas_resource_state(gas, (*town).player) == 3 {
					gas_building_count = 1;
				}
			}
			gas = (*town).gas_buildings[1];
			if (*town).gas_buildings[1] != null_mut() {
				if bw::get_gas_resource_state(gas, (*town).player) == 3 { 
					gas_building_count += 1;
				}
				if gas_building_count == 2 {
					//jump to blockO
				}
			}
			gas = (*town).gas_buildings[2];
			if gas == null_mut() || bw::get_gas_resource_state(gas, (*town).player) != 3 {
				//jump to blockP
			}
			gas_building_count += 1;
			//jump to blockR w/ comparison EBX and 2 (no condition jump)
			
			if gas_building_count == 2 {
				//jump to blockR
			}			
		}
		//blockL
		gas_building_count = 0;
		if (*town).gas_buildings[0] != null_mut() && {
			if bw::get_gas_resource_state(gas, (*town).player) == 3 {
				gas_building_count = 1;
			}	
		}
		if (*town).gas_buildings[1] != null_mut(){
			let state = bw::get_gas_resource_state(gas, (*town).player);
			if state == 3{
				gas_building_count += 1;
				if gas_building_count == state {
					//jump to blockO
				}
			}
		}
		//blockT
		if (*town).gas_buildings[2] == null_mut(){
			//jump to blockP 
		}
		let state = bw::get_gas_resource_state(gas, (*town).player);
		if state == 3 {
			//jump to blockP
		}
		gas_building_count += 1;
		let comparison = (gas_building_count == state);
		//blockR
		if !comparison {
			//jump to blockP (or if ebx != 2, if jumped from blockN)
		}
		//blockO
		gas = (*town).gas_buildings[2];
		//blockK
		if gas != null_mut(){
			//jump to blockQ
		}
		gas = bw::get_town_gas(3,1,town);
		//blockP
		*/
		

/*
# Ai_WorkerAi(), arg 1 Unit *unit
	
	:blockR
	004353E7  |>  75 0A         JNZ SHORT 004353F3
	jump to blockP if ebx != eax # or if ebx != 2, if jumped from blockN
	
	:blockO
	004353E9  |>  8BC7          MOV EAX,EDI
	eax = edi # AiTown.gas_buildings[0x2]
	
	:blockK
	004353EB  |>  85C0          TEST EAX,EAX
	compare eax to 0
	
	004353ED  |.  0F85 B5010000 JNZ 004355A8
	jump to blockQ if eax != 0
	
	:blockP
	004353F3  |>  6A 01         PUSH 1
	puhs 1
	
	004353F5  |.  6A 03         PUSH 3
	push 3
	
	004353F7  |.  E8 94D7FFFF   CALL 00432B90
	call 
	
	004353FC  |.  85C0          TEST EAX,EAX
	compare eax to 0
	
	004353FE  |.  0F85 A4010000 JNZ 004355A8
	jump to blockQ if eax != 0
	
	00435404  |.  8B46 30       MOV EAX,DWORD PTR DS:[ESI+30]
	eax = AiTown.gas_buildings[0] # Unit for Ai_GetGasResourceState
	
	00435407  |.  33DB          XOR EBX,EBX
	ebx = 0
	
	00435409  |.  85C0          TEST EAX,EAX
	compare AiTown.gas_buildings[0] to 0
	
	0043540B  |.  74 13         JE SHORT 00435420
	jump to blockU if AiTown.gas_buildings[0] = 0
	
	0043540D  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	00435411  |.  E8 CAD2FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435416  |.  83F8 03       CMP EAX,3
	compare eax to 3
	
	00435419  |.  75 05         JNZ SHORT 00435420
	jump to blockU if eax != 3
	
	0043541B  |.  BB 01000000   MOV EBX,1
	ebx = 1
	
	:blockU
	00435420  |>  8B7E 34       MOV EDI,DWORD PTR DS:[ESI+34]
	edi = AiTown.gas_buildings[1]
	
	00435423  |.  85FF          TEST EDI,EDI
	compare AiTown.gas_buildings[1] to 0
	
	00435425  |.  74 16         JE SHORT 0043543D
	jump to blockV if AiTown.gas_buildings[1] = 0
	
	00435427  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	0043542B  |.  8BC7          MOV EAX,EDI
	eax = edi # Unit for Ai_GetGasResourceState
	
	0043542D  |.  E8 AED2FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435432  |.  83F8 03       CMP EAX,3
	compare eax to 3
	
	00435435  |.  75 06         JNZ SHORT 0043543D
	jump to blockV if eax != 3
	
	00435437  |.  43            INC EBX
	ebx += 1
	
	00435438  |.  83FB 02       CMP EBX,2
	compare ebx to 2
	
	0043543B  |.  74 1D         JE SHORT 0043545A
	jump to blockW if ebx = 2
	
	:blockV
	0043543D  |>  8B7E 38       MOV EDI,DWORD PTR DS:[ESI+38]
	edi = AiTown.gas_buildings[2]
	
	00435440  |.  85FF          TEST EDI,EDI
	compare AiTown.gas_buildings[2] to 0
	
	00435442  |.  74 20         JE SHORT 00435464
	jump to blockX if AiTown.gas_buildings[2] = 0
	
	00435444  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	00435448  |.  8BC7          MOV EAX,EDI
	eax = edi # Unit for Ai_GetGasResourceState
	
	0043544A  |.  E8 91D2FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	0043544F  |.  83F8 03       CMP EAX,3
	compare eax to 3
	
	00435452  |.  75 10         JNZ SHORT 00435464
	jump to blockX if eax != 3
	
	00435454  |.  43            INC EBX
	ebx += 1
	
	00435455  |.  83FB 02       CMP EBX,2
	compare ebx to 2
	
	00435458  |.  75 0A         JNZ SHORT 00435464
	jump to blockX if ebx != 2
	
	:blockW
	0043545A  |>  8BC7          MOV EAX,EDI
	eax = edi # arg1 Unit
	
	0043545C  |.  85C0          TEST EAX,EAX
	compare eax to 0
	
	0043545E  |.  0F85 44010000 JNZ 004355A8
	jump to blockQ if eax != 0
	
	:blockX
	00435464  |>  8B46 30       MOV EAX,DWORD PTR DS:[ESI+30]
	eax = AiTown.gas_buildings[0] # Unit for Ai_GetGasResourceState
	
	00435467  |.  33DB          XOR EBX,EBX
	ebx = 0
	
	00435469  |.  85C0          TEST EAX,EAX
	compare AiTown.gas_buildings[0] to 0
	
	0043546B  |.  74 13         JE SHORT 00435480
	jump to blockY if AiTown.gas_buildings[0] = 0
	
	0043546D  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	00435471  |.  E8 6AD2FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435476  |.  83F8 03       CMP EAX,3
	compare eax to 3
	
	00435479  |.  75 05         JNZ SHORT 00435480
	jump to blockY if eax != 3
	
	0043547B  |.  BB 01000000   MOV EBX,1
	ebx = 1
	
	:blockY
	00435480  |>  8B7E 34       MOV EDI,DWORD PTR DS:[ESI+34]
	edi = AiTown.gas_buildings[1]
	
	00435483  |.  85FF          TEST EDI,EDI
	compare edi to 0
	
	00435485  |.  74 15         JE SHORT 0043549C
	jump to blockZ if AiTown.gas_buildings[1] = 0
	
	00435487  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	0043548B  |.  8BC7          MOV EAX,EDI
	eax = AiTown.gas_buildings[1] # Unit for Ai_GetGasResourceState
	
	0043548D  |.  E8 4ED2FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435492  |.  83F8 03       CMP EAX,3
	compare eax to 3
	
	00435495  |.  75 05         JNZ SHORT 0043549C
	jump to blockZ if eax != 3
	
	00435497  |.  43            INC EBX
	ebx += 1
	
	00435498  |.  3BD8          CMP EBX,EAX
	compare ebx to eax
	
	0043549A  |.  74 1C         JE SHORT 004354B8
	jump to block1 if ebx = eax
	
	:blockZ
	0043549C  |>  8B7E 38       MOV EDI,DWORD PTR DS:[ESI+38]
	edi = AiTown.gas_buildings[2]
	
	0043549F  |.  85FF          TEST EDI,EDI
	compare AiTown.gas_buildings[2] to 0
	
	004354A1  |.  74 1F         JE SHORT 004354C2
	jump to block2 if AiTown.gas_buildings[2] = 0
	
	004354A3  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	004354A7  |.  8BC7          MOV EAX,EDI
	eax = AiTown.gas_buildings[2] # Unit for Ai_GetGasResourceState
	
	004354A9  |.  E8 32D2FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	004354AE  |.  83F8 03       CMP EAX,3
	compare eax to 3
	
	004354B1  |.  75 0F         JNZ SHORT 004354C2
	jump to block2 if eax != 3
	
	004354B3  |.  43            INC EBX
	ebx += 1
	
	004354B4  |.  3BD8          CMP EBX,EAX
	compare ebx to eax
	
	004354B6  |.  75 0A         JNZ SHORT 004354C2
	jump to block2 if ebx != eax
	
	:block1
	004354B8  |>  8BC7          MOV EAX,EDI
	eax = edi
	
	004354BA  |.  85C0          TEST EAX,EAX
	compare eax to 0
	
	004354BC  |.  0F85 E6000000 JNZ 004355A8
	jump to blockQ if eax != 0
	
	:block2
	004354C2  |>  6A 01         PUSH 1
	push 1 # arg2 index
	
	004354C4  |.  6A 02         PUSH 2
	push 2 # arg1 gas_state
	
	004354C6  |.  E8 C5D6FFFF   CALL 00432B90
	call Ai_GetTownGas # uses esi for AiTown *town
	
	004354CB  |.  85C0          TEST EAX,EAX
	compare eax to 0
	
	004354CD  |.  0F85 D5000000 JNZ 004355A8
	jump to blockQ if eax != 0
	
	004354D3  |.  8B5E 30       MOV EBX,DWORD PTR DS:[ESI+30]
	ebx = AiTown.gas_buildings[0]
	
	004354D6  |.  85DB          TEST EBX,EBX
	compare AiTown.gas_buildings[0] to 0
	
	004354D8  |.  8945 FC       MOV DWORD PTR SS:[EBP-4],EAX
	var4 = eax
	
	004354DB  |.  74 17         JE SHORT 004354F4
	jump to block3 if AiTown.gas_buildings[0] = 0
	
	004354DD  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	004354E1  |.  8BC3          MOV EAX,EBX
	eax = AiTown.gas_buildings[0] # Unit for Ai_GetGasResourceState
	
	004354E3  |.  E8 F8D1FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	004354E8  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	004354EB  |.  75 07         JNZ SHORT 004354F4
	jump to block3 if eax != 2
	
	004354ED  |.  C745 FC 01000>MOV DWORD PTR SS:[EBP-4],1
	var4 = 1
	
	:block3
	004354F4  |>  8B7E 34       MOV EDI,DWORD PTR DS:[ESI+34]
	edi = AiTown.gas_buildings[1]
	
	004354F7  |.  85FF          TEST EDI,EDI
	compare AiTown.gas_buildings[1] to 0
	
	004354F9  |.  74 1C         JE SHORT 00435517
	jump to block4 if AiTown.gas_buildings[1] = 0
	
	004354FB  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	004354FF  |.  8BC7          MOV EAX,EDI
	eax = AiTown.gas_buildings[1] # Unit for Ai_GetGasResourceState
	
	00435501  |.  E8 DAD1FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435506  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	00435509  |.  75 0C         JNZ SHORT 00435517
	jump to block4 if eax != 2
	
	0043550B  |.  8B45 FC       MOV EAX,DWORD PTR SS:[EBP-4]
	eax = var4
	
	0043550E  |.  40            INC EAX
	eax += 1
	
	0043550F  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	00435512  |.  8945 FC       MOV DWORD PTR SS:[EBP-4],EAX
	var4 = eax
	
	00435515  |.  74 20         JE SHORT 00435537
	jump to block5 if eax = 2
	
	:block4
	00435517  |>  8B7E 38       MOV EDI,DWORD PTR DS:[ESI+38]
	edi = AiTown.gas_buildings[2]
	
	0043551A  |.  85FF          TEST EDI,EDI
	compare AiTown.gas_buildings[2] to 0
	
	0043551C  |.  74 1F         JE SHORT 0043553D
	jump to block6 if AiTown.gas_buildings[2] = 0
	
	0043551E  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	00435522  |.  8BC7          MOV EAX,EDI
	eax = edi # Unit for Ai_GetGasResourceState
	
	00435524  |.  E8 B7D1FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435529  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	0043552C  |.  75 0F         JNZ SHORT 0043553D
	jump to block6 if eax != 2
	
	0043552E  |.  8B45 FC       MOV EAX,DWORD PTR SS:[EBP-4]
	eax = var4
	
	00435531  |.  40            INC EAX
	eax += 1
	
	00435532  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	00435535  |.  75 06         JNZ SHORT 0043553D
	jump to block6 if eax != 2
	
	:block5
	00435537  |>  8BC7          MOV EAX,EDI
	eax = edi
	
	00435539  |.  85C0          TEST EAX,EAX
	compare eax to 0
	
	0043553B  |.  75 6B         JNZ SHORT 004355A8
	jump to blockQ if eax != 0
	
	:block6
	0043553D  |>  85DB          TEST EBX,EBX
	compare ebx to 0
	
	0043553F  |.  C745 FC 00000>MOV DWORD PTR SS:[EBP-4],0
	var4 = 0
	
	00435546  |.  74 17         JE SHORT 0043555F
	jump to block7 if ebx = 0
	
	00435548  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	0043554C  |.  8BC3          MOV EAX,EBX
	eax = ebx # Unit for Ai_GetGasResourceState
	
	0043554E  |.  E8 8DD1FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435553  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	00435556  |.  75 07         JNZ SHORT 0043555F
	jump to block7 if eax != 2
	
	00435558  |.  C745 FC 01000>MOV DWORD PTR SS:[EBP-4],1
	var4 = 1
	
	:block7
	0043555F  |>  8B7E 34       MOV EDI,DWORD PTR DS:[ESI+34]
	edi = AiTown.gas_buildings[1]
	
	00435562  |.  85FF          TEST EDI,EDI
	compare AiTown.gas_buildings[1] to 0
	
	00435564  |.  74 1C         JE SHORT 00435582
	jump to block8 if AiTown.gas_buildings[1] = 0
	
	00435566  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	0043556A  |.  8BC7          MOV EAX,EDI
	eax = edi # Unit for Ai_GetGasResourceState
	
	0043556C  |.  E8 6FD1FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435571  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	00435574  |.  75 0C         JNZ SHORT 00435582
	jump to block8 if eax != 2
	
	00435576  |.  8B45 FC       MOV EAX,DWORD PTR SS:[EBP-4]
	eax = var4
	
	00435579  |.  40            INC EAX
	eax += 1
	
	0043557A  |.  83F8 03       CMP EAX,3
	compare eax to 3
	
	0043557D  |.  8945 FC       MOV DWORD PTR SS:[EBP-4],EAX
	var4 = eax
	
	00435580  |.  74 20         JE SHORT 004355A2
	jump to block9 if eax = 3
	
	:block8
	00435582  |>  8B7E 38       MOV EDI,DWORD PTR DS:[ESI+38]
	edi = AiTown.gas_buildings[2]
	
	00435585  |.  85FF          TEST EDI,EDI
	compare AiTown.gas_buildings[2] to 0
	
	00435587  |.  74 1D         JE SHORT 004355A6
	jump to block10 if AiTown.gas_buildings[2] = 0
	
	00435589  |.  0FB656 18     MOVZX EDX,BYTE PTR DS:[ESI+18]
	edx = AiTown.player # player for Ai_GetGasResourceState
	
	0043558D  |.  8BC7          MOV EAX,EDI
	eax = edi # Unit for Ai_GetGasResourceState
	
	0043558F  |.  E8 4CD1FFFF   CALL 004326E0
	call Ai_GetGasResourceState
	
	00435594  |.  83F8 02       CMP EAX,2
	compare eax to 2
	
	00435597  |.  75 0D         JNZ SHORT 004355A6
	jump to block10 if eax != 2
	
	00435599  |.  8B4D FC       MOV ECX,DWORD PTR SS:[EBP-4]
	ecx = var4
	
	0043559C  |.  41            INC ECX
	ecx += 1
	
	0043559D  |.  83F9 03       CMP ECX,3
	compare ecx to 3
	
	004355A0  |.  75 04         JNZ SHORT 004355A6
	jump to block10 if ecx != 3
	
	:block9
	004355A2  |>  8BC7          MOV EAX,EDI
	eax = edi
	
	004355A4  |.  EB 02         JMP SHORT 004355A8
	jump to blockQ
	
	:block10
	004355A6  |>  33C0          XOR EAX,EAX
	eax = 0
	
	:blockQ
	004355A8  |>  8B56 2C       MOV EDX,DWORD PTR DS:[ESI+2C]
	edx = Unit.mineral
	
	004355AB  |.  8B4D 08       MOV ECX,DWORD PTR SS:[EBP+8]
	ecx =  # Unit *worker for Ai_Harvest
	
	004355AE  |.  52            PUSH EDX
	push edx # arg1 Unit *other_resource for Ai_Harvest
	
	004355AF  |.  E8 DCE5FFFF   CALL 00433B90
	call Ai_Harvest # uses eax to Unit *resource
	
	:blockD
	004355B4  |>  5E            POP ESI
	retrieve esi

	:RETURN
	004355B5  |>  5F            POP EDI
	004355B6  |.  5B            POP EBX
	004355B7  |.  8BE5          MOV ESP,EBP
	004355B9  |.  5D            POP EBP
	004355BA  \.  C2 0400       RETN 4
	return 
*/	


/*
//for pointer check
	}
	else {
		return;
	}*/
}



