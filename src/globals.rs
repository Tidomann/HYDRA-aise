use std::ffi::CString;
use std::ptr::null_mut;
use std::slice;

use bincode;
use bw_dat::{UnitId, UpgradeId};
use samase;
use ai::GuardState;
use aiscript::{
    self, AiMode, AttackTimeoutState, MaxWorkers, PlayerMatch, Town, TownId, UnitMatch, WriteModifier,
};
use data_extender::{self, AttackExtender, FlingyExtender};
use block_alloc::BlockAllocSet;
use bw;
use game::Game;
use idle_orders::IdleOrders;
use idle_tactics::{IdleTactics, BuildQueueFlags, BuildQueueFlagStatus};
//use idle_orders::Rate;
use fxhash::FxHashMap;
use recurse_checked_mutex::{Mutex, MutexGuard};
use rng::Rng;
//use rstar::RTree;
//use rstar::{RTreeObject, AABB};
use swap_retain::SwapRetain;
use toolbox;
use unit::{self, HashableUnit, Unit, SerializableSprite};
use config::{self};
use unit_search::UnitSearch;

lazy_static! {
    static ref GLOBALS: Mutex<Globals> = Mutex::new(Globals::new());
    static ref SAVE_STATE: Mutex<Option<SaveState>> = Default::default();
}

pub struct SaveState {
    pub first_ai_script: SendPtr<bw::AiScript>,
}

pub struct SendPtr<T>(pub *mut T);
unsafe impl<T> Send for SendPtr<T> {}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct KCPos {
    player1: u8,
    player2: u8,
    unit: u16,
}

impl KCPos {
    pub fn new(player1: u8, player2: u8, unit: u16) -> KCPos {
        KCPos {
            player1,
            player2,
            unit,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct KillCount {
    pos: KCPos,
    value: u32,
}

impl KillCount {
    pub fn new(pos: KCPos, value: u32) -> KillCount {
        KillCount {
            pos,
            value,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct RevealState {
    pub pos: bw::Rect,
    pub time: u16,
    pub reveal_type: RevealType,
    pub players: PlayerMatch,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub enum RevealType {
    RevealFog,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BaseLayout {
    pub pos: bw::Rect,
    pub player: u8,
    pub unit_id: UnitId,
    pub amount: u8,
    pub town_id: u8,
    pub town: Town,
    pub priority: u8,
	pub unique_id: String,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct BaseLayouts {
    pub layouts: Vec<BaseLayout>,
}

impl BaseLayouts {
    pub fn try_add(&mut self, value: BaseLayout) {
        if !self.layouts.iter().any(|i| i == &value) {
            let insert_pos = self
                .layouts
                .binary_search_by(|a| value.priority.cmp(&a.priority))
                .unwrap_or_else(|e| e);
            self.layouts.insert(insert_pos, value)
        }
    }

    pub fn try_remove(&mut self, value: &BaseLayout) {
        if let Some(pos) = self.layouts.iter().position(|i| i == value) {
            self.layouts.remove(pos);
        }
    }
}


#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
pub struct BuildMax {
	pub town: Option<Town>,
	pub quantity: u16,
	pub unit_id: UnitId,
}


#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct UnitQueue {
    pub player: u8,
    pub current_quantity: u8,
    pub unit_id: UnitId,
    pub factory_id: UnitMatch,
    pub town: Option<Town>,
    pub pos: bw::Rect,
    pub priority: u8,
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
pub struct UpgradeQueue {
	pub player: u8,
	pub level: u8,
	pub upgrade_id: u16,
    pub town: Option<Town>,
    pub priority: u8,	
}	



#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub enum BuildQueueStatus {
	Init,
	MoveToBuild,
	Constructing,
	SCV_ReconstructPick,
	SCV_ReconstructMove,
	End
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BuildQueue {
	pub player: u8,
//	pub amount: u32,
	pub flags: BuildQueueFlags,
	pub unit_id: UnitId,
    pub town: Option<Town>,
    pub priority: u8,
	pub delay: u32,
	//
	pub assigned_worker: Option<Unit>,
	pub assigned_structure: Option<Unit>,
	pub status: BuildQueueStatus,
	pub last_frame_status: BuildQueueFlagStatus,
}	

impl BuildQueue {
	pub unsafe fn new(script: *mut bw::AiScript, id: UnitId, flags: BuildQueueFlags, priority: u8)->BuildQueue {
		BuildQueue {
			player: (*script).player as u8,
			flags: flags,
			unit_id: id,
			town: Town::from_ptr((*script).town),
			priority: priority,
			delay: 24,
			assigned_worker: None,
			assigned_structure: None,
			status: BuildQueueStatus::Init,
			last_frame_status: Default::default(),
		}	
	}
}	
	
impl UnitQueue {
    pub unsafe fn can_train(&mut self, unit: Unit) -> bool {
        use bw_dat::order::{DIE, ARCHON_WARP, DARK_ARCHON_MELD, TRAIN, UNIT_MORPH, BUILDING_MORPH, ZERG_BUILD_SELF};
		if !unit.is_completed(){
			return false;
		}
		if unit.order()==DIE || unit.sprite().is_none() {
			return false;
		}
		if unit.target().is_some(){
			return false;
		}
		
		if unit.is_building_addon(){
			
			return false;
		}
		if unit.is_disabled(){
			return false;
		}
		if (*unit.0).ai==null_mut(){
			return false;
		}
		
        if !self.factory_id.matches(&unit) || unit.player() != self.player {
			
            return false;
        }
        let is_train_order = match unit.order() {
            UNIT_MORPH | BUILDING_MORPH | ARCHON_WARP | DARK_ARCHON_MELD | ZERG_BUILD_SELF => true,
            _ => unit.secondary_order() == TRAIN,
        };
        if is_train_order {
            return false;
        }

        if unit.is_air() && unit.id().is_building() {
            return false;
        }
        if !unit.id().is_building() /*&& unit.id() != LARVA*/ {
            return true;
        }

        match &mut self.town {
            None => return true,
            Some(s) => return s.has_building(unit),
        }
		
    }
}

impl UpgradeQueue {
    pub unsafe fn can_upgrade(&mut self, unit: Unit, current_level: u8, _upgrades: &mut Upgrades) -> bool {
        use bw_dat::order::{DIE, UPGRADE, TRAIN, BUILDING_MORPH, RESEARCH_TECH, LIFTOFF, BUILDING_LAND};
	//	bw_print!("id: {}",self.upgrade_id);
        if unit.player() != self.player {				
            return false;
        }
		match unit.order() {
			DIE|UPGRADE|TRAIN|RESEARCH_TECH|BUILDING_MORPH|LIFTOFF|BUILDING_LAND=>{
				return false;
			},
			_=>{},
		}
		if unit.sprite().is_none(){
			return false;
		}
		if unit.is_air(){
			return false;
		}
		if unit.is_building_addon(){
			return false;
		}
		if unit.is_disabled(){
			return false;
		}
		//later - must be replaced by new aiscript func
		
		//temp hardcode fix
		let config = config::config();
		if !config.overlord {
			match unit.id().0 {
				131|132|133|143|144|146|156|160|158|161|167|183|184|134|162|172=>{
						
					return false;
				},
				_=>{},
			}
		}
		
		//debug!("Aise Calls CheckUpgradeReq: player {}, upgrade id {}, (check for unit id {})",self.player,self.upgrade_id,unit.id().0);
		if bw::check_upgrade_req(unit.0, self.upgrade_id as u32, self.player as u32)!=1 {
			/*if self.upgrade_id == 69 && unit.id().0==131 {
				bw_print!("bad req {}",*bw::last_error);
			}*/
			return false;
		}
		
		/*
		if aiscript::check_upgrade_req_new(unit.0, self.upgrade_id as u32, self.player as u32, upgrades)!=1 {
			return false;
		}*/
		/*
		let reqs = match bw::upgrade_dat_requirements_simple(self.upgrade_id) {
			Some(s) => s,
			None => return false,
        };*/
		//let game = Game::get();
		/*
		let n = ai_spending::can_satisfy_dat_request(game, self.player, reqs, current_level).map_err(RequestSatisfyError::DatReq).is_ok();
		if !n {
			return false;
		}*/
        match &mut self.town {
            None => return true,
            Some(s) => return s.has_building(unit),
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Queues {
    pub queue: Vec<UnitQueue>,
	pub upgrade_queue: Vec<UpgradeQueue>,
	pub build_queue: Vec<BuildQueue>,
}
/*
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct TrainSequence {
	pub unit_id: UnitId,
	pub count: u32,
	pub player: u8,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct TrainState {
	pub state: Vec<TrainSequence>,
}*/

impl Queues {
	pub fn adjust_removed_unit(&mut self, unit: Unit){
		for u in &mut self.build_queue {
			if let Some(unit) = u.assigned_worker {
				u.status = BuildQueueStatus::Init;
				u.assigned_worker = None;
				u.assigned_structure = None;
			}
			if let Some(unit) = u.assigned_structure {
				u.status = BuildQueueStatus::Init;
				u.assigned_worker = None;
				u.assigned_structure = None;
			}
		}
	}
	pub fn clear_units(&mut self, ids: UnitMatch, players: PlayerMatch){
		self.queue.swap_retain(|x| !(players.matches(x.player) && ids.matches_unit_id(&x.unit_id)));
	}
	pub fn clear_upgrades(&mut self, upgrade_id: u16, players: PlayerMatch){
		self.upgrade_queue.swap_retain(|x| !(players.matches(x.player) && x.upgrade_id==upgrade_id));
	}	
    pub fn add(&mut self, value: UnitQueue) {
        if !self.queue.iter().any(|i| i == &value) {
            let insert_pos = self
                .queue
                .binary_search_by(|a| value.priority.cmp(&a.priority))
                .unwrap_or_else(|e| e);
            self.queue.insert(insert_pos, value)
        }
    }
	pub fn add_upgrade(&mut self, value: UpgradeQueue){
        if !self.upgrade_queue.iter().any(|i| i == &value) {
            let insert_pos = self
                .upgrade_queue
                .binary_search_by(|a| value.priority.cmp(&a.priority))
                .unwrap_or_else(|e| e);
            self.upgrade_queue.insert(insert_pos, value)
        }	
	}
}
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct BuildMaxSet {
    pub buildmax: Vec<BuildMax>,
}
impl BuildMaxSet {
    pub fn add(&mut self, value: BuildMax) {
		
        if !self.buildmax.iter().any(|i| i == &value) {
			/*
            let insert_pos = self
                .buildmax
				.binary_search_by(|a| value.unit_id.0.cmp(&a.unit_id.0))
                .unwrap_or_else(|e| e);0
            self.buildmax.insert(insert_pos, value)*/
			self.buildmax.push(value);
        }
    }
}


#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
pub struct LiftLandState {
    pub unit: Unit,
    pub stage: LiftLandStage,
    pub is_returning: bool,
    pub target: bw::Point,
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
pub struct LiftLandBuilding {
    pub player: u8,
    pub unit_id: UnitId,
    pub src: bw::Rect,
    pub tgt: bw::Rect,
    pub town_src: Town,
    pub town_tgt: Town,
    pub return_hp_percent: u8,
    pub state: Option<LiftLandState>,
}

impl LiftLandBuilding {
    pub fn stage(&mut self) -> LiftLandStage {
        match self.state {
            Some(s) => s.stage,
            None => LiftLandStage::LiftOff_Start,
        }
    }

    pub fn init_state(&mut self, unit: Unit) {
        self.state = Some(LiftLandState {
            unit: unit,
            stage: LiftLandStage::LiftOff_Start,
            is_returning: false,
            target: bw::Point {
                x: 0,
                y: 0,
            },
        });
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
#[allow(non_camel_case_types)]
pub enum LiftLandStage {
    LiftOff_Start,
    LiftOff_End,
    Fly,
    FindLocation,
    Land,
    End,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct LiftLand {
    pub structures: Vec<LiftLandBuilding>,
}

impl LiftLand {
    pub fn add(&mut self, value: LiftLandBuilding, amount: u8) {
        for _ in 0..amount {
            self.structures.push(value);
        }
    }

    pub fn unit_removed(&mut self, unit: Unit) {
        for liftland in &mut self.structures {
            let state = liftland.state;
            if let Some(state) = state {
                if state.unit == unit {
                    liftland.state = None;
                }
            }
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct KillsTable {
    pub kills: Vec<KillCount>,
}

impl KillsTable {
    pub fn try_add(&mut self, kt_pos: KCPos, amount: u32) {
        match self.kills.iter_mut().position(|i| i.pos == kt_pos) {
            Some(pos) => {
                let i = &mut self.kills[pos];
                i.value = i.value.saturating_add(amount);
            }
            None => {
                self.kills.push(KillCount::new(kt_pos, amount));
            }
        }
    }

    pub fn try_set(&mut self, kt_pos: KCPos, amount: u32) {
        match self.kills.iter_mut().position(|i| i.pos == kt_pos) {
            Some(pos) => {
                let i = &mut self.kills[pos];
                i.value = amount;
            }
            None => {
                self.kills.push(KillCount::new(kt_pos, amount));
            }
        }
    }

    pub fn count_kills(&mut self, p1: u8, p2: u8, uid: u16) -> u32 {
        let mut count: u32 = 0;
        for i in &self.kills {
            if i.pos.player1 == p1 && i.pos.player2 == p2 {
                if uid == i.pos.unit {
                    count += i.value;
                    break;
                }
            }
        }
        count
    }

    pub fn try_sub(&mut self, kt_pos: KCPos, amount: u32) {
        match self.kills.iter_mut().position(|i| i.pos == kt_pos) {
            Some(pos) => {
                if amount < self.kills[pos].value {
                    self.kills[pos].value -= amount;
                } else {
                    self.kills.swap_remove(pos);
                }
            }
            None => (),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BunkerDecl {
    pub pos: bw::Rect,
    pub unit_id: UnitMatch,
    pub bunker_id: UnitMatch,
    pub quantity: u8,
    pub player: u8,
    pub bunker_quantity: u8,
    pub priority: u8,
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct BunkerState {
    single_bunker_states: Vec<SingleBunkerState>,
}

impl BunkerState {
    pub fn add_targeter(&mut self, targeter: Unit, bunker: Unit) {
        match self
            .single_bunker_states
            .iter_mut()
            .position(|x| x.bunker == bunker)
        {
            Some(s) => self.single_bunker_states[s].associated_units.push(targeter),
            None => {
                self.single_bunker_states.push(SingleBunkerState {
                    associated_units: vec![targeter],
                    bunker,
                });
            }
        }
    }

    fn unit_removed(&mut self, unit: Unit) {
        self.single_bunker_states
            .swap_retain(|x| x.bunker.0 != unit.0);
        for link in &mut self.single_bunker_states {
            link.associated_units.swap_retain(|x| x.0 != unit.0);
        }
    }

    pub fn count_associated_units(&self, bunker: Unit) -> Option<u8> {
        self.single_bunker_states
            .iter()
            .find(|link| link.bunker == bunker)
            .map(|link| link.associated_units.len() as u8)
    }

    pub fn in_list(&self, unit: Unit) -> bool {
        self.single_bunker_states
            .iter()
            .any(|state| state.associated_units.iter().any(|&x| x == unit))
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct SingleBunkerState {
    pub associated_units: Vec<Unit>,
    pub bunker: Unit,
}


#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct RenameStatus {
    pub area: bw::Rect,
    pub unit_id: UnitId,
    pub name: CString,
    pub players: PlayerMatch,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct RenameUnitState {
    pub states: Vec<RenameStatus>,
}

impl RenameUnitState {
    pub fn try_add(&mut self, value: RenameStatus) {
        fn area(x: &bw::Rect) -> u32 {
            (x.right.saturating_sub(x.left) as u32) * (x.bottom.saturating_sub(x.top) as u32)
        }

        if !self.states.iter().any(|i| i == &value) {
            let area_size = area(&value.area);
            let insert_pos = self
                .states
                .binary_search_by_key(&area_size, |x| area(&x.area))
                .unwrap_or_else(|e| e);

            self.states.insert(insert_pos, value)
        }
    }

    pub fn try_remove(&mut self, value: &RenameStatus) {
        if let Some(pos) = self.states.iter().position(|i| i == value) {
            self.states.remove(pos);
        }
    }
}

#[derive(Debug, Default, Serialize, Deserialize, Clone, PartialEq)]
pub struct BankKey {
    pub label: String,
    pub category: String,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct BankValue {
    pub key: BankKey,
    pub value: u32,
}

impl BankValue {
    pub fn new(key: BankKey, value: u32) -> BankValue {
        BankValue {
            key,
            value,
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Bank {
    pub bank_data: Vec<BankValue>,
}

impl Bank {
    pub fn reset(&mut self) {
        self.bank_data.clear();
    }

    pub fn get(&self, key: &BankKey) -> u32 {
        match self.bank_data.iter().position(|i| i.key == *key) {
            Some(pos) => self.bank_data[pos].value as u32,
            None => 0,
        }
    }

    pub fn update<F: FnOnce(u32) -> u32>(&mut self, key: BankKey, update_fn: F) {
        match self.bank_data.iter_mut().position(|i| i.key == key) {
            Some(pos) => {
                let new = update_fn(self.bank_data[pos].value);
                if new == 0 {
                    self.bank_data.swap_remove(pos);
                } else {
                    self.bank_data[pos].value = new;
                }
            }
            None => {
                let new = update_fn(0);
                if new != 0 {
                    self.bank_data.push(BankValue::new(key, new));
                }
            }
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct BunkerCondition {
    pub bunker_states: Vec<(BunkerDecl, BunkerState)>,
}

impl BunkerCondition {
    pub fn add(&mut self, decl: BunkerDecl) {
        let insert_pos = self
            .bunker_states
            .binary_search_by(|a| decl.priority.cmp(&a.0.priority))
            .unwrap_or_else(|e| e);
        self.bunker_states
            .insert(insert_pos, (decl, BunkerState::default()));
    }

    pub fn in_list(&self, unit: Unit) -> bool {
        self.bunker_states
            .iter()
            .any(|(_, state)| state.in_list(unit))
    }

    pub fn unit_removed(&mut self, unit: Unit) {
        for (_, state) in &mut self.bunker_states {
            state.unit_removed(unit);
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct ReplaceValue {
    pub first: UnitId,
    pub second: UnitId,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct UnitReplace {
    pub unit_data: Vec<ReplaceValue>,
}


#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct Waygating {
	pub nydus_links: FxHashMap<u16,Vec<Unit>>,
}

impl Waygating {
	pub fn unit_removed(&mut self, unit: Unit){
		for (_k, set) in &mut self.nydus_links {
			set.swap_retain(|x| *x != unit);
			
		}
		
	}
	pub fn add_nydus(&mut self, unit: Unit, region_group: u16){
		let result = match self.nydus_links.entry(region_group) {
		   Entry::Vacant(entry) => entry.insert(Vec::new()),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		//bw_print!("Add nydus");
		result.push(unit);
	}
	pub unsafe fn find_nydus_with_shortest_path(&mut self, player: u8, source_position: bw::Point, target_position: bw::Point)->Option<Unit>{
		
		let pathing = bw::pathing();
		let region_source = bw::get_region(source_position).unwrap();
		let source_group = (*pathing).regions[region_source as usize].group;
		let region_target = bw::get_region(target_position).unwrap();
		let target_group = (*pathing).regions[region_target as usize].group;	
		let result = match self.nydus_links.entry(source_group){
		   Entry::Vacant(_e) => {return None;},//should happen if there are no nyduses on the map in same r
		   Entry::Occupied(entry) => entry.into_mut(),			
		};
		let orig_distance = bw::distance(source_position, target_position);
		for n in &mut result.iter() {
//			bw_print!("Loop nyduses");
			if let Some(nydus_exit) = n.nydus_exit(){
				if n.player()==player{
					let exit_region = bw::get_region(nydus_exit.position()).unwrap();
					let exit_region_group = (*pathing).regions[exit_region as usize].group;
					let new_distance = bw::distance(nydus_exit.position(),target_position);
					if exit_region_group == target_group && (new_distance as f64)*1.5<orig_distance as f64 {
						return Some(*n);
					}				
				}

			}
		}
		return None;		
	}
	pub unsafe fn find_good_nydus(&mut self, unit: Unit, x: u16, y: u16)->Option<Unit>{
		let pathing = bw::pathing();
		let region_source = bw::get_region(unit.position()).unwrap();
		let source_group = (*pathing).regions[region_source as usize].group;
		let region_target = bw::get_region(bw::Point{x: x as i16, y: y as i16}).unwrap();
		let target_group = (*pathing).regions[region_target as usize].group;
		let result = match self.nydus_links.entry(source_group) {
		   Entry::Vacant(_e) => {return None;},
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		for n in &mut result.iter() {
//			bw_print!("Loop nyduses");
			if let Some(nydus_exit) = n.nydus_exit(){
				if n.player()==unit.player(){
					let exit_region = bw::get_region(nydus_exit.position()).unwrap();
					let exit_region_group = (*pathing).regions[exit_region as usize].group;
					//bw_print!("loop some nydus");
					if exit_region_group == target_group {
//						bw_print!("Nydus found");
						return Some(*n);
					}				
				}

			}
		}
		return None;
	}
    fn new() -> Waygating {
        Waygating {
			nydus_links: Default::default(),
			
        }
    }	
}

impl UnitReplace {
    pub fn replace_check(&self, unit_id: UnitId) -> UnitId {
        for u in &self.unit_data {
            if u.first == unit_id {
                return u.second;
            }
        }
        return unit_id;
    }

    pub fn add(&mut self, first: UnitId, second: UnitId) {
        let replace_value = ReplaceValue {
            first: first,
            second: second,
        };
        self.unit_data.push(replace_value);
    }
}

//hydra

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct CustomUpgradeId {
	pub mineral_cost_base: u16,
	pub mineral_cost_factor: u16,
	pub gas_cost_base: u16,
	pub gas_cost_factor: u16,
	pub time_cost_base: u16,
	pub time_cost_factor: u16,
	pub restriction_flags: u16,
	pub icon_frame: u16,
	pub label: u16,
	pub race: u8,
	pub max_repeats: u8,
	pub broodwar_flag: u8,
	//extra info
	pub used: bool,
}


#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct UnitStructTimer {
	pub unit_link: Unit,
	pub timer: u16,
}


#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
#[allow(non_camel_case_types)]
pub enum Relic {
	NoRelic,

	Tempest,
	Penance,
	BlindJudge,
	//Emperor,
	//MindForge,
	//Boltmaker,
	Conviction,
	EndlessVigil,
	//ImmortalFortress,
	Intervention,
	LightOfSaalok,
	CrestOfAdun,
	//BlessedSummons,
	//Heartseeker,
	ShadeSigil,
	BindingCrucible,
	VirtuousAttenuator,
	PredatorsCloak,
	AstralAnchor,
}

/*
#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
#[allow(non_camel_case_types)]
pub enum Mutation {
	///
}
*/

pub fn get_relic_from_id(relic_id: u16)->Relic{
	let relic = match relic_id {
		//weapons
		200 => Relic::Tempest,
		201 => Relic::Penance,
		202 => Relic::BlindJudge,
		206 => Relic::Conviction,
		//armor
		210 => Relic::EndlessVigil,
		212 => Relic::Intervention,
		//augments
		220 => Relic::LightOfSaalok,
		221 => Relic::CrestOfAdun,
		224 => Relic::ShadeSigil,
		225 => Relic::BindingCrucible,
		226 => Relic::VirtuousAttenuator,
		227 => Relic::PredatorsCloak,
		228 => Relic::AstralAnchor,
		_=> Relic::NoRelic,
	};
	relic
}

pub fn relic_to_integer(relic: Relic)->u32{
	let id = match relic {
		Relic::Tempest => 200,
		Relic::Penance => 201,
		Relic::BlindJudge => 202,
		Relic::Conviction => 206,
		//armor
		Relic::EndlessVigil => 210,
		Relic::Intervention => 212,
		//augments
		Relic::LightOfSaalok => 220,
		Relic::CrestOfAdun => 221,
		Relic::ShadeSigil => 224,
		Relic::BindingCrucible => 225,
		Relic::VirtuousAttenuator => 226,
		Relic::PredatorsCloak => 227,
		Relic::AstralAnchor => 228,
		_=> 0,		
	};
	id
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct ProtossHero {
	pub tempest_links: Vec<UnitStructTimer>,
	pub penance_links: Vec<UnitStructTimer>,
	pub boltmaker_links: Vec<UnitStructTimer>,
	pub conviction_links: Vec<UnitStructTimer>,
	pub endless_vigil_timers: Vec<u16>,
	pub intervention_activated: bool,
	pub blessed_summons_channel: u16,
	pub shade_sigil_cloak: u16,
}
/*
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct ChasmalColony {
	pub adaptive_barrier: bool,
	pub adrenal_selection: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct DuranMirror {
	pub ability: Ability,
	pub timer: u16,
}
*/

#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
#[allow(non_snake_case)]
pub enum CommTowerMenu {
	General,
	Orders,
	Proposal,
	Listening,
	PendingMessages,
	MessageDetails,
	PickTargetForListening,
	PickTargetForOrders,
	PickTargetForProposal,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct CommunicationTowerData
{
//	pub player_selected: u8,//255 - no selected
	pub id_option: u16,
	pub resource_setup: u32,
	pub current_menu: CommTowerMenu,
	pub suboption: CommTowerMenu,
}

impl Default for CommunicationTowerData {
    fn default() -> CommunicationTowerData {
        CommunicationTowerData {
//			player_selected: 255,
			current_menu: CommTowerMenu::General,
			suboption: CommTowerMenu::General,
			id_option: 1,
			resource_setup: 500,
        }
    }
}
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct SpriteStruct {
	pub meskalloid: bool,
	pub special_mineral: u32,
	pub basilisk_gimmick: u8,
	pub maelstrom_offset: u32,
	pub chunk_res_id: u32,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct UnitStruct {
    pub unit: Unit,//for persisting and old vector units
	//todborne start 
	pub moderation: Vec<u16>,
	pub moderation_failsafe_timer: u8,
	pub can_overload: bool,
	pub deathwish: bool,
	pub steeled_spatula: bool,
	pub prepare_to_explode_todborne: bool,
	pub void_timer: u8,
	pub safe_void: bool,
	pub goodboy_timer: u8,
	pub frosted_timer: u8,
	pub death_by_dough: Option<u8>,
	//todborne end
	pub generic_switch: FxHashMap<u32,bool>,
	pub generic_value: FxHashMap<u32,i32>,
	pub generic_timer: FxHashMap<u32,u32>,
	pub generic_unit: FxHashMap<u32,Unit>,
	pub nydus_target_area: bw::Point,
	//amoncrap
	pub high_yield: bool,
	//overlord/kmc mod
	pub scrap: u8,
	pub comm_tower: Option<CommunicationTowerData>,//buttonset+listening info
	pub expelling_waste: Option<bool>,
	pub vermin: Option<u32>,
	pub meskalloid: (u8,u8),
	//overlord/kmc mod end
	//hydra start
	pub signify_slowdown: bool,
	pub last_orders: bool,
	pub prepare_to_explode: bool,
	pub malice_count: u32,
	
	pub hierophant_signify: Vec<UnitStructTimer>,
	pub legionnaire: Vec<UnitStructTimer>,
	
	pub parasite_timers: [u16; 8],
	pub madrigal_timers: Vec<u32>,
	pub blind_timer: u16,
	pub creeper: Option<Unit>,
	pub cyprian_safety_off: Option<bool>,//true - safety off, false - safety on
	
	//pub adrenaline_packs: Option<u16>,
	pub relics_equipped: Vec<Relic>,
	pub relics_stored: Vec<Relic>,
	pub warp_relay_link: Option<Unit>,
	pub warp_relay_feedback: Vec<Unit>,
	pub phase_link_unit: Option<Unit>,
	pub phase_link_feedback: Vec<Unit>,
	pub matrix_caster: Option<Unit>,
	pub aperture: bool,
	pub phase_link: bool,
	pub planetcracker_buildup: u16,
	pub planetcracker_timer: u16,
	//misc
	pub pickup_ai_type: bool,//false - default pickup ai, true - nydus pickup ai
	pub observance_link: Option<Unit>,
	pub sublime_shepherd_casters: Vec<(Unit,u32)>,
	
	//pub mutations: Vec<Mutation>,
	//pub medstims: Option<u16>, //transfer later here from unknown data
	//pub overcharge_slow: Option<u16>, //transfer later here
	//pub power_siphon_timer: Option<u16>,
	//pub power_transfer_timer: Option<u16>,
	//pub energy_new: u32,
	//pub raynor: Option<Raynor>,
	//pub kerrigan: Option<Kerrigan>,
	//pub edict: Option<u16>,
	//pub half_life: Option<bool>,
	//pub reflector_matrix: Option<bool>,
	//pub aspect_drain: Option<bool>,
	//pub lazarus: Option<u16>,
	//pub apostle_link: Option<UnitStructTimer>,
	//pub pennant_link: Option<Unit>,
	//pub protoss_hero: Option<ProtossHero>,
	//pub blind_judge_resurrection: Option<u16>,
	//pub struck_by_boltmaker: bool,
	pub mind_control_links: Vec<Unit>,
	pub mind_controlled: Option<u8>,
}

/*

*/
#[derive(Debug, Hash, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
#[allow(non_camel_case_types)]
pub enum HydraEffect {
	None = 0,
	BlindJudgeResurrection,
	ScreamerAcidPool,
	PoolHit,
	PlanetCracker,
	ApertureOn,
	ApertureOff,
	ActivatePlanetcracker,
	PhaseLinkOn,
	PhaseLinkOff,
	KhaydarinControlOn,
	KhaydarinControlOff,				
	IonCannonHit,
	AugurDeathEffect,
	PlanetcrackerBuildup,
	PlanetcrackerOff,
	PlanetcrackerBurst,	
	WarpFlash,
	MindControl,
	Observance,
	Impale,
	
	SafetyOff,
	SafetyOn,
	ActivateMedstims,
	DeactivateMedstims,
	
	ReverseThrustOn,
	ReverseThrustOff,
	SublimeShepherdOn,
	SublimeShepherdOff,
	TempoChangeOn,
	TempoChangeOff,
	DisruptionWebOn,
	DisruptionWebOff,
	
	UpdateSpeed,
	RemoveUnit,
	IssueOrder,
	AppendOrder,
	AppendOrderUnit,
	UpdateMineralAnimation,
	BeginUpgrade,
	CreateUnit,
	
	NextQueuedOrder,
	Stop,
	KillUnit,
	VerminPoolLoop,
	VerminPoolEffect,
	ExpelWasteOn,
	ExpelWasteOff,
	TurbomodeOn,
	TurbomodeOff,
	AltitudeUp,
	AltitudeDown,
	MissilesOn,
	MissilesOff,
	BasiliskOn,
	BasiliskOff,
	AggressiveMiningOn,
	AggressiveMiningOff,
	CustomStim,
	VerminPoolHit,
	//todborne
	SeverityOverload,
	FeelingTheBern,
	Activist_Respawn,
	
	
	SetSignifyVisualEffect,
	SetEnsnare,
	SetPlague,
	SetDefensiveMatrix,
	AdrenalineVisualEffect,
	
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct LongEffect {
	pub effect_id: HydraEffect,
	pub timer: u16,
	pub position: bw::Point,
	pub player: u8,
	pub energy: u16,
	pub unit_id: u16,
	pub misc: u16,
	pub unit: Unit,
	pub target: Unit,
}
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct SpriteEffect {
	pub timer: u16,
	pub global_timer: u16,
	pub respawn_timer: u16,
	pub sprite: SerializableSprite,
	pub effect_id: HydraEffect,
	pub source: Unit,
	pub position: bw::Point,
	pub origin: bw::Point,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct Escalation {
	pub escalation_id: u32,
	pub current_unit: UnitId,
}



#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct EscalationSystem {
	pub esc_list: Vec<Escalation>,
}
impl EscalationSystem {
	pub fn add_esc(&mut self, default_id: u16, esc_id: u32){
		let esc = Escalation { escalation_id: esc_id, 
							  current_unit: UnitId(default_id),};
		self.esc_list.push(esc);
	}
}


pub fn reverse_escalation(unit_id: u16)->u16{
	/*let result = match unit_id {
		32 => 92,//cyprian
		92 => 32,
		//
		34 => 15, //shaman
		15 => 34,
		//
		3 => 186,//paladin
		186 => 3,
		//
		124 => 185,//sentry turret
		185 => 124,
		//
		9 => 98,//azazel
		98 => 9,
		//
		1 => 187,//savant
		187 => 1,

		_=>228,//no unit
	};
	*/
	let config = config::config();
	for esc in config.escalations.esc_list.clone() {
		if unit_id == esc.from.0 {
			return esc.to.0;
		}
		if unit_id == esc.to.0 {
			return esc.from.0;
		}
	}
	228
}

pub unsafe fn code_insert(exe: &mut whack::ModulePatcher, address: usize, code: &[u8]){
    for (i, &item) in code.iter().enumerate() {
		exe.replace_val(address+i,item);
	}	
}
pub unsafe fn simple_wrapper(exe: &mut whack::ModulePatcher, code: &[u8], address: usize, nops: usize){
	let executable = exe.exec_alloc(code.len());
	if nops>0 {
		exe.nop(address+5, nops);
	}
	executable.copy_from_slice(&code);
	let diff = (executable.as_ptr() as u32).wrapping_sub(address as u32+5);
	exe.replace_val(address+1, diff);
	exe.replace_val(address, 0xe8 as u8);
}

pub unsafe fn wrapper_patch(exe: &mut whack::ModulePatcher, code: &[u8], address: usize, nop_address: usize, nops: usize){
	let executable = exe.exec_alloc(code.len());
	if nops>0 {
		exe.nop(nop_address, nops);
	}
	executable.copy_from_slice(&code);
	let diff = (executable.as_ptr() as u32).wrapping_sub(address as u32+5);
	exe.replace_val(address+1, diff);
	exe.replace_val(address, 0xe8 as u8);
}

#[derive(Debug, Copy, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub enum OverlordFaction {
	NoFaction = 0,
    KelMorianCombine,
	TalQiratGuild,
	AlphaSquadron,
	GammaSquadron,
	NovaSquadron,
	EpsilonSquadron,
	OmegaSquadron,
	ColonialMilitia,
	Umojans,
	Pirates,
	Miners,
	Scavengers,
	Mercenaries,	
	Cerberus,
	SteelCompany,
}

impl Default for OverlordFaction {
    fn default() -> OverlordFaction {
        OverlordFaction::NoFaction
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct OrderSetting {
	pub attack_wave_target: Option<bw::Point>,
	pub attack_wave_prepare: Option<bw::Point>,
//	probably add unit id settings/preferences later
}


#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
#[allow(non_camel_case_types)]
pub enum ActionId {
	SendResources,
	Ally,
	CeaseFire,
	Surrender,
	
	Help,
	ChangeComposition,
	CustomQuest,
}


#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Copy)]
pub enum ActionButton {
	SendMinerals,
	SendGas,
	Ally,
	CeaseFire,
	Surrender,
	AdjustResourcesSent,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Copy)]
pub enum ListeningButton {
	Start, 
	Stop,
}
#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub enum QuestId {
	ProtectBase,
	ProtectResarea,
	ProtectArtillery,
	Attack,
	ProvideSupport,
	ProvideAirCover,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct DiplomacyQuest {
	pub q_type: QuestId,
	pub target_region: bw::Rect,
	pub affiliated_unit_id: UnitId,
	pub affiliated_town: Town,
	pub abstract_count: u32,
	pub delay: u32,
	pub current_player: u8,
	pub target_player: u8,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct DiplomacyAction {
	pub id: ActionId,
	pub quest: Option<DiplomacyQuest>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct DiplomacyTimer {
	pub timer: u32,
	pub strength: u32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ListeningSystem {
	pub generic_timer: u32,
	pub generic_max_const: u32,
}

impl Default for ListeningSystem {
    fn default() -> ListeningSystem {
        ListeningSystem {
			generic_timer: 0,
			generic_max_const: 2880,
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct DiplomacySetting {
	pub disposition: [u32; 0xc],
	pub faction: OverlordFaction,
	pub name: String,
	pub registered_button_string: u16,
	pub alliance_status: [bool; 0x8],
	pub order_setting: OrderSetting,
	pub action_timers: Vec<(DiplomacyAction,DiplomacyTimer)>,
	pub listening: [bool; 0x8],
	pub listening_system: [ListeningSystem; 0x8],
}
impl DiplomacySetting {
//set_attack_preference(Some(attack_to_issued));
	pub fn set_attack_preference(&mut self, val: Option<bw::Point>){
		self.order_setting.attack_wave_target = val;
	}
}


#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Faction {
	pub name: CString,
	pub color_index: u8,
	pub icon_id: u16,
	pub string_id: u16,
	pub faction_id: OverlordFaction,//requires mod-specific hooks/settings
}
#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub enum VarType {
	Boolean,
	I32,
	U32,
	Jump,
	String,
	Unit,
}

impl Default for VarType {
    fn default() -> VarType {
        VarType::Boolean
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Variable {
	pub var_type: VarType,
	pub int32: Option<i32>,
	pub uint32: Option<u32>,
	pub jump: Option<u32>,
	pub boolean: Option<bool>,
	pub unit: Option<Unit>,
}
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct TownStateValue {
	pub persisting: bool,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct TownState {
	pub states: Vec<(Town,TownStateValue)>,
}

impl TownState {
	pub fn add_persist(&mut self, town: Town, persist: bool){
		for (t,state) in &mut self.states {
			if town.0==t.0 {
				state.persisting = persist;
				return;
			}
		}
		let state = TownStateValue {
			persisting: persist,
		};
		self.states.push((town,state));
	}
	
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct TriggerState {
	pub variables: Vec<(String,Variable,PlayerMatch)>,
}



#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
pub enum EventType {
	Standard,
	TakingWeaponDamage,
}

impl Default for EventType {
    fn default() -> EventType {
        EventType::Standard
    }
}


#[derive(Debug,Clone, Default, Serialize, Deserialize)]
pub struct WaitForElem {

	pub pos: u32,
	
	pub countdown: u32,
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
pub struct DamageET {
	pub spot: bw::Rect,
	pub weapon_id: u32,
	pub attacker_player: u8,
	pub target_player: u8,
	pub attacker_unit: UnitId,
	pub target_unit: UnitId,
}

#[derive(Debug,Clone, Default, Serialize, Deserialize)]
pub struct RegisteredEvent {
	pub event_type: EventType,
	pub infinite: bool,
	pub timer: u32,
	pub take_damage: Option<DamageET>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct RegisterState {
	pub events: Vec<RegisteredEvent>,
}



impl RegisterState {
	pub fn decrement(&mut self){
		for e in &mut self.events {
			e.timer = e.timer.saturating_sub(1);
		}
		self.events.swap_retain(|x| x.timer !=0 || x.infinite);
	}
	pub fn register_combat_event(&mut self, attacker: Unit, target: Unit, weapon_id: u32, timer: u32){
		let damage_event = DamageET{ spot: bw::Rect::from_point(target.position()), weapon_id, attacker_player: attacker.player(), 
									target_player: target.player(), attacker_unit: attacker.id(), target_unit: target.id()};
									
		let mut event: RegisteredEvent = Default::default();
		event.event_type = EventType::TakingWeaponDamage;
		event.timer = timer;
		event.infinite = match timer {
			0=>true,
			_=>false,
		};
		event.take_damage = Some(damage_event);
		self.events.push(event);
	}
	pub fn combat_detected(&mut self, player_1: PlayerMatch,player_2: PlayerMatch, area: bw::Rect32, unit_id: UnitMatch, unit_id2: UnitMatch)->bool {
		let area = bw::Rect {
            left: area.left as i16,
            top: area.top as i16,
            right: area.right as i16,
            bottom: area.bottom as i16,
        };
		for event in &mut self.events {
			if event.event_type==EventType::TakingWeaponDamage {
				if let Some(damage) = event.take_damage {
					/*bw_print!("Condition: {} (req: {} {}, vals: {:?} {:?}) {} {}",
						(player_1.matches(damage.attacker_player) && player_2.matches(damage.target_player) || 
						(player_1.matches(damage.target_player) && player_2.matches(damage.attacker_player))),
						damage.attacker_player,damage.target_player,player_1,player_2,
						
						damage.spot.overlaps(&area),
						(unit_id.matches_unit_id(&damage.attacker_unit) || unit_id.matches_unit_id(&damage.target_unit)));
						*/
						/*
					if ((player_1.matches(damage.attacker_player) && player_2.matches(damage.target_player) || 
						(player_1.matches(damage.target_player) && player_2.matches(damage.attacker_player))) && damage.spot.overlaps(&area)
						&& (  (unit_id.matches_unit_id(&damage.attacker_unit) && unit_id2.matches_unit_id(&damage.target_unit)) ||
							   (unit_id2.matches_unit_id(&damage.attacker_unit) && unit_id.matches_unit_id(&damage.target_unit))  ))	
					{
						return true;
					}		*/	
					if player_1.matches(damage.attacker_player) && player_2.matches(damage.target_player) && damage.spot.overlaps(&area)
						&& unit_id.matches_unit_id(&damage.attacker_unit) && unit_id2.matches_unit_id(&damage.target_unit)	
					{
						return true;
					}
				}
				
			}
		}
		false
	}
}

impl TriggerState {
	pub fn set_jmp(&mut self, jump_string: String, val: u32, players:PlayerMatch){
		let mut variable: Variable = Default::default();
		variable.var_type=VarType::Jump;
		variable.jump = Some(val);
		for (varstring,v,pm) in &mut self.variables {
			if &jump_string==varstring && v.var_type==VarType::Jump && pm.match_bothdir(players){
				*v = variable;
				return;
			}
		}
		self.variables.push((jump_string,variable,players));
		
		
	}
	pub fn get_jump_pos(&mut self, jump_string: String, players: PlayerMatch)->Option<u32>{
		debug!("Get jump pos of {} {:?}",jump_string,players);
		for (varstring,v,pm) in &mut self.variables {
			debug!("{} {}",varstring,&jump_string);
			if &jump_string==varstring {
				debug!("eq");
			}
			
			if &jump_string==varstring && v.var_type==VarType::Jump && pm.match_with_script(players){
				return v.jump;
			}
		}
		bw_print!("Error: Incorrect jump string {}",jump_string);
		return None;
	}	
	pub fn get_var(&mut self, string: String, players: PlayerMatch)->Option<Variable>{
		for (varstring,v,pm) in &mut self.variables {
			if &string==varstring  && pm.match_with_script(players) {
				return Some(v.clone());
			}
		}	
		return None;
	}
	pub fn to_variable(string: String)->Variable {
		let mut variable: Variable = Default::default();
		if string.parse::<f64>().is_ok(){
			variable.var_type=VarType::I32;
			variable.int32 = Some(string.trim().parse().unwrap());
		}
		else {
			variable.var_type = VarType::Boolean;
			match string.as_ref(){
				"true"=>{
					variable.boolean=Some(true);
				},
				"false"=>{
					variable.boolean=Some(false);
				},
				_=>{},
			}
		}	
		variable
	}
	pub fn set_boolean(&mut self, string: String, val: bool,players: PlayerMatch){
		let mut variable: Variable = Default::default();
		variable.var_type=VarType::Boolean;
		variable.boolean = Some(val);
		if let Some(val) = self.get_bool(string.clone(),players){
			for (varstring,v,pm) in &mut self.variables {
				if &string==varstring && v.var_type==VarType::Boolean && pm.match_with_script(players) {
					*v=variable.clone();
				}
			}
		}
		else {
			self.variables.push((string,variable,players));	
		}
	}	
	pub fn set_unit(&mut self, string: String, unit: Unit, players: PlayerMatch){
		let mut variable: Variable = Default::default();
		variable.var_type=VarType::Unit;
		variable.unit = Some(unit);
		bw_print!("Add unit variable {}",string);
		self.variables.push((string,variable,players));
	}
	pub fn set_signed_integer(&mut self, string: String, int: i32, modifier: WriteModifier,players:PlayerMatch,
		rand: u32){
		
		let mut variable: Variable = Default::default();
		variable.var_type=VarType::I32;
		if let Some(mut val) = self.get_signed_integer(string.clone(),players){
			match modifier {
				WriteModifier::Add=>{
					val = val.saturating_add(int);
				},
				WriteModifier::Subtract=>{
					val = val.saturating_sub(int);
				},			
				WriteModifier::Set=>{
					val = int;
				},
				WriteModifier::Randomize=>{
					val = rand as i32;
				},
				_=>{
					panic!("Illegal write modifier {:?} in set_i32 var",modifier);
				},
			}
			variable.int32=Some(val);
			for (varstring,v,pm) in &mut self.variables {
				if &string==varstring && v.var_type==VarType::I32 && pm.match_with_script(players) {
					*v=variable.clone();
				}
			}
		}
		else {
			variable.int32=Some(int);
			self.variables.push((string,variable,players));		
		}
	}
	
	pub fn get_bool(&mut self, string: String, players: PlayerMatch)->Option<bool>{
		for (varstring,v,pm) in &mut self.variables {
			if &string==varstring && v.var_type==VarType::Boolean && pm.match_bothdir(players) {
				return v.boolean;
			}
		}
		return None;
	}	
	pub fn get_signed_integer(&mut self, string: String, players: PlayerMatch)->Option<i32>{
		for (varstring,v,pm) in &mut self.variables {
			if &string==varstring && v.var_type==VarType::I32 && pm.match_bothdir(players) {
				return v.int32;
			}
		}
		return None;
	}	
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Cullen {
	pub unlocked_classes: Vec<u16>,
	pub equipped_class: u16,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct NewGameState {
	//add stuff later 
	pub unit_structs: Vec<UnitStruct>,
	pub new_units: FxHashMap<HashableUnit, UnitStruct>,
	pub custom_sprites: FxHashMap<SerializableSprite, SpriteStruct>,
	//move here all unit_struct over time
	pub persist_unit_structs: Vec<UnitStruct>,//not erased by unit_removed
	pub diplomacy: [DiplomacySetting; 8],
	pub escalations: [EscalationSystem;8],
	//pub intervention_exists: [bool; 8],
	pub long_term_effects: Vec<LongEffect>,
	pub sprite_effects: Vec<SpriteEffect>,
	pub factions: FxHashMap<u16,Faction>,
	pub global_right_click_status: bool,//unused for now, will be used later w/ hook
	pub set_right_click_status: [bool; 8],
	
	
	pub cullen: Option<Cullen>,
	//pub activist_effect_rtree: rstar::RTree<[f64; 2]>,
	pub activist_hashmap: FxHashMap<(i16,i16),Vec<(i16,i16)>>,
	pub generic_variable: FxHashMap<u32,i32>,
//	pub activist_rtree_state: bool,
	pub last_button: u8,
	
}
use std::collections::hash_map::Entry;
impl NewGameState {
/*
              if let Some(&unit) = self.unit_mapping.get(&sprite) {
                if (*unit).sprite == sprite {
                    return Unit::from_ptr(unit);
                }
            }
*/
//		fn get_hash_unit_mut(&mut self, unit: *mut bw::Unit)
		
		
		pub unsafe fn link_relay_to_structure(&mut self, relay: Unit, structure: Unit){
			//bw_print!("Connect {} to {}",relay.id().0,structure.id().0);
			for (_k, unitstruct) in &mut self.new_units {
				unitstruct.warp_relay_feedback.swap_retain(|x| *x != structure);
			}
			let u_str = self.default_unit_struct(relay);
			let result = match self.new_units.entry(HashableUnit(relay)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};						
			result.warp_relay_feedback.push(structure);
			//
			let u_str = self.default_unit_struct(structure);
			let result = match self.new_units.entry(HashableUnit(structure)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};						
			result.warp_relay_link = Some(relay);	
		}
		
		pub unsafe fn set_generic_switch(&mut self, unit: Unit, switch_id: u32){
			let mut u_str = self.default_unit_struct(unit);
			u_str.generic_switch.insert(switch_id,true);
			let table = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => {					
					entry.insert(u_str);					
					return;
				},
			   Entry::Occupied(entry) => &mut entry.into_mut().generic_switch,
			};					
			let extract = match table.entry(switch_id){
			   Entry::Vacant(entry) => entry.insert(false),
			   Entry::Occupied(entry) => entry.into_mut(),			
			};
			*extract = !(*extract);
		}
		pub unsafe fn set_generic_value(&mut self, unit: Unit, switch_id: u32, value: i32){
			let mut u_str = self.default_unit_struct(unit);
			u_str.generic_value.insert(switch_id,value);
			let table = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => {	
					entry.insert(u_str);	
					return;
				},
			   Entry::Occupied(entry) => &mut entry.into_mut().generic_value,
			};
			let extract = match table.entry(switch_id){
			   Entry::Vacant(entry) => entry.insert(0),
			   Entry::Occupied(entry) => entry.into_mut(),			
			};
			*extract = value;
		}		
		pub unsafe fn set_generic_unit(&mut self, unit: Unit, switch_id: u32, target: Unit){
			let mut u_str = self.default_unit_struct(unit);
			u_str.generic_unit.insert(switch_id,target);
			let table = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => {	
					entry.insert(u_str);
					return;
				},
			   Entry::Occupied(entry) => &mut entry.into_mut().generic_unit,
			};
			let extract = match table.entry(switch_id){
			   Entry::Vacant(entry) => entry.insert(target),
			   Entry::Occupied(entry) => entry.into_mut(),			
			};
			*extract = target;		
		}
		pub unsafe fn clear_generic_unit(&mut self, unit: Unit, switch_id: u32){
			let table = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(_) => {	
					return;
				},
			   Entry::Occupied(entry) => &mut entry.into_mut().generic_unit,
			};
			table.remove(&switch_id);
		}
		pub unsafe fn set_generic_timer(&mut self, unit: Unit, switch_id: u32, value: u32){
			let mut u_str = self.default_unit_struct(unit);
			u_str.generic_timer.insert(switch_id,value);
			let table = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => {	
					entry.insert(u_str);
					return;
				},
			   Entry::Occupied(entry) => &mut entry.into_mut().generic_timer,
			};
			let extract = match table.entry(switch_id){
			   Entry::Vacant(entry) => entry.insert(0),//was 0
			   Entry::Occupied(entry) => entry.into_mut(),			
			};
			*extract = value;		
		}
		pub unsafe fn set_generic_variable(&mut self, switch_id: u32, value: i32){
			let extract = match self.generic_variable.entry(switch_id){
			   Entry::Vacant(entry) => entry.insert(value),//was 0
			   Entry::Occupied(entry) => entry.into_mut(),			
			};
			*extract = value;		
		}
		
		pub unsafe fn add_generic_value(&mut self, unit: Unit, switch_id: u32, value: i32){
			//bw_print!("Add generic value: {} {}",switch_id,value);
			let mut u_str = self.default_unit_struct(unit);
			u_str.generic_value.insert(switch_id,0);
			let table = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => {					
					entry.insert(u_str);					
					return;
				},
			   Entry::Occupied(entry) => &mut entry.into_mut().generic_value,
			};					
			let extract = match table.entry(switch_id){
			   Entry::Vacant(entry) => entry.insert(0),//was 0
			   Entry::Occupied(entry) => entry.into_mut(),			
			};
			//bw_print!("Before: {}",extract);
			*extract = extract.saturating_add(value as i32);
			//bw_print!("After: {}",extract);			
		}			
		pub unsafe fn get_generic_switch(&mut self, unit_ptr: *mut bw::Unit, switch_id: u32)->u32{
			let table = match self.new_units.entry(HashableUnit(Unit::from_ptr(unit_ptr).unwrap())){
				Entry::Vacant(_e) => {return 0;},
				Entry::Occupied(entry) => &mut entry.into_mut().generic_switch,
			};
			let extract = match table.entry(switch_id){
			   Entry::Vacant(_e) => {return 0;},
			   Entry::Occupied(entry) => entry.into_mut(),			
			};		
			*extract as u32	
		}
		pub unsafe fn get_generic_value(&mut self, unit_ptr: *mut bw::Unit, switch_id: u32)->u32{
			let table = match self.new_units.entry(HashableUnit(Unit::from_ptr(unit_ptr).unwrap())){
				Entry::Vacant(_e) => {return 0;},
				Entry::Occupied(entry) => &mut entry.into_mut().generic_value,
			};
			let extract = match table.entry(switch_id){
			   Entry::Vacant(_e) => {return 0;},
			   Entry::Occupied(entry) => entry.into_mut(),			
			};		
			*extract as u32	
		}
		pub unsafe fn get_generic_unit(&mut self, unit_ptr: *mut bw::Unit, switch_id: u32)->Option<Unit>{
			let table = match self.new_units.entry(HashableUnit(Unit::from_ptr(unit_ptr).unwrap())){
				Entry::Vacant(_e) => {return None;},
				Entry::Occupied(entry) => &mut entry.into_mut().generic_unit,
			};
			let extract = match table.entry(switch_id){
			   Entry::Vacant(_e) => {return None;},
			   Entry::Occupied(entry) => entry.into_mut(),			
			};		
			Some(*extract)	
		}
		pub unsafe fn get_generic_timer(&mut self, unit_ptr: *mut bw::Unit, switch_id: u32)->u32{
			
			let table = match self.new_units.entry(HashableUnit(Unit::from_ptr(unit_ptr).unwrap())){
				Entry::Vacant(_e) => {
					return 0;				
				},
				Entry::Occupied(entry) => &mut entry.into_mut().generic_timer,
			};
			let extract = match table.entry(switch_id){
			   Entry::Vacant(_e) => {
					return 0;
			   
			   },
			   Entry::Occupied(entry) => entry.into_mut(),			
			};		
			*extract as u32	
		}
		pub unsafe fn get_generic_variable(&mut self, switch_id: u32)->u32{
			let extract = match self.generic_variable.entry(switch_id){
			   Entry::Vacant(_e) => {
					return 0;
				},
			   Entry::Occupied(entry) => entry.into_mut(),			
			};		
			*extract as u32	
		}
		
/*
		pub unsafe fn is_nydus_pickup(&mut self, unit: Unit)->bool {		
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => {return false;},
				Entry::Occupied(entry) => entry.into_mut(),
			};
			return result.pickup_ai_type;
		}	
		pub unsafe fn get_nydus_area(&mut self, unit: Unit)->bw::Point {		
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => {return bw::Point{x: 0, y: 0};},
				Entry::Occupied(entry) => entry.into_mut(),
			};
			return result.nydus_target_area;
		}		*/	
		pub unsafe fn set_pickup_status(&mut self, unit: Unit, status: bool){
			let u_str = self.default_unit_struct(unit);		
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};				
			result.pickup_ai_type=status;
		}
		pub unsafe fn set_buildup_timer(&mut self, unit: Unit, val: u32){
			let u_str = self.default_unit_struct(unit);		
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};				
			//bw_print!("Set buildup timer of {} to {}",unit.id().0,val);
			result.planetcracker_buildup=val as u16;		
		}
		pub unsafe fn is_cracking(&mut self, unit: Unit)->bool{
			let u_str = self.default_unit_struct(unit);		
			let result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};				
			(result.planetcracker_buildup+result.planetcracker_timer)!=0	
		}
		pub unsafe fn get_matrix_caster_player(&mut self, unit: Unit)->u32{
			let u_str = self.default_unit_struct(unit);		
			let result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => {return 8;},
			   Entry::Occupied(entry) => entry.into_mut(),
			};				
			if let Some(caster) = result.matrix_caster{
				return caster.player() as u32;
			}
			return 8;
		}
		pub unsafe fn get_sublime_shepherds(&mut self, unit: Unit)->u32{
			let u_str = self.default_unit_struct(unit);		
			let result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => {return 0;},
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			return result.sublime_shepherd_casters.len() as u32;
		}		
		pub unsafe fn get_energy_targets(&mut self, unit: Unit)->Vec<Unit>{
			let unit_search = UnitSearch::from_bw();
			let rect = bw::Rect { left: unit.position().x.saturating_sub(96),
								  top: unit.position().y.saturating_sub(96),
								  right: unit.position().x.saturating_add(96),
								  bottom: unit.position().y.saturating_add(96) };
			let units = unit_search
				.search_iter(&rect)
				.filter(|x| *x!=unit && x.id().is_spellcaster())
				.collect::<Vec<_>>();	
			units
		
		}
		pub unsafe fn add_madrigal_timer(&mut self, unit: Unit, timer: u32){
			let u_str = self.default_unit_struct(unit);		
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			result.madrigal_timers.push(timer);
		}
		pub unsafe fn set_matrix_caster(&mut self, unit: Unit, target: Unit){
			let u_str = self.default_unit_struct(target);		
			let mut result = match self.new_units.entry(HashableUnit(target)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			result.matrix_caster = Some(unit);
		}		
		pub unsafe fn set_sublime_shepherd(&mut self, unit: Unit, target: Unit){
			let u_str = self.default_unit_struct(target);		
			let result = match self.new_units.entry(HashableUnit(target)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			let mut find = result.sublime_shepherd_casters.iter().find(|x| x.0==unit);
			if let Some((_u,mut time)) = &mut find {
				time = 48;
			}
			else {
				result.sublime_shepherd_casters.push((unit,48));
			}
		}		
				
		
		pub unsafe fn set_planetcracker_timer(&mut self, unit: Unit, val: u32){
			let u_str = self.default_unit_struct(unit);		
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};				
			result.planetcracker_timer=val as u16;		
		}		
		/*
		pub unsafe fn set_nydus_target_area(&mut self, unit: Unit, position: bw::Point){
			let u_str = self.default_unit_struct(unit);		
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};				
			result.nydus_target_area=position;
		}		
		*/
		pub unsafe fn can_overload(&mut self, target: Unit)->u32{
			let result = match self.new_units.entry(HashableUnit(target)){
				Entry::Vacant(_e) => {return 0;},
				Entry::Occupied(entry) => entry.into_mut().can_overload,
			};
			let answer = match result {
				false=>0,
				true=>1,
			};
			answer
		}
		pub unsafe fn set_cluster_as_high_yield(&mut self, unit: Unit){
			let u_str = self.default_unit_struct(unit);
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			result.high_yield = true;
		}
		pub unsafe fn set_deathwish(&mut self, unit: Unit){
			let u_str = self.default_unit_struct(unit);
			
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			result.deathwish = true;
		}		
	
		pub unsafe fn relay_deployed(&mut self, unit: Unit)->bool{
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => {return false;},
				Entry::Occupied(entry) => entry.into_mut().aperture,
			};
			result
		}	
		pub unsafe fn clarion_deployed(&mut self, unit: Unit)->bool{
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => {return false;},
				Entry::Occupied(entry) => entry.into_mut().phase_link,
			};
			result
		}			
		pub unsafe fn get_linked_relay_if_exist(&mut self, unit: Unit)->u32 {
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => { return 0;},
				Entry::Occupied(entry) => entry.into_mut(),
			};		
			if let Some(relay) = result.warp_relay_link {
				return relay.0 as u32;
			}
			return 0;
		}
		pub unsafe fn linked_to_clarion(&mut self, unit: Unit)->u32 {
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => { return 0;},
				Entry::Occupied(entry) => entry.into_mut(),
			};		
			return result.phase_link_unit.is_some() as u32;
		}
		
		pub fn set_mind_control_link(&mut self, archon: Unit, target: Unit){
			let arc_str = self.default_unit_struct(archon);
			let arch = match self.new_units.entry(HashableUnit(archon)){
				Entry::Vacant(entry) => entry.insert(arc_str),
				Entry::Occupied(entry) => entry.into_mut(),
			};	
			arch.mind_control_links.push(target);
			let targ_str = self.default_unit_struct(target);
			let targ = match self.new_units.entry(HashableUnit(target)){
				Entry::Vacant(entry) => entry.insert(targ_str),
				Entry::Occupied(entry) => entry.into_mut(),
			};
			targ.mind_controlled = Some(target.player());
		}
		pub fn sum_mindcontrol_cost(&mut self, archon: Unit)->u32 {
			let mut sum = 0;
			let arch = match self.new_units.entry(HashableUnit(archon)){
				Entry::Vacant(_e) => { return 0;},
				Entry::Occupied(entry) => entry.into_mut(),
			};	
			for link in &mut arch.mind_control_links {
				let mut cost = link.id().supply_cost();
				if link.id()==unit::id::OVERLORD {
					cost = 4;
				}
				sum += cost;
			}
			sum
		}
		
		pub fn spread_clarion_damage(&mut self, target: Unit, shield_dmg: u32, damage_type: u32, direction: u32){
			use std::cmp;
			let result = match self.new_units.entry(HashableUnit(target)){
					Entry::Vacant(_e) => { bw_print!("Error! Incorrect clarion link!"); return;},
					Entry::Occupied(entry) => entry.into_mut(),
			};
			if let Some(link) = result.phase_link_unit {
				let clarion = match self.new_units.entry(HashableUnit(link)){
					Entry::Vacant(_e) => { 
						bw_print!("Error 3! Clarion"); return;
					},
					Entry::Occupied(entry) => entry.into_mut(),
				};
				unsafe {
				if clarion.phase_link_feedback.len()==0 {
					(*target.0).shields -= shield_dmg as i32;
					if damage_type != 0 && (*target.0).shields != 0 {
						bw::show_shield_overlay(direction,target.0);
					}
				}
				else {
					let divided = shield_dmg/clarion.phase_link_feedback.len() as u32;
					for unit in &mut clarion.phase_link_feedback {
						(*unit.0).shields -= cmp::min(divided as i32, (*unit.0).shields);
						if damage_type != 0 && (*unit.0).shields != 0 {
//							bw::show_shield_overlay(direction,unit.0);
/*
							let img;
							if unit.id().has_medium_overlay(){
								img = 1103;
							}
							else if unit.id().has_large_overlay(){
								img = 1104;
							}
							else {
								img = 1102;
							}
							bw::add_overlay_above_main(0,0,0,(*unit.0).sprite, img);
							*/
						}		
					}								
				}
				}
			}
			else {
				bw_print!("Error 2! Incorrect clarion link!");
			}
		}
		pub fn clear_clarion_links(&mut self, clarion: Unit){
			for (_k, unitstruct) in &mut self.new_units {
				if let Some(link) = unitstruct.phase_link_unit {
					if link==clarion {
						unitstruct.phase_link_unit = None;
					}
					unsafe {
						bw::remove_overlays(link.0,1102,1104);
					}
				}
			}		
		}
		pub fn place_clarion_links(&mut self, clarion: Unit){
			let linked_units;
			self.clear_clarion_links(clarion);
			let u_str = self.default_unit_struct(clarion);
			let result = match self.new_units.entry(HashableUnit(clarion)){
				Entry::Vacant(entry) => { entry.insert(u_str)},
				Entry::Occupied(entry) => entry.into_mut(),
			};
			result.phase_link_feedback.clear();
			unsafe {
				let search = UnitSearch::from_bw();
				let game = Game::get();
				
				let rect = bw::Rect { left: clarion.position().x.saturating_sub(128),
									  top: clarion.position().y.saturating_sub(128),
									  right: clarion.position().x.saturating_add(128),
									  bottom: clarion.position().y.saturating_add(128) };
											
				linked_units = search
					.search_iter(&rect)
					.filter(|x| game.allied(x.player(),clarion.player()) && !x.id().is_building() && 
					x.id().has_shields() && x.shields()>0 
					&& bw::fdistance(clarion.position(),x.position())<=128)			
					.collect::<Vec<_>>();
				let mut count = 0;
				for u in &linked_units {
					result.phase_link_feedback.push(*u);
					//
					let img;
					if u.id().has_medium_overlay(){
						img = 1103;
					}
					else if u.id().has_large_overlay(){
						img = 1104;
					}
					else {
						img = 1102;
					}
					
					bw::add_overlay_above_main(0,0,0,(*u.0).sprite, img);
				}
				drop(result);
			}
			let mut count = 0;
			for u in &linked_units {
				let u_str = self.default_unit_struct(*u);
				let mut result_child = match self.new_units.entry(HashableUnit(*u)){
						Entry::Vacant(entry) => { entry.insert(u_str)},
						Entry::Occupied(entry) => entry.into_mut(),
				};
				if result_child.phase_link_unit.is_none(){
					result_child.phase_link_unit = Some(clarion);
					/*count += 1;
					if count >= 5 {
						return;
					}*/
				}
					
			}			
		}
		pub unsafe fn relay_conduit_count(&mut self, unit: Unit)->u32 {
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => { return 0;},
				Entry::Occupied(entry) => entry.into_mut(),
			};		
			return result.warp_relay_feedback.len() as u32;
		}		
		
		pub unsafe fn if_linked_to_relay(&mut self, unit: Unit)->u32 {
			let result = match self.new_units.entry(HashableUnit(unit)){
				Entry::Vacant(_e) => { return 0;},
				Entry::Occupied(entry) => entry.into_mut(),
			};		
			if result.warp_relay_link.is_some() {
				return 1;
			}
			return 0;
		}		
		pub unsafe fn set_aperture_status(&mut self, unit: Unit, status: bool){
			let u_str = self.default_unit_struct(unit);
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			result.aperture = status;
		}	
		pub unsafe fn set_phaselink_status(&mut self, unit: Unit, status: bool){
			let u_str = self.default_unit_struct(unit);
			let mut result = match self.new_units.entry(HashableUnit(unit)) {
			   Entry::Vacant(entry) => entry.insert(u_str),
			   Entry::Occupied(entry) => entry.into_mut(),
			};		
			result.phase_link = status;
		}								
		

		pub fn add_bj_effect(&mut self, attacker: *mut bw::Unit, target: *mut bw::Unit){
			unsafe{
			if attacker==null_mut() || target==null_mut(){
				return;
			}
			let unit = match Unit::from_ptr(attacker) {
				Some(s) => s,
				None => {
					return;
				},
			};
			let energy = (*target).energy;
			let lef = LongEffect{effect_id: HydraEffect::BlindJudgeResurrection, timer: 24, energy: energy, 
				player: (*attacker).player, position: (*target).position, unit_id: (*target).unit_id, unit: unit, misc: 0,
				target: unit};
			self.long_term_effects.push(lef);
			}
		}
		pub fn place_screamer_pool(&mut self, source: Unit, x: u32, y: u32, player: u32){
			let lef = LongEffect{effect_id: HydraEffect::ScreamerAcidPool, timer: 120, energy: 0, 
				player: player as u8, position: bw::Point{x: x as i16,y: y as i16}, unit_id: 0, unit: source, misc: 0,
				target: source};
			self.long_term_effects.push(lef);
			
		}
		
		pub unsafe fn add_single_effect(&mut self, unit: *mut bw::Unit, effect_id: HydraEffect){
			let unit = match Unit::from_ptr(unit) {
				Some(s) => s,
				None => {
					return;
				},
			};
			let ef = LongEffect{effect_id: effect_id, timer: 1, 
				position: (*unit.0).position, player: (*unit.0).player, energy: (*unit.0).energy, unit_id: (*unit.0).unit_id,
				unit: unit, misc: 0, target: unit};
			
			self.long_term_effects.push(ef);
		}
		pub unsafe fn add_single_effect_point_spec(&mut self, unit: *mut bw::Unit, effect_id: HydraEffect, position: bw::Point,
													val: u32){
			let unit = match Unit::from_ptr(unit) {
				Some(s) => s,
				None => {
					return;
				},
			};
			let ef = LongEffect{effect_id: effect_id, timer: 1, 
				position: position, player: (*unit.0).player, energy: (*unit.0).energy, unit_id: (*unit.0).unit_id,
				unit: unit, misc: val as u16, target: unit};
			
			self.long_term_effects.push(ef);
		}
		pub unsafe fn add_single_effect_unit_spec(&mut self, unit: *mut bw::Unit, effect_id: HydraEffect, target: *mut bw::Unit,
													val: u32){
			let unit = match Unit::from_ptr(unit) {
				Some(s) => s,
				None => {
					return;
				},
			};
			let target = match Unit::from_ptr(target) {
				Some(s) => s,
				None => {
					return;
				},
			};
			let ef = LongEffect{effect_id: effect_id, timer: 1, 
				position: unit.position(), player: (*unit.0).player, energy: (*unit.0).energy, unit_id: (*unit.0).unit_id,
				unit: unit, misc: val as u16, target: target};
			
			self.long_term_effects.push(ef);
		}
		pub unsafe fn add_single_effect_misc(&mut self, unit: *mut bw::Unit, effect_id: HydraEffect, misc: u16){
			let unit = match Unit::from_ptr(unit) {
				Some(s) => s,
				None => {
					return;
				},
			};
			let ef = LongEffect{effect_id: effect_id, timer: 1, 
				position: (*unit.0).position, player: (*unit.0).player, energy: (*unit.0).energy, unit_id: (*unit.0).unit_id, 
						unit: unit, misc: misc, target: unit};
			
			self.long_term_effects.push(ef);
		}		
	 
	  pub fn initialize_escalations(&mut self){
		for i in 0..8 {
			//first value = initial escalation unit id
			//second value = upgrade id
			let config = config::config();
			let game = Game::get();
			for esc in config.escalations.esc_list.clone() {
				self.escalations[i].add_esc(esc.from.0,esc.upg_id);
				game.set_unit_availability(i as u8,esc.to,false);
			}
			/*
			self.escalations[i].add_esc(32,62);//Escalate Firebat/Cyprian, default Firebat
			//self.escalations[i].add_esc(2,???);//BW - Escalate Vulture/Aspect, default Vulture
			self.escalations[i].add_esc(3,78);//Escalate Goliath/Paladin, default Goiath
			self.escalations[i].add_esc(124,79);//Escalate Missile turret/Sentry turret, default Sentry turret
			self.escalations[i].add_esc(9,67);//Escalate Science vessel/Azazel, default Science vessel
			self.escalations[i].add_esc(1,71);//Escalate Ghost/Savant, default Ghost
			self.escalations[i].add_esc(34,107);//Escalate Medic/Shaman, default Medic
			let game = Game::get();
			game.set_unit_availability(i as u8, UnitId(15), false);
			game.set_unit_availability(i as u8, UnitId(92), false);
			game.set_unit_availability(i as u8, UnitId(186), false);
			game.set_unit_availability(i as u8, UnitId(185), false);
			game.set_unit_availability(i as u8, UnitId(98), false);
			game.set_unit_availability(i as u8, UnitId(187), false);
		*/

		}
	  }
	  
	  pub fn load_relic_to_reliquary(&mut self, relic_id: u16, reliquary: Unit){
			let relic = get_relic_from_id(relic_id);
			bw_print!("Try to load relic: {} {}",relic_id,reliquary.id().0);
			if relic == Relic::NoRelic {
				bw_print!("Wrong relic id {}",relic_id);
				return;
			}
			for u in &mut self.unit_structs {
				if u.unit==reliquary {
					bw_print!("Load relic {} to reliquary",relic_id);
					return;
				}
			}	
			bw_print!("No previous reliquaries found");
			let mut u_str = self.default_unit_struct(reliquary);
			u_str.relics_stored.push(relic);
			self.unit_structs.push(u_str);
	  } 
	  pub fn get_escalation_from_upgrade(&mut self, upg: u16, player: u32)->UnitId{
		 for i in &mut self.escalations[player as usize].esc_list {
			if i.escalation_id == upg as u32 {
				return i.current_unit;
			}
		 }
		 return UnitId(228);//none
	  }
	  pub fn switch_escalation_status(&mut self, upg: u16, player: u32){
		for i in &mut self.escalations[player as usize].esc_list {
			if i.escalation_id == upg as u32 {
				i.current_unit = UnitId(reverse_escalation(i.current_unit.0));
				return;
			}
		}

	  }	  
	  
		

	  
	  pub fn unit_removed(&mut self, unit: Unit) {
		self.free_all(unit);//mind control	
		unsafe {
			if (*unit.0).sprite!=null_mut(){
				self.custom_sprites.remove(&SerializableSprite((*unit.0).sprite));
			}
		}
		let mut single_effects = Vec::new();
		self.long_term_effects.swap_retain(|x| x.unit != unit);
		for u in &mut self.unit_structs {
			
			unsafe {
			for h in u.hierophant_signify.iter() {
				if h.unit_link.0==unit.0 {
					 aiscript::clear_signify_flags(u.unit);
				}
			}
			}
			u.hierophant_signify.swap_retain(|x| x.unit_link.0 != unit.0);
			//}
			u.legionnaire.swap_retain(|x| x.unit_link.0 != unit.0);		
			if u.hierophant_signify.len()==0 {
				unsafe {
				(*u.unit.0)._dc132 |= 0x10; }
			}
		}
		let config=config::config();
		
		
		if config.moderator_behavior {
			unsafe {
			let moderation = self.get_moderation_stacks(unit);
			
			if moderation>0 && self.can_overload(unit)!=0 {
				//bw_print!("Send overload");
				self.add_single_effect_misc(unit.0, HydraEffect::SeverityOverload, moderation as u16/2);
			}
			
			}
		}
		
		//
		unsafe {
		for (_k, unitstruct) in &mut self.new_units {
			unitstruct.warp_relay_feedback.swap_retain(|x| *x != unit);
			unitstruct.phase_link_feedback.swap_retain(|x| *x != unit);
			unitstruct.sublime_shepherd_casters.swap_retain(|(x,_t)| *x!=unit);
			unitstruct.mind_control_links.swap_retain(|x| *x != unit);
			unitstruct.generic_unit.retain(|_,x| *x!=unit);
			if let Some(link) = unitstruct.warp_relay_link {
				if link==unit{
					unitstruct.warp_relay_link = None;
				}
			}
			
			if let Some(creeper) = unitstruct.creeper {
				//(*creeper.0).order_flags |= 0x4;
				//bw::kill_unit(creeper.0);	
//				self.add_single_effect_misc(creeper.0,HydraEffect::RemoveUnit,0);
				single_effects.push((creeper.0,HydraEffect::RemoveUnit));
				unitstruct.creeper = None;		
			}
		}
		
		self.sprite_effects.swap_retain(|x| x.source.0 != unit.0);	
        self.unit_structs.swap_retain(|x| x.unit.0 != unit.0);
		self.new_units.remove(&HashableUnit(unit));
		for ef in single_effects {
			self.add_single_effect_misc(ef.0,ef.1,0);
		}
		}
    }
	pub fn free_all(&mut self, unit: Unit){
		let result = match self.new_units.entry(HashableUnit(unit)){
			Entry::Vacant(_e) => {
				return;
			},
			Entry::Occupied(entry) => entry.into_mut(),
		};
		let links = result.mind_control_links.clone();
		if links.len()==0 {
			return;
		}
		result.mind_control_links.clear();
		unsafe {
			for l in links{
				let result_new = match self.new_units.entry(HashableUnit(l)){
					Entry::Vacant(_e) => {
						continue;
					},
					Entry::Occupied(entry)=> entry.into_mut(),
				};
				if result_new.mind_controlled.is_none(){
					continue;
				}
				let player = result_new.mind_controlled.unwrap() as u16;
				self.add_single_effect_misc(l.0, HydraEffect::MindControl, player);
			}
		}
	}
	
	pub fn get_legionnaire_marks(&mut self, attacker: Unit, targ: Unit)->u32 {
		for a in &mut self.unit_structs {
			if a.unit==targ {
				let mut n = 0;
				for b in &mut a.legionnaire {
					if b.unit_link!=attacker {
						n+=1;
					}
				}
				if n>0 {
					return n;
				}
				return 0;
			}
		}
		0
	}
	pub fn is_executing_last_orders(&mut self,unit_ptr: *mut bw::Unit)->u32 {
		for u in &mut self.unit_structs {
			if u.unit.0==unit_ptr {
				if u.last_orders {
					return 1;
				}
			}
		}
		0
	}

	pub fn is_blowing_up(&mut self,unit_ptr: *mut bw::Unit)->u32 {
		
		for u in &mut self.persist_unit_structs {
			if u.unit.0==unit_ptr {
				if u.prepare_to_explode {
					return 1;
				}
			}
		}
		0
	}	
	
	

	pub fn has_hierophant_malice(&mut self, unit_ptr: *mut bw::Unit)->u32{
		for u in &mut self.unit_structs {
			let mut subunit_condition = false;
			unsafe {
			if (*u.unit.0).subunit != null_mut() {
				if (*u.unit.0).subunit == unit_ptr {
					subunit_condition = true;
				}
			}
			}
			if u.unit.0==unit_ptr || subunit_condition {
				if u.signify_slowdown {
					return 1;	
				}
			} 
		}
		0	
	}
	pub fn hierophant_attackspeed_proc(&mut self, result: u32, unit: Unit)->u32{
		let mut speed = result;
		
		for u in &mut self.unit_structs {
			let mut subunit_condition = false;
			unsafe {
				if (*u.unit.0).subunit != null_mut() {
					if (*u.unit.0).subunit == unit.0 {
						subunit_condition = true;
					}
				}
			}
			if u.unit==unit || subunit_condition {
				if u.signify_slowdown {
					speed += speed/3;
				}
				return speed;
			} 
		}
		speed
	}
	
	pub fn hierophant_movespeed_proc(&mut self, result: u32, unit: Unit)->u32{
		let speed = result;
		for u in &mut self.unit_structs {
			if u.unit==unit {
				if u.hierophant_signify.len()>0{
					for hier in &mut u.hierophant_signify {
					
						unsafe{
						if hier.unit_link.0!=null_mut(){
							let angle = bw::get_angle(unit.position().y as u32,unit.position().x as u32,
								hier.unit_link.position().x as u32,hier.unit_link.position().y as u32) as i32;
							let unit_angle = (*unit.0).movement_direction as i32;
							let delta = 128 - ((angle - unit_angle).abs() - 128).abs(); 
							if delta >= 128-(64/2) {//64=90 degrees arc
								return speed/2;
							}
							
						}
						
						}
					}
				}
				return speed;
			} 
		}
		speed
	}
	
	pub fn add_cyprian_variable(&mut self, cyprian: Unit){
		for u in &mut self.unit_structs {
			if u.unit==cyprian {
				u.cyprian_safety_off = Some(false);
				return;
			}
		}	
		let mut u_str = self.default_unit_struct(cyprian);
		u_str.cyprian_safety_off = Some(false);
		self.unit_structs.push(u_str);
	}
	
	
	pub fn set_last_orders(&mut self, scout: Unit){
		for u in &mut self.unit_structs {
			if u.unit==scout {
				u.last_orders = true;
				return;
			}
		}	
		let mut u_str = self.default_unit_struct(scout);
		u_str.last_orders = true;
		self.unit_structs.push(u_str);
	}

	pub unsafe fn set_maelstrom_offset(&mut self, unit: *mut bw::Unit, offset: u32){
		let l_str = self.default_sprite_struct((*unit).sprite);
		let result = match self.custom_sprites.entry(SerializableSprite((*unit).sprite)){
		   Entry::Vacant(entry) => entry.insert(l_str),
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.maelstrom_offset = offset;
	}
	pub unsafe fn maelstrom_offset(&mut self, ptr: *mut bw::Sprite)->u32{
		if ptr==null_mut(){
			return 0;
		}
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(_e) => {
				return 0;
		   },
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.maelstrom_offset as u32
	}
	pub fn prepare_to_explode(&mut self, scout: Unit){
		for u in &mut self.persist_unit_structs {
			if u.unit==scout {
				u.prepare_to_explode = true;
				return;
			}
		}	
		let mut u_str = self.default_unit_struct(scout);
		u_str.prepare_to_explode = true;
		//bw_print!("Set expl2");
		self.persist_unit_structs.push(u_str);
	}
	
	pub fn set_cyprian_status(&mut self, cyprian: Unit, status: u32){
		for u in &mut self.unit_structs {
			if u.unit==cyprian {
				/*if let Some(safety_off) = u.cyprian_safety_off {
					safety_off = (status != 0);
				}*/
				u.cyprian_safety_off = Some(status!=0);
				return;
			}
		}
		let mut u_str = self.default_unit_struct(cyprian);
		u_str.cyprian_safety_off = Some(status!=0);
		self.unit_structs.push(u_str);
	}	
	
	pub fn has_relic(&mut self, hero: *mut bw::Unit, relic: u32)->u32 {
		//bw_print!("Search for relic {}",relic);
		let relic_val = get_relic_from_id(relic as u16);
		let unit = match Unit::from_ptr(hero) {
			Some(s) => s,
			None => {
				//bw_print!("Null pointer");
				return 0;
			},

		};

		//bw_print!("Hero id {}",unit.id().0);		
		for u in &mut self.unit_structs {
			if u.unit==unit {
				//bw_print!("Found hero unit in unit struct {}",u.unit.id().0);
				for rel in &mut u.relics_equipped {	
					
					if *rel==relic_val {
						//bw_print!("Relic found in loop");
						return 1;//relic found
					}
					else {
						//bw_print!("Wrong relic: {} {}",relic_to_integer(*rel),relic_to_integer(relic_val));
					}
				}
			}
			/*
			if u.relics_equipped.len()>0 {
				bw_print!("Error: Found unknown unit with relics, {}",u.unit.id().0);
			}*/
		}
		//bw_print!("No hero units with relic found in array");
		0
	}
	
	pub fn get_relic_by_index(&mut self, reliquary_ptr: *mut bw::Unit, index: u32)->u32 {
		let unit = match Unit::from_ptr(reliquary_ptr) {
			Some(s) => s,
			None => {
				bw_print!("No reliquary pointer found, return 0");
				return 0;
			},
		};
		bw_print!("Loop unit structs...[later replace by fxhashmap]");
		for u in &mut self.unit_structs {
			if u.unit==unit {
				bw_print!("Unit Match!");
				let mut i: u32 = 1;
				bw_print!("Relics n: {}",u.relics_stored.len());
				for rel in &mut u.relics_stored {
					bw_print!("Relic: {}, this index is {}",relic_to_integer(*rel),i);
					if index==i {
						let relic_id = relic_to_integer(*rel);
						return relic_id;
					}
					i += 1;
				}
			}
			else {
				if u.relics_stored.len()>0{
					bw_print!("Found incorrect unit with relics!");
				}
			}
		}
		0
	}
	
	pub fn get_cyprian_value(&mut self, cyprian_ptr: *mut bw::Unit)->u32 {
		let unit = match Unit::from_ptr(cyprian_ptr) {
			Some(s) => s,
			None => {
				return 0;
			},
		};
		for u in &mut self.unit_structs {
			if u.unit==unit {
				if let Some (status) = u.cyprian_safety_off {
					return status.into();
				}
				else {
					return 0;
				}
			}
		}
		0
	}
	
	pub fn get_escalation_value(&mut self, player: u32, id: UnitId)->u32 {
		if player > 7 {
			return 228;
		}
		for i in &mut self.escalations[player as usize].esc_list {
			if id == i.current_unit || id.0 == reverse_escalation(i.current_unit.0) {
				/*if id==i.current_unit {
					bw_print!("{} = current escalation unit, current = {}",id.0,i.current_unit.0);
				}
				if id.0==reverse_escalation(i.current_unit.0){
					bw_print!("{} = reverse escalation unit, current = {}",id.0,i.current_unit.0);
				}
				bw_print!("Recv_match: {}, escalation upg id: {}",i.current_unit.0,i.escalation_id);*/
				return i.current_unit.0 as u32;
			}
		}
		return 228;//no unit
	}
	
	
	pub fn set_escalation(&mut self, upgrade_id: u32, player: u32){
		for i in &mut self.escalations[player as usize].esc_list {
			if i.escalation_id == upgrade_id {
				let reverse = reverse_escalation(i.current_unit.0);
				if reverse==228 {
					bw_print!("Wrong escalation id! {} {}",i.current_unit.0,upgrade_id);
					return;
				}
				i.current_unit = UnitId(reverse as u16);
				return;
			}
		}	
		bw_print!("No escalation settings found!");
	}
	pub fn default_sprite_struct(&mut self, ptr: *mut bw::Sprite)->SpriteStruct {
		let s_str = SpriteStruct {
			meskalloid: false,
			basilisk_gimmick: 0,
			special_mineral: 0,
			chunk_res_id: 0,
			maelstrom_offset: 0,
		};
		s_str
	}
	pub fn default_unit_struct(&mut self, source: Unit)->UnitStruct {
		let mut u_str = UnitStruct {unit: source, 
								
								legionnaire: Vec::new(),
								hierophant_signify:Vec::new(),
								signify_slowdown: false,
								last_orders: false,
								prepare_to_explode: false,
								steeled_spatula: false,
								prepare_to_explode_todborne: false,
								parasite_timers: [0; 8],
								blind_timer: 0,
								creeper: None,
								cyprian_safety_off: None,
								relics_stored: Vec::new(),
								relics_equipped: Vec::new(),
								generic_switch: Default::default(),
								generic_value: Default::default(),
								generic_unit: Default::default(),
								generic_timer: Default::default(),
								warp_relay_feedback: Vec::new(),
								warp_relay_link: None,
								phase_link_feedback: Vec::new(),
								phase_link_unit: None,							
								aperture: false,
								phase_link: false,
								planetcracker_buildup: 0,
								planetcracker_timer: 0,
								mind_control_links: Vec::new(),
								mind_controlled: None,
								matrix_caster: None,
								madrigal_timers: Vec::new(),
								//todborne
								moderation: Vec::new(),
								moderation_failsafe_timer: 0,
								can_overload: false,
								comm_tower: None,
								high_yield: false,
								deathwish: false,
								pickup_ai_type: false,
								void_timer: 0,
								safe_void: false,
								goodboy_timer: 0,
								frosted_timer: 0,
								death_by_dough: None,
								//overlord
								meskalloid: (0,0),
								scrap: 0,
								expelling_waste: None,
								vermin: None,
								observance_link: None,
								sublime_shepherd_casters: Vec::new(),
								nydus_target_area: bw::Point{x: 0, y: 0},
								malice_count: 0,
								};
		let config = config::config();
		if config.comm_tower_struct {
			if source.id().0 == 190 { // psi disruptor
				u_str.comm_tower = Some(Default::default());
			}		
		}
		u_str
	}
	pub fn default_result(&mut self, unit: Unit)->&mut UnitStruct {
		let u_str = self.default_unit_struct(unit);
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};		
		result
	}
	
	pub fn add_creeper_link(&mut self, attacker: Unit, target: Unit){
		let u = self.default_result(attacker);
		u.creeper = Some(target);
	}
	pub fn take_relic(&mut self, reliquary: Unit, relic: u32){

		bw_print!("Unit found in reliquary!");
		//add restriction later
		for u in &mut self.unit_structs {
			if u.unit==reliquary {//must be found, since reliquary is initialized after relic purchase
				let rel = get_relic_from_id(relic as u16);
				u.relics_stored.swap_retain(|x| *x != rel);
				return;
			}
		}			
		bw_print!("Bug: No reliquaries in unit structs found!");
	
		
	}
	
	pub fn equip_relic(&mut self, unit_ptr: Unit, relic: u32){
		let relic = get_relic_from_id(relic as u16);
		for u in &mut self.unit_structs {
			if u.unit==unit_ptr {
				u.relics_equipped.push(relic);
				bw_print!("Unit found in array, add relic");
				return;
			}
		}
		bw_print!("No unit found in array, add relic and new unit to array");
		bw_print!("Unit id: {}",unit_ptr.id().0);
		let mut u_str = self.default_unit_struct(unit_ptr);
		u_str.relics_equipped.push(relic);
		self.unit_structs.push(u_str);		
	}
	
	pub fn add_hierophanted_unit(&mut self, hier: Unit, targ: Unit, up_lev: u32){
		if targ.id().is_building(){
			return;
		}	
		let str_timer = UnitStructTimer{ unit_link: hier, timer: 72};		
        for u in &mut self.unit_structs {
			//bw_print!("Hier proc: loop...");
			if u.unit==targ {
				//bw_print!("Hier proc: found comparison");
				let mut signify_exist = false;
				for i in &mut u.hierophant_signify {
					if i.unit_link==hier {
						i.timer = 72;
						u.malice_count += 1;
						
						if u.malice_count >= 5 {
							u.signify_slowdown = true;
						}
						return;
					}
					i.timer = 72;					
					//bw_print!("Hier proc: Set timer");
				}
				if u.hierophant_signify.len()>0 {
					signify_exist = true;
				}
				if !signify_exist {
					u.hierophant_signify.push(str_timer);
				}		/*		
				match up_lev {
					0=>{ u.signify_slowdown = false; },
					_=>{ u.signify_slowdown = true; },
				}*/
				u.malice_count += 1;
				if u.malice_count >= 5 {
					u.signify_slowdown = true;
				}
				//bw_print!("Slowdown/timer state: {}",u.signify_slowdown);
				return;
			}
		}
		//no unit found
		//bw_print!("Hierophant Combat Proc, Signify Malice Upgrade Level: {}",up_lev);
		let slowdown = match up_lev {
			0=>{false},
			_=>{true},
		};
		let mut u_str = self.default_unit_struct(targ);
//		u_str.signify_slowdown = slowdown;
		u_str.signify_slowdown = false;
		u_str.malice_count = 1;
		u_str.hierophant_signify = vec![str_timer];
		self.unit_structs.push(u_str);
		
	}
	
	pub unsafe fn add_parasite_timer(&mut self, player: u32, queen: *mut bw::Unit, targ: Unit){
		if player >= 8 {
			return;
			debug!("Trying to add parasite timer to wrong player {}",player);
		}
		let targ_result = self.default_result(targ);
		//targ_result.parasite_timers[player as usize] = 240;
		/*
		if let Some(queen) = Unit::from_ptr(queen){
			//let queen_result = self.default_result(queen);
			if (*queen.0).matrix_timer > 0 {
				self.add_single_effect_misc(targ.0, HydraEffect::SetDefensiveMatrix, 0);
			}
			if (*queen.0).plague_timer > 0 {
				self.add_single_effect_misc(targ.0, HydraEffect::SetPlague, 0);
			}
			if (*queen.0).ensnare_timer > 0 {
				self.add_single_effect_misc(targ.0, HydraEffect::SetEnsnare, 0);
			}
			if self.default_result(queen).signify_slowdown {
				targ_result.signify_slowdown = true;
			}
			
			if queen_result.hierophant_signify.len()>0 {
				targ_result.hierophant_signify.append(&mut queen_result.hierophant_signify);
				self.add_single_effect_misc(targ.0, HydraEffect::SetSignifyVisualEffect, 0);
			}
			if (*queen.0).is_blind != 0 {
				(*targ.0).is_blind = 1;
				targ_result.blind_timer = queen_result.blind_timer;
			}
			if queen_result.observance_link.is_some(){
				targ_result.observance_link = queen_result.observance_link;
			}
			let queen_adrenaline = self.get_generic_timer(queen.0,6);
			if self.get_generic_timer(targ.0,6)==0 && queen_adrenaline!=0 {
				self.set_generic_timer(targ,6,1+(2*24));
				self.add_single_effect(targ.0, HydraEffect::AdrenalineVisualEffect);
			}		
		}*/
		
	}
	pub fn add_blind_timer(&mut self, targ: Unit){
		let result = self.default_result(targ);
		result.blind_timer = 120;
	}	
	pub fn clear_blind_timer(&mut self, targ: Unit){
		let result = self.default_result(targ);
		result.blind_timer = 0;
	}		
	pub fn add_legionnaire_unittarget(&mut self, leg: Unit, targ: Unit){
		let str_timer = UnitStructTimer{ unit_link: leg, timer: 48};
        for u in &mut self.unit_structs {
			if u.unit==targ {
				for i in &mut u.legionnaire {
					if i.unit_link==leg {
						return;
					}
				}
				u.legionnaire.push(str_timer);
				return;
			}
		}
		//no unit found
		let mut u_str = self.default_unit_struct(targ);
		u_str.legionnaire = vec![str_timer];
		self.unit_structs.push(u_str);
	}
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Upgrades {
	pub mineral_cost_base: Vec<u16>,
	pub mineral_cost_factor: Vec<u16>,
	pub gas_cost_base: Vec<u16>,
	pub gas_cost_factor: Vec<u16>,
	pub time_cost_base: Vec<u16>,
	pub time_cost_factor: Vec<u16>,
	pub restriction_flags: Vec<u16>,
	pub icon_frame: Vec<u16>,
	pub label: Vec<u16>,
	pub race: Vec<u8>,
	pub max_repeats: Vec<u8>,
	pub broodwar_flag: Vec<u8>,
	//
	pub upgrade_limit: Vec<u8>,//bw 0xc
	pub upgrade_level: Vec<u8>,//bw 0xc
	pub length: u8,
	//
	pub globals_loaded: bool,
	pub upgrades_firegraft_offsets: Vec<u16>,
}

impl Upgrades {

	pub fn get_upgrade_level(&mut self, id: u32, player: u32)->u8{
		self.upgrade_level[(id+(player*self.length as u32)) as usize]
	}
	pub fn get_max_upgrade_level(&mut self, id: u32, player: u32)->u8{
		//debug!("Read at offset {}",id+(player*self.length as u32));
		self.upgrade_limit[(id+(player*self.length as u32)) as usize]
	}
	pub fn set_upgrade_level(&mut self, id: u32, player: u32, value: u32){
		self.upgrade_level[(id+(player*self.length as u32)) as usize]=value as u8;
	}
	pub fn set_max_upgrade_level(&mut self, id: u32, player: u32, value: u32){
		self.upgrade_limit[(id+(player*self.length as u32)) as usize]=value as u8;
	}	
	pub fn mineral_cost(&mut self, id: u32)->u32 {
		self.mineral_cost_base[id as usize] as u32
	}
	pub fn mineral_cost_extra(&mut self, id: u32)->u32 {
		self.mineral_cost_factor[id as usize] as u32
	}
	pub fn gas_cost(&mut self, id: u32)->u32 {
		self.gas_cost_base[id as usize] as u32
	}
	pub fn gas_cost_extra(&mut self, id: u32)->u32 {
		self.gas_cost_factor[id as usize] as u32
	}	
	pub fn time_cost(&mut self, id: u32)->u32 {
		self.time_cost_base[id as usize] as u32
	}
	pub fn time_factor(&mut self, id: u32)->u32 {
		self.time_cost_factor[id as usize] as u32
	}		
	pub fn restriction(&mut self, id: u32)->u32 {
		self.restriction_flags[id as usize] as u32
	}		
	pub fn _label(&mut self, id: u32)->u32 {
		self.label[id as usize] as u32
	}
	pub fn _icon(&mut self, id: u32)->u32 {
		self.icon_frame[id as usize] as u32
	}	
	pub fn _race(&mut self, id: u32)->u32 {
		self.race[id as usize] as u32
	}		
	pub fn repeat_count(&mut self, id: u32)->u32 {
		self.max_repeats[id as usize] as u32
	}
	pub fn bw_flag(&mut self, id: u32)->u32 {
		self.broodwar_flag[id as usize] as u32
	}	

	pub fn req_flags(&mut self, id: u32)->u32 {
		self.restriction_flags[id as usize] as u32
	}	
	pub fn copy_initial_from_dattable(&mut self, initialize_arrays: bool){
		
		//
		if !self.globals_loaded && initialize_arrays {
			self.upgrade_limit.clear();
			self.upgrade_level.clear();
			self.mineral_cost_base.clear();
			self.mineral_cost_factor.clear();
			self.gas_cost_base.clear();
			self.gas_cost_factor.clear();
			self.time_cost_base.clear();
			self.time_cost_factor.clear();
			self.restriction_flags.clear();
			self.icon_frame.clear();
			self.label.clear();
			self.race.clear();
			self.max_repeats.clear();
			self.upgrades_firegraft_offsets.clear();
			self.broodwar_flag.clear();
			for i in 0..61 {
				let upg = UpgradeId(i);	
				self.mineral_cost_base.push(upg.get(0) as u16);
				self.mineral_cost_factor.push(upg.get(1) as u16);
				self.gas_cost_base.push(upg.get(2) as u16);
				self.gas_cost_factor.push(upg.get(3) as u16);
				self.time_cost_base.push(upg.get(4) as u16);
				self.time_cost_factor.push(upg.get(5) as u16);
				self.restriction_flags.push(upg.get(6) as u16);
				self.icon_frame.push(upg.get(7) as u16);
				self.label.push(upg.get(8) as u16);
				self.race.push(upg.get(9) as u8);				
				self.max_repeats.push(upg.get(10) as u8);								
				self.broodwar_flag.push(upg.get(11) as u8);								
			}
			//add none as null fields
			self.mineral_cost_base.push(0);
			self.mineral_cost_factor.push(0);
			self.gas_cost_base.push(0);
			self.gas_cost_factor.push(0);
			self.time_cost_base.push(0);
			self.time_cost_factor.push(0);
			self.restriction_flags.push(0);
			self.icon_frame.push(0);
			self.label.push(0);
			self.race.push(0);				
			self.max_repeats.push(0);								
			self.broodwar_flag.push(0);	
			self.upgrades_firegraft_offsets.clear();
			self.load_extended_upgrades();
		}

		let mut active_patcher = ::PATCHER.lock().unwrap();
		let mut exe = active_patcher.patch_exe(0x00400000);
		unsafe{
		let min = &mut self.mineral_cost_base[0] as *mut u16;
		let min_factor = &mut self.mineral_cost_factor[0] as *mut u16;
		let gas = &mut self.gas_cost_base[0] as *mut u16;
		let gas_factor = &mut self.gas_cost_factor[0] as *mut u16;
		let time = &mut self.time_cost_base[0] as *mut u16;
		let time_factor = &mut self.time_cost_factor[0] as *mut u16;
		let restriction = &mut self.restriction_flags[0] as *mut u16;
		let icon = &mut self.icon_frame[0] as *mut u16;
		let label = &mut self.label[0] as *mut u16;
		//let race = &mut self.race[0] as *mut u16;//editor only
		let repeat = &mut self.max_repeats[0] as *mut u8;
		//let broodwar = &mut self.broodwar_flag[0] as *mut u16;//editor only
		//bw_print!("Gas ref: {:?}",gas);
		exe.replace_val(0x00453F57+3, min);
		exe.replace_val(0x0042D1CF+3, min);
		exe.replace_val(0x004367F3+3, min);
		exe.replace_val(0x004540EF+3, min);
		exe.replace_val(0x004541B6+3, min);
		exe.replace_val(0x004CA980+4, min);
		exe.replace_val(0x004CAAA0+4, min);//ok
		
		exe.replace_val(0x0042D1C7+4, min_factor);		
		exe.replace_val(0x004367EB+4, min_factor);		
		exe.replace_val(0x00453F50+3, min_factor);		
		exe.replace_val(0x004540E7+4, min_factor);		
		exe.replace_val(0x004541AE+4, min_factor);		
		exe.replace_val(0x004CA990+4, min_factor);		
		exe.replace_val(0x004CAAB0+4, min_factor);//ok		
		
		exe.replace_val(0x0042D20D+3, gas);				
		exe.replace_val(0x00436828+3, gas);				
		exe.replace_val(0x00453F07+3, gas);				
		exe.replace_val(0x00454142+3, gas);				
		exe.replace_val(0x004541F6+3, gas);				
		exe.replace_val(0x004CA9A0+4, gas);				
		exe.replace_val(0x004CAAC0+4, gas);//ok	

		exe.replace_val(0x0042D205+4, gas_factor);	
		exe.replace_val(0x00436820+4, gas_factor);	
		exe.replace_val(0x00453F00+3, gas_factor);	
		exe.replace_val(0x0045413A+4, gas_factor);	
		exe.replace_val(0x004541EE+4, gas_factor);
//		exe.replace_val(0x004BF183+1, gas_factor);
//		exe.replace_val(0x004BF18D+1, gas_factor);	
//		exe.replace_val(0x004BF2CF+1, gas_factor);	
		exe.replace_val(0x004BF491+1, gas_factor);	
		exe.replace_val(0x004CA9B0+4, gas_factor);	
		exe.replace_val(0x004CAAD0+4, gas_factor);	
		
		exe.replace_val(0x00453FA7+3, time);	
		exe.replace_val(0x004CA9BD+4, time);	
		exe.replace_val(0x004CAADD+4, time);
		
		exe.replace_val(0x00453FA0+3, time_factor);	
		exe.replace_val(0x004CA9C5+4, time_factor);	
		exe.replace_val(0x004CAAE5+4, time_factor);	

		exe.replace_val(0x0046D5C4+4, restriction);	
		exe.replace_val(0x0046DFEE+4, restriction);	
		exe.replace_val(0x0046E0A6+4, restriction);	
		
		
		exe.replace_val(0x004256DC+4, icon);	
		exe.replace_val(0x004265CD+4, icon);	
		
		exe.replace_val(0x004575D9+4, label);	
		exe.replace_val(0x004575F2+4, label);	
		exe.replace_val(0x00457865+4, label);	
		exe.replace_val(0x004578C1+4, label);	
		
		exe.replace_val(0x0045957E+2, repeat);	
		exe.replace_val(0x004B21AE+2, repeat);	
		
		//comparison jumps
		
		let jmp = 0xEB as u8;
		let nops_2 = 0x9090 as u16;
		let nops_4 = 0x90909090 as u32;
		//let je = 0x74 as u8;
		exe.replace_val(0x00454A8D, nops_2);
		
		//
		let config=config::config();
		//remove temporarily to test hook, re-add later
		if !config.debug_upgrade_req {
			exe.replace_val(0x0046DFD6, jmp);
			exe.replace_val(0x0046DFC4, nops_4);			
		}
		
	
		
		
		
		}
		unsafe {
		let game = Game::get();
		let mut increment = true;
		if !self.globals_loaded && initialize_arrays {
			self.length=0;
			
			for p in 0..12 {
				//let mut currents: Vec<u8>;
				//let mut maxs: Vec<u8>;
				//currents = Vec::new();
				//maxs = Vec::new();
				for u in 0..61 {
					//let max = (*game.0).upgrade_limit_bw[p as usize][u as usize];
					//let current = (*game.0).upgrade_level_bw[p as usize][u as usize];	
					let max: u8;
					let current: u8;
					if u <46 {
						max = (*game.0).upgrade_limit_sc[p as usize][u as usize];
						current = (*game.0).upgrade_level_sc[p as usize][u as usize];
					}
					else {
						max = (*game.0).upgrade_limit_bw[p as usize][u-0x2e as usize];
						current = (*game.0).upgrade_level_bw[p as usize][u-0x2e as usize];
					}
					self.upgrade_limit.push(max);
					self.upgrade_level.push(current);
					if increment {
						self.length+=1;
					}
				}
				//add dummy
				self.upgrade_limit.push(1);
				self.upgrade_level.push(0);
				if increment {
					self.length+=1;
				}
				if self.max_repeats.len()>62 {
					//
					for u in 62..self.max_repeats.len() {
						//debug!("Add {} at offset {}",u,self.upgrade_limit.len());
						self.upgrade_limit.push(self.max_repeats[u]);
						self.upgrade_level.push(0);
						if increment {
							self.length+=1;
						} 
					}
				}
				
				increment=false;
				
			}
		}
		
		//exe.replace_val(0x0046D5B8+3, self.length as u8);
		
		//bw_print!("Size of: {}",self.upgrade_limit.len());
		//bw_print!("Upgrade : {} {}",self.upgrade_level[62],self.upgrade_limit[62]);
		
		let upgrade_max = &mut self.upgrade_limit[0] as *mut u8;	
		exe.replace_val(0x00403415+2, self.length as u8);//length of array 
		//DIF: requires hook - 28i
		exe.replace_val(0x00403418+4, upgrade_max);//upgrade 

		exe.replace_val(0x004CE804+2, self.length as u8);
		//DIF: requires hook - 10i
		exe.replace_val(0x004CE80A+3, upgrade_max);//DO NOT COMMENT!
		//
		//
		//
		let upgrade_level = &mut self.upgrade_level[0] as *mut u8;	
		
		exe.replace_val(0x004033F0+2, self.length as u8);//[Done]
		//DIF: requires hook - 10i
		exe.replace_val(0x004033F3+3, upgrade_level);//[Done]
		//
		exe.replace_val(0x004257C7+2, self.length as u8);//[Done]
		//DIF: requires hook - 82i, but I'd rather not hook that
		exe.replace_val(0x004257CD+3, upgrade_level);//[Done]//Ss_DrawWeapon
		
		exe.replace_val(0x0042D1B4+2, self.length as u8);//[Done]
		//DIF: hook, unknown, 45i
		exe.replace_val(0x0042D1B7+3, upgrade_level);	//[Done]	
		//		
		exe.replace_val(0x0042D1F7+2, self.length as u8);//[Done]
		//DIF: hook, unknown, 45i, same as previous
		exe.replace_val(0x0042D1FA+3, upgrade_level);		//[Done]	
		//
		exe.replace_val(0x004325BB+2, self.length as u8);//[Done]
		//DIF: hook, unknown, 38i
		exe.replace_val(0x004325BE+3, upgrade_level);//[Done]			
		//
		exe.replace_val(0x004367D8+2, self.length as u8);//[Done]
		//add spending requests from towns, 392i
		exe.replace_val(0x004367DB+3, upgrade_level);//[Done]
		//
		exe.replace_val(0x00436812+2, self.length as u8);//[Done]
		//same as previous
		exe.replace_val(0x00436815+3, upgrade_level);//[Done]
		//
		exe.replace_val(0x00453EEC+2, self.length as u8);//[done]
		//hook, 20 (gas)
		exe.replace_val(0x00453EF2+3, upgrade_level);	//[done]//DO NOT COMMENT
		//
		exe.replace_val(0x00453F3C+2, self.length as u8);//[done]
		//hook, another, 20 (ore)
		exe.replace_val(0x00453F42+3, upgrade_level);	//[done]//DO NOT COMMENT
		//
		exe.replace_val(0x00453F8C+2, self.length as u8);//[done]
		exe.replace_val(0x00453F92+3, upgrade_level);	//[done]//get upgrade time cost
		
		//get armor level
		exe.replace_val(0x00453FEE+2, self.length as u8);//[done]
		exe.replace_val(0x00453FF4+3, upgrade_level);	//[done]//DO NOT COMMENT
		//
		exe.replace_val(0x00454057+2, self.length as u8);//[done]
		//get armor upgrade
		exe.replace_val(0x0045405A+4, upgrade_level);	//[done]
		//
		exe.replace_val(0x004540D4+2, self.length as u8);//[done]
		//refundUpgrade75percent 
		exe.replace_val(0x004540D7+3, upgrade_level);	//[done]
		//
		exe.replace_val(0x0045412C+2, self.length as u8);//[done]
		//refund75?
		exe.replace_val(0x0045412F+3, upgrade_level);	//[done]
		
		exe.replace_val(0x00454194+2, self.length as u8);//[done]
		//refundUpgradeFull 
		exe.replace_val(0x00454197+3, upgrade_level);//[done]	
		
		exe.replace_val(0x004541E0+2, self.length as u8);//[done]
		//00454170
		exe.replace_val(0x004541E3+3, upgrade_level);//[done]	
		
		/*exe.replace_val(0x004546FF+2, self.length as u8);//[done]
		exe.replace_val(0x00454702+3, upgrade_level);//[done]
		
		exe.replace_val(0x00454747+2, self.length as u8);//[done]
		exe.replace_val(0x0045474A+3, upgrade_level);//[done]*/
		
		//orders_upgrade
		exe.replace_val(0x00454ADC+2, self.length as u8);//[done]
		exe.replace_val(0x00454ADF+3, upgrade_level);//[done]

		
		//BeginUpgrade 00454A80 000000EB
		exe.replace_val(0x0046D9F6+2, self.length as u8);//[done]
		exe.replace_val(0x0046D9F9+3, upgrade_level);	//[done]
		
		//CheckDatRequirements
		exe.replace_val(0x00475E6B+2, self.length as u8);//[done]
		exe.replace_val(0x00475E6E+3, upgrade_level);	//[done]
		//00475E40 = Ss_GetSingleUpgradeBonus(), edx weapon_id, esi Unit *unit
		exe.replace_val(0x00475EF3+2, self.length as u8);//[done]
		exe.replace_val(0x00475EF6+3, upgrade_level);//[done]	
		//00475EC0 = GetUpgradeDmgBonus(), eax Unit *unit, edx weapon_id
		exe.replace_val(0x0047B366+2, self.length as u8);//[done]//??????????????
		exe.replace_val(0x0047B369+3, upgrade_level);	//[done]
		//increaseUpgradeLevel 0047B340 
		exe.replace_val(0x0047B39C+2, self.length as u8);//[done]//??????????????
		
		exe.replace_val(0x0047B39F+3, upgrade_level);//[done]	
		//increaseUpgradeLevel 
		exe.replace_val(0x0048AD0E+2, self.length as u8);//[done](???????????)
		
		/*exe.replace_val(0x0048AD14+3, upgrade_level);	//[done] 
		//set upgrade level -
		exe.replace_val(0x004CE790+2, self.length as u8); //[done]*/
		
		exe.replace_val(0x004CE796+3, upgrade_level);//[done]	
		
		//getUpgradesLevel 004CE7A0 00000022
		exe.replace_val(0x004CE7B4+2, self.length as u8);
		exe.replace_val(0x004CE7BA+3, upgrade_level);
		//

		exe.replace_val(0x004E2FA3+2, self.length as u8);
		//401i - restore unit pointers?
		exe.replace_val(0x004E2FA9+3, upgrade_level);	
		
		//code wrappers	
		//this wrapper patches 54747
		//REPLACE [0xc1, 0xd0, 0xe0, 0xf0] by actual values
		let _lev = upgrade_level as u32;
		let _lim = upgrade_max as u32;
		/*
		let code = [
			0x53, //push ebx
			0x50, //push eax
			0x89, 0xd0, //mov eax,edx
			0xbb, 0xc1, 0xd0, 0xe0, 0xf0, //mov ebx, length value
			0xf7, 0xe3, //mul ebx
			0x89, 0xc2, //mov edx,eax
			0x58, //pop eax
			0x5b, //pop ebx
			0x8a, 0x84, 0x02, 0xfe, 0xf2, 0x58, 0x00, //mov al,BYTE PTR [edx+eax*1+length]
			0xc3, //ret
		];
		let executable = exe.exec_alloc(code.len());//usize
		executable.nop(0x0045474A, 7);
		executable.copy_from_slice(code);//
		let diff = (executable.as_ptr() as u32).wrapping_sub(0x00454747);
		exe.replace_val(0x00454747, diff);
		*/
		
		//New wrapper (do not try!):
		//wrapper_patch(&mut exe, &code,0x00454747,0x0045474A+2,5,self.length as u32);//set upgrade level

		
		// !!!BYTES AFTER MUST BE PATCHED WITH NOPS
		
		//this wrapper replaces IMUL EAX,EAX
		//
		/*
		let call_opc = 0xe8 as u8;
*/
		let u = self.length as u32;
		let u1 = (u & 0xff) as u8;
		let u2 = ((u & 0xff00)>>8) as u8;
		let u3 = ((u & 0xff0000)>>16) as u8;
		let u4 = ((u & 0xff000000)>>24)as u8;/*
		bw_print!("Pointer values: {:x},  {:x} {:x} {:x} {:x}",u,u1,u2,u3,u4);*/
		let code = [	//set upgrade level 004CE770s func
			0x53, //push ebx
			0x52, //push edx
			0xbb,u1,u2,u3,u4, //mov ebx, length value
			0xf7, 0xe3, //mul ebx
			0x5a, //pop edx
			0x5b, //pop ebx (ebx should be multiplied)
			0xf, 0xb7, 0xc9, //movzx ecx,cx 			
			0xc3, //ret
		];
		let ls: [u8;4] = [
		(_lev & 0xff) as u8,
		((_lev & 0xff00)>>8) as u8,
		((_lev & 0xff0000)>>16) as u8,
		((_lev & 0xff000000)>>24) as u8];
		let mx: [u8;4] = [
		(_lim & 0xff) as u8,
		((_lim & 0xff00)>>8) as u8,
		((_lim & 0xff0000)>>16) as u8,
		((_lim & 0xff000000)>>24) as u8];
		/*
		
		let executable = exe.exec_alloc(code.len());//usize
		exe.nop(0x004CE793, 3);
		executable.copy_from_slice(&code);
		let diff = (executable.as_ptr() as u32).wrapping_sub(0x004CE790+5);
		exe.replace_val(0x004CE790+1, diff);
		exe.replace_val(0x004CE790, call_opc);*/
		//
		//
//pub unsafe fn wrapper_patch(&mut exe, &code: &[u8], address: usize, nop_address: usize, nops: usize, length: u32){
		wrapper_patch(&mut exe, &code,0x004CE790,0x004CE793,3);//set upgrade level
		
		//0045474A+2 (5 nops)
		let code = [
			0x52,	//push edx
			0x56,	//push esi
			0x89,0xc6,	//mov esi,eax
			0xb8,u1,u2,u3,u4,	//mov eax,length
			0xf7,0xe2,	//mul edx
			0x89,0xc2,	//mov edx,eax
			0x89,0xf0,	//mov eax,esi
			0x8a,0x84,0x02,ls[0], ls[1], ls[2], ls[3],	//mov al,byte ptr ds:[edx+eax+level offset]
			0x5e,	//pop esi
			0x5a,	//pop edx
			0xc3,	//ret
		];
		wrapper_patch(&mut exe, &code,0x00454747,0x0045474A+2,5);//order_upgrade_1
		
		let code_546ff = [	//set upgrade level 004CE770s func (2)
			0x53, //push ebx
			0x52, //push edx
			0xbb,u1,u2,u3,u4, //mov ebx, length value
			0xf7, 0xe3, //mul ebx
			0x8a, 0x84, 0x08, ls[0], ls[1], ls[2], ls[3],	
			//mov al,byte ptr ds:[eax+ecx*1+offset]
			0x5a, //pop edx
			0x5b, //pop ebx
			0xc3, //ret
		];
		wrapper_patch(&mut exe, &code_546ff,0x004546FF,0x00454702+2,5);//order_upgrade_2
		
		
		//start try
		
		let code = [ 
			0x50,	//push eax 
			0xB8, u1, u2, u3, u4, 
			0xF7, 0xE2, 	//mul edx
			0x89, 0xC2, 	//mov edx,eax
			0x8A, 0x94, 0x32, ls[0], ls[1], ls[2], ls[3],	 
			//mov dl,byte ptr ds:[edx+esi+offset]
			0x58, 	//pop eax
			0xC3,	//ret
		];
		wrapper_patch(&mut exe, &code,0x004033F0,0x004033F3+2,5);//isUnitUpgradeAvailable 004033D0 00000057
		
		let code = [
			0x50, 
			0x89, 0xC2, 
			0xB8, u1, u2, u3, u4, 
			0xF7, 0xE2, 
			0x58, 
			0x0F, 0xB7, 0xD1, 
			0xC3,
		];
		wrapper_patch(&mut exe, &code,0x004257C7,0x004257CA+2,1);//Ss_DrawWeapon()
		
		let code = [ 0x52, 0x50, 0x57, 0xB8, u1, u2, u3, u4, 0xF7, 0xE7, 0x89, 0xC7, 0x8A, 0x9C, 0x1F, 
		ls[0], ls[1], ls[2], ls[3],	 
		0x5f, 0x58, 0x5A, 0xC3 ];		
		
		
		wrapper_patch(&mut exe, &code,0x0042D1B4,0x0042D1B7+2,5);//unk len45 func
		// (called from {AI_HasUpgradeResources 00448750 00000055} and {BeginUpgrade 00454A80 000000EB}
		//this is also bug I think, and should be fixed
		//should be checked, I think edx is not preserved
		
		
		//u - length 
		//ls - level 
/*		let code = [ 0x52, 0xB8, u1, u2, u3, u4, 0xF7, 0xE7, 0x8A, 0x9C, 0x37, 
		ls[0], ls[1], ls[2], ls[3],	 
		0x5A, 0xC3 ];*/
		
		let code = [0x52, 0x50, 0xB8, 
			u1, u2, u3, u4,
			0xF7, 0xE7, 0x8A, 0x9C, 0x30, 
			ls[0], ls[1], ls[2], ls[3],	 
			0x58, 0x5A, 0xC3];
		wrapper_patch(&mut exe, &code,0x0042D1F7,0x0042D1FA+2,5);//unk len45 func, 2nd case
		//SOURCE OF BUG!????
		/*
		0:  52                      push   edx
		1:  b8 00 00 00 00          mov    eax,length (???)
		6:  f7 e7                   mul    edi
		8:  8a 9c 37 00 00 00 00    mov    bl,BYTE PTR [edi+esi*1+0x0]
		f:  5a                      pop    edx
		10: c3                      ret
		*/
		
		/*
		I think correct is:
		0:  52                      push   edx
									push eax 
		1:  b8 00 00 00 00          mov    eax,length (???)
		6:  f7 e7                   mul    edi
		8:  8a 9c 37 00 00 00 00    mov    bl,BYTE PTR [eax+esi*1+0x0]
		
									pop eax
		f:  5a                      pop    edx
		10: c3                      ret
		*/
		
		//level is BL I think, not EAX
		
		
		let code = [ 0x56, 0x89, 0xC6, 0xB8, u1, u2, u3, u4, 0xF7, 0xE2, 0x89, 0xC2, 0x89, 0xF0, 0x8A, 0x84, 0x02, 
		ls[0], ls[1], ls[2], ls[3],	
		0x5E, 0xC3];
		wrapper_patch(&mut exe, &code,0x004325BB,0x004325BE+2,5);//unk len38 func//called from AI_UpgradesFinished 00436320 00000539
		//called from (? - prev function 45len)
		////
		let code = [ 0x50, 0xB8, u1, u2, u3, u4, 0xF7, 0xE2, 0x89, 0xC2, 0x8A, 0x94, 0x32, 
		ls[0], ls[1], ls[2], ls[3], 
		0x58, 0xC3, ];
		wrapper_patch(&mut exe, &code,0x004367D8,0x004367DB+2,5);//392i spending requests func
		
		let code = [ 0x50, 0xB8, u1, u2, u3, u4, 0xF7, 0xE1, 0x89, 0xC1, 0x8A, 0x8C, 0x39, 
		ls[0], ls[1], ls[2], ls[3], 
		0x58, 0xC3 ];
		wrapper_patch(&mut exe, &code,0x00436812,0x00436815+2,5);//same as before
		/////
		
		let code = [ 0x53, 0xBB, u1, u2, u3, u4, 0xF7, 0xE3, 0x0F, 0xB7, 0xD1, 0x5B, 0xC3 ];
		
		wrapper_patch(&mut exe, &code,0x00453EEC,0x00453EEF+2,1);//20i gas func
		wrapper_patch(&mut exe, &code,0x00453F3C,0x00453F3F+2,1);//20i ore func (same wrapper)
		wrapper_patch(&mut exe, &code,0x00453F8C,0x00453F8F+2,1);//getUpgradeTimeCost  (same wrapper)
		
		////////
		let code = [ 0x53, 0xBB, u1, u2, u3, u4, 0xF7, 0xE3, 0x0F, 0xB7, 0xD1, 0x5B, 0xC3 ];
		wrapper_patch(&mut exe, &code,0x00453FEE,0x00453FF1+2,1);//get armor upgrade
		
		let code = [ 0x53, 0x52, 0xBB, u1, u2, u3, u4, 0xF7, 0xE3, 0x0F, 0xB6, 0x84, 0x08, ls[0], ls[1], ls[2], ls[3], 0x5a, 0x5B, 0xC3 ];
		wrapper_patch(&mut exe, &code,0x00454057,0x0045405A+2,6);//get armor upgrade2
		//
		
		
		let code = [ 0x53, 0x50, 0xBB, u1, u2, u3, u4, 0xF7, 0xE3, 0x8A, 0x84, 0x10, ls[0], ls[1], ls[2], ls[3], 0x58, 0x5B, 0xC3 ];
		
		wrapper_patch(&mut exe, &code,0x004540D4,0x004540D7+2,5);//refund75
		
		
		let code = [ 0x50, 0x53, 0xB8, u1,u2,u3,u4, 0x89, 0xC3, 0xF7, 0xE2, 0x89, 0xC2, 0x89, 0xD8, 0x8A, 0x84, 0x02,
			ls[0], ls[1], ls[2], ls[3], 0x5B, 0x58, 0xC3 ];
		wrapper_patch(&mut exe, &code,0x0045412C,0x0045412F+2,5);//refund75(?)

		let code = [ 0x50, 0xB8, u1,u2,u3,u4, 0xF7, 0xE2, 0x89, 0xC2, 0x8A, 0x94, 0x3A, ls[0],ls[1],ls[2],ls[3], 0x58, 0xC3 ];
		simple_wrapper(&mut exe, &code,0x00454194,5);//refund100(?)
		/////
		
		let code = [ 0x50, 0xB8, u1,u2,u3,u4, 0xF7, 0xE2, 0x89, 0xC2, 0x8A, 0x94, 0x0A, ls[0],ls[1],ls[2],ls[3], 0x58, 0xC3];
		simple_wrapper(&mut exe, &code,0x004541E0,5);//refund 100(2nd)
		
		wrapper_patch(&mut exe, &code_546ff,0x00454ADC,0x00454ADF+2,5);//start_upgrade, same wrapper is earlier (imul eax,eax; al = eax+ecx+offset)
		//check dat requirements wrappers
		let code = [ 0x50, 0x52, 0xB8, u1, u2, u3, u4, 0xF7, 0xE2, 0x89, 0xC2, 0x8A, 0x8C, 0x0A, 
		ls[0], ls[1], ls[2], ls[3], 0x5a, 0x58, 0xC3 ];
		//0x52 / 0x5a is not required? added to check the crash	
		wrapper_patch(&mut exe,&code,0x0046D9F6,0x0046D9F9+2,5);//begin_upgrade
		
		let code = [ 0x50, 0x52, 0xB8, u1, u2, u3, u4, 0xF7, 0xE2, 0x89, 0xC2, 0x8A, 0x8C, 0x0A, 
		ls[0], ls[1], ls[2], ls[3], 0x5a, 0x58, 0xC3 ];	
		wrapper_patch(&mut exe, &code,0x00475E6B,0x00475E6E+2,5);//checkdatreq
		//check dat requirements wrappers

		let code = [ 0x50, 0xB8, u1, u2, u3, u4, 0xF7, 0xE2, 0x89, 0xC2, 0x8A, 0x8C, 0x0A, 
		ls[0], ls[1], ls[2], ls[3], 0x58, 0xC3 ];
		wrapper_patch(&mut exe,&code,0x0046D9F6,0x0046D9F9+2,5);//get upgrade level		
			
		let code_ce7b4 = [ 0x52, 0xBA, u1, u2, u3, u4, 0xF7, 0xE2, 0x0F, 0xB7, 0xD1, 0x5A, 0xC3 ];
		//get upgrade level
		wrapper_patch(&mut exe,&code_ce7b4,0x004CE7B4,0x004CE7B7+2,1);
		//new bug
		
		//401i-restore unit pointers
		wrapper_patch(&mut exe,&code_ce7b4,0x004E2FA3,0x004E2FA6+2,1);//same as before		
		
		// UPGRADE MAX LIMIT
		let code = [ 0x53, 0x52, 0xBB, u1,u2,u3,u4, 0xF7, 0xE3, 0x0F, 0xB6, 0x84, 0x08, 
					mx[0],mx[1],mx[2],mx[3], 0x5A, 0x5B, 0xC3 ];
		wrapper_patch(&mut exe,&code,0x00403415,0x00403418+2,6);//DIF: req hook - 28i - isUnitUpgradeAvailable 004033D0 00000057
			
		let code = [ 0x53, 0xBB, u1, u2, u3, u4, 0xF7, 0xE3, 0x0F, 0xB7, 0xD1, 0x5B, 0xC3 ];
		//let code = [ 0x53, 0x50, 0xBB, u1, u2, u3, u4, 0xF7, 0xE3, 0x0F, 0xB7, 0xD1, 0x58, 0x5B, 0xC3 ];
		wrapper_patch(&mut exe,&code,0x004CE804,0x004CE807+2,1);//DIF: req hook - 10i, called from UpgradeAllowed 0046DFC0 00000119

		
		
		
		
		
		}//unsafe
		if !self.globals_loaded {
			self.globals_loaded = true;
		}
		//debug!("Extended length: {}", self.upgrade_level.len());
/*
		debug!("Data ext: m {} {} g {} {} t {} {} r {} if {} l {} r {} mr {} bf {} u {} {}",self.mineral_cost_base.len(),self.mineral_cost_factor.len(),
									   self.gas_cost_base.len(),self.gas_cost_factor.len(),
									   self.time_cost_base.len(),self.time_cost_factor.len(),
									   self.restriction_flags.len(),self.icon_frame.len(),
										self.label.len(), self.race.len(), self.max_repeats.len(),
										self.broodwar_flag.len(), self.upgrade_limit.len(), self.upgrade_level.len()
										);*/

		
		
	}
	pub fn load_extended_upgrades(&mut self){
		unsafe {
		
		let (data, len) = match samase::read_file("samase\\upgrades_ext.txt") {
            Some(s) => s,
            None => { return;},
        };
		let slice = std::slice::from_raw_parts(data, len);
		for line in slice.split(|&x| x == b'\n') 
		{
            let line = String::from_utf8_lossy(line);
            let line = line.trim();
            if line.starts_with("#") || line.starts_with(";") || line.is_empty() {
                continue;
            }
			let command_list: Vec<&str> = line.split(':').collect();
			if command_list.len()==2 {
				let name = command_list[0];
				let value: u32 = command_list[1].trim().parse().unwrap();
				match name {
					"ore_cost"=> { self.mineral_cost_base.push(value as u16); },
					"ore_cost_factor"=> { self.mineral_cost_factor.push(value as u16); },					
					"gas_cost"=> { self.gas_cost_base.push(value as u16); },
					"gas_cost_factor"=> { self.gas_cost_factor.push(value as u16); },					
					"time_cost"=> { self.time_cost_base.push(value as u16); },
					"time_cost_factor"=> { self.time_cost_factor.push(value as u16); },					
					"reqindex"=> { self.restriction_flags.push(value as u16); },					
					"icon"=> { self.icon_frame.push(value as u16); },	
					"label"=> { self.label.push(value as u16); },		
					"race"=> { self.race.push(value as u8); },		
					"max_repeats"=> { self.max_repeats.push(value as u8); },	
					"broodwar"=> { self.broodwar_flag.push(value as u8); },
					_=>{ bw_print!("Error! Wrong upgrades_ext entry: {} {}",name,value);
						 return; },
				}
			}
        }

		}
	}
	
    fn new() -> Upgrades {
		let upg = Upgrades {
			mineral_cost_base: Vec::new(),
			mineral_cost_factor: Vec::new(),
			gas_cost_base: Vec::new(),
			gas_cost_factor: Vec::new(),
			time_cost_base: Vec::new(),
			time_cost_factor: Vec::new(),
			restriction_flags: Vec::new(),
			icon_frame: Vec::new(),
			label: Vec::new(),
			race: Vec::new(),
			max_repeats: Vec::new(),
			broodwar_flag: Vec::new(),
			upgrade_limit: Vec::new(),
			upgrade_level: Vec::new(),
			length: 0,
			globals_loaded: false,
			upgrades_firegraft_offsets: Vec::new(),
			
			
		};
		upg
		
		
	}
}


/// Cycles through region ids, so each player do one heavy region-specific check per frame
/// to distribute load.
#[derive(Default, Serialize, Deserialize)]
pub struct RegionIdCycle {
    pos: [u16; 8],
    region_count: u16,
}


impl RegionIdCycle {
    pub fn is_inited(&self) -> bool {
        self.region_count != 0
    }

    pub fn init(&mut self, region_count: u16) {
        self.pos = [0; 8];
        self.region_count = region_count;
    }

    /// Returns player id and region id iter for the player.
    /// If the region id iter is not fully consumed, it'll continue next time from where
    /// it left off. Otherwise it'll reset to 0.
    pub fn cycle_all<'a>(&'a mut self) -> impl Iterator<Item = (u8, RegionCycleIter<'a>)> {
        let limit = self.region_count;
        self.pos.iter_mut().enumerate().map(move |(player, pos)| {
            let iter = RegionCycleIter {
                pos,
                limit,
            };
            (player as u8, iter)
        })
    }
}

pub struct RegionCycleIter<'a> {
    pos: &'a mut u16,
    limit: u16,
}

impl<'a> Iterator for RegionCycleIter<'a> {
    type Item = u16;
    fn next(&mut self) -> Option<Self::Item> {
        if *self.pos >= self.limit {
            *self.pos = 0;
            None
        } else {
            let result = *self.pos;
            *self.pos += 1;
            Some(result)
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Globals {
    pub attack_timeouts: [AttackTimeoutState; 8],
    pub idle_orders: IdleOrders,
    pub kills_table: KillsTable,
    pub base_layouts: BaseLayouts,
    pub lift_lands: LiftLand,
    pub queues: Queues,
	pub build_max: BuildMaxSet,
    pub max_workers: Vec<MaxWorkers>,
    pub town_ids: Vec<TownId>,
    pub bunker_states: BunkerCondition,
    pub renamed_units: RenameUnitState,
    pub guards: GuardState,
    pub bank: Bank,
	pub unit_replace: UnitReplace,							   
    pub reveal_states: Vec<RevealState>,
    pub under_attack_mode: [Option<bool>; 8],
	pub placement_check_mode: [bool; 8],		
	pub waygating: Waygating,
    pub ai_mode: [AiMode; 8],
    pub region_safety_pos: RegionIdCycle,
    // For tracking deleted towns.
    // If the tracking is updated after step_objects, it shouldn't be possible for a town
    // to be deleted and recreated in the same frame. (As recreation happens in scripts,
    // and deletion happens on last unit dying) Better solutions won't obviously hurt though.
    pub towns: Vec<Town>,
    pub rng: Rng,
	//project hydra
//	pub creep_range: u32,
	pub idle_tactics: IdleTactics,
	pub upgrades: Upgrades,
	pub ngs: NewGameState,
	pub triggers: TriggerState,
	pub events: RegisterState,
	pub town_states: TownState,
//	pub flingy_extender: FlingyExtender,
	pub attack_extender: [AttackExtender; 8],
	//pub trains: TrainState,
	
	pub init: bool,
	//
	//
    #[serde(serialize_with = "aiscript::serialize_scripts")]
    #[serde(deserialize_with = "aiscript::deserialize_scripts")]
    pub ai_scripts: BlockAllocSet<aiscript::Script>,
	pub last_frame_search: UnitSearch,
	pub rectangles: Vec<bw::Rect>,
	pub gptp_save_bytes: Vec<u8>,
}
impl Globals {
    fn new() -> Globals {
        Globals {
            attack_timeouts: [AttackTimeoutState::new(); 8],
            idle_orders: Default::default(),
            kills_table: Default::default(),
            base_layouts: Default::default(),
            lift_lands: Default::default(),
            queues: Default::default(),
			build_max: Default::default(),
            max_workers: Vec::new(),
            town_ids: Vec::new(),
			idle_tactics: Default::default(),
            reveal_states: Vec::new(),
            bunker_states: Default::default(),
            renamed_units: Default::default(),
            guards: GuardState::new(),
            bank: Default::default(),
			unit_replace: Default::default(),	
			attack_extender: Default::default(),
            under_attack_mode: [None; 8],
			placement_check_mode: [false; 8],			
            ai_mode: [Default::default(); 8],
            region_safety_pos: RegionIdCycle::default(),
            towns: Vec::new(),
            rng: Default::default(),
            ai_scripts: BlockAllocSet::new(),
			//hydra
			upgrades: Upgrades::new(),
			ngs: Default::default(),
			waygating: Waygating::new(),
			init: false,
			triggers: Default::default(),
			events: Default::default(),
			town_states: Default::default(),
			last_frame_search: Default::default(),
			gptp_save_bytes: Vec::new(),
			rectangles: Vec::new(),
//			flingy_extender: FlingyExtender::new(),
//			creep_range: 0x9c4000,
        }
    }


    // Should only be called on hook start to prevent deadlocks.
    // Inline never since it keeps getting inlined and lazy_static init code is fat ;_;
    #[inline(never)]
    pub fn get(caller: &'static str) -> MutexGuard<'static, Globals> {
        GLOBALS.lock(caller)
    }


	pub fn set_unit_search(&mut self){
		unsafe {
		self.last_frame_search = UnitSearch::from_bw();
		}
	}
	
    pub fn unit_removed(&mut self, unit: Unit) {
        self.idle_orders.unit_removed(unit);
        self.idle_tactics.unit_removed(unit);	
		self.bunker_states.unit_removed(unit);
        self.lift_lands.unit_removed(unit);
//		self.ngs.unit_removed(unit);//moved to order hook
		self.waygating.unit_removed(unit);	
		self.queues.adjust_removed_unit(unit);
		self.last_frame_search.values.swap_retain(|(u,_rect)| *u!=unit);
		self.triggers.variables.swap_retain(|x| x.1.unit!=Some(unit));
    }
}

pub fn save_state(caller: &'static str) -> MutexGuard<'static, Option<SaveState>> {
    SAVE_STATE.lock(caller)
}

pub unsafe extern fn init_game() {
    aiscript::invalidate_cached_unit_search();
    *Globals::get("init") = Globals::new();
    #[cfg(feature = "opengl")]
    crate::gl::game_init();
}

pub unsafe extern fn wrap_save(
    data: *const u8,
    len: u32,
    _player: u32,
    _unique_player: u32,
    orig: unsafe extern fn(*const u8, u32),
) {
    trace!("Saving..");
    let mut globals = Globals::get("before save");
    aiscript::claim_bw_allocated_scripts(&mut globals);

    let first_ai_script = bw::first_ai_script();
    bw::set_first_ai_script(null_mut());
    defer!({
        bw::set_first_ai_script(first_ai_script);
    });
    *save_state("init save state") = Some(SaveState {
        first_ai_script: SendPtr(first_ai_script),
    });
    drop(globals);

    orig(data, len);
}

pub unsafe extern fn save(set_data: unsafe extern fn(*const u8, usize)) {
    let mut globals = Globals::get("save");//wasn't mut previously
    unit::init_save_mapping();
    aiscript::init_save_mapping();
    defer!(aiscript::clear_save_mapping());
    defer!(unit::clear_save_mapping());
	let config = config::config();
	let gptp_test = bw::gptp_save();
	
/*	if config.gptp_globals {
		let gptp = &*bw::gptp_save();
		let slice = std::slice::from_raw_parts(gptp.ptr, gptp.len as usize);
		globals.gptp_save_bytes = slice.iter().cloned().collect();	
//		bw_print!("Ptr: {:?}, len {:?}",gptp.ptr,gptp.len);
	}*/
    match bincode::serialize(&*globals) {
        Ok(o) => {
            set_data(o.as_ptr(), o.len());
        }
        Err(e) => {
            error!("Couldn't save game: {}", e);
            bw_print!("(Aise) Couldn't save game: {}", e);
        }
    }
	globals.gptp_save_bytes.clear();
}

use first_frame_event;
pub unsafe extern fn load(ptr: *const u8, len: usize) -> u32 {
    aiscript::invalidate_cached_unit_search();
    unit::init_load_mapping();
    defer!(unit::clear_load_mapping());
    aiscript::init_load_mapping();
    defer!(aiscript::clear_load_mapping());
    let slice = slice::from_raw_parts(ptr, len);
    let mut data: Globals = match bincode::deserialize(slice) {
        Ok(o) => o,
        Err(e) => {
            error!("Couldn't load game: {}", e);
            return 0;
        }
    };
	let config = config::config();
	//get ptr and len from
/*	if config.gptp_globals {
		if data.gptp_save_bytes.len()>0 {
			bw::gptp_load(&mut data.gptp_save_bytes[0] as *const u8,data.gptp_save_bytes.len() as u32);
			data.gptp_save_bytes.clear();
		}
		else {
			bw_print!("Error: Length is 0");
		}
	}*/
		
	
	//upgrade ext for projects
	
	data.upgrades.globals_loaded = false;
	if config.extended_upgrades {
		let mut init_check = true;
		if data.upgrades.upgrade_limit.len() > 0 {
			init_check = false;
		}
		data.upgrades.copy_initial_from_dattable(init_check);
		
		let game = Game::get();
		first_frame_event(&mut data,game,false);
	}
    *Globals::get("load") = data;
    1
}
