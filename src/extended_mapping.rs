use std::ffi::CString;
use std::collections::hash_map::Entry;
use samase;
use aiscript::{self};
use config::{self};
use game::Game;
use bw;
use bitflags::bitflags;
use globals::{
    self, Globals, Faction, OverlordFaction,
};

#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct ExtUpg {
	pub upgrade_id: i32,
	pub initial: u8,
	pub max: u8,
	pub escalation_status: u16,
}

//const EXT_ED_VERSION: u32 = 1000;

#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct ExtendedEditorData {
	pub version: u32,
	pub disposition: Vec<Vec<u8>>,
	pub factions: Vec<FData>,
	pub upgrades: Vec<Vec<ExtUpg>>,
	pub alliances: Vec<Vec<u8>>,
}

#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct FData {
	pub i: u8,
	pub index: u16,//faction index
}

pub unsafe fn map_data_setup(map_data: ExtendedEditorData, globals: &mut Globals, game: Game){
	//debug!("Len: {}",map_data.upgrades.len());
	for (player_a,i) in map_data.disposition.iter().enumerate(){
		for (player_b, j) in i.iter().enumerate() {
			/*if(player_a==0){
				bw_print!("Disposition: {} {} -> {}",player_a,player_b,j);
			}*/
			//debug!("Disposition: {} {} -> {}",player_a,player_b,j);
			if player_a<=player_b {
				if player_a==0{
				//bw_print!("Disposition: {} {} -> {}",player_a,player_b,j);
					globals.ngs.diplomacy[player_a as usize].disposition[player_b as usize] = *j as u32;
					globals.ngs.diplomacy[player_b as usize].disposition[player_a as usize] = *j as u32;	
				}
			}
		}
	}
	globals.ngs.factions.clear();
	let (data, len) = match samase::read_file("factions.ini") {
		Some(s) => s,
		None => { bw_print!("No faction data"); return;},
    };
	let slice = std::slice::from_raw_parts(data, len);
	for line in slice.split(|&x| x == b'\n') 
	{	
		let line = String::from_utf8_lossy(line);
		let line = line.trim();
		if line.starts_with("#") || line.starts_with(";") || line.is_empty() {
			continue;
		}
		let command_list: Vec<&str> = line.split('\t').collect();		
		if command_list.len()==5 {
			let id = command_list[0].trim().parse().unwrap();
			let faction_id = match id {
				1=>OverlordFaction::KelMorianCombine,
				2=>OverlordFaction::TalQiratGuild,
				3=>OverlordFaction::AlphaSquadron,
				4=>OverlordFaction::GammaSquadron,
				5=>OverlordFaction::NovaSquadron,
				6=>OverlordFaction::EpsilonSquadron,
				7=>OverlordFaction::OmegaSquadron,
				8=>OverlordFaction::ColonialMilitia,
				9=>OverlordFaction::Umojans,
				10=>OverlordFaction::Pirates,
				11=>OverlordFaction::Miners,
				12=>OverlordFaction::Scavengers,
				13=>OverlordFaction::Mercenaries,
				14=>OverlordFaction::Cerberus,
				15=>OverlordFaction::SteelCompany,
				_=>OverlordFaction::NoFaction,
			};
			let icon_id = command_list[3].trim().parse().unwrap();
			let string_id = command_list[4].trim().parse().unwrap();
			let faction = Faction { name: CString::new(command_list[1].trim()).unwrap(), 
								color_index: command_list[2].trim().parse().unwrap(), faction_id, icon_id, string_id: string_id};
			/*let mut result = match globals.ngs.factions.entry(id as u16) {
			   Entry::Vacant(entry) => entry.insert(faction),
			   Entry::Occupied(entry) => entry.into_mut(),
			};*/
			match globals.ngs.factions.entry(id as u16) {
			   Entry::Vacant(entry) => {
					entry.insert(faction);
				},
			   Entry::Occupied(_) => {},
			}
		}
//		bw_print!("Size: {}",command_list.len());
	}

	
	for (player, faction) in map_data.factions.iter().enumerate(){
		let result = match globals.ngs.factions.entry(faction.index as u16) {
		   Entry::Vacant(_) => { return; },
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
//		bw_print!("Map faction data: {} {}",faction.i,faction.index);
//		bw_print!("Set {} faction to {}",player,faction.index);
		globals.ngs.diplomacy[player as usize].faction = result.faction_id;
		
	}
	let config = config::config();
	for (player,i) in map_data.upgrades.iter().enumerate(){
		//debug!("Player Id in MapData: {}",player);
		for (upgrade_id, j) in i.iter().enumerate() {
			if upgrade_id>=62 {
				//bw_print!("set upgrade...");
				aiscript::set_new_upgrade_level_current(game,globals,upgrade_id as u16, player as u8, j.initial as u8);
				aiscript::set_new_upgrade_level_max(game,globals,upgrade_id as u16, player as u8, j.max as u8);
				if j.escalation_status != 0xe4 {
					
					let unit_id = j.escalation_status;
					let escalation = match config.escalations.esc_list.clone().iter_mut().find(|x| x.upg_id==upgrade_id as u32){
						Some(_s)=>true,
						None=>false,
					};
					let old_uid = globals.ngs.get_escalation_from_upgrade(upgrade_id as u16,player as u32);
					let new_uid = globals::reverse_escalation(old_uid.0);
					if escalation && old_uid.0!=unit_id && new_uid==unit_id {
						aiscript::escalate(game,globals,player as u8,upgrade_id as u16);
					}
				}
			}
		}
	}
	let game = Game::get();
	for (player_a,i) in map_data.alliances.iter().enumerate(){
		for (player_b, j) in i.iter().enumerate() {
			if player_a>player_b {
				let status = *j as u32;
				match status {
					0=>{
						//bw_print!("E");
						game.set_alliance(player_a as u8,player_b as u8,0);
					},
					1=>{
						//bw_print!("N");
						game.set_alliance(player_a as u8,player_b as u8,2);
					},
					2=>{
						//bw_print!("{} and {}: A",);
						game.set_alliance(player_a as u8,player_b as u8,2);
						bw::visions[player_a as usize] |= 1<<player_b;
						bw::visions[player_b as usize] |= 1<<player_a;
						if config.comm_tower_struct {
							globals.ngs.diplomacy[player_a as usize].alliance_status[player_b as usize] = true;
							globals.ngs.diplomacy[player_b as usize].alliance_status[player_a as usize] = true;											
						}
					},
					_=>{},
				}
			}
		}
	}
}
