use std::mem;
use std::ptr::null_mut;

use libc::c_void;
use winapi::um::processthreadsapi::{GetCurrentProcess, TerminateProcess};

use samase_shim::PluginApi;

use bw;
use bw_dat;
use order::OrderId;
use unit::UnitId;
use windows;
use config;
use extended_mapping::ExtendedEditorData;

struct GlobalFunc<T: Copy>(Option<T>);

impl<T: Copy> GlobalFunc<T> {
    fn get(&self) -> T {
        self.0.unwrap()
    }

    fn try_init(&mut self, val: Option<*mut c_void>) -> bool {
        let val = match val {
            Some(s) => s,
            None => return false,
        };
        unsafe {
/*            assert_eq!(mem::size_of::<T>(), 4);
            let mut typecast_hack: T = mem::uninitialized();
            *(&mut typecast_hack as *mut T as *mut *mut c_void) = val;
            self.0 = Some(typecast_hack);*/
			assert_eq!(mem::size_of::<T>(), mem::size_of::<*mut c_void>());
            let mut typecast_hack: mem::MaybeUninit<T> = mem::MaybeUninit::uninit();
            *(typecast_hack.as_mut_ptr() as *mut *mut c_void) = val;
            self.0 = Some(typecast_hack.assume_init());

        }
        true
    }

    fn init(&mut self, val: Option<*mut c_void>, desc: &str) {
        if !self.try_init(val) {
            fatal(&format!("Can't get {}", desc));
        }
    }
}

fn fatal(text: &str) -> ! {
    let msg = format!("This StarCraft version is not supported :(\n({})", text);
    windows::message_box("Aiscript extension plugin", &msg);
    unsafe {
        TerminateProcess(GetCurrentProcess(), 0x4230daef);
    }
    unreachable!();
}

static mut GAME: GlobalFunc<fn() -> *mut bw::Game> = GlobalFunc(None);
pub fn game() -> *mut bw::Game {
    unsafe { GAME.get()() }
}

static mut AI_REGIONS: GlobalFunc<fn() -> *mut *mut bw::AiRegion> = GlobalFunc(None);
pub fn ai_regions(player: u32) -> *mut bw::AiRegion {
    assert!(player < 8);
    unsafe { *(AI_REGIONS.get()()).offset(player as isize) }
}

static mut PLAYER_AI: GlobalFunc<fn() -> *mut bw::PlayerAiData> = GlobalFunc(None);
pub fn player_ai(player: u32) -> *mut bw::PlayerAiData {
    assert!(player < 8);
    unsafe { (PLAYER_AI.get()()).offset(player as isize) }
}

static mut FIRST_ACTIVE_UNIT: GlobalFunc<fn() -> *mut bw::Unit> = GlobalFunc(None);
pub fn first_active_unit() -> *mut bw::Unit {
    unsafe { FIRST_ACTIVE_UNIT.0.map(|x| x()).unwrap_or(null_mut()) }
}

static mut FIRST_HIDDEN_UNIT: GlobalFunc<fn() -> *mut bw::Unit> = GlobalFunc(None);
pub fn first_hidden_unit() -> *mut bw::Unit {
    unsafe { FIRST_HIDDEN_UNIT.0.map(|x| x()).unwrap_or(null_mut()) }
}

static mut FIRST_AI_SCRIPT: GlobalFunc<fn() -> *mut bw::AiScript> = GlobalFunc(None);
pub fn first_ai_script() -> *mut bw::AiScript {
    unsafe { FIRST_AI_SCRIPT.0.map(|x| x()).unwrap_or(null_mut()) }
}

static mut SET_FIRST_AI_SCRIPT: GlobalFunc<fn(*mut bw::AiScript)> = GlobalFunc(None);
pub fn set_first_ai_script(value: *mut bw::AiScript) {
    unsafe {
        SET_FIRST_AI_SCRIPT.0.map(|x| x(value));
    }
}

static mut FIRST_FREE_AI_SCRIPT: GlobalFunc<fn() -> *mut bw::AiScript> = GlobalFunc(None);
pub fn first_free_ai_script() -> *mut bw::AiScript {
    unsafe { FIRST_FREE_AI_SCRIPT.0.map(|x| x()).unwrap_or(null_mut()) }
}

static mut SET_FIRST_FREE_AI_SCRIPT: GlobalFunc<fn(*mut bw::AiScript)> = GlobalFunc(None);
pub fn set_first_free_ai_script(value: *mut bw::AiScript) {
    unsafe {
        SET_FIRST_FREE_AI_SCRIPT.0.map(|x| x(value));
    }
}

static mut GUARD_AIS: GlobalFunc<fn() -> *mut bw::GuardAiList> = GlobalFunc(None);
pub fn guard_ais() -> *mut bw::GuardAiList {
    unsafe { GUARD_AIS.0.map(|x| x()).unwrap_or(null_mut()) }
}

static mut PATHING: GlobalFunc<fn() -> *mut bw::Pathing> = GlobalFunc(None);
pub fn pathing() -> *mut bw::Pathing {
    unsafe { PATHING.0.map(|x| x()).unwrap_or(null_mut()) }
}

static mut PLAYERS: GlobalFunc<fn() -> *mut bw::Player> = GlobalFunc(None);
pub fn players() -> *mut bw::Player {
    unsafe { PLAYERS.get()() }
}

static mut PLAYER_AI_TOWNS: GlobalFunc<fn() -> *mut bw::AiTownList> = GlobalFunc(None);
pub fn active_towns() -> *mut bw::AiTownList {
    unsafe { PLAYER_AI_TOWNS.0.map(|x| x()).unwrap_or(null_mut()) }
}

static mut UNITS_DAT: GlobalFunc<fn() -> *mut bw_dat::DatTable> = GlobalFunc(None);
pub fn units_dat() -> *mut bw_dat::DatTable {
    unsafe { UNITS_DAT.get()() }
}

static mut WEAPONS_DAT: GlobalFunc<fn() -> *mut bw_dat::DatTable> = GlobalFunc(None);
pub fn weapons_dat() -> *mut bw_dat::DatTable {
    unsafe { WEAPONS_DAT.get()() }
}

static mut UPGRADES_DAT: GlobalFunc<fn() -> *mut bw_dat::DatTable> = GlobalFunc(None);
pub fn upgrades_dat() -> *mut bw_dat::DatTable {
    unsafe { UPGRADES_DAT.get()() }
}

static mut TECHDATA_DAT: GlobalFunc<fn() -> *mut bw_dat::DatTable> = GlobalFunc(None);
pub fn techdata_dat() -> *mut bw_dat::DatTable {
    unsafe { TECHDATA_DAT.get()() }
}

static mut ORDERS_DAT: GlobalFunc<fn() -> *mut bw_dat::DatTable> = GlobalFunc(None);
pub fn orders_dat() -> *mut bw_dat::DatTable {
    unsafe { ORDERS_DAT.get()() }
}

static mut GET_REGION: GlobalFunc<fn(u32, u32) -> u32> = GlobalFunc(None);
pub fn get_region(x: u32, y: u32) -> u32 {
    unsafe { GET_REGION.get()(x, y) }
}

static mut DAT_REQUIREMENTS: GlobalFunc<fn(u32, u32) -> *const u16> = GlobalFunc(None);
pub fn requirements(ty: u32, id: u32) -> *const u16 {
    unsafe { DAT_REQUIREMENTS.get()(ty, id) }
}

static mut CHANGE_AI_REGION_STATE: GlobalFunc<fn(*mut bw::AiRegion, u32)> = GlobalFunc(None);
pub fn change_ai_region_state(region: *mut bw::AiRegion, state: u32) {
    unsafe { CHANGE_AI_REGION_STATE.get()(region, state) }
}

static mut ISSUE_ORDER: GlobalFunc<
    unsafe extern fn(*mut bw::Unit, u32, u32, u32, *mut bw::Unit, u32),
> = GlobalFunc(None);

pub fn issue_order(
    unit: *mut bw::Unit,
    order: OrderId,
    x: u32,
    y: u32,
    target: *mut bw::Unit,
    fow_unit: UnitId,
) {
    assert!(x < 0x10000);
    assert!(y < 0x10000);
    assert!(unit != null_mut());
    unsafe { ISSUE_ORDER.get()(unit, order.0 as u32, x, y, target, fow_unit.0 as u32) }
}

static mut PRINT_TEXT: GlobalFunc<fn(*const u8)> = GlobalFunc(None);
// Too common to be inlined. Would be better if PRINT_TEXT were changed to always be valid
// (But C ABI is still worse for binsize)
#[inline(never)]
pub fn print_text(msg: *const u8) {
    unsafe {
        if let Some(print) = PRINT_TEXT.0 {
            print(msg);
        }
    }
}

static mut RNG_SEED: GlobalFunc<fn() -> u32> = GlobalFunc(None);
pub fn rng_seed() -> Option<u32> {
    unsafe {
        if let Some(rng) = RNG_SEED.0 {
            Some(rng())
        } else {
            None
        }
    }
}

static mut READ_FILE: GlobalFunc<fn(*const u8, *mut usize) -> *mut u8> = GlobalFunc(None);
pub fn read_file(name: &str) -> Option<(*mut u8, usize)> {
    // Uh, should work fine
    let cstring = format!("{}\0", name);
    let mut size = 0usize;
    let result = unsafe { READ_FILE.get()(cstring.as_ptr(), &mut size) };
    if result == null_mut() {
        None
    } else {
        Some((result, size))
    }
}

unsafe fn aiscript_opcode(
    api: *const PluginApi,
    opcode: u32,
    hook: unsafe extern fn(*mut bw::AiScript),
) {
    let ok = ((*api).hook_aiscript_opcode)(opcode, mem::transmute(hook));
    if ok == 0 {
        fatal("Unable to hook aiscript opcodes");
    }
}

#[no_mangle]
pub unsafe extern fn samase_plugin_init(api: *const PluginApi) {
    //let required_version = 10;//commit
	let required_version = 12;
	
    if (*api).version < required_version {
        fatal(&format!(
            "Newer samase is required. (Plugin API version {}, this plugin requires version {})",
            (*api).version,
            required_version,
        ));
    }

    aiscript_opcode(api, 0x00, ::aiscript::goto);
    aiscript_opcode(api, 0x01, ::aiscript::notowns_jump);	
	aiscript_opcode(api, 0x09, ::aiscript::wait_build);
	aiscript_opcode(api, 0x0a, ::aiscript::wait_buildstart);	
//	aiscript_opcode(api, 0x0b, ::aiscript::attack_clear);//1.16
	aiscript_opcode(api, 0x0c, ::aiscript::attack_add);			
//	aiscript_opcode(api, 0x1f, ::aiscript::send_suicide);//1.16
    aiscript_opcode(api, 0x40, ::aiscript::call);
    aiscript_opcode(api, 0x41, ::aiscript::ret);
    aiscript_opcode(api, 0x46, ::aiscript::do_morph);
    aiscript_opcode(api, 0x4c, ::aiscript::train);
    aiscript_opcode(api, 0x71, ::aiscript::attack_to);
    aiscript_opcode(api, 0x72, ::aiscript::attack_timeout);
    aiscript_opcode(api, 0x73, ::aiscript::issue_order);
    aiscript_opcode(api, 0x74, ::aiscript::deaths);
    aiscript_opcode(api, 0x75, ::idle_orders::idle_orders);
    aiscript_opcode(api, 0x76, ::aiscript::if_attacking);
    aiscript_opcode(api, 0x77, ::aiscript::unstart_campaign);
    aiscript_opcode(api, 0x78, ::aiscript::max_workers);
    aiscript_opcode(api, 0x79, ::aiscript::under_attack);
    aiscript_opcode(api, 0x7a, ::aiscript::aicontrol);
    aiscript_opcode(api, 0x7b, ::aiscript::bring_jump);
    aiscript_opcode(api, 0x7c, ::aiscript::create_script);
    aiscript_opcode(api, 0x7d, ::aiscript::player_jump);
    aiscript_opcode(api, 0x7e, ::aiscript::kills_command);
    aiscript_opcode(api, 0x7f, ::aiscript::wait_rand);
    aiscript_opcode(api, 0x80, ::aiscript::upgrade_jump);
    aiscript_opcode(api, 0x81, ::aiscript::tech_jump);
    aiscript_opcode(api, 0x82, ::aiscript::random_call);
    aiscript_opcode(api, 0x83, ::aiscript::attack_rand);
    aiscript_opcode(api, 0x84, ::aiscript::supply);
    aiscript_opcode(api, 0x85, ::aiscript::time_command);
    aiscript_opcode(api, 0x86, ::aiscript::resources_command);
    aiscript_opcode(api, 0x87, ::aiscript::set_town_id);
    aiscript_opcode(api, 0x88, ::aiscript::remove_build);
    aiscript_opcode(api, 0x89, ::aiscript::guard_command);
    aiscript_opcode(api, 0x8a, ::aiscript::base_layout_old);
    aiscript_opcode(api, 0x8b, ::aiscript::print_command);
    aiscript_opcode(api, 0x8c, ::aiscript::attacking);
    aiscript_opcode(api, 0x8d, ::aiscript::base_layout);
    aiscript_opcode(api, 0x8e, ::aiscript::unit_avail);
    aiscript_opcode(api, 0x8f, ::aiscript::load_bunkers);
    aiscript_opcode(api, 0x90, ::aiscript::ping);
    aiscript_opcode(api, 0x91, ::aiscript::reveal_area);
    aiscript_opcode(api, 0x92, ::aiscript::tech_avail);
    aiscript_opcode(api, 0x93, ::aiscript::remove_creep);
    aiscript_opcode(api, 0x94, ::aiscript::save_bank);
    aiscript_opcode(api, 0x95, ::aiscript::load_bank);
    aiscript_opcode(api, 0x96, ::aiscript::bank_data_old);
    aiscript_opcode(api, 0x97, ::aiscript::unit_name);
    aiscript_opcode(api, 0x98, ::aiscript::bank_data);
    aiscript_opcode(api, 0x99, ::aiscript::lift_land);
    aiscript_opcode(api, 0x9a, ::aiscript::queue);
    aiscript_opcode(api, 0x9b, ::aiscript::aise_debug);
    aiscript_opcode(api, 0x9c, ::aiscript::replace_requests);
    aiscript_opcode(api, 0x9d, ::aiscript::defense_command);
	aiscript_opcode(api, 0x9e, ::aiscript::max_build);
	aiscript_opcode(api, 0x9f, ::aiscript::attack_to_deaths);
	aiscript_opcode(api, 0xa0, ::aiscript::container_jump);	
	aiscript_opcode(api, 0xa1, ::idle_tactics::custom_script);
	aiscript_opcode(api, 0xa2, ::aiscript::local_jump);
	aiscript_opcode(api, 0xa3, ::aiscript::resarea_jump);	
	aiscript_opcode(api, 0xa4, ::aiscript::set_free_id);	
	aiscript_opcode(api, 0xa5, ::idle_tactics::set_pos_variable);	
	aiscript_opcode(api, 0xa6, ::idle_tactics::set_var_local);		

    GAME.init(((*api).game)().map(|x| mem::transmute(x)), "Game object");
    AI_REGIONS.init(
        ((*api).ai_regions)().map(|x| mem::transmute(x)),
        "AI regions",
    );
    PLAYER_AI.init(((*api).player_ai)().map(|x| mem::transmute(x)), "Player AI");
    GET_REGION.init(
        ((*api).get_region)().map(|x| mem::transmute(x)),
        "get_region",
		
    );
    DAT_REQUIREMENTS.init(
        ((*api).dat_requirements)().map(|x| mem::transmute(x)),
        "dat_requirements",
    );
    FIRST_ACTIVE_UNIT.init(
        ((*api).first_active_unit)().map(|x| mem::transmute(x)),
        "first active unit",
    );
    FIRST_HIDDEN_UNIT.init(
        ((*api).first_hidden_unit)().map(|x| mem::transmute(x)),
        "first hidden unit",
    );
    FIRST_AI_SCRIPT.init(
        ((*api).first_ai_script)().map(|x| mem::transmute(x)),
        "first_ai_script",
    );
    SET_FIRST_AI_SCRIPT.init(
        ((*api).set_first_ai_script)().map(|x| mem::transmute(x)),
        "set_first_ai_script",
    );
    FIRST_FREE_AI_SCRIPT.init(
        ((*api).first_free_ai_script)().map(|x| mem::transmute(x)),
        "first_free_ai_script",
    );
    SET_FIRST_FREE_AI_SCRIPT.init(
        ((*api).set_first_free_ai_script)().map(|x| mem::transmute(x)),
        "set_first_free_ai_script",
    );
    GUARD_AIS.init(
        ((*api).first_guard_ai)().map(|x| mem::transmute(x)),
        "guard ais",
    );
    PATHING.init(((*api).pathing)().map(|x| mem::transmute(x)), "pathing");
	PLAYERS.init(((*api).players)().map(|x| mem::transmute(x)), "players");

    match ((*api).issue_order)() {
        None => ((*api).warn_unsupported_feature)(b"Ai script issue_order\0".as_ptr()),
        Some(s) => ISSUE_ORDER.0 = Some(mem::transmute(s)),
    }
	
    let read_file = ((*api).read_file)();
    READ_FILE.0 = Some(mem::transmute(read_file));
    CHANGE_AI_REGION_STATE.init(
        ((*api).change_ai_region_state)().map(|x| mem::transmute(x)),
        "change_ai_region_state",
    );

    if !crate::feature_disabled("everything_else") {
        let result = ((*api).hook_step_objects)(::frame_hook, 0);
        if result == 0 {
            fatal("Couldn't hook step_objects");
        }
        let result = ((*api).hook_step_objects)(::frame_hook_after, 1);
        if result == 0 {
            fatal("Couldn't hook step_objects");
        }
        let result = ((*api).hook_step_order)(::step_order_hook);
        if result == 0 {
            fatal("Couldn't hook step_order");
        }
        let result = ((*api).hook_step_order_hidden)(::step_order_hidden_hook);
        if result == 0 {
            fatal("Couldn't hook soi");
        }
    }
    UNITS_DAT.init(((*api).dat)(0).map(|x| mem::transmute(x)), "units.dat");
    bw_dat::init_units(units_dat());
    WEAPONS_DAT.init(((*api).dat)(1).map(|x| mem::transmute(x)), "weapons.dat");
    bw_dat::init_weapons(weapons_dat());
    UPGRADES_DAT.init(((*api).dat)(3).map(|x| mem::transmute(x)), "upgrades.dat");
    bw_dat::init_upgrades(upgrades_dat());
    TECHDATA_DAT.init(((*api).dat)(4).map(|x| mem::transmute(x)), "techdata.dat");
    bw_dat::init_techdata(techdata_dat());
    ORDERS_DAT.init(((*api).dat)(7).map(|x| mem::transmute(x)), "orders.dat");
    bw_dat::init_orders(orders_dat());
	init_config(true);
	init_grafting_data(true);
	
    PRINT_TEXT.0 = Some(mem::transmute(((*api).print_text)()));
    RNG_SEED.0 = Some(mem::transmute(((*api).rng_seed)()));
    let mut result = ((*api).extend_save)(
        "aise\0".as_ptr(),
        Some(::globals::save),
        Some(::globals::load),
        ::globals::init_game,
    );
    if result != 0 {
        result = ((*api).hook_ingame_command)(6, ::globals::wrap_save, None);
    }
	extern fn length_function_cmd99(_data: *const u8, _max_length: u32) -> u32 {
	  0xc
	}//length==12
	((*api).hook_ingame_command)(0x99, ::aiscript::proc_test, Some(length_function_cmd99));
	((*api).hook_ingame_command)(0x98, ::aiscript::comm_tower_hooks, Some(length_function_cmd99));
	
    if result != 0 {
        let ptr = ((*api).player_ai_towns)();
        if ptr.is_none() {
            result = 0;
        } else {
            PLAYER_AI_TOWNS.0 = Some(mem::transmute(ptr));
        }
    }
    if result == 0 {
        ((*api).warn_unsupported_feature)(b"Saving\0".as_ptr());
    }
    ::init();
				
}

pub unsafe extern fn init_config(_exit_on_error: bool) {
    {
        match read_config() {
            Ok(o) => {
                config::set_config(o);
                //break;
            }
            Err(msg) => {
                windows::message_box("Aise", &msg);
				config::default_config();
				
                /*if exit_on_error {
                    TerminateProcess(GetCurrentProcess(), 0x42302aef);
                }*/
            }
        }
    }
}

pub unsafe extern fn init_grafting_data(exit_on_error: bool){
	match read_grafting_data(){
		Ok(o) => {
			config::set_grafting_data(o);
		},
		Err(msg)=>{
			//windows::message_box("Aise", &msg);
			config::set_graftdata_default();
			
			/*if exit_on_error {
				TerminateProcess(GetCurrentProcess(), 0x42302aef);
            }*/
		},
	}
}
pub unsafe fn read_grafting_data() -> Result<config::GraftingDataProcessed, String> {
	let (data, len) = match read_file("samase\\ext_req.ext"){
		Some(s)=>s,
		None=>{
			let msg = format!("Unable to read grafting data\n");
			return Err(msg);
		},
	};
	let slice = std::slice::from_raw_parts(data, len);
	std::fs::write("dump.bin", slice).unwrap();
	let mut file_reopen = match File::open("dump.bin") {
		Ok(o) => o,
		Err(e) => {
			let msg = format!("Reopen error:\n{}", e);
			return Err(msg);
		}
	};
	let deserialize_data: config::GraftingData = match bincode::deserialize_from(&mut file_reopen) {
		Ok(o) => o,
		Err(e) => {
			let msg = format!("Deserialize error:\n{}", e);
			return Err(msg);
		}
	};
	let process = match config::process_grafting_data(deserialize_data){
		Ok(o) => o,
		Err(e) => {
			let msg = format!("Processing error:\n{}", e);
			return Err(msg);
		}
	};
	Ok(process)
}

pub unsafe fn read_config() -> Result<config::Config, String> {
	let (data, len) = match read_file("samase\\config.ini") {
            Some(s) => s,
            None => { return Err(format!("Configuration file samase/config.ini not found."))},
    };
	let slice = std::slice::from_raw_parts(data, len);
    match config::read_config(slice) {
        Ok(o) => Ok(o),
        Err(e) => {
            use std::fmt::Write;
            let mut msg = String::new();
            for c in e.iter_chain() {
                writeln!(msg, "{}", c).unwrap();
            }
            let msg = format!("Unable to read config:\n{}", msg);
            Err(msg)
        }
    }
} 


use std::fs::{File};

//pub unsafe fn read_mapping_tool_data() -> Option<ExtendedEditorData> {
pub unsafe fn read_mapping_tool_data() -> Option<ExtendedEditorData> {
	//let mut mapping_data: ExtendedEditorData = Default::default();
	/*
	let mut file = match File::open(path) {
            Ok(o) => o,
            Err(e) => {
                bw_print!("Bank load error: {}", e);
                return;
            }
	}*/
	//bw_print!("Reading...");
	let (data, len) = match read_file("extension.dat"){
		Some(s)=>s,
		None=>{
			//bw_print!("No data found!");
			return None;},
	};
	//bw_print!("File is found {:?} {}",data,len);
	let slice = std::slice::from_raw_parts(data, len);
	std::fs::write("dump.bin", slice).unwrap();
	let mut file_reopen = match File::open("dump.bin") {
		Ok(o) => o,
		Err(e) => {
			bw_print!("Reopen error {}",e);
			return None;
		}
	};
	
	
	let test_data2: ExtendedEditorData = match bincode::deserialize_from(&mut file_reopen) {
            Ok(o) => { /*bw_print!(" deserializes just fine"); */o},
            Err(e) => {
//                bw_print!("Bank load error: {}", e);
                return None;
            }
    };
	
	/*
	
	
	/*
	*/
	

//	std::fs::remove_file("dump.bin");
	
	std::fs::write("dump2.bin", slice).unwrap();
	*/
	Some(test_data2)
	/*
	let mapping_data: ExtendedEditorData = match bincode::deserialize_from(slice) {
        Ok(o) => { bw_print!("Ok"); return o},
        Err(e) => {
			bw_print!("Can't deserialize {}",e);
            error!("Couldn't deserialize mapping data: {}", e);
            return None
        }
    };
	//
	
	Some(mapping_data)*/
}

