use config::{self, Config};
use globals::{
    Globals,
};
use bw_dat::{UnitId};
use std::ptr::null_mut;
use bw;
use game::Game;
use aiscript;
use unit::{Unit};
use std::ptr;

// 00437F20 = Ai_HasUnitsForAttackForce(), edx player -- https://pastebin.com/raw/NwriVgR5
// resolves 00437F31
/*
pub unsafe fn has_units_for_attack_force_hook (
    player: u32,
    orig: &dyn Fn(player)->i32,)->i32 {
        let ai = bw::player_ai(player);
        let mut atkGroup: i16 = (*ai).attack_force; // v2
        let mut atkArray = [i32:63]; // v13
        let mut i: u32 = 0;
        let mut v3: i16 = atkGroup;
        let mut v6: i32 = 0;
        for i in 0..63 {
            if atkGroup == 0 {
                break;
            }
            atkArray[i] = atkGroup - 1;
            atkGroup += 1;
            v3 = atkGroup;
        }
        let mut firstUnit = bw::first_player_unit[player];
        if firstUnit != null_mut() {
            v6 = i-1;
            loop {
                'label22_wait: loop {
                    let mut firstUnitSprite = (*firstUnit.0).sprite;
                    if firstUnitSprite != null_mut() {
                        let mut firstUnitOrder = (*firstUnit.0).order; // v7
                        if firstUnitOrder != null_mut() {
                            let mut firstUnitId = (*firstUnit.0).unit_id; // v8
                            if (*firstUnitId.0).is_subunit == 0 {
                                let mut firstUnitId2 = (*firstUnit.0).unit_id; // v9
                                if firstUnitId == 0x1E { //epic siege tank hardcode -- if sieged tank, consider it unsieged tank
                                    firstUnitId2 = 5;
                                }
                                if ((*firstUnitId2.0).ai_flags & 1) == 0 { // not sure what 1 is
                                    if (*firstUnit.0).flags & 1 { // completed
                                        if firstUnitOrder != 0x6A && firstUnitOrder != 0x96 && bw::orders_dat_use_weapon_targeting[firstUnitOrder] == 0 {
                                            // 0x6a = completing archon summon, 0x96 = reset collision 1
                                            let mut firstUnitAi = (*firstUnit.0).ai;
                                            if firstUnitAi != null_mut() {
                                                let mut v10: i32 = 0;
                                                if i > 0 {
                                                    while firstUnitId2 != atkArray[v10] {
                                                        if (v10 += 1) >= i {
                                                            // label 22
                                                            break 'label22_wait;
                                                        }
                                                    }
                                                    atkArray[v10] = 228;
                                                    if v10 == v6 {
                                                        i -= 1;
                                                        v6 -= 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break 'label22_wait;
                }
                // label 22
                firstUnit = (*firstUnit.0).next_player_unit;
                if firstUnit == null_mut() {
                    break;
                }
            }
        }
        let mut v11: i32 = 0;
        if i == null_mut() {
            return 1;
        }
        while atkArray[v11] == 228 {
            if (v11 += 1) >= i {
                return 1;
            }
        }
    return 0;
}
*/
// 00438050 = GetMissingAttackForceUnits(), arg 1 AiRegion *region, arg 2 dword *out_unit_ids, arg 3 only_accept_completed -- https://pastebin.com/raw/vUSUGniF
// resolves 00438061
/*
pub unsafe fn get_missing_attack_force_units_hook (
    region: *mut bw::AiRegion,
    attacker_ids: u32,
    campaign_bool: i32,
    orig: &dyn Fn(*mut AiRegion,attacker_ids,campaign_bool)->u32,)->u32 {
        let ai = bw::player_ai((*region).player);
        let mut atkGroup: i16 = (*ai).attack_force; // v4
        let mut atkForce: i16 = atkGroup; // v3
        let mut i: i16 = 0;
        let mut v14 = (*bw::first_player_unit[player].0).ai; // probably wrong
        for i in 0..63 {
            if atkGroup == 0 {
                break;
            }
            attacker_ids[i] = atkForce - 1;
            atkGroup += 1;
            atkForce = atkGroup;
        }
        let mut player: i32 = (*region).player; // v6
        let mut firstUnit1 = bw::first_player_unit[player]; // v7
        let mut firstUnit2 = bw::first_player_unit[player]; // v18
        if firstUnit1 != null_mut() {
            loop {
                let mut firstUnitSprite = (*firstUnit1.0).sprite;
                if firstUnitSprite != null_mut() {
                    let mut firstUnitOrder = (*firstUnit1.0).order; // v8
                    if firstUnitOrder != null_mut() {
                        let mut firstUnitId = (*firstUnit1.0).unit_id; // v9
                        if (*firstUnitId.0).is_subunit == 0 {
                            break;
                        }
                    }
                }
                //label 37
                firstUnit2 = (*firstUnit1.0).next_player_unit;
                if firstUnit2 != null_mut() {
                    return i;
                }
                firstUnit1 = (*firstUnit1.0).next_player_unit;
            }
            let mut firstUnitId2 = (*firstUnit1.0).unit_id; // v10
            if firstUnitId == 0x1E { //epic siege tank hardcode -- if sieged tank, consider it unsieged tank
                firstUnitId2 = 5;
            }
            let mut v11: i32 = 1;
            let mut v12: i32 = (*firstUnit1.0).flags & 1; // completed
            if v12 == 0 || firstUnitOrder == 0x6A || firstUnitOrder == 0x96 { // 0x6a = completing archon summon, 0x96 = reset collision 1
                if campaign_bool == 1 {
                    // label 36
                    firstUnit1 = firstUnit2;
                    // label 37
                    firstUnit2 = (*firstUnit1.0).next_player_unit;
                    if firstUnit2 != null_mut() {
                        return i;
                    }
                    firstUnit1 = (*firstUnit1.0).next_player_unit;
                }
                if firstUnitId2 == 0x24 || firstUnitId2 == 0x3B || firstUnitId2 == 0x61 { // 0x24 = zerg egg, 0x3b = zerg cocoon, 0x61 = lurker egg
                    firstUnitId2 = (*firstUnit2.0).build_queue;
                    let mut v13: i32 = bw::units_dat_special_ability_flags[firstUnitId2]
                    if (v13 & 0x400) = 1 {
                        v11 = 2;
                    }
                }
            }
            if ((*firstUnit2.0).ai_flags & 1) == 0 {
                if v12 == null_mut() || firstUnitOrder == 0x6A || firstUnitOrder == 0x96 || ((v14.0 = (*firstUnit2.0).ai != 0) && ((*v14.0).ai_type == 4) && ((*v14.0).region = region) == 1) {
                    // 0x6a = completing archon summon, 0x96 = reset collision 1
                    if v11 != null_mut() {
                        let mut v15: i32 = i - 1;
                        loop {
                            'label35_wait loop {
                                let mut v16: i32 = 0;
                                if i != null_mut() {
                                    while firstUnitId2 != attacker_ids[v16] {
                                        if (v16 += 1) >= i {
                                            break 'label35_wait;
                                        }
                                    attacker_ids[v16] = 0xE4; // none
                                        if v16 == v15 {
                                            i -= 1;
                                            v15 -= 1;
                                        }
                                    }
                                }
                                break 'label35_wait;
                            }
                            v11 -= 1;
                        }
                    }
                }
            }
            // label 36
            firstUnit1 = firstUnit2;
            // label 37
            firstUnit2 = (*firstUnit1.0).next_player_unit;
            if firstUnit2 != null_mut() {
                return i;
            }
            firstUnit1 = (*firstUnit1.0).next_player_unit;
        }
        return i;
    }
*/
// 0043ABB0 = Ai_AttackTo?(), arg 1 player, arg 2 x, arg 3 y, arg 4 bool always_overwrite_last_attack, arg 5 allow_fallback_air_only -- https://pastebin.com/raw/CnpBsRsj
// resolves 0043AC54, 0043AC5B, and 0043B1C4
/*
pub unsafe fn ai_attack_to_hook (
    player: i32,
    x: i32,
    y: i32,
    overwrite_last: bool,
    air_fallback_only: bool,
    orig: &dyn Fn(player,x,y,overwrite_last,air_fallback_only)->bool,)->bool {
        let mut v7: i16 = 0;
        let mut v8: i16 = 0;
        let mut regionList = [u8:2500];
        let mut v10: i32 = 0;

        let ai = bw::player_ai(player);
        let mut playerVar = player; // v5
        if (*ai).attack_failed == 1 {
            return 1;
        }
        let mut atkForce = (*ai).attack_force;
        let ai_region = bw::ai_regions(player);
        if atkForce != null_mut() {
            return (&ai_region[atkForce] - 47) != 8;
        }
        ptr::write_bytes(regionList, 0, sizeof(regionList));
        bw::ai_get_own_regions(regionList, player);
        bw::ai_get_enemy_regions(player, regionList);
        while bw::ai_get_neighbor_region_types(regionList) == 1 {
            bw::ai_finish_region_access(regionList);
        }
        v7 = atkForce[0];
        v8 = atkForce;
        v10 = (v7 != 0);
        if v7 != null_mut() {
            while bw::units_dat_kill_score[2 * v7 + 226] & 4 {
                v7 = v8[1];
                v8 += 1;
                if v7 != null_mut() {
                    // label 12
                    if (bw::ai_set_attack_prep_region(v10, 0, player, regionList, x, y, overwrite_last) == 0) && (bw::ai_set_attack_prep_region(v10, 1, player, regionList, x, y, overwrite_last) == 0) {
                        if air_fallback_only == 0 || v10 != null_mut() {
                            return 1;
                        }
                        if (bw::ai_set_attack_prep_region(1, 0, player, regionList, x, y, overwrite_last) == 0 && (bw::ai_set_attack_prep_region(1, 1, player, regionList, x, y, overwrite_last) == 0) {
                            (*ai).attack_failed == 1;
                            return 1;
                        }
                    }
                    return 0;
                }
            }
            v10 = 0;
        }
        // label 12
        (*ai).flags |= (1u & 0x00_ff) << 8;
        if (bw::ai_set_attack_prep_region(v10, 0, player, regionList, x, y, overwrite_last) == 0) && (bw::ai_set_attack_prep_region(v10, 1, player, regionList, x, y, overwrite_last) == 0) {
            if air_fallback_only == 0 || v10 != null_mut() {
                return 1;
            }
            if (bw::ai_set_attack_prep_region(1, 0, player, regionList, x, y, overwrite_last) == 0 && (bw::ai_set_attack_prep_region(1, 1, player, regionList, x, y, overwrite_last) == 0) {
                (*ai).attack_failed == 1;
                return 1;
            }
        }
        return 0;
    }
*/
// 0043B570 = AiScript_EvalHarass(), arg 1 player, ecx x, eax y -- https://pastebin.com/raw/e9EpUKVL
// resolves 0043B596 and 0043B59E
/*
pub unsafe fn aiscript_eval_harass_hook(
    x: i32,
    y: i32,
    player: i32,
    orig: &dyn Fn(x, y, player)->i32,)->i32 {
        let mut v3: i16 = 0;
        let mut v4: i16 = 0;
        let mut totalGroundStr: u32 = 0;
        let mut totalAirStr: u32 = 0;
        let mut v7: i32 = 0;
        let mut v8: i32 = 0;
        let mut v9: *mut Unit = 0;
        let mut v10: i32 = 0;
        let mut v11: *mut Sprite = 0;
        let mut v12: u8 = 0;
        let mut i32: result = 0;
        let mut ai_groundStr: u32 = 0;
        let mut ai_airStr: u32 = 0;
        let mut en_groundStr: u32 = 0;
        let mut en_airStr: u32 = 0;

        if bw::ai_attack_to(player, x, y, 0, 0) == 1 {
            result = 0;
            return result;
        }
        let ai = bw::player_ai(player);
        v3 = (*ai).attack_force[0];
        v4 = (*ai).attack_force;
        if v3 != null_mut() {
            while v4 != 0 {
                totalAirStr += bw::unit_strength[v3 - 1];
                totGroundStr += bw::unit_strength[v3 + 227];
                v4 += 1;
                v3 = v4;
            }
            ai_groundStr = totalGroundStr;
            ai_airStr = totalAirStr;
        }
        v9 = *bw::first_active_unit_var;
        let ai_region = bw::ai_regions(player);
        v10 = (&ai_region[atkForce] - 25) << 6;
        let pathing = pathing();
        v8 = ((*pathing.0).regions[0].area + v10) >> 8;
        v7 = (((*pathing.0).regions[0].area + 2) + v10) >> 8; // I think that's how you get region.y
        en_groundStr = 0;
        loop {
            if v9 == null_mut() {
                break;
            }
            v11 = (*v9.0).sprite;
            if v11 != null_mut() {
                v12 = (*v9.0).player;
                if (v12 == 11) {
                    v12 = (*v11.0).player;
                }
                if (bw::alliances[player][v12] == 0) && bw::fast_distance((*v11.0).position, v8, (*v11.0).position + 2, v7) < 1024 {
                    en_airStr += (*v9.0).air_strength;
                    en_groundStr += (*v9.0).ground_strength;
                }
            }
            v9 = (*v9.0).next;
        }
        if ai_airStr <= en_airStr && ai_groundStr <= en_groundStr {
            result = 0;
        }
        else {
            result = 1;
        }
        return result;
    }
*/
// 0043E050 = Ai_SuicideMission(), arg 1 player, eax bool random_suicide -- https://pastebin.com/raw/LcNKeAF7
// resolves 0043E1EE
/*
pub unsafe fn ai_suicide_mission_hook(
    rand_suicide: u8,
    player: u32,
    orig: &dyn Fn(rand_suicide, player)) {
        let mut a1Var = a1;
        if a1Var != null_mut { 
            let mut i = bw::first_player_unit[player];
            for i in 0..12 {
                if i == null_mut() {
                    break;
                }
                let mut v3 = (*i.0).sprite;
                if v3 != null_mut() {
                    let mut uOrder = (*i.0).order;
                    if uOrder != null_mut() {
                        let mut v4 = (*i.0).unit_id;
                        if (*v4.0).is_subunit() == 0 && v4 != 0xE && v4 != 0x23 && ((*v3.0).flags & 0x20 != 1) && bw::is_threat(0, i) != 1 {
                            bw::set_suicide_target(i);
                        }
                    }
                }
                i = (*i.0).next;
            }
            return;
        }
        let mut v5 = player;
        let mut v16 = player;
        let ai = bw::player_ai(player);
        if (*ai).strategic_suicide_mission_cooldown == 0 {
            let mut v6 = bw::first_player_unit[player];
            let mut v15 = 2500;
            let mut j: *mut Region = 0;
            for j in 0..X {
                if v6 == null_mut() {
                    break;
                }
                let mut spriteVar = (*v6.0).sprite;
                if spriteVar == null_mut() {
                    continue;
                }
                let mut orderVar = (*v6.0).order;
                if orderVar == null_mut() {
                    continue;
                }
                let mut v7 = (*v6.0).ai;
                let mumt v13 = (*v6.0).ai;
                if v7 == null_mut() || bw::is_transport(v6) == || ((*v6.0).flags & 0x2 && (*ai)flags & 0x20) {
                    continue;
                }
                let mut v10 = bw::get_region2((*spriteVar.0).position, ((*spriteVar.0).position + 2));
                let mut v9 = (*v7.0).type;
                v6 = (*v6.0).next;
                let ai_region = bw::ai_regions(player);
                let mut v8 = &ai_region[player][v10];
                if v9 == 1 {
                    (*v7.0).unit.ai = 0;
                    bw::add_military_ai(v8, (*v7.0).unit, 1);
                    (*v13.0).unit = 0;
                }
                else if v9 != 4 {
                    // label 34
                    v5 = player;
                }
                v11 = (*v6.0).ai;
                if v11 != null_mut() {
                    let mut v12 = (*v11.0).region.state;
                    if v12 == 1 || v12 == 8 || v12 == 9 || v12 == 2 {
                        // label 34
                        v5 = player;
                    }
                }
                if v15 != 2500 {
                    // label 33
                    bw::remove_unit_ai(v6, 0);
                    bw::add_military_ai(j, v6, 1);
                }
                ptr::write_bytes((*ai).attack_force, 0, sizeof((*ai).attack_force));
                (*ai).attack_force = 0;
                bw::ai_attack_clear(player);
                if bw::ai_attack_to(player, (*v6.0).sprite.position, (*v6.0).sprite.position + 2, 0, 1) == 0 {
                    (*ai).strategic_suicide_mission_cooldown = 24;
                    v15 = (*ai).attack_force - 1;
                    j = &ai_region[player][v15];
                    // label 33
                    bw::remove_unit_ai(v6, 0);
                    bw::add_military_ai(j, v6, 1);
                }
                // label 34
                v5 = player;
            }
        }
    }
*/
// 00447040 = Ai_RemoveFromAttackForce(), eax unit_id -- https://pastebin.com/raw/9cWaCGku
// resolves 00447059 and 0044707C
/*
pub unsafe fn ai_remove_from_attack(
    unit_id: i32,
    player: i32,
    orig: &dyn Fn(unit_id, player)) {
        let mut v2: i32 = unit_id + 1;
        let mut v3: u32 = 0;
        let ai = player_ai(player);
        let mut v4: i16 = (*ai).attack_force;
        if unit_id == 0x1E { // epic siege tank hardcode - if sieged tank, consider unsieged tank
            unit_id = 5;
        }
        while v4 != v2 {
            v3 += 1;
            v4 += 1;
            if v3 >= 63 {
                return;
            }
        }
        (*ai).attack_force[v3 + 628 * player] = 228;
    }
*/
// 00447090 = Ai_IsAttackTimedOut(), -- https://pastebin.com/raw/dSZbhPUa
// resolves 00447226... I think?
/*
pub unsafe fn ai_attack_timeout(
    player: i32,
    orig: &dyn Fn(player)->i32)->i32 {
        let mut v1 = player;
        let ai = player_ai(player);
        let mut result = (*ai).last_attack_second;
        if result != null_mut() {
            result = bw::elapsed_seconds_var - result >= (*ai).flags & 0x20 != 0 ? 180 : 120;
            if result != null_mut {
                (*ai).last_attack_second = 0;
            }
        }
        return result;
    }
*/
// 00447230 = Ai_AddToAttackForce(), edi unit_id, eax count, ecx player -- https://pastebin.com/raw/jz3ZLuH3
// resolves 0044724F, 0044725F, and 0044727B
/*
pub unsafe fn ai_attack_add(
    result: u32,
    player: i32,
    unit_id: i32,
    orig: &dyn Fn(result, player, unit_id)) {
        let mut counter: u32 = result;
        let mut plOffset: u32 = 0;
        let mut attackGroups: i16 = 0;
        let mut v7: i16 = 0;
        let ai = player_ai(player);
        let mut atkForce = (*ai).attack_force;

        if player < 8 && result < 63 && unit_id < 0xE4 {
            plOffset = 628 * player;
            let mut i: u32 = 0;
            if atkForce != null_mut() {
                atkGroups = (*ai).attackForce;
                while i < 63 {
                    atkGroups += 1;
                    i += 1;
                    if atkGroups == null_mut() {
                        //label 8
                        if counter != 0 {
                            v7 = (*ai).attack_force[plOffset + i];
                            while counter != 0 {
                                counter -= 1;
                                if i >= 63 {
                                    break;
                                }
                                v7 = unit_id + 1;
                                i += 1;
                                v7 += 1;
                            }
                        }
                    }
                }
            }
            else {
                if counter != 0 {
                    v7 = (*ai).attack_force[plOffset + i];
                    while counter != 0 {
                        counter -= 1;
                        if i >= 63 {
                            break;
                        }
                        v7 = unit_id + 1;
                        i += 1;
                        v7 += 1;
                    }
                }
            }
        }
    }
*/
// 00447ED0 = SavePlayerAiData(), arg 1 FILE *file -- https://pastebin.com/raw/gwDBdr0c
// resolves 00448173
/*
pub unsafe fn save_player_ai_data(
    file: *mut File,
    orig: &dyn Fn(file)->i32)->i32 {
        let mut v1: u32 = 0;
        let ai1 = player_ai(v1);
        let ai2 = player_ai(v1+1);
        let ai3 = player_ai(v1+2);
        let ai4 = player_ai(v1+3);
        let mut v3: i32 = 0;
        let mut v6: i32 = 0;
        let mut v9: i32 = 0;
        let mut v12: i32 = 0;
        while v1 < 8 {
            let mut freeMedic = (*ai1).free_medic;
            if freeMedic != null_mut() {
                let mut v4 = (freeMedic - bw::units as i32) / (0x150 as u32 + 1)
                if v4 <= 0x6A4 {
                    v3 = ((freeMedic - bw::units as i32) / (0x150 as u32 + 1) | (*(freeMedic + 165 as u8) << 11));
                    else {
                        v3 = 0;
                    }
                }
            }
            else {
                v3 = 0;
            }
            let mut v5 = (*ai2).free_medic;
            (*ai1).free_medic = v3;
            if v5 != null_mut() {
                let mut v7 = (v5 - bw::units as i32) / (0x150 as u32 + 1)
                if v7 <= 0x6A4 {
                    v6 = ((v5 - bw::units as i32) / (0x150 as u32 + 1) | (*(v5 + 165 as u8) << 11));
                    else {
                        v6 = 0;
                    }
                }
            }
            else {
                v6 = 0;
            }
            let mut v8 = (*ai3).free_medic;
            (*ai2).free_medic = v6;
            if v8 != null_mut() {
                let mut v10 = (v8 - bw::units as i32) / (0x150 as u32 + 1)
                if v10 <= 0x6A4 {
                    v9 = ((v8 - bw::units as i32) / (0x150 as u32 + 1) | (*(v8 + 165 as u8) << 11));
                    else {
                        v9 = 0;
                    }
                }
            }
            else {
                v9 = 0;
            }
            let mut v11 = (*ai4).free_medic;
            (*ai3).free_medic = v9;
            if v11 != null_mut() {
                let mut v13 = (v11 - bw::units as i32) / (0x150 as u32 + 1)
                if v13 <= 0x6A4 {
                    v12 = ((v11 - bw::units as i32) / (0x150 as u32 + 1) | (*(v11 + 165 as u8) << 11));
                    else {
                        v12 = 0;
                    }
                }
            }
            else {
                v12 = 0;
            }
            (*ai4).free_medic = v12;
            v1 += 4;
            let ai1 = player_ai(v1);
            let ai2 = player_ai(v1+1);
            let ai3 = player_ai(v1+2);
            let ai4 = player_ai(v1+3);
        }
        let ai1 = player_ai(0);
        let ai2 = player_ai(1);
        let ai3 = player_ai(2);
        let ai4 = player_ai(3);
        let ai5 = player_ai(4);
        let ai6 = player_ai(5);
        let ai7 = player_ai(6);
        let ai8 = player_ai(7);
        let mut freeMedic1 = (*ai1).free_medic;
        let mut freeMedic2 = (*ai2).free_medic;
        let mut freeMedic3 = (*ai3).free_medic;
        let mut freeMedic4 = (*ai4).free_medic;
        let mut freeMedic5 = (*ai5).free_medic;
        let mut freeMedic6 = (*ai6).free_medic;
        let mut freeMedic7 = (*ai7).free_medic;
        let mut freeMedic8 = (*ai8).free_medic;
        let mut v15: *mut Unit = 0;
        let mut v16: *mut Unit = 0;
        let mut v17: *mut Unit = 0;
        let mut v18: *mut Unit = 0;
        let mut v19: *mut Unit = 0;
        let mut v20: *mut Unit = 0;
        let mut v21: *mut Unit = 0;
        let mut v22: *mut Unit = 0;
        let mut result = bw::write_compressed((*ai1), 0x2740 as u32, file);
        if freeMedic1 != null_mut() {
            v15 = bw::unit_reference[freeMedic1 & 0x7FF];
        }
        else {
            v15 = 0;
        }
        freeMedic1 = v15 as i32;
        if freeMedic2 != null_mut() {
            v16 = bw::unit_reference[freeMedic2 & 0x7FF];
        }
        else {
            v16 = 0;
        }
        freeMedic2 = v16 as i32;
        if freeMedic3 != null_mut() {
            v17 = bw::unit_reference[freeMedic3 & 0x7FF];
        }
        else {
            v17 = 0;
        }
        freeMedic3 = v17 as i32;
        if freeMedic4 != null_mut() {
            v18 = bw::unit_reference[freeMedic4 & 0x7FF];
        }
        else {
            v18 = 0;
        }
        freeMedic4 = v18 as i32;
        if freeMedic5 != null_mut() {
            v19 = bw::unit_reference[freeMedic5 & 0x7FF];
        }
        else {
            v19 = 0;
        }
        freeMedic5 = v19 as i32;
        if freeMedic6 != null_mut() {
            v20 = bw::unit_reference[freeMedic6 & 0x7FF];
        }
        else {
            v20 = 0;
        }
        freeMedic6 = v20 as i32;
        if freeMedic7 != null_mut() {
            v21 = bw::unit_reference[freeMedic7 & 0x7FF];
        }
        else {
            v21 = 0;
        }
        freeMedic7 = v21 as i32;
        if freeMedic8 != null_mut() {
            v22 = bw::unit_reference[freeMedic8 & 0x7FF];
        }
        else {
            v22 = 0;
        }
        freeMedic8 = v22 as i32;
        return result;
    }
*/
// 0045B850 = ProgressAiScript(), arg 1 AiScript *script -- https://pastebin.com/raw/iP396PRy
// resolves 0045BCC9 and 0045C086
/*
pub unsafe fn progress_ai_script(
    script: *mut Aiscript,
    orig: &dyn Fn(script)) {
        match script {
            0xB => { // attack_clear
                let mut player = (*script.0).player;
                let ai = player_ai(player);
                ptr::write_bytes((*ai).attack_force, 0, sizeof (*ai).attack_force);
                (*ai).attack_force = 0;
                (*ai).last_attack_second = 0;
                bw::ai_attack_clear(player);
                continue;
            }
            0x1F => { // send_suicide
                let mut v99 = bw::aiscript_get_byte(script);
                let mut player (*script.0).player;
                let ai = player_ai(player);
                ptr::write_bytes((*ai).attacK_force, 0, sizeof (*ai).attack_force);
                (*ai).attack_force = 0;
                (*ai).last_attack_second = 0;
                bw::ai_attack_clear(player);
                bw::send_suicide(v99, player);
                // goto setAttackTimer
                (*ai).last_attack_second = bw::elapsed_seconds_var - 175;
                continue;
            }
            _ => { // default
                if bw::first_active_ai_script == script {
                    bw::first_active_ai_script = (*script.0).next;
                }
                let mut v187 = (*script.0).next;
                if v187 != null_mut() {
                    (*v187.0).next = (*script.0).next;
                }
                let mut scriptVar = (*script.0).next;
                if scriptVar != null_mut() {
                    (*scriptVar.0).prev = (*script.0).prev;
                }
                let mut v188 = bw::active_ai_scripts;
                (*script.0).prev = 0;
                (*script.0).next = (*v188[100].0).next;
                let mut v189 = (*v188[100].0).next;
                if v189 != null_mut() {
                    (*v189.0).prev = script;
                }
                (*v188[100].0).next = script;
                return;
            }
        }
    }
*/
// 0043E670 = AiRegion_TrainAttackForce?(), eax AiRegion *region -- https://pastebin.com/raw/UgHvYJDu

// 0043EEC0 = AiRegion_GroupingForAttack(), arg 1 AiRegion *region -- https://pastebin.com/raw/ee6m7eCR

// 0043D5D0 = ProgressMilitaryAi?(), arg 1 Unit *unit, arg 2 move_order?, arg 3 x, arg 4 y | Ai_OrderMilitaryMove?() -- https://pastebin.com/raw/tcP5ZNuq
/*
pub unsafe fn progress_military_ai_hook(
    unit: *mut Unit,
    arg2: u32,
    arg_x: u32,
    arg_y: u32,
    orig: &dyn Fn(*mut Unit,u32,u32,u32)) {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit, arg2, arg_x, arg_y);
			},
        };
        let mut uAi = (*unit.0).ai;
        let mut unitCopy = unit;
        if uAi != null_mut() {
            if (uAi.0).ai_type == 4 && (uAi.0).unit == unit && (bw::units_dat_flags[(*unit.0).unit_id] & 1) != 1 { // type military, !building
                let mut uFlags = (*unit.0).flags;
                if (uFlags & 1) = 1 { // Completed
                    let mut uOrder = (*unit.0).order;
                    if uOrder != 0x6A && uOrder != 0x96 { // Completing archon summon, Reset collision 1
                        if (uFlags & 0x20) = 1 { // In building
                            bw::unload_unit(unit.0);
                        }
                        else if uOrder = 0x62 || uOrder = 0x7 { // Siege, Ignore
                            return;
                        }
                        let mut tankCheck = ((*unit.0).unit_id == 0x1E); // Siege Tank (Siege Mode)
                        let mut v31 = 0;
                        if tankCheck == 1 {
                            if (*unit.0).order = 0x6 { // Unsiege
                                return;
                            }
                            let mut tankCheck |= bw::has_target_in_range(uOrder, unit.0) & 0x00_ff;
                            if tankCheck == 1 {
                                if bw::ai_update_attack_target(unit, 0, 1, 0) == 1 {
                                    let mut autoTarget = bw::get_auto_target(unit.0);
                                    if autoTarget != null_mut() {
                                        bw::clear_target(unit.0);
                                        bw::attack_unit(autoTarget, unit.0, 0, 0);
                                    }
                                }
                                return;
                            }
                            if (*unit.0).unit_id == 0x1E { // Siege Tank (Siege Mode)
                                bw::issue_order_targ_nothing(unit.0, 0x63); // Unsiege
                                unitCopy = unit;
                                let mut v30 = 0;
                            }
                        }
                        if bw::ai_is_military(unitCopy.0) == 1 {
                            let mut uRegion = (*unitCopy.0).ai.region;
                            if bw::ai_is_pure_spellcaster(*unitCopy.0) == 1 [
                                if {
                                    arg2 = ((*unitCopy.0).unit_id != 0x22 ? 158 : 177); // 0x22 = Medic
                                }
                                let mut uFlags2 = (*unitCopy.0).flags;
                                if uFlags2 & 0x40 == 1 || uFlags2 & 0x20 == 1 { // In transport, In building
                                    let mut pCopy = (*unitCopy.0).player;
                                    let mut regionId2 = bw::get_region2(arg_x, arg_y);
                                    let mut regionId = bw::get_region1(unitCopy.0);
                                    if bw::ai_are_connected(pCopy, regionId2, regionId) == 1 {
                                        let mut regionId3 = bw::get_region1(unit.0);
                                        bw::get_region_center_from_id(regionId3, x1, y1); // x1 and y1 are NOT defined in the function. Must be stored in registers?
                                        let mut x1Copy = x1;
                                        bw::get_angle(arg_y, x1, arg_x, y1);
                                        bw::rand_short(56);
                                        let mut randAngle = bw::rand_short(56) & 0x7F;
                                        let mut randAngleX = arg_x + (randAngle * bw::retreat_array[v21].x >> 8); // v21 is NOT defined in the function.
                                        let mut randAngleY = arg_y + (randAngle * bw::retreat_array[v21].y >> 8);
                                        x1 = bw::get_angle(y + (randAngle * bw::retreat_array[v21].y >> 8), x1Copy, randAngleX, y1);
                                        bw::fast_distance(x1Copy, randAngleX, y1, randAngleY);
                                        let mut randVar = bw::rand_short(56);
                                        let mut mathVar = v26 + (randVar & 0x7F) - 0x40;
                                        if mathVar < 0 {
                                            mathVar = 0;
                                        }
                                        let mut randAngleX2 = x1copy + (mathVar * bw::retreat_array[x1].x >> 8);
                                        let mut randAngleY2 = y1 + (mathVar * bw::retreat_array[x1].y >> 8);
                                        if randAngleX2 >= 0 && randAngleX2 < bw::map_width_pixels && randAngleY2 >= 0 && randAngleY2 < bw::map_height_pixels {
                                            x1 = bw::get_region2(randAngleX2, y1 + (mathVar * bw::retreat_array[x1].y >> 8)) << 6;
                                            let mut regionId4 = bw::get_region2(arg_x, arg_y);
                                            if bw::pathing_var.regions[0].group + x1 == bw::pathing_var.regions[regionId4].group {
                                                arg_x = randAngleX2;
                                                arg_y = randAngleY2;
                                            }
                                        }
                                    }
                                    if v31 != 0 {
                                        let mut uOrderQueue = (*unit.0).order_queue_begin;
                                        if uOrderQueue != null_mut() {
                                            bw::append_order_targeting_ground(unit.0, arg2, arg_x, arg_y, 0);
                                            bw::order_done(unit.0);
                                        }
                                    }
                                    else {
                                        bw::issue_order_targeting_ground(unit.0, arg2, arg_x, arg_y);
                                    }
                                }
                                else {
                                    bw::ai_unk_order(0x9C, unitCopy.0); // Script (Computer)
                                }
                            ]
                        }
                    }
                }
            }
        }
    }
*/
/*
// 0043B0B0 = AiScript_Rush(), -- https://pastebin.com/raw/2eaDTgKR
pub unsafe fn ai_rush_hook(
    arg_x: u32, 
    arg_y: u32,
    player: u32,
    arg2: u32,
    orig: &dyn Fn(u32, u32, u32, u32,)->u32)->u32 {
        //
    }
*/