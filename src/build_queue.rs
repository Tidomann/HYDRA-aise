#![allow(non_snake_case)]
use failure::{/*Context,*/Error};
use bw_dat::{order, TechId, UnitId, OrderId};
use unit::{Unit};
use unit_search::{self, UnitSearch, PriorityType};
use config;
use crate::ai::{self, has_resources};
use samase;
use pathfinding::{self, InfluenceZone, InfluenceShape, InfluenceFind, InfluenceType, InfluenceFlags};
use std::cmp;
use std::ptr::null_mut;
use swap_retain::SwapRetain;
use aiscript::{
    self, PlayerMatch, Town, UnitMatch, Position, ReadModifier, ReadModifierType, ScriptData, ModifierAction, BoolModifier, Script, WriteModifier,
	LocalModifier,
};
use globals::{
    self, Globals, BaseLayouts, BaseLayout, NewGameState, HydraEffect, Queues, UpgradeQueue, VarType, TownStateValue, BuildQueue,
	BuildQueueStatus
};
use idle_tactics::{self, IdleTactics, BQTileFlags};
use bw;
use game::Game;


#[derive(Debug, Serialize, Deserialize, Clone, Copy, Eq, PartialEq)]
pub enum ResType {
	Ore,
	Gas,
	Any,
}

pub unsafe fn parse_resource(text: &str) -> ResType{
	let result = match text {
		"ore"|"minerals"|"min"=>ResType::Ore,
		"gas"=>ResType::Gas,
		_=>ResType::Any,
	};
	result
}


//IfNeeded,Ifreq must be REPLACED WITH BOOL MODIFIER!!!
fn player_resources(res: ResType, player: PlayerMatch, game: Game)->i32{
		use std::cmp;
		let mut res_count = 0;
		for (p,_bool) in player.players.iter().enumerate() {
			let v = match res {
				ResType::Ore=>game.minerals(p as u8) as i32,
				ResType::Gas=>game.gas(p as u8) as i32,
				_=>cmp::max(game.minerals(p as u8),game.gas(p as u8)) as i32,
			};
			res_count += v;
		}
		res_count
	}

unsafe fn tile_has_flag(tile_pos: bw::Point, tile_flags: &idle_tactics::BQTileFlags, 
						game: Game, player: u8)->(bool,u32) {
	//originally tile_pos was Option for iterator find
	let mut result = (false,0);
//	if let Some(tile_pos) = tile_pos {
	let map_width = (*game.0).map_width_tiles as i16;
	let x_tile = tile_pos.x/32;
	let y_tile = tile_pos.y/32;
	let offset = x_tile+(map_width * y_tile);
	let tile = *(*bw::tile_flags).offset(offset as isize);
	match tile_flags {
		BQTileFlags::Creep=>{
			if tile & 0x400000 !=0 || tile & 0x40000000 != 0 {
				result.0 = true;
				result.1 |= 0x1;
				return result;
			}
		},
		BQTileFlags::Power=>{
			let powered = bw::is_powered(
				(x_tile as u32) * 32,
				(y_tile as u32) * 32,
				player,
				160,//gateway, used as placeholder structure requiring psi
			);
			result.0 = powered != 0;
			if result.0 {
				result.1 |= 0x2;
			}
			return result;
		},
	}	
	result
}


fn unit_in_town(unit: Unit, town: Option<Town>)->bool{
	if let Some(town) = town {
		if unit.id().is_worker(){
			return town.has_worker(unit);
		}
		else {
			return town.has_building(unit);
		}
	}
	false
}

fn units_in_area(unit_id: &UnitMatch, players: PlayerMatch, src: &aiscript::Position, loc_mod: LocalModifier, search: &UnitSearch, town: Option<Town>)->u32{
	let count = search
        .search_iter(&src.area)
        .filter(|u| players.matches(u.player()) && unit_id.matches(u))
		.filter(|u| loc_mod == LocalModifier::Global || unit_in_town(*u,town))
        .count() as u32;
		
	count
}


/*

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BuildQueue {
	pub player: u8,
	pub amount: u32,
	pub flags: BuildQueueFlags,
	pub unit_id: UnitId,
    pub town: Option<Town>,
    pub priority: u8,
	pub delay: u32,
	//
	pub last_frame_status: BuildQueueFlagStatus,
}	

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BaseLayout {
    pub pos: bw::Rect,
    pub player: u8,
    pub unit_id: UnitId,
    pub amount: u8,
    pub town_id: u8,
    pub town: Town,
    pub priority: u8,
	pub unique_id: String,
}

*/

/*
#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BuildQueueFlags {
	pub resources: Vec<(u8,MultiModifier,u32,ResType)>,//player-dependent
	pub tiles: Vec<(String,BQTileFlags,bool)>,//player and worker (!)-dependent
	pub ifreqs: Vec<(BoolModifier,Option<UnitId>)>,//player-dependent 
	pub owns: Vec<(u8,MultiModifier,u32,UnitId,LocalModifier,aiscript::Position)>,//player-dependent
	pub if_layouts: Vec<String>,//player-dependent
	pub any_layouts: Vec<BoolModifier>,
	pub ifneeded: Vec<((u32,UnitId))>,//player-dependent
	pub spending_type: SpendingType,
	pub action_type: ActionType,
}

Resources(%AplayerId %Boperator %CresourceQuantity %DresourceType)
Tile(%AlayoutId [from new_layout] %Boperator [wait or 0] %Cflag [creep or power] %Dboolean)
IfReq(%Aoperator %Bboolean [%CunitId]) | if %CunitId is present, check that unit's tech requirements instead of %2unitId's requirements
Own(%Aplayerid %Boperator %Cquantity %Dunitid [%EtownCheck] [local or global] [%Farea]) | compare %Aplayerid %Cquantity of %Dunitid using %Boperator, taking %EtownCheck and %Farea into account
IfLayout(%booleanId)
AnyLayout(%layoutId [from new_layout]) | can be combined with | to take multiple identifiers
IfNeeded | like player_need
Type(%Atype [request or queue])
Add | adds %1quantity to existing queues/requests, like add_build
Subtract | subtract %1quantity
Max
*/

impl BuildQueue {
	pub unsafe fn set_flags(&mut self, game: Game, search: &UnitSearch, layouts: &mut BaseLayouts){
		self.last_frame_status.status = false;
		self.last_frame_status.creep = false;
		self.last_frame_status.power = false;
		self.last_frame_status.layouts.clear();
		if self.flags.resources.len()>0 && !self.flags.resources.iter().any(|(player,op,q,restype)| 
					idle_tactics::check_modifier(*op,player_resources(*restype,*player,game),*q as i32)){
			
			return;
		}
		
/*		if self.flags.tiles.len()>0 && !self.flags.tiles.iter().any(|(layoutId,bqflags,condition)|
			tile_has_flag(layouts.layouts.iter()
			.find(|x| x.unique_id==*layoutId)
			.map(|x| bw::point_from_rect(x.pos)), bqflags, game, self.player)==condition.value)
		{
			return;//originally not tuple
		};		*/
		if self.flags.tiles.len()>0 {
			let mut layout_count = 0;
			for (layoutId,bqflags,condition) in self.flags.tiles.iter(){
				for x in layouts.layouts.iter(){
					if x.unique_id==*layoutId {
						let (tile_result,flags) = tile_has_flag(bw::point_from_rect(x.pos), bqflags, 
															game, self.player);//remove unwrap later
						self.last_frame_status.creep = flags & 0x1 != 0;
						self.last_frame_status.power = flags & 0x2 != 0;
						if tile_result==condition.value {
							layout_count+=1;
							self.last_frame_status.layouts.push(x.clone());
						}					
					}
				}
			}
			if layout_count == 0 {
				return;
			}
		}
		
		
		
		/*
		//can't be used because of...issues
		if !self.flags.ifreqs.iter().any(|(condition,unit_id_opt)|
			if let Some(id) = unit_id_opt {
				bw::check_unit_dat_requirements(self.player as u32,id.0 as u32,
			}
			else {
			
			}
		){
			return;
		}*/ //CAN'T BE USED HERE, UNIT-SPECIFIC
		
		if self.flags.owns.len()>0 && !self.flags.owns.iter().any(|(player,op,q,unit_id,loc_mod, area)| 
			idle_tactics::check_modifier(*op,units_in_area(unit_id,*player,area,*loc_mod,search,self.town) as i32,*q as i32) )
		{
			return;
		}
//		if !self.flags.if_layouts.iter().any(|boolmod| 

/*		if self.flags.any_layouts.len()>0 && !self.flags.any_layouts.iter()
					.any(|layoutId| layouts.layouts.iter().any(|x| x.unique_id==*layoutId && x.player==self.player))
		{
			return;
		}*/
		if self.flags.any_layouts.len()>0 {
			let mut layout_count = 0;
			for layoutId in self.flags.any_layouts.iter(){
				for x in layouts.layouts.iter(){
					if x.unique_id==*layoutId && x.player==self.player{
						layout_count+=1;
						self.last_frame_status.layouts.push(x.clone());
					}
				}
			}
			if layout_count == 0 {
				return;
			}
		}
		
		
		//ifneeded
		//type 
		
		self.last_frame_status.status = true;
	}
}

pub unsafe fn global_buildqueue(
	globals: &mut Globals,
    search: &UnitSearch,
	game: Game,
) {
	let config = config::config();
	for build in &mut globals.queues.build_queue {
		build.set_flags(game,search,&mut globals.base_layouts);
		if let Some(unit) = build.assigned_worker {
			if !unit.is_constructing_building(){
				build.status=BuildQueueStatus::Init;
				build.assigned_worker=None;
			}
		}
		match build.status {
			BuildQueueStatus::MoveToBuild=>{
				build_queue_move(build, &config,game);
			},
			BuildQueueStatus::Constructing=>{
				build_queue_construct(build, &config,game);
			},		
			BuildQueueStatus::SCV_ReconstructMove=>{
			
			},
			_=>{},
		}
	}
	globals.queues.build_queue.swap_retain(|x| x.status!=BuildQueueStatus::End);
}

pub fn is_protoss_morph(
	id: UnitId)->bool{
		let result = match id.0 {
			158|161|182|183|184=>true,
			_=>false,
		};
		result
	}
pub unsafe fn can_build(
	game: Game,
	unit: Unit,
	id_built: UnitId,
	town: Town,)->bool {
		let mut can = false;
		
		if town.has_building(unit) || town.has_worker(unit){
			can = true;
		}
		use bw_dat::order::{DIE, DRONE_BUILD, SCV_BUILD, PROBE_BUILD, CONSTRUCTING_BUILDING, UNIT_MORPH, 
		BUILDING_MORPH,RETURN_GAS,RETURN_MINERALS};
		match unit.order() {
			DIE | DRONE_BUILD | SCV_BUILD | PROBE_BUILD | CONSTRUCTING_BUILDING | UNIT_MORPH | 
			BUILDING_MORPH |RETURN_GAS |RETURN_MINERALS=>{
				return false;
			},
			_=>{},
		}
		if !can || !has_resources(game,unit.player(),&ai::unit_cost(id_built)) || unit.is_disabled() {
			return false;
		}
		if !unit.is_completed(){
			return false;
		}
		if unit.order().0==0 || unit.sprite().is_none() {
			return false;
		}
		if (*unit.0).ai==null_mut(){
			return false;
		}
		
		return true;
	}
	
pub unsafe fn build_queue_move(	
	build: &mut BuildQueue,
	config: &config::Config,
	game: Game
){
	use bw_dat::order::{DRONE_BUILD, SCV_BUILD, PROBE_BUILD};
	match build.assigned_worker.unwrap().order(){
		DRONE_BUILD|SCV_BUILD|PROBE_BUILD=>{},
		_=>{
			build.status = BuildQueueStatus::Constructing;
			bw_print!("Constructing");
		},
	}
}

pub unsafe fn build_queue_construct(
	build: &mut BuildQueue,
	config: &config::Config,
	game: Game
){
	use bw_dat::order::DIE;
	use bw_dat::unit::SCV;
	if let Some(worker) = build.assigned_worker {
		match worker.order(){
			DIE=>{
				let mut is_terran_worker = false;
				if worker.id().0==7 {//scv
					is_terran_worker = true;
				}
				if config.overlord {
					if worker.id().0==77 || worker.id().0==41 {
						is_terran_worker = true;
					}
				}
				if is_terran_worker {
					build.status = BuildQueueStatus::SCV_ReconstructPick;
				}
			},
			_=>{
				build.status = BuildQueueStatus::End;
			},
		}	
	}
}

pub unsafe fn set_layout_pos(layout: &BaseLayout, game: Game, search: &UnitSearch, unit_id: UnitId, town: *mut bw::AiTown,
					builder: Unit,
				  	assume_pushing_units: bool)->Option<bw::Point>{
	let rect_x = layout.pos.right / 32;
	let rect_y = layout.pos.bottom / 32;
	let pos_x = layout.pos.left / 32;
	let pos_y = layout.pos.top / 32;
	let offset_x = layout.pos.left - (pos_x * 32);
	let offset_y = layout.pos.top - (pos_y * 32);
	for i in pos_x..rect_x + 1 {
		for j in pos_y..rect_y + 1 {												 
			let mut ok = aiscript::check_placement(game, search, builder, i, j, unit_id,assume_pushing_units,
										bw::Point{x: (i*32)+offset_x,y: (j*32+offset_y)});	
			if ok {
				let mut workers = (*town).workers;
				while workers != null_mut() {
					let current = (*workers).parent;
					if current != builder.0 {
						if (*current).order_target_pos.x == i * 32 &&
							(*current).order_target_pos.y == j * 32
						{
							ok = false;
							break;
						}
					}
					workers = (*workers).next;
				}
			}
			if ok {
				return Some(bw::Point{x: (i*32)+offset_x, y: (j*32)+offset_y });				
			}
		}
	}
	None
}
pub unsafe fn build_queue_init(
	build: &mut BuildQueue,
	layouts: &mut BaseLayouts,
	search: &UnitSearch,
	config: &config::Config,
	picked_unit: Unit,
	game: Game
){
	if build.last_frame_status.status && build.player==picked_unit.player() 
		&& bw::check_unit_dat_requirements(picked_unit.player() as u32, build.unit_id.0 as u32, picked_unit.0)==1 {
		if /*is_zerg_morph(picked_unit.id()) || */ is_protoss_morph(picked_unit.id()) && config.hydra_misc {
		
		}
		else {
			if build.delay != 0 {
				build.delay = build.delay.saturating_sub(1);
				return;
			}			
			if let Some (town) = build.town {
				if !can_build(game,picked_unit,build.unit_id,town) || picked_unit.id().race()!=build.unit_id.race(){
					return;
				}
				use bw_dat::unit::*;
				let mut pos = bw::Point{x: 0, y: 0};
				let ptr = &mut pos as *mut bw::Point;
				let mut range = 64;
				if build.unit_id==MISSILE_TURRET {
					range = 40;
				}
				//if layoutIds from any/tile - use possible ones
				//either - set flag to run orig
				
				//
				// Causes panics
				//
				
				// Add layout to exclude list
				use bw_dat::unit::*;
				let can_build = match build.last_frame_status.layouts.len()>0 {
					true=>{
						let mut result = false;
						for l in build.last_frame_status.layouts.iter(){
							let position = set_layout_pos(&l,game,search,build.unit_id,town.0,picked_unit,true);
							if let Some(position) = position {
								pos = position;
								result = true;
							}
						}
						result
					},
					_=>{
						//(*picked_unit.0).order_signal |= 0x80;//smh
						let result = bw::choose_placement_position_func(build.unit_id.0 as u32, (*town.0).position, 
													ptr, range, picked_unit.0)!=0;
						//(*picked_unit.0).order_signal &= !0x80;
						result
					},
				};
				if can_build {
					let order = match picked_unit.id(){
						SCV=>0x1e,
						DRONE=>0x19,
						PROBE=>0x1f,
						_=>0x1e,
					};
					picked_unit.issue_order_ground(OrderId(order), pos);
					let result = bw::prepare_build_unit(build.unit_id.0 as u32, picked_unit.0);
					if result != 0 {
						build.status = BuildQueueStatus::MoveToBuild;
						build.assigned_worker = Some(picked_unit);
					}
				}	
			}
		}
	}		
}
	/*
pub unsafe fn refinery_type(unit_id: UnitId)->Option<UnitId>{
	use bw_dat::unit::*;
	let result = match unit_id {
		SCV=>REFINERY,
		DRONE=>DRONE,
		PROBE=>ASSIMILATOR,
		_=>{return None;},
	};
	Some(result)
}*/

pub unsafe fn unit_buildqueue_hook(
	
    queues: &mut Queues,
	layouts: &mut BaseLayouts,
	search: &UnitSearch,
	config: &config::Config,
    picked_unit: Unit,
	game: Game
) {
	let config = config::config();
	for build in &mut queues.build_queue {
		match build.status {
			BuildQueueStatus::Init=>{
				build_queue_init(build, layouts, search, &config,picked_unit,game);
			},
			BuildQueueStatus::SCV_ReconstructPick=>{
				//build_queue_reconstruct(build, &config, picked_unit, game);
			},
			_=>{},
		}

	}
}

/*fn is_turret(built_id: UnitId, config: &config::Config)->bool{
	use bw_dat::unit::*;
	return built_id==MISSILE_TURRET;
}*/


			
