extern crate bw_dat;

#[cfg(debug_assertions)]
extern crate backtrace;
extern crate bincode;
#[macro_use]
extern crate bitflags;
extern crate byteorder;
extern crate bstr;
extern crate chrono;
extern crate directories;
extern crate fern;
extern crate fxhash;
#[macro_use]
extern crate lazy_static;
extern crate libc;
#[macro_use]
extern crate log;
#[macro_use]
extern crate memoffset;
extern crate parking_lot;
//extern crate pathfinding;
extern crate quick_error;
//extern crate combine;
extern crate rand;
extern crate rand_xorshift;
extern crate rstar;
#[macro_use]
extern crate scopeguard;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate smallvec;
extern crate thread_local;
extern crate winapi;
#[macro_use]
extern crate whack;
extern crate samase_shim;
#[macro_use] extern crate failure;

#[cfg(feature = "opengl")]
extern crate cgmath;
#[cfg(feature = "opengl")]
extern crate euclid;
#[cfg(feature = "opengl")]
extern crate font_kit;
#[cfg(feature = "opengl")]
extern crate gl as opengl;
#[cfg(feature = "opengl")]
extern crate glium;

#[macro_use]
mod macros;

pub mod mpqdraft;
pub mod samase;

#[cfg(feature = "opengl")]
mod gl;

mod ai;
mod ai_spending;
mod aiscript;
mod block_alloc;
mod bw;
mod datreq;
mod game;
mod globals;
mod hooks;
mod idle_orders;
mod idle_tactics;
mod build_queue;
mod data_extender;
mod config;
mod flingy;
mod list;
mod parse;
mod extended_mapping;
mod overlord;
mod order;
mod pathfinding;
mod recurse_checked_mutex;
mod rng;
mod script;
mod swap_retain;
mod todborne;
mod toolbox;
mod unit;
mod unit_search;
mod windows;

use std::ptr::null_mut;
use std::sync::atomic::{AtomicBool, Ordering};

use libc::c_void;
use winapi::um::processthreadsapi::{GetCurrentProcess, TerminateProcess};
use config::{GTP_OpcodeData,WeaponsDatEntry};
use crate::game::Game;
use crate::globals::Globals;
use crate::unit::Unit;

//use bw_dat::{TechId, UnitId, UpgradeId};

lazy_static! {
    static ref PATCHER: whack::Patcher = whack::Patcher::new();
}

fn init() {
    if cfg!(debug_assertions) {
        let _ = fern::Dispatch::new()
            .format(|out, message, record| {
                out.finish(format_args!(
                    "{}[{}:{}][{}] {}",
                    chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                    record.file().unwrap_or(""),
                    record.line().unwrap_or(0),
                    record.level(),
                    message
                ))
            })
            .level(log::LevelFilter::Trace)
            .chain(fern::log_file("aise.log").unwrap())
            .apply();
    }

    std::panic::set_hook(Box::new(|info| {
        use std::fmt::Write;

        #[cfg(debug_assertions)]
        fn backtrace() -> String {
            use std::path::Path;

            let mut backtrace = String::new();
            backtrace::trace(|frame| {
                let ip = frame.ip();
                let symbol_address = frame.symbol_address();

                backtrace::resolve(ip, |symbol| {
                    let mut line = format!("    {:p}", symbol_address);
                    if symbol_address != ip {
                        write!(line, " ({:p})", symbol_address).unwrap();
                    }
                    let module = windows::module_from_address(symbol_address as *mut _);
                    if let Some((name, base)) = module {
                        if let Some(fname) = Path::new(&name).file_name() {
                            write!(line, " {:?} {:p}", fname, base).unwrap();
                        } else {
                            write!(line, " {:?} {:p}", name, base).unwrap();
                        }
                    }
                    if let Some(name) = symbol.name() {
                        write!(line, " -- {}", name).unwrap();
                    }
                    if let Some(filename) = symbol.filename() {
                        if let Some(lineno) = symbol.lineno() {
                            write!(line, " -- {:?}:{}", filename, lineno).unwrap();
                        } else {
                            write!(line, " -- {:?}:???", filename).unwrap();
                        }
                    }
                    writeln!(backtrace, "{}", line).unwrap();
                });
                true // keep going to the next frame
            });
            backtrace
        }

        #[cfg(not(debug_assertions))]
        fn backtrace() -> String {
            "".into()
        }

        let mut msg = String::new();
        match info.location() {
            Some(s) => writeln!(msg, "Panic at {}:{}", s.file(), s.line()).unwrap(),
            None => writeln!(msg, "Panic at unknown location").unwrap(),
        }
        let payload = info.payload();
        let panic_msg = match payload.downcast_ref::<&str>() {
            Some(s) => s,
            None => match payload.downcast_ref::<String>() {
                Some(s) => &s[..],
                None => "(???)",
            },
        };
        writeln!(msg, "{}", panic_msg).unwrap();
        if cfg!(debug_assertions) {
            write!(msg, "Backtrace:\n{}", backtrace()).unwrap();
        }
        error!("{}", msg);
        windows::message_box("Aise panic", &msg);
        unsafe {
            TerminateProcess(GetCurrentProcess(), 0x4230daef);
        }
    }));
}

static IS_1161: AtomicBool = AtomicBool::new(false);

#[cfg(debug_assertions)]
fn feature_disabled(name: &str) -> bool {
    use parking_lot::Mutex;
    lazy_static! {
        static ref DISABLED_FEATURES: Mutex<Option<Vec<String>>> = Mutex::new(None);
    }
    let mut disabled_features = DISABLED_FEATURES.lock();
    let disabled_features = disabled_features.get_or_insert_with(|| unsafe {
        let feats = [
            "attack_to",
            "attack_timeout",
            "issue_order",
            "if_attacking",
            "unstart_campaign",
            "set_town_id",
            "remove_build",
            "max_workers",
            "under_attack",
            "aicontrol",
            "supply",
            "resources",
            "reveal_area",
            "load_bank",
            "remove_creep",
            "time",
            "attacking",
            "unit_name",
            "deaths",
            "wait_rand",
            "kills_command",
            "player_jump",
            "upgrade_jump",
            "load_bunkers",
            "unit_avail",
            "tech_jump",
            "tech_avail",
            "random_call",
            "attack_rand",
            "bring_jump",
            "base_layout",
            "queue",
            "lift_land",
            "guard",
            "create_script",
            "idle_orders",
            "everything_else",
        ];
        let (data, len) = match samase::read_file("samase\\aise_disabled_features.txt") {
            Some(s) => s,
            None => return Vec::new(),
        };
        let slice = std::slice::from_raw_parts(data, len);
        let mut result = Vec::new();
        for line in slice.split(|&x| x == b'\n') {
            let line = String::from_utf8_lossy(line);
            let line = line.trim();
            if line.starts_with("#") || line.starts_with(";") || line.is_empty() {
                continue;
            }
            if !feats.iter().any(|&x| x == line) {
                let msg = format!("Feature '{}' not known", line);
                windows::message_box("Aiscript extension plugin", &msg);
                TerminateProcess(GetCurrentProcess(), 0x4230daef);
            }
            result.push(line.into());
        }
        result
    });
    disabled_features.iter().any(|x| x == name)
}

#[cfg(not(debug_assertions))]
fn feature_disabled(_name: &str) -> bool {
    false
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern fn Initialize() {
    IS_1161.store(true, Ordering::Release);
    // 1.16.1 init
    unsafe {
        let f: fn() = || {
            let ctx = samase_shim::init_1161();
            samase::samase_plugin_init(ctx.api());

            let mut active_patcher = ::PATCHER.lock().unwrap();

            #[cfg(feature = "opengl")]
            gl::init_hooks(&mut active_patcher);

            let mut exe = active_patcher.patch_exe(0x00400000);
            bw::init_funcs(&mut exe);
            bw::init_vars(&mut exe);
			let config=config::config();
            if !feature_disabled("everything_else") {

                exe.hook_opt(bw::increment_death_scores, aiscript::increment_deaths);
                exe.hook_opt(
                    bw::choose_placement_position,
                    aiscript::choose_building_placement,
                );
				/*
                exe.hook_opt(
                    bw::update_building_placement_state_hook,
                    aiscript::update_placement_hook,
                );*/ //new unit mover, old code disabled
                exe.hook_opt(bw::ai_spellcast, aiscript::ai_spellcast_hook);
                exe.hook_opt(bw::get_unit_name, aiscript::unit_name_hook);
                exe.hook_opt(bw::ai_focus_unit_check, aiscript::ai_attack_focus_hook);
				exe.hook_opt(bw::start_building, aiscript::start_building_hook);
				exe.hook_opt(bw::drone_land, aiscript::drone_land_hook);		
				exe.hook_opt(bw::start_zerg_building,aiscript::start_zerg_building_hook);	
				
				exe.hook_opt(bw::add_spending_request,aiscript::add_spending_request_hook);	
				exe.hook_opt(bw::ai_add_supply_requests,aiscript::supply_requests);	
				
				//count
				//
				
				//for vorvalings
				
				exe.hook_opt(bw::progress_military_ai,aiscript::progress_military_ai_hook);
				
				//Project Hydra
				exe.hook_opt(bw::ai_spending_req_trainunit,aiscript::ai_spending_req_trainunit_hook);
				
				
				exe.hook_opt(bw::isUnselectable,aiscript::unselectable_hook);
				exe.hook_opt(bw::ai_set_finished_unit_ai,aiscript::ai_set_finished_unit_ai);
				exe.hook_opt(bw::ai_count_economy_units,aiscript::ai_count_economy_units);
				
				//exe.hook_opt(bw::check_dat_req,aiscript::check_dat_req_hook);
				{
					//safety checks
					exe.hook_opt(bw::remove_from_ai_structs, hooks::remove_from_ai_structs);
					exe.hook_opt(bw::increase_upgrade_level, hooks::increase_upgrade_level);
				}
//				exe.hook_opt(bw::building_vision_sync, hooks::building_vision_sync_hook);
				
				//NYDUS, ENABLE LATER
				if config.ai_nydus_move && !config.overlord {
					exe.hook_opt(bw::order_zergbuildself,aiscript::zergbuildself_hook);
					exe.hook_opt(bw::ai_prepare_movingto,aiscript::preparemovingto_hook);				
				}
				//exe.hook_opt(bw::get_portrait, aiscript::get_portrait_hook);
				//exe.hook_opt(bw::progress_speed,aiscript::progress_speed);
				
				if config.bullet_hardcode.modified {
					exe.hook_opt(bw::progress_flingy_turning,aiscript::progress_flingy_turning);
				}
				//if config.cullen_resource_micro {
					exe.hook_opt(bw::return_resources,aiscript::return_resources_hook);
				//}
				
				
				if config.debug_upgrade_req {
					exe.hook_opt(bw::check_upgrade_req_hook,aiscript::check_upgrade_req_hook);
				}
				//DISABLE LATER
				//exe.hook_opt(bw::ai_spendreq_buildmorph, aiscript::ai_spendreq_buildmorph);
				if config.allow_any_race_build {
					exe.hook_opt(bw::are_same_race_hook,aiscript::are_same_race_hook);
				}
				if config.extended_tooltips {
					exe.replace_val(bw::TooltipSurfaceBytes, 0xa0u32 * 480);
					exe.replace_val(bw::TooltipSurfaceHeight, 480u16);
					exe.replace_val(bw::TooltipTextSurfaceBytes, 0xa0u32 * 480);
					exe.replace_val(bw::TooltipTextSurfaceHeight, 480u16);
				}
				if config.hydra_misc {
					exe.hook_opt(bw::order_defensive_matrix_hook, hooks::order_defensive_matrix_hook);
					//exe.hook_opt(bw::update_nydus_placement, hooks::update_nydus_placement_state_hook);//bugged
					exe.hook_opt(bw::cancel_building_morph_hook, hooks::cancel_building_morph_hook);
					//exe.hook_opt(bw::order_building_morph_hook, hooks::order_building_morph_hook);//bugged
				}
				
				/*
				if config.extended_attacks {
					exe.hook_opt(bw::ai_remove_from_attack, hooks::ai_remove_from_attack);
					exe.hook_opt(bw::ai_add_to_attackforce, hooks::ai_attack_add);				
					exe.hook_opt(bw::has_units_for_attack_force_hook, hooks::has_units_for_attack_force_hook);				
					exe.hook_opt(bw::ai_attack_to_hook, hooks::ai_attack_to_hook);					
					exe.hook_opt(bw::get_missing_attack_force_units_hook, hooks::get_missing_attack_force_units_hook);			
					exe.hook_opt(bw::grouping_for_attack, hooks::grouping_for_attack);
					exe.hook_opt(bw::train_attack_force, hooks::train_attack_force);		
				}
				
				*/
				
				
				//
				exe.hook_opt(bw::load_game_data, hooks::load_game_data);
				if config.extended_data {
					data_extender::init_image_ext_new(&mut exe);
				}
				
				if config.extended_data {
					//
					//sprites.dat rewrite
					//
					data_extender::init_new_sprite_ext(&mut exe);
				}
				
				//
				
				
				exe.hook_opt(bw::ai_finish_unit,aiscript::ai_finish_unit_hook);
				exe.hook_opt(bw::try_progress_spending_queue_hook,aiscript::try_progress_spending_queue_hook);
				//exe.hook_opt(bw::order_ai_pickup,aiscript::order_ai_pickup);
				
				
				//exe.hook_opt(bw::unknown_gas_func,aiscript::unknown_gas_func_hook);
				//
				
				//patcher for fake gptp function
				exe.hook_opt(bw::get_psi_statement, aiscript::get_psi_statement_hook);

				
				/*
				exe.hook_opt(bw::update_creep_disappearance,aiscript::creep_recede_hook);//
				exe.hook_opt(bw::does_provide_creep,aiscript::does_provide_creep_hook);//
				
				exe.hook_opt(bw::update_creep_disappearance_2,aiscript::creep_disappearance_2);
				exe.hook_opt(bw::stop_creep_disappearing,aiscript::stop_creep_hook);	
				exe.hook_opt(bw::unk_creep_func,aiscript::unk_creep_hook);*/
				
				//
				if config.new_energy_ui {//bug EDX
					exe.hook_opt(bw::ss_draw_energy,aiscript::ss_draw_energy_hook);
				}
				
				exe.hook_opt(bw::irradiate,aiscript::irradiate_hook);
//				exe.hook_opt(bw::ai_make_detector_behavior, hooks::ai_make_detector_behavior);
//				exe.hook_opt(bw::ai_workerai, hooks::ai_workerai);
				//for test only!
				
				
				exe.hook_opt(bw::maelstrom_proc,hooks::maelstrom_proc);
				
				/*
				exe.hook_opt(bw::ai_tryprogressspendingqueue_worker_hook,
							hooks::ai_tryprogressspendingqueue_worker_hook);*/
							
				exe.hook_opt(bw::ai_workerai, hooks::ai_workerai);
				exe.hook_opt(bw::update_placement_mapflags, hooks::maptileflag_placement_hook);
				exe.hook_opt(bw::is_tile_blocked_by_hook, hooks::is_tile_blocked_by_hook);
				
				
				
				//exe.hook_opt(bw::update_building_placement_units, aiscript::update_building_placement_units);
				
				
				//for test only!
				//exe.hook_opt(bw::get_miss_chance,aiscript::get_miss_chance);
				/*if config.no_miss_chance {
					exe.replace_val(0x0048C0B6+1, 0x01u8);							
				}*/
//				exe.hook_opt(bw::has_rally_hook,aiscript::has_rally_hook);
				if config.overlord {
//					exe.hook_opt(bw::create_unit_hook,hooks::create_unit);
					exe.hook_opt(bw::ss_drawsupplyunitstatus,aiscript::ss_drawsupplyunitstatus);
					exe.hook_opt(bw::modify_unit_counters2,aiscript::unit_counters2_hook);
					exe.hook_opt(bw::finish_unit_hook,aiscript::finish_unit_hook);
					exe.hook_opt(bw::kill_unit_hook, aiscript::kill_unit_hook);
					exe.hook_opt(bw::is_resource_container_for_blocking,aiscript::is_resource_container_for_blocking);
					exe.hook_opt(bw::get_upgrade_level_hook, hooks::get_upgrade_level_hook);
					
					
					//exe.hook_opt(bw::update_creep_building_placementstate,aiscript::creep_maptileflag_placement_hook);
				//	exe.hook_opt(bw::find_blocking_resource, aiscript::find_blocking_resource);
					//exe.hook_opt(bw::increment_kills,aiscript::increment_kills);
					//exe.hook_opt(bw::ss_drawkills,aiscript::ss_drawkills);

				}
				
				exe.hook_opt(bw::ai_reacttohit,aiscript::ai_reacttohit);
				//Testing only!
				
				
				
				//
				
				//exe.hook_opt(bw::draw_sprite,aiscript::draw_sprite_hook);	
				//exe.hook_opt(bw::ss_drawarmor,aiscript::ss_drawarmor_hook);	
				exe.hook_opt(bw::try_begin_delete_town,aiscript::try_begin_delete_town);
				exe.hook_opt(bw::delete_ai_town_if_needed,aiscript::delete_ai_town_if_needed);
				
				//
			    exe.hook_opt(bw::do_weapon_damage,aiscript::do_weapon_damage);
				exe.hook_opt(bw::modify_speed,aiscript::modify_speed_hook);
				exe.hook_opt(bw::attack_cooldown,aiscript::attack_cooldown_hook);
				exe.hook_opt(bw::bullet_state_init,aiscript::bullet_state_init_hook);
				exe.hook_opt(bw::reaver_check_movement_distance,aiscript::reaver_check_movement_distance_hook);
				
				
				exe.hook_opt(bw::initialize_bullet,aiscript::initialize_bullet_hook);
				
				
				//exe.hook_opt(bw::do_missile_dmg,aiscript::missile_dmg_hook);	
				//gptp-aise helper
				exe.hook_opt(bw::can_target_spell, aiscript::can_target_spell_fromgptp);
				//debugging
				//exe.hook_opt(bw::order_computerguard,aiscript::order_computerguard);
				//exe.hook_opt(bw::try_return_home,aiscript::return_guard_hook);
				exe.hook_opt(bw::modify_unit_counters1,aiscript::unit_counters1_hook);
				
				
//				exe.hook_opt(bw::can_load_unit, aiscript::can_load_unit_hook);
				exe.hook_opt(bw::order_upgrade,aiscript::order_upgrade);
				exe.hook_opt(bw::order_tech,aiscript::order_tech);
				
				
	//			exe.hook_opt(bw::move_sprite,aiscript::move_sprite);
				//testing only
				//exe.hook_opt(bw::can_attack_unit,aiscript::can_attack_unit);
				
				//attempt to solve guard crash
				
				exe.hook_opt(bw::add_guard_ai,aiscript::add_guard_ai);
				exe.hook_opt(bw::pre_create_guard_ai,aiscript::pre_create_guard_ai);
				
				
				//
				exe.hook_opt(bw::iscript_playframe, aiscript::playframe_hook);
				if config.overlord {
					exe.hook_opt(bw::cmdbtn_handler,aiscript::cmdbtn_handler_hook);
				}
				//TODBORNE
//				exe.hook_opt(bw::btnsact_placeaddon,aiscript::btnsact_placeaddon_hook);

				//Pr0nogo
				exe.hook_opt(bw::ai_train_unit_hook,hooks::ai_train_unit_hook);
				exe.hook_opt(bw::set_build_hp_gain_hook,hooks::set_build_hp_gain_hook);
				exe.hook_opt(bw::btnscond_terran_basic_hook,hooks::btnscond_terran_basic_hook);
				exe.hook_opt(bw::btnscond_terran_adv_hook,hooks::btnscond_terran_adv_hook);
				//for escalations - hook don't work (ecx issue maybe?)
				//exe.hook_opt(bw::get_stat_txt_str,aiscript::GetStatTxtString);
				//old - 0x52, 0x57, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5F, 0x5A, 0xC3 ]
				
				//globals::wrapper_patch(&mut exe, &[0x52, 0x57, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5F, 0x5A, 0xC3] ,0x00459411,0,  0);
				//globals::wrapper_patch(&mut exe, &[0x52, 0x57, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5F, 0x5A, 0xC3] ,0x0045947E,0,  0);
	// 0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3 
	/*
	//53575654baf0364c00ffd25c5e5f5bc3*/
				//globals::wrapper_patch(&mut exe, &[0x51, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x59, 0xC3] ,0x00425C28,0,  0);
				/*
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x00457729,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x00457791,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x00457805,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x0045786D,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x004578C9,0,  0;
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x004588E1,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x0049276F,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x0049298B,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x004BE56A,0,  0);
				globals::wrapper_patch(&mut exe, &[0x53, 0x57, 0x56, 0x54, 0xBA, 0xF0, 0x36, 0x4C, 0x00, 0xFF, 0xD2, 0x5C, 0x5E, 0x5F, 0x5B, 0xC3] ,0x004C36F0,0,  0);
*/
				//
				
				//exe.hook_opt(bw::gen_minitile_walkability_arr, aiscript::gen_minitile_walkability_arr);//for testing only
				//exe.hook_opt(bw::set_upgrade,aiscript::set_upgrade);
				
				//exe.nop(0x0042BCB1, 6);
				/*
				exe.replace_val(0x0042BC36, 0x9090u16);
				exe.replace_val(0x0042BC36+2, 0xa1);
				exe.replace_val(0x0042BC36+3, 0x0057f1d4u32);*/
				

				//globals::wrapper_patch(
				/*let code = [
					0x81, 0xFB, 0xFF, 0x7F, 0x00, 0x00, 0x77, 0x01, 0xC3, 0xBB, 0x01, 0x00, 0x00, 0x00, 0xC3
				];
				globals::wrapper_patch(&mut exe, &code,0x0049C6C9,0x0049C6C9+5,  1);
				*/
				
				//hook - computer/human switch
				if config.computer_switch {
//					exe.hook_opt(bw::single_player_init_game, hooks::single_player_init_game_hook);				
//					exe.hook_opt(bw::command_startgame, aiscript::command_startgame_hook);				
				}
				if config.comm_tower_struct {
					exe.hook_opt(bw::damage_unit,aiscript::damage_unit_hook);
				}

				
				//hook - use_tech stuff
				//exe.hook_opt(bw::process_commands,aiscript::process_commands_hook);
				
				let config = config::config();
				if config.extended_megatiles {
					exe.replace_val(0x004BD72B+1,0x40000u32);//allocate 40000 bytes for megatiles
					//
					exe.replace_val(0x004C2CAF+1,0x40000u32);
					exe.replace_val(0x004D0063+1,0x40000u32);
					exe.replace_val(0x004D061A+1,0x40000u32);
					
					//
					let ff = [0xff];			
					globals::code_insert(&mut exe,0x004832EF+2,&ff);//AND EAX,7FFF
					globals::code_insert(&mut exe,0x0042BCB1+3,&ff);//AND ECX,7FFF
					globals::code_insert(&mut exe,0x0049B9FB+3,&ff);//AND ESI,7FFF
					globals::wrapper_patch(&mut exe,&[0x8D, 0x97, 0x18, 0x3F, 0x00, 0x00, 0x31, 0xDB, 0x50, 0x52, 0x51, 0x31, 0xC0, 0x66, 0xA1, 0xD4, 0xF1, 0x57, 0x00, 0x8B, 0x55, 
					0x0C, 0x0F, 0xAF, 0xD0, 0x03, 0x55, 0x08, 0x57, 0x8B, 0x3D, 0x94, 0x84, 0x62, 0x00, 0x8A, 0xBC, 0x57, 0x01, 0x00, 0x02, 0x00, 0x8B, 0x0D, 0x60, 0x12, 0x6D, 
					0x00, 0x8B, 0x0C, 0x91, 
					0xF7, 0xC1, 0x00, 0x00, 0x00, 0x40, 
					0x75, 0x05, 0x5F, 0x59, 0x5A, 0x58, 0xC3, 
					0xF7, 0xC1, 0x00, 0x00, 0x80, 0x00, 
					0x74, 0x05, 0x5F, 
					0x59, 0x5A, 0x58, 0xC3, 0xB7, 0x00, 0x5F, 0x59, 0x5A, 0x58, 0xC3],0x0049BB08,0x0049BB08+5,1);											
				
					globals::code_insert(&mut exe,0x004A4321+3,&ff);
					globals::code_insert(&mut exe,0x004A4351+3,&ff);
					globals::code_insert(&mut exe,0x004A4383+3,&ff);
					globals::code_insert(&mut exe,0x004A43B4+3,&ff);
					globals::code_insert(&mut exe,0x004A41FE+3,&ff);
					globals::code_insert(&mut exe,0x004A427E+3,&ff);	
					//horizontal flag / creep border
					
					//example (old): eax is 2a, ebx is 802a
					//v1
					//offset 0x8000 flag 
					globals::wrapper_patch(&mut exe, &[0x80, 0x8A, 0x01, 0x00, 0x02, 0x00, 0x80, 
					0xC3],0x004BCF30,0x004BCF30+5,5);//old version of hook has 0x10000 instead of 0x20000			
					//or 0x0 in farty's build patch
					
					//chk megatile: set 0x80 flag to shifted array instead of megatile array
					
					globals::wrapper_patch(&mut exe,&[0x80, 0x8C, 0x48, 0x01, 0x00, 0x02, 0x00, 0x80,0xc3],0x0047E2FA,0,0);//the same
					//creep_modify: set 0x80 flag to shifted array
					
					globals::wrapper_patch(&mut exe,&[0x31, 0xDB, 0x66, 0x8B, 0x1E, 0x50,0x31, 0xc0,  0x8A, 0x86, 0x01, 0x00, 0x02, 0x00, 0x83, 0xF8, 
						0x00, 0x74, 0x08, 0x81, 0xCB, 0x00, 0x00, 0x00, 0x80, 0x58, 0xC3, 0x58, 0xC3],0x0049C6C4,0,0);				
					globals::code_insert(&mut exe,0x0049C6C9,&[0x81,0xe3, 0xFF, 0xFF, 0xFF, 0x7F ]);	
					//add 0x8000_0000 flag to register if there's creep flag
					globals::wrapper_patch(&mut exe,&[0x31, 0xDB, 0x66, 0x8B, 0x1A, 0x50,0x31, 0xc0,  0x8A, 0x82, 0x01, 0x00, 0x02, 0x00, 0x83, 0xF8, 
						0x00, 0x74, 0x08, 0x81, 0xCB, 0x00, 0x00, 0x00, 0x80, 0x58, 0xC3, 0x58, 0xC3],0x0049C57B,0,0);
					globals::code_insert(&mut exe,0x0049C580,&[0x81,0xe3, 0xFF, 0xFF, 0xFF, 0x7F ]);
					//add 0x8000_0000 flag to register if there's creep flag
					globals::code_insert(&mut exe,0x0049C6CF,&[0x90, 0x39, 0xd8]);
					//cmp ax,bx -> cmp eax,ebx
					globals::code_insert(&mut exe,0x0049C586,&[0x90, 0x39, 0xd8]);	
					//cmp ax,bx -> cmp eax,ebx

					globals::wrapper_patch(&mut exe,&[0x66, 0x8B, 0x44, 0x58, 0x14, 0x0F, 0xB7, 0xC0, 0xC3],0x0049C576,0,0);			
					//ax->eax
					globals::wrapper_patch(&mut exe,&[0x66, 0x8B, 0x44, 0x58, 0x14, 0x0F, 0xB7, 0xC0, 0xC3],0x0049C6BF,0,0);
					//ax->eax
				}
			}
        };
        samase_shim::on_win_main(f);
    }
}

#[derive(Copy,Clone)]
pub struct SortGuardLayout {
	pub x: i16,
	pub y: i16,
	pub id: u16,
	pub player: u8,
	pub index: usize,
}

unsafe extern fn add_names(names: &mut Vec<(String,u32)>){

	let (data, len) = match samase::read_file("samase\\unitdef.txt") {
		Some(s) => s,
		None => { return;},
	};
	let slice = std::slice::from_raw_parts(data, len);
	for line in slice.split(|&x| x == b'\n') 
	{
		let line = String::from_utf8_lossy(line);
		let line = line.trim();
		if line.starts_with("#") || line.starts_with(";") || line.starts_with("@") || line.is_empty() {
			continue;
		}
		if !(line.starts_with("military") || line.starts_with("building")){
			continue;
		}
		
		let command_list: Vec<&str> = line.split('=').collect();
				
		if command_list.len()==2 {
			let header = command_list[0];
			let header_break: Vec<&str> = header.trim().split(' ').collect();				
			let value: u32 = command_list[1].trim().parse().unwrap();				
			if header_break.len()==2 {
				names.push((header_break[1].to_string(),value));
			}
		}
	}
}



use std::collections::hash_map::Entry;
use std::time::Instant;

unsafe extern fn first_frame_event(globals: &mut Globals, game: Game, first_frame: bool){
		let config = config::config();
		for e in config.weapons_dat_rewrite.iter(){
			match e.entry {
				WeaponsDatEntry::Dmg_Upgrade=>{ 
					bw::weapons_dat_dmg_upg[e.index]=e.value as u8 
				},
				WeaponsDatEntry::Flingy=>bw::weapons_dat_flingy[e.index]=e.value,
			}
		}
	
		if config.escalations_enabled {
			globals.ngs.initialize_escalations();
		}
		if first_frame {
			if let Some(map_data) = samase::read_mapping_tool_data(){	
				extended_mapping::map_data_setup(map_data, globals, game);
				let _ = std::fs::remove_file("dump.bin");
			}
		}
		if config.switchable_type_default.is_some(){
			for i in 0..8 {
				bw::set_upgrade_func(1,i,config.switchable_type_default.unwrap() as u16);
			}			
		}
		
}
unsafe extern fn frame_hook() {
    let search = unit_search::UnitSearch::from_bw();
    let mut globals = Globals::get("frame hook1");
    let globals = &mut *globals;
    let game = game::Game::get();
	//debug!("frame hook lock");
	
	//TIME
	//let start = Instant::now();
	/*
	if game.frame_count() == 0 {
		debug!("Write player races...");
		for i in 0..8 {
			debug!("Player {}, race: {}",i,bw::player_list[i as usize].race);
		}
	}
	*/
	
	
	
	
    aiscript::claim_bw_allocated_scripts(globals);
	
    ai::update_region_safety(&mut globals.region_safety_pos, game, &search);
	globals.set_unit_search();
    aiscript::attack_timeouts_frame_hook(globals, game);
    globals.idle_orders.step_frame(&mut globals.rng, &search);
	//TIME
	//let t_idle_orders = start.elapsed();
	idle_tactics::idle_tactics_frame(&mut globals.idle_tactics,&search,&mut globals.ngs, game);
	//TIME
	//let t_idle_tactics = start.elapsed()-t_idle_orders;
    aiscript::under_attack_frame_hook(globals);
    aiscript::reveal_vision_hook(globals, game);
    ai::update_guard_needs(game, &mut globals.guards);
	//TIME
	//let t_guard_needs = start.elapsed()-t_idle_tactics;	
    ai::continue_incomplete_buildings();
    aiscript::lift_land_hook(&mut globals.lift_lands, &search, game);
    aiscript::queues_frame_hook(&mut globals.queues, &mut globals.upgrades, &mut globals.ngs, &search, game);
	//TIME
	//let t_queues = start.elapsed()-t_guard_needs;	
	//debug!("frame hook queue continue lock");
	let config = config::config();
	aiscript::ngs_hook(&config,&mut globals.ngs);
	
	//TIME
	//let t_ngs = start.elapsed()-t_queues;	
	let guards = samase::guard_ais().add(0 as usize);
	let mut new_ai = (*(*guards).array).first_free;
	//let mut count = 0;
	while new_ai!=null_mut(){
		new_ai = (*new_ai).next;
		//count += 1;
	}
	//bw_print!("Free guards: {}",count);
	//debug!("Free guards: {}",count);	
	
	
	
	if game.frame_count()==0 && globals.init==false {	
		let pathing = bw::pathing();
		let graft = config::grafting_data();
		let mut upgs = graft.upgrades.clone();
		let result = match upgs.entry(62) {
		   Entry::Vacant(entry) => { entry.insert(GTP_OpcodeData::new()) },
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		data_extender::flingy_ext_debug();
		
//		bw_print!("Config excav data: {}, tileset: {}",config.excavation_settings.sets[(*game.0).tileset as usize].len()
//														,(*game.0).tileset);
		
		/*
		if cfg!(debug_assertions) {
			
			let mut graft = config::grafting_data();
			bw_print!("Evaluation...");
			
			for unit in unit::active_units() {
				if unit.id().0 == 137 {//137 is valid
				//
					let n = graft.evaluate_unit(unit,unit.player(),62,game,globals);
					bw_print!("Evaluation results: {}",n);
				}
			}
		}
		*/
		
		/*
		for v in graft.upgrade_reqs.clone() {
			if v.upgrade_id==62 {
				bw_print!("ESCALATE {}",v.upgrade_id);
				for a in v.reqs {
					bw_print!("OPCODE [0] - {} {} {}",a.opcode.id,a.opcode.current_comparison_index,a.opcode.current_value_index);
					for b in a.list {
						bw_print!("OPCODE [1] - {} {} {}",b.opcode.id,b.opcode.current_comparison_index,b.opcode.current_value_index);
						if b.list.len()>0 {
							for c in b.list {
								bw_print!("OPCODE [2] - {} {} {}",c.opcode.id,c.opcode.current_comparison_index,c.opcode.current_value_index);
							}
							
						}
					}
				}
			}
		}*/
		
		for unit in unit::active_units() {
			if unit.id().0==134 {				
				let region_source = bw::get_region(unit.position()).unwrap();
				let source_group = (*pathing).regions[region_source as usize].group;
				globals.waygating.add_nydus(unit,source_group);
			}
		}

		globals.init=true;
		if config.extended_upgrades {
			globals.upgrades.copy_initial_from_dattable(true);//initialize extended upgrades
		}
		first_frame_event(globals,game,true);
		/*
		for e in config.weapons_dat_rewrite.iter(){
			match e.entry {
				WeaponsDatEntry::Dmg_Upgrade=>{ 
					bw::weapons_dat_dmg_upg[e.index]=e.value as u8 
				},
				WeaponsDatEntry::Flingy=>bw::weapons_dat_flingy[e.index]=e.value,
			}
		}
		
		
		if config.escalations_enabled {
			globals.ngs.initialize_escalations();
		}
		if let Some(map_data) = samase::read_mapping_tool_data(){	
			extended_mapping::map_data_setup(map_data, globals, game);
			let _ = std::fs::remove_file("dump.bin");
		}
		if config.switchable_type_default.is_some(){
			for i in 0..8 {
				bw::set_upgrade_func(1,i,config.switchable_type_default.unwrap() as u16);
			}			
		}*/
	}
	if game.frame_count()==0 {
		let map_width = (*game.0).map_width_tiles;
		let map_height = (*game.0).map_height_tiles;
		let area = bw::location(63);
		if !(area.area.left == 0 && area.area.top == 0 && area.area.right == map_width as i32*32 && area.area.bottom == map_height as i32*32){
			
			/*bw_print!("Incorrect Anywhere area detected: current: {} {} {} {}, correct {} {} {} {}",
						area.area.left, area.area.top, area.area.right, area.area.bottom,
						0,0,map_width*32,map_height*32);*/
			(*game.0).locations[63 as usize].area.left = 0;
			(*game.0).locations[63 as usize].area.top = 0;
			(*game.0).locations[63 as usize].area.right = map_width as i32*32;
			(*game.0).locations[63 as usize].area.bottom = map_height as i32*32;
		
						
		}
	}
	//base_layout/guard debug logger
	if cfg!(debug_assertions) && config.overlord {
		
		if game.frame_count()==0 && (*game.0).deaths[227][11]==0 {	
			(*game.0).deaths[227][11]=1;
			 bw_print!("Writing base layout / guard to aise.log...");				 
			 let mut write_guards: Vec<SortGuardLayout>; 
			 let mut write_layouts: Vec<SortGuardLayout>;
			 write_layouts = Vec::new();
			 write_guards = Vec::new();
			 let map_width = (*game.0).map_width_tiles;
			 //bw_print!("Iterate...");
			 debug!("Width: {}",map_width);
			 for unit in unit::active_units() {	
				//let index = 1700-((unit.0 as usize - 0x0059CCA8) / 336); 
				/*let offset = (unit.position().x as i16/32) + map_width as i16*(unit.position().y as i16/32);
				let tile_id = *(*bw::tile_ids).offset(offset as isize);*/
				
				let offset = (unit.position().x/32) as u16 + (((unit.position().y/32) as u16)*map_width);
				let tile_id = *(*bw::tile_ids).offset(offset as isize) as u32;
				
				let index = tile_id;
//				if unit.is_invincible() && !(unit.id().0>=176 && unit.id().0<=178) && unit.id().0!=188 && index>34751{
				if /*tile_id > 34570 && unit.is_hallucination() &&*/ !(unit.id().0>=176 && unit.id().0<=178){
					//bw_print!("good index");
					
					if unit.id().is_building() {
						//bw_print!("Push");
						write_layouts.push(SortGuardLayout{x: unit.position().x, y: unit.position().y, id: unit.id().0, player: unit.player(), index: index as usize});
					}
					else {
						write_guards.push(SortGuardLayout{x: unit.position().x, y: unit.position().y, id: unit.id().0, player: unit.player(), index: index as usize});
					}
				}
				/*if unit.id().0==174 {
					bw_print!("Scrap processor: {}",tile_id);
				}*/
			 }
			 write_layouts.sort_unstable_by_key(|x| x.player as usize); 
			 write_guards.sort_unstable_by_key(|x| x.player as usize); 
			 let mut guardlist: Vec<Vec<SortGuardLayout>>;
			 let mut layoutlist: Vec<Vec<SortGuardLayout>>;
			 let mut g_c: Vec<SortGuardLayout>;
			 let mut l_c: Vec<SortGuardLayout>;
			 guardlist = Vec::new();
			 layoutlist = Vec::new();
			 g_c = Vec::new();
			 l_c = Vec::new();
			 let mut pl = 12;
			 for a in write_layouts {
				if a.player != pl {
					pl = a.player;
					if l_c.len()!=0 {
						l_c.sort_unstable_by_key(|x| x.index as usize);
						layoutlist.push(l_c);
						l_c=Vec::new();
					}
				}
				l_c.push(a);
			 }
			 if l_c.len()!=0 {
					l_c.sort_unstable_by_key(|x| x.index as usize);
					layoutlist.push(l_c);
			}
			 pl = 12;
			 for a in write_guards {
				if a.player != pl {
					pl = a.player;
					if g_c.len()!=0 {
						g_c.sort_unstable_by_key(|x| x.index as usize);
						guardlist.push(g_c);
						g_c=Vec::new();
					}
				}
				g_c.push(a);
			 }
			 if g_c.len()!=0 {
						g_c.sort_unstable_by_key(|x| x.index as usize);
						guardlist.push(g_c);
			}
			let mut names: Vec<(String,u32)>;
			names = Vec::new();
			add_names(&mut names);
			for a in layoutlist {
				debug!("PLAYER {}:",a[0].player);
				for b in a {
					let mut layout_id: &str = &b.id.to_string();
					'outer: for name in &mut names {
						let str_value: &str = &name.0;
						if name.1==b.id as u32 {
							layout_id = str_value;
							break 'outer;
						}
					}
					debug!("base_layout {} Set ({} {}) 1 255 50",layout_id,b.x,b.y);
				}
			}
			for a in guardlist {
				debug!("PLAYER {}:",a[0].player);
				for b in a {
					let mut layout_id: &str = &b.id.to_string();
					'outer2: for name in &mut names {
						let str_value: &str = &name.0;
						if name.1==b.id as u32 {
							layout_id = str_value;
							break 'outer2;
						}
					}
					debug!("guard {} ({} {}) 1 0 5",layout_id,b.x,b.y);
				}
			}
		}
	}
	//debug!("frame hook unit loop continue lock");
	//TIME 
	
	if config.overlord {
		overlord::diplomacy_ai(globals,&search,&config,game);
	}
	build_queue::global_buildqueue(globals, &search, game);
    for unit in unit::active_units() {
		build_queue::unit_buildqueue_hook(&mut globals.queues,&mut globals.base_layouts,&search,&config,unit,game);
		aiscript::ngs_unit_hook(globals, unit);
        aiscript::bunker_fill_hook(&mut globals.bunker_states, unit, &search);
		if config.comm_tower_struct && unit.id().0==190 {
			aiscript::commtower_rally_point_refresh(globals, unit);
		}		
		idle_tactics::idle_tactics_unit_hook(&mut globals.idle_tactics, &mut globals.ngs, unit, &search, game);	
		
        if let Some(ai) = unit.building_ai() {
            let town = (*ai).town;
            if town != null_mut() {
                if let Some(max_workers) = aiscript::max_workers_for(globals, town) {
                    (*town).worker_limit = max_workers;
                }
            }
            let iter = (*ai)
                .train_queue_types
                .iter_mut()
                .zip((*ai).train_queue_values.iter_mut());
            for (ty, val) in iter {
                if *ty == 2 && *val != null_mut() {
                    let ai = *val as *mut bw::GuardAi;
                    if let Some(parent) = Unit::from_ptr((*ai).parent) {
                        if parent != unit {
                            // Guard ai share bug, remove the ai from queue
                            debug!(
                                "Guard AI share for unit 0x{:x} at {:?}",
                                unit.id().0,
                                unit.position()
                            );
                            *val = null_mut();
                            *ty = 0;
                        }
                    }
                }
            }
        }
        if let Some(ai) = unit.guard_ai() {
            // Guard ai share bug, just make this a military then.
            // Should happen for cocoons/lurker eggs, but do it for any just-in-case
            if (*ai).parent != unit.0 {
                debug!(
                    "Guard AI share for unit 0x{:x} at {:?}",
                    unit.id().0,
                    unit.position()
                );
                (*unit.0).ai = null_mut();
/*                let region =
                    ai::ai_region(unit.player(), unit.position()).expect("Unit out of bounds??");*/
					//commit
				let ai_regions = bw::ai_regions(unit.player() as u32);
				let region = ai::ai_region(ai_regions, unit.position()).expect("Unit out of bounds??");
				//debug!("addmilitary continue lock");
                ai::add_military_ai(unit, region, true);
            }
        }
    }
	//TIME
	//let t_units = start.elapsed()-t_ngs;	
	
	//debug!("Time elapsed orders {:?} tactics {:?} guards {:?} queues {:?} ngs {:?} unitloop+buildnew {:?}",t_idle_orders,t_idle_tactics,t_guard_needs,t_queues,t_ngs,t_units);	
	/*if config.extended_attacks && game.frame_count()%1440==0 {
		for i in 0..8 {
			bw_print!("time passed: {} min, player: {}, attackers: {}",game.frame_count()/1440,i,globals.attack_extender[i].unit_ids.len());
		}	
	}*/
	
	#[cfg(feature = "opengl")]
    gl::new_frame(&globals);


    FIRST_STEP_ORDER_OF_FRAME.store(true, Ordering::Relaxed);
	
			
}

unsafe extern fn frame_hook_after() {

	let game = Game::get();
	let search = unit_search::UnitSearch::from_bw();
	aiscript::queues_buildings_frame_hook(game, &search);
    let mut globals = Globals::get("frame hook after");
    aiscript::update_towns(&mut globals);
	
	globals.events.decrement();
    aiscript::attack_timeouts_frame_hook_after(&mut globals);
	let mut events = Vec::new();
	let mut sprite_events = Vec::new();
	aiscript::ngs_event_loop(&mut globals.ngs,&mut events,&mut sprite_events);
	drop(globals);
	aiscript::ngs_events(events,sprite_events);	
}

// For hooking the point after frame's ai step but before any unit orders.
// Not in global struct for performance concern of locking its mutex thousand+
// extra times a frame.
static FIRST_STEP_ORDER_OF_FRAME: AtomicBool = AtomicBool::new(false);

unsafe extern fn step_order_hook(u: *mut c_void, orig: unsafe extern fn(*mut c_void)) {
	let game = Game::get();
    if FIRST_STEP_ORDER_OF_FRAME.load(Ordering::Relaxed) {
        FIRST_STEP_ORDER_OF_FRAME.store(false, Ordering::Relaxed);
        let mut globals = Globals::get("step order hook (start)");
		let globals = &mut *globals;
		//no mut before change
//      ai_spending::frame_hook(globals);
        
        // TODO Init lazily
        let unit_search = unit_search::UnitSearch::from_bw();
        ai_spending::frame_hook(game, &unit_search, globals);
    }

    let unit = unit::Unit(u as *mut bw::Unit);
    let prev_order = unit.order();
    let mut temp_ai = None;
	
    match unit.order() {
/*        order::id::DIE => {
            let mut globals = Globals::get("step order hook (die)");
            globals.unit_removed(unit);
			drop(globals);
        }*/
        order::id::COMPUTER_AI => {
            if let Some(_) = unit.building_ai() {
                let player = unit.player();
                if (*unit.0).order_timer == 0 && player < 8 {
                    // Handle trains here instead of letting BW to handle them with
                    // stupid guard-related behaviour.
                    let ai = ai::PlayerAi::get(player);
                    ai.check_train(unit, game::Game::get());
                }
            }
 
        }
        order::id::ZERG_BIRTH => {
            // See below for unit morph.
            // Make ai temporarily null so bw doesn't try to use it if it's not building.
            let ai = (*unit.0).ai as *mut bw::BuildingAi;
            if ai != null_mut() && (*ai).ai_type != 3 && (*ai).ai_type != 2 {
                temp_ai = Some(ai as *mut c_void);
                (*unit.0).ai = null_mut();
            }
        }
        _ => (),
    }
    orig(u);
	let config = config::config();
    match prev_order {
		order::id::DIE => {
            let mut globals = Globals::get("step order hook (die)");
            globals.ngs.unit_removed(unit);
			drop(globals);		
		
		},
        order::id::UNIT_MORPH => {
            // Fix a bw bug where an unit will keep the egg's building ai for a few frames
            // after being born. If it dies during those frames, bw doesn't remove its ai
            // correctly since it isn't a building anymore.
            // Should be fine to add guard/military now, even though blizzard doesn't do that?
            let should_have_building_ai = |unit: Unit| {
                use bw_dat::unit as id;
				let mut good_egg = true;
				if unit.id() == id::EGG && config.hydra_misc {
					//check if larval egg
					good_egg = match (*unit.0).egg_origin {
						35=>true,
						x=>false,
					};
				}
                match unit.id() {
                    id::LARVA | id::OVERLORD => true,
					id::EGG => good_egg,
                    id::VESPENE_GEYSER => false,
                    x => x.is_building(),
                }
            };
			let config = config::config();
			if config.overlord {
				if (*unit.0).remaining_build_time > 1 {
					let mut globals = Globals::get("step order hook (unit morph, overlord)");
					let switch = globals.ngs.get_generic_switch(unit.0,0);
					if switch == 1 {
						let current_level = aiscript::get_new_upgrade_level(game,&mut globals,62,unit.player());
						if current_level > 0 {
							(*unit.0).remaining_build_time = (*unit.0).remaining_build_time.saturating_sub(1);
						}
					}
					globals.ngs.set_generic_switch(unit,0);
				}
			}
            if !should_have_building_ai(unit) {
                if let Some(building_ai) = unit.building_ai() {
                    let pos = (*unit.0).current_build_slot as usize;
                    let value = (*building_ai).train_queue_values[pos];
                    let ty = (*building_ai).train_queue_types[pos];

                    let town = (*building_ai).town;
                    let first_free = &mut (*(*town).free_buildings).first_free;
                    list::ListEntry::move_to(building_ai, &mut (*town).buildings, first_free);
                    (*unit.0).ai = null_mut();

                    match ty {
                        // Military
                        1 => ai::add_military_ai(unit, value as *mut bw::AiRegion, false),
                        // Guard
                        2 => {
                            let guard = value as *mut bw::GuardAi;
                            // Should ideally be caught in frame_hook fix for guard ais, but
                            // this is to not regress over bw.
                            // Cocoons/lurker eggs shouldn't have building ai and not reach here.
                            if (*guard).parent.is_null() {
                                (*guard).parent = unit.0;
                                (*guard).home = (*guard).other_home;
                                (*unit.0).ai = guard as *mut c_void;
                            } else {
							/*
                                let region = ai::ai_region(unit.player(), unit.position())*/
                                let ai_regions = bw::ai_regions(unit.player() as u32);
                                let region = ai::ai_region(ai_regions, unit.position())
                                    .expect("Unit out of bounds??");
                                ai::add_military_ai(unit, region, false);
                            }
                        }
                        // New guard
                        _ => ai::add_guard_ai(unit),
                    }
                }
            }
        }
        _ => (),
    }
    if let Some(ai) = temp_ai {
        (*unit.0).ai = ai;
        // Add ai for dual birthed units
        if unit.id().flags() & 0x400 != 0 {
            // Bw inserts shown units at second pos for some reason..
            if let Some(other) = unit::active_units().nth(1) {
                // This gets actually checked several times since the birth order lasts a few
                // frames, but it should be fine.
                if other.id() == unit.id() && (*other.0).ai.is_null() {
                    if other.player() == unit.player() {
					/*
                        let region = ai::ai_region(other.player(), other.position())*/
                        let ai_regions = bw::ai_regions(other.player() as u32);
                        let region = ai::ai_region(ai_regions, other.position())						
                            .expect("Unit out of bounds??");
                        ai::add_military_ai(other, region, false);
                    }
                }
            }
        }
    }
}

unsafe extern fn step_order_hidden_hook(u: *mut c_void, orig: unsafe extern fn(*mut c_void)) {
    let unit = unit::Unit(u as *mut bw::Unit);
    match unit.order() {
        order::id::DIE => {
            let mut globals = Globals::get("step order hidden hook (die)");
            globals.unit_removed(unit);
        },
        _ => (),
    }
    orig(u);
	match unit.order() {
        order::id::DIE => {
            let mut globals = Globals::get("step order hidden hook (die2)");
            globals.ngs.unit_removed(unit);
        }
        _ => (),
    }
}

fn lower_bound_by_key<T, C: Ord, F: Fn(&T) -> C>(slice: &[T], val: C, key: F) -> usize {
    use std::cmp::Ordering;
    slice
        .binary_search_by(|a| match key(a) < val {
            true => Ordering::Less,
            false => Ordering::Greater,
        })
        .unwrap_err()
}
