use std::cell::RefCell;
use std::hash::{Hash, Hasher};
use std::ptr::null_mut;
use std::sync::atomic::{AtomicUsize, Ordering}; 
use byteorder::{ReadBytesExt, LE};
use fxhash::FxHashMap;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use bw;
use bw_dat::{tech, upgrade};
use game::Game;
use order::OrderId;

use config::{self};
pub use bw_dat::unit as id;
pub use bw_dat::UnitId;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Unit(pub *mut bw::Unit);

unsafe impl Send for Unit {}
unsafe impl Sync for Unit {}

//hashable unit don't had serialize before
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct HashableUnit(pub Unit);

impl Hash for HashableUnit {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        hasher.write_usize((self.0).0 as usize);
    }
}

impl Serialize for Unit {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        use serde::ser::Error;
        match save_mapping().borrow().get(&HashableUnit(*self)) {
            Some(id) => id.serialize(serializer),
            None => Err(S::Error::custom(format!(
                "Couldn't get id for unit {:?}",
                self
            ))),
        }
    }
}

impl<'de> Deserialize<'de> for Unit {
    fn deserialize<S: Deserializer<'de>>(deserializer: S) -> Result<Self, S::Error> {
        use serde::de::Error;
        let id = u32::deserialize(deserializer)?;
        match load_mapping().borrow().get(&id) {
            Some(&unit) => Ok(unit),
            None => Err(S::Error::custom(format!(
                "Couldn't get unit for id {:?}",
                id
            ))),
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone, Debug, Hash)]
pub struct SerializableSprite(pub *mut bw::Sprite);
static SAVE_SPRITE_ARRAY: AtomicUsize = AtomicUsize::new(0);

impl serde::Serialize for SerializableSprite {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let offset = (self.0 as usize).wrapping_sub(SAVE_SPRITE_ARRAY.load(Ordering::Relaxed));
        (offset as u32).serialize(serializer)
    }
}

impl<'de> serde::Deserialize<'de> for SerializableSprite {
    fn deserialize<S: Deserializer<'de>>(deserializer: S) -> Result<Self, S::Error> {
        use serde::de::Error;
        let offset = u32::deserialize(deserializer)?;
        if offset != 0 {
            let ptr = (SAVE_SPRITE_ARRAY.load(Ordering::Relaxed) as usize)
                .wrapping_add(offset as usize) as *mut bw::Sprite;
            Ok(SerializableSprite(ptr))
        } else {
            Err(S::Error::custom(format!("Can't load sprite")))
        }
    }
}  

pub struct SaveIdMapping {
    next: Option<Unit>,
    list: SaveIdState,
    id: u32,
    in_subunit: bool,
}

enum SaveIdState {
    ActiveUnits,
    HiddenUnits,
}

fn save_id_mapping() -> SaveIdMapping {
    SaveIdMapping {
        next: Unit::from_ptr(bw::first_active_unit()),
        list: SaveIdState::ActiveUnits,
        id: 0,
        in_subunit: false,
    }
}

impl Iterator for SaveIdMapping {
    type Item = (Unit, u32);
    fn next(&mut self) -> Option<Self::Item> {
        unsafe {
            while self.next.is_none() {
                match self.list {
                    SaveIdState::ActiveUnits => {
                        self.next = Unit::from_ptr(bw::first_hidden_unit());
                        self.list = SaveIdState::HiddenUnits;
                    }
                    SaveIdState::HiddenUnits => return None,
                }
            }
            self.id += 1;
            let result = (self.next.unwrap(), self.id);
            let unit = self.next.unwrap().0;
            if (*unit).subunit != null_mut() && !self.in_subunit {
                self.next = Unit::from_ptr((*unit).subunit);
                self.in_subunit = true;
            } else {
                if self.in_subunit {
                    self.in_subunit = false;
                    let parent = (*unit).subunit;
                    self.next = Unit::from_ptr((*parent).next);
                } else {
                    self.next = Unit::from_ptr((*unit).next);
                }
            }
            Some(result)
        }
    }
}

ome2_thread_local! {
    SAVE_ID_MAP: RefCell<FxHashMap<HashableUnit, u32>> =
        save_mapping(RefCell::new(FxHashMap::default()));
    LOAD_ID_MAP: RefCell<FxHashMap<u32, Unit>> =
        load_mapping(RefCell::new(FxHashMap::default()));
}

pub fn init_save_mapping() {
    *save_mapping().borrow_mut() = save_id_mapping()
        .map(|(x, y)| (HashableUnit(x), y))
        .collect();
}

pub fn clear_save_mapping() {
    save_mapping().borrow_mut().clear();
}

pub fn init_load_mapping() {
    *load_mapping().borrow_mut() = save_id_mapping().map(|(x, y)| (y, x)).collect();
}

pub fn clear_load_mapping() {
    load_mapping().borrow_mut().clear();
}
impl Unit {
    pub fn from_ptr(ptr: *mut bw::Unit) -> Option<Unit> {
        if ptr == null_mut() {
            None
        } else {
            Some(Unit(ptr))
        }
    }

	pub fn ground_range(&self)->u32 {
		unsafe {
			let mut max_range = 0;
			if let Some(weapon) = self.id().ground_weapon() {
				if weapon.can_target_ground(){
					max_range = weapon.max_range();
				}
			}
			if let Some(weapon) = self.id().air_weapon() {
				if weapon.can_target_ground() && weapon.max_range()>max_range{
					max_range = weapon.max_range();
				}
			}
			max_range
		}
	}
//	target_range
	pub fn target_range(&self, target: Unit)->u32{
		unsafe {
			if target.is_air(){
				return self.air_range();
			}
			return self.ground_range();
		}
	}
	pub fn air_range(&self)->u32 {
		unsafe {
			let mut max_range = 0;
			if let Some(weapon) = self.id().ground_weapon() {
				if weapon.can_target_air(){
					max_range = weapon.max_range();
				}
			}
			if let Some(weapon) = self.id().air_weapon() {
				if weapon.can_target_air() && weapon.max_range()>max_range{
					max_range = weapon.max_range();
				}
			}
			max_range
		}
	}
	
    pub fn sprite(&self) -> Option<*mut bw::Sprite> {
        unsafe {
            match (*self.0).sprite == null_mut() {
                true => None,
                false => Some((*self.0).sprite),
            }
        }
    }

    pub fn player(&self) -> u8 {
        unsafe { (*self.0).player }
    }

    pub fn id(&self) -> UnitId {
        UnitId(unsafe { (*self.0).unit_id })
    }

	pub fn current_train_id(&self) -> UnitId {
		UnitId(unsafe { (*self.0).build_queue[(*self.0).current_build_slot as usize] })
	}										   
    pub fn position(&self) -> bw::Point {
        unsafe { (*self.0).position }
    }
    pub fn order_target_pos(&self) -> bw::Point {
        unsafe { (*self.0).order_target_pos }
    }												 

    pub fn order(&self) -> OrderId {
        OrderId(unsafe { (*self.0).order })
    }

    pub fn secondary_order(&self) -> OrderId {
        OrderId(unsafe { (*self.0).secondary_order })
    }

    pub fn hitpoints(&self) -> i32 {
        unsafe { (*self.0).hitpoints }
    }

    pub fn hp_percent(&self) -> i32 {
        self.hitpoints().saturating_mul(100) / (self.id().hitpoints())
    }
    pub fn shield_percent(&self) -> i32 {
        self.shields().saturating_mul(100) / (self.id().shields())
    }
    pub fn spider_mines(&self, game: Game) -> u8 {
        if game.tech_researched(self.player(), tech::SPIDER_MINES) || self.id().is_hero() {
            unsafe { (*self.0).unit_specific[0] }
        } else {
            0
        }
    }
	pub fn resources_carried(&self) -> u8 {
		unsafe { (*self.0).unit_specific[0xf] }
	}
	pub fn powerup_flags(&self) -> u8 {
		unsafe { (*self.0).carried_powerup_flags }
	}
	

    pub fn hangar_count(&self) -> u8 {
        // Count fighters outside hangar if carrier
        unsafe {
            match self.id() {
                id::CARRIER | id::GANTRITHOR => {
                    (*self.0).unit_specific[8] + (*self.0).unit_specific[9]
                }
                _ => (*self.0).unit_specific[8],
            }
        }
    }
	pub fn scarabs_outside(&self) -> u8 {
		unsafe {
			(*self.0).unit_specific[9]
		}
	}

    pub fn hangar_cap(&self, game: Game) -> u8 {
        match self.id() {
            id::CARRIER | id::GANTRITHOR => {
                let upgrade = upgrade::CARRIER_CAPACITY;
                if self.id().is_hero() || game.upgrade_level(self.player(), upgrade) > 0 {
                    8
                } else {
                    4
                }
            }
            id::REAVER | id::WARBRINGER => {
                let upgrade = upgrade::REAVER_CAPACITY;
                if self.id().is_hero() || game.upgrade_level(self.player(), upgrade) > 0 {
                    10
                } else {
                    5
                }
            }
            _ => 0,
        }
    }
	pub fn has_way_of_attacking(&self)->bool {
		if self.id().ground_weapon().is_some() || self.id().air_weapon().is_some(){
			return true;
		}
		match self.id(){
			id::CARRIER | id::GANTRITHOR | id::REAVER | id::WARBRINGER => {
				if self.hangar_count()!=0{
					return true;
				}
			},
			_=>{},
		}
		if let Some(subunit) = self.subunit_linked() {
			if subunit.id().ground_weapon().is_some() || subunit.id().air_weapon().is_some(){
				return true;
			}
		}
		return false;
	
	}
	pub fn can_train_hangar(&self, game: Game) -> bool {
		if self.order()==bw_dat::order::DIE || self.sprite().is_none(){
			return false;
		}
		return self.hangar_count()<self.hangar_cap(game);
	}

    pub fn is_transport(&self, game: Game) -> bool {
        let upgrade = upgrade::VENTRAL_SACS;
		
		//debug!("UDEBUG - istran");
        if self.id() == id::OVERLORD && game.upgrade_level(self.player(), upgrade) == 0 {
            false
        } else {
            self.id().cargo_space_provided() > 0
        }
    }

	pub fn burrowed(&self) -> bool {
		unsafe { (*self.0).flags & 0x10 != 0 }
	}
    pub fn has_nydus_exit(&self) -> bool {
        unsafe {
            let nydus = Unit::from_ptr(
                (&(*self.0).unit_specific2[..]).read_u32::<LE>().unwrap() as *mut bw::Unit
            );
            match nydus {
                Some(n) => n.is_completed(),
                None => false,
            }
        }
    }
    pub fn nydus_exit(&self) -> Option<Unit> {
        unsafe {
            let nydus = Unit::from_ptr(
                (&(*self.0).unit_specific2[..]).read_u32::<LE>().unwrap() as *mut bw::Unit
            );
            return nydus;
        }
    }	
    pub fn has_nuke(&self) -> bool {
        unsafe {
            let nuke = Unit::from_ptr(
                (&(*self.0).unit_specific2[..]).read_u32::<LE>().unwrap() as *mut bw::Unit
            );
            match nuke {
                Some(n) => n.is_completed(),
                None => false,
            }
        }
    }
	 
	pub fn resources(&self) -> u16 {
		unsafe {
			(&(*self.0).unit_specific2[..]).read_u16::<LE>().unwrap() as u16
		}
	}
	pub fn has_cooldown(&self) -> bool {
		unsafe {
			if (*self.0).ground_cooldown>0 || (*self.0).air_cooldown>0 {
				return true;
			}
			false
		}
	}

    pub fn shields(&self) -> i32 {
        if self.id().has_shields() {
            unsafe { (*self.0).shields }
        } else {
            0
        }
    }

    pub fn health(&self) -> i32 {
        self.hitpoints().saturating_add(self.shields())
    }

    pub fn energy(&self) -> u16 {
        unsafe { (*self.0).energy }
    }

    pub fn is_air(&self) -> bool {
        unsafe { (*self.0).flags & 0x4 != 0 }
    }

    /// Is the unit cloaked or burrowed (So it requires detection)
    pub fn is_invisible(&self) -> bool {
        unsafe { (*self.0).flags & 0x300 != 0 }
    }
	
	pub fn is_undetected(&self, owner: u8) -> bool {
		unsafe {
			self.is_invisible() && ((*self.0).detection_status & (1<<owner))==0
		}
	}
	
    pub fn is_invincible(&self) -> bool {
        unsafe { (*self.0).flags & 0x04000000 != 0 }
    }

    pub fn orders(&self) -> Orders {
        unsafe {
            Orders {
                next: (*self.0).order_queue_begin,
                this: self,
                first: true,
            }
        }
    }

    pub fn target(&self) -> Option<Unit> {
        unsafe { Unit::from_ptr((*self.0).target) }
    }

	pub fn unit_ai(&self) -> Option<*mut bw::UnitAi> {
		unsafe {
			let ai = (*self.0).ai as *mut bw::UnitAi;
			if ai != null_mut() {
				Some(ai)
			} else {
				None
			}
		}
	}
	
    pub fn worker_ai(&self) -> Option<*mut bw::WorkerAi> {
        unsafe {
            let ai = (*self.0).ai as *mut bw::WorkerAi;
            if ai != null_mut() && (*ai).ai_type == 2 {
                Some(ai)
            } else {
                None
            }
        }
    }
	pub fn military_ai(&self) -> Option<*mut bw::MilitaryAi> {
        unsafe {
            let ai = (*self.0).ai as *mut bw::MilitaryAi;
            if ai != null_mut() && (*ai).ai_type == 4 {
                Some(ai)
            } else {
                None
            }
        }	
	}
	pub fn ai_type(&self) -> Option<u8> {
        unsafe {
			let ai = (*self.0).ai as *mut bw::UnitAi;
			if ai != null_mut(){
				Some((*ai).ai_type)
			}
			else {
				None
			}
			
        }
    }
	
    pub fn building_ai(&self) -> Option<*mut bw::BuildingAi> {
        unsafe {
            let ai = (*self.0).ai as *mut bw::BuildingAi;
            if ai != null_mut() && (*ai).ai_type == 3 {
                Some(ai)
            } else {
                None
            }
        }
    }

    pub fn guard_ai(&self) -> Option<*mut bw::GuardAi> {
        unsafe {
            let ai = (*self.0).ai as *mut bw::GuardAi;
            if ai != null_mut() && (*ai).ai_type == 1 {
                Some(ai)
            } else {
                None
            }
        }
    }

    pub fn matches_id(&self, other: UnitId) -> bool {
        let id = self.id();
        match other {
            id::ANY_UNIT => true,
            id::GROUP_MEN => id.group_flags() & 0x8 != 0,
            id::GROUP_BUILDINGS => id.group_flags() & 0x10 != 0,
            id::GROUP_FACTORIES => id.group_flags() & 0x20 != 0,
            other => id == other,
        }
    }

    pub fn collision_rect(&self) -> bw::Rect {
        let collision_rect = self.id().dimensions();
        let position = self.position();									
        bw::Rect {
            left: (position.x - collision_rect.left).max(0),
            right: position.x + collision_rect.right + 1,
            top: (position.y - collision_rect.top).max(0),
            bottom: position.y + collision_rect.bottom + 1,
        }											
    }
	pub fn collision_rect_ext(&self) -> bw::Rect {
		if !self.is_landed_building(){
			return self.collision_rect();
		}
        let position = self.position();									
        bw::Rect {
            left: (position.x - self.id().placement().width as i16/2).max(0),
            right: position.x + self.id().placement().width as i16/2 + 1,
            top: (position.y - self.id().placement().height as i16/2).max(0),
            bottom: position.y + self.id().placement().height as i16/2 + 1,
        }											
    }
	pub fn issue_secondary_order(&self, order: OrderId) {
        unsafe {
		/*
			if self.id().0 == 111 && (*self.0).secondary_order==0x25 {
				panic!("Aise panicked - something has issued order to police academy! {:p} {}",(*self.0).currently_building,
						order.0);
			}*/
            if (*self.0).secondary_order != order.0 {
                (*self.0).secondary_order = order.0;
                (*self.0).secondary_order_state = 0;
                // Uhh.. Is this sensible to allow to be done from AI scripts?
                (*self.0).currently_building = null_mut();
                (*self.0).unke8 = 0;
                (*self.0).unkea = 0;
            }
        }
    }

    pub fn is_completed(&self) -> bool {
        unsafe { (*self.0).flags & 0x1 != 0 }
    }

    pub fn is_landed_building(&self) -> bool {
        unsafe { (*self.0).flags & 0x2 != 0 }
    }

    pub fn is_burrowed(&self) -> bool {
        unsafe { (*self.0).flags & 0x10 != 0 }
    }/*
	pub fn in_transport(&self) -> bool {
        unsafe { (*self.0).flags & 0x40 != 0 }
    }*/
    pub fn is_hallucination(&self) -> bool {
        unsafe { (*self.0).flags & 0x4000_0000 != 0 }
    }

    pub fn subunit_linked(&self) -> Option<Unit> {
        unsafe { Unit::from_ptr((*self.0).subunit) }
    }

    pub fn addon(&self) -> Option<Unit> {
        unsafe {
            if self.id().is_building() {
                let ptr = (&(*self.0).unit_specific[..]).read_u32::<LE>().unwrap() as *mut bw::Unit;
                Unit::from_ptr(ptr)
            } else {
                None
            }
        }
    }

    pub fn tech_in_progress(self) -> bw_dat::TechId {
        unsafe {
            if self.id().is_building() {
                bw_dat::TechId((*self.0).unit_specific[0x8].into())
            } else {
                tech::NONE
            }
        }
    }

    pub fn upgrade_in_progress(self) -> bw_dat::UpgradeId {
        unsafe {
            if self.id().is_building() {
                bw_dat::UpgradeId((*self.0).unit_specific[0x9].into())
            } else {
                upgrade::NONE
            }
        }
    }
	 pub fn upgrade_time(self) -> u16 {
        unsafe {
            if self.id().is_building() && self.is_landed_building() {
			  (&(*self.0).unit_specific[0x6..]).read_u16::<LE>().unwrap()
            } else {
                0
            }
        }
    }
	pub fn current_harvest_target(self) -> *mut bw::Unit {
        unsafe {
            if self.worker_ai().is_some() {
			  (&(*self.0).unit_specific[0x8..]).read_u32::<LE>().unwrap() as *mut bw::Unit
            } else {
                null_mut()
            }
        }
    }
	pub fn used_resource_area(self) -> u8 {
        unsafe {
            if self.worker_ai().is_some() {
			  (*self.0).unit_specific2[0x8].into()
            } else {
                0
            }
        }
    }
	
	pub fn current_harvest_target_opt(self) -> Option<*mut bw::Unit> {
        unsafe {
            if self.worker_ai().is_some() {
			  
			  let unit = (&(*self.0).unit_specific[0x8..]).read_u32::<LE>().unwrap() as *mut bw::Unit;
			  if unit != null_mut(){
				Some(unit)
			  }
			  else {
				None
			  }
            } else {
                None
            }
        }
    }
	
	pub fn ai_used_resource(self) -> u8 {
        unsafe {
            if self.worker_ai().is_some() {
			  (*self.0).unit_specific2[0x9].into()
            } else {
                0
            }
        }
    }
	pub fn upgrade_level(self) -> u16 {
        unsafe {
            if self.id().is_building() {
                (*self.0).unit_specific[0x9].into()
            } else {
                0x3d
            }
        }
    }
	pub fn upgrade_in_progress_raw(self) -> u8 {
        unsafe {
			(*self.0).unit_specific[0xd].into()

        }
    }	
    pub fn cargo_count(&self) -> u8 {
        unsafe { (*self.0).loaded_units.iter().filter(|&&x| x != 0).count() as u8 }
    }
	/*
	//bugged for unclear reason
	pub fn cargo_weight(&self) -> u32 {
		unsafe {
			let mut result = 0;
			for i in 0..8 {
				let index = (*self.0).loaded_units[i];
				if index != 0 {
					//replace 59cca8 later for extender purposes
					let ptr = (0x0059CCA8+((index-1) & 0x7ff)) as *mut bw::Unit;
					if ptr != null_mut(){
						let uid = UnitId((*ptr).unit_id);
						result += uid.cargo_space_required();					
					}
					return 0;
				}
			}
			result
		}
	}*/
    pub fn issue_order(&self, order: OrderId, pos: bw::Point, unit: Option<Unit>) {
        if self.can_issue_order(order) {
            let unit_ptr = unit.map(|x| x.0).unwrap_or(null_mut());
            unsafe { bw::issue_order(self.0, order, pos, unit_ptr, id::NONE) }
        }
    }

    pub fn issue_order_ground(&self, order: OrderId, target: bw::Point) {
        self.issue_order(order, target, None)
    }

    pub fn issue_order_unit(&self, order: OrderId, target: Unit) {
        self.issue_order(order, target.position(), Some(target));
    }

    pub fn is_disabled(&self) -> bool {
        unsafe {
            (*self.0).lockdown_timer != 0 ||
                (*self.0).stasis_timer != 0 ||
                (*self.0).maelstrom_timer != 0 ||
                (*self.0).flags & 0x400 != 0
        }
    }
	pub fn reacts(&self) -> bool {
		unsafe {
			(*self.0).flags & 0x20000 != 0
		}
	}

    pub fn can_issue_order(&self, order: OrderId) -> bool {
        // This is checked by targeted command/rclick/etc command handlers, but bw accepts
        // it otherwise, but doesn't clear related unit, so things would end up buggy.
		if self.order() == bw_dat::order::BUILD_NYDUS_EXIT && (self.order()==order || !self.has_nydus_exit()) {
			return false;
		}
		let config = config::config();
		let mut is_terran_worker = false;
		if self.id()==id::SCV {
			is_terran_worker = true;
		}
		if config.overlord {
			if self.id().0==77 || self.id().0==41 {
				is_terran_worker = true;
			}
		}
        if is_terran_worker && self.order() == bw_dat::order::CONSTRUCTING_BUILDING {
            return order == bw_dat::order::STOP;
        }
        // Technically should also check datreqs, oh well
        self.is_completed() && !self.is_disabled()
    }
	
    pub fn is_hidden(&self) -> bool {
        unsafe { (*(*self.0).sprite).flags & 0x20 != 0 }
    }
	
	pub fn build_queue_empty(self) -> bool {
	  unsafe {
	  let pos = (*self.0).current_build_slot as usize;
	  if (*self.0).build_queue[pos] == id::NONE.0 {
			return true;
	  }
	  false
	  
	  }
	}

    pub fn empty_build_slot(self) -> Option<u8> {
        unsafe {
            let mut pos = (*self.0).current_build_slot as usize;
            for _ in 0..5 {
                if pos == 5 {
                    pos = 0;
                }
                if (*self.0).build_queue[pos] == id::NONE.0 {
                    return Some(pos as u8);
                }
                pos += 1;
            }
            None
        }
    }
	
    pub fn is_building_addon(self) -> bool {

        let currently_building = unsafe { Unit::from_ptr((*self.0).currently_building) };
        if let Some(currently_building) = currently_building {
            self.secondary_order() == bw_dat::order::BUILD_ADDON &&
                self.is_landed_building() &&
                !currently_building.is_completed()
        } else {
            false
        }
    }
    pub fn is_constructing_building(self) -> bool {
        let current_build_unit =
            unsafe { UnitId((*self.0).build_queue[(*self.0).current_build_slot as usize]) };
        current_build_unit != id::NONE && current_build_unit.is_building()
    }
	
	
	pub fn first_queued_unit(self) -> Option<UnitId> {
        let current_build_unit =
            unsafe { UnitId((*self.0).build_queue[(*self.0).current_build_slot as usize]) };
        if current_build_unit == id::NONE {
            None
        } else {
            Some(current_build_unit)
        }
    }
}

pub struct Orders<'a> {
    next: *mut bw::Order,
    this: &'a Unit,
    first: bool,
}

pub struct Order {
    pub id: OrderId,
    pub position: bw::Point,
    pub target: Option<Unit>,
}

impl<'a> Iterator for Orders<'a> {
    type Item = Order;
    fn next(&mut self) -> Option<Order> {
        unsafe {
            if self.first {
                self.first = false;
                Some(Order {
                    id: self.this.order(),
                    position: (*self.this.0).order_target_pos,
                    target: Unit::from_ptr((*self.this.0).target),
                })
            } else if self.next != null_mut() {
                let order = self.next;
                self.next = (*order).next;
                Some(Order {
                    id: OrderId((*order).order_id),
                    position: (*order).position,
                    target: Unit::from_ptr((*order).target),
                })
            } else {
                None
            }
        }
    }
}

pub fn active_units() -> UnitListIter {
    UnitListIter(bw::first_active_unit())
}

#[allow(dead_code)]
pub unsafe fn active_player_units(player: u8) -> UnitPlayerListIter {
    UnitPlayerListIter(bw::first_player_unit[player as usize])
}
pub fn hidden_units() -> UnitListIter {
    UnitListIter(bw::first_hidden_unit())
}

pub struct UnitListIter(*mut bw::Unit);

impl Iterator for UnitListIter {
    type Item = Unit;
    fn next(&mut self) -> Option<Unit> {
        unsafe {
            if self.0 == null_mut() {
                None
            } else {
                let result = Some(Unit(self.0));
                self.0 = (*self.0).next;
                result
            }
        }
    }
}
pub struct UnitPlayerListIter(*mut bw::Unit);
impl Iterator for UnitPlayerListIter {
    type Item = Unit;
    fn next(&mut self) -> Option<Unit> {
        unsafe {
            if self.0 == null_mut() {
                None
            } else {
                let result = Some(Unit(self.0));
                self.0 = (*self.0).next_player_unit;
                result
            }
        }
    }
}

