#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]

use std::ptr::{null, null_mut};
use std::sync::atomic::Ordering;
use std::os::raw::c_char;
use bw_dat::{OrderId, TechId, UnitId, UpgradeId};

use samase;

pub mod structs;

pub use self::structs::*;

pub fn is_scr() -> bool {
    crate::IS_1161.load(Ordering::Acquire) == false
}

pub fn player_ai(player: u32) -> *mut PlayerAiData {
    samase::player_ai(player)
}

pub fn ai_regions(player: u32) -> *mut AiRegion {
    samase::ai_regions(player)
}

pub fn pathing() -> *mut Pathing {
    samase::pathing()
}

pub fn players() -> *mut Player {
    samase::players()
}

pub fn get_region(pos: Point) -> Option<u16> {
    let Point {
        x,
        y,
    } = pos;
    unsafe {
        let game = game();
        let bounds = ((*game).map_width_tiles * 32, (*game).map_height_tiles * 32);
        if bounds.0 as i16 <= x || bounds.1 as i16 <= y {
            None
        } else {
            Some(samase::get_region(x as u32, y as u32) as u16)
        }
    }
}

pub fn game() -> *mut Game {
    samase::game()
}

pub fn rng_seed() -> u32 {
    samase::rng_seed().unwrap_or_else(|| {
        // Oh well, rng.rs only uses this for the initial seed
        unsafe { (*game()).frame_count.wrapping_add(1234) }
    })
}

pub fn elapsed_seconds() -> u32 {
    unsafe { (*game()).elapsed_seconds }
}

pub fn location(location: u8) -> Location {
    unsafe { (*game()).locations[location as usize] }
}

pub fn point_from_rect(rect: Rect) -> Point {
    Point {
        x: rect.left + (rect.right - rect.left) / 2,
        y: rect.top + (rect.bottom - rect.top) / 2,
    }
}

pub fn point32_from_rect(rect: Rect) -> Point32 {
    Point32 {
        x: (rect.left + (rect.right - rect.left) / 2) as i32,
        y: (rect.top + (rect.bottom - rect.top) / 2) as i32,
    }
}
pub unsafe fn issue_order(
    unit: *mut Unit,
    order: OrderId,
    pos: Point,
    target: *mut Unit,
    fow_unit: UnitId,
) {
    samase::issue_order(unit, order, pos.x as u32, pos.y as u32, target, fow_unit)
}

pub fn first_active_unit() -> *mut Unit {
    samase::first_active_unit()
}

pub fn first_hidden_unit() -> *mut Unit {
    samase::first_hidden_unit()
}

pub fn first_ai_script() -> *mut AiScript {
    samase::first_ai_script()
}

pub fn set_first_ai_script(script: *mut AiScript) {
    samase::set_first_ai_script(script)
}

pub fn first_free_ai_script() -> *mut AiScript {
    samase::first_free_ai_script()
}

pub fn set_first_free_ai_script(script: *mut AiScript) {
    samase::set_first_free_ai_script(script)
}

pub fn guard_ais(player: u8) -> *mut GuardAi {
    unsafe {
        assert!(player < 8);
        (*samase::guard_ais().offset(player as isize)).first
    }
}

pub fn guard_array() -> *mut GuardAiArray {
    unsafe { (*samase::guard_ais()).array }
}

pub fn change_ai_region_state(region: *mut AiRegion, state: u32) {
    samase::change_ai_region_state(region, state);
}

lazy_static! {
    static ref SAMASE_AISCRIPT_BIN: usize =
        samase::read_file("scripts\\aiscript.bin").unwrap().0 as usize;
    static ref SAMASE_BWSCRIPT_BIN: usize =
        samase::read_file("scripts\\bwscript.bin").unwrap().0 as usize;
}

pub fn aiscript_bin() -> *mut u8 {
    *SAMASE_AISCRIPT_BIN as *mut u8
}

pub fn bwscript_bin() -> *mut u8 {
    *SAMASE_BWSCRIPT_BIN as *mut u8
}

pub fn unit_dat_requirements(unit: UnitId) -> Option<*const u16> {
    let result = samase::requirements(0, unit.0 as u32);
    if result == null() {
        None
    } else {
        Some(result)
    }
}

pub fn upgrade_dat_requirements(upgrade: UpgradeId) -> Option<*const u16> {
    let result = samase::requirements(1, upgrade.0 as u32);
    if result == null() {
        None
    } else {
        Some(result)
    }
}

pub fn tech_research_dat_requirements(tech: TechId) -> Option<*const u16> {
    let result = samase::requirements(2, tech.0 as u32);
    if result == null() {
        None
    } else {
        Some(result)
    }
}


// BW algorithm
pub fn distance(a: Point, b: Point) -> u32 {
    let x = (a.x as i32).wrapping_sub(b.x as i32).abs() as u32;
    let y = (a.y as i32).wrapping_sub(b.y as i32).abs() as u32;
    let (greater, lesser) = (x.max(y), x.min(y));
    if greater / 4 > lesser {
        greater
    } else {
        greater * 59 / 64 + lesser * 99 / 256
    }
}
pub fn fdistance(a: Point, b: Point) -> u32 {
	let x = a.x as f64 - b.x as f64;
	let y = a.y as f64 - b.y as f64;
	let sum = x.powf(2.0)+y.powf(2.0);
	sum.sqrt() as u32
}


pub fn distance32(a: Point32, b: Point32) -> u32 {
    let x = (a.x).wrapping_sub(b.x).abs() as u32;
    let y = (a.y).wrapping_sub(b.y).abs() as u32;
    let (greater, lesser) = (x.max(y), x.min(y));
    if greater / 4 > lesser {
        greater
    } else {
        greater * 59 / 64 + lesser * 99 / 256
    }
}

pub fn rect_distance(a: &Rect, b: &Rect) -> u32 {
    let horizontal_overlap = a.left < b.right && a.right > b.left;
    let vertical_overlap = a.top < b.bottom && a.bottom > b.top;
    let x_diff = match horizontal_overlap {
        true => 0,
        false => match a.left < b.left {
            true => b.left - a.right,
            false => a.left - b.right,
        },
    };
    let y_diff = match vertical_overlap {
        true => 0,
        false => match a.top < b.top {
            true => b.top - a.bottom,
            false => a.top - b.bottom,
        },
    };

    distance(
        Point {
            x: 0,
            y: 0,
        },
        Point {
            x: x_diff,
            y: y_diff,
        },
    )
}

//pub fn town_array_start() -> *mut AiTown {
pub fn town_array() -> *mut AiTownArray {
    let ptr = samase::active_towns();
    if ptr.is_null() {
        null_mut()
    } else {
        unsafe { (*ptr).array }
    }
}

pub fn first_active_ai_town(player: u8) -> *mut AiTown {
    unsafe { (*samase::active_towns().add(player as usize)).first }
}

whack_hooks!(stdcall, 0x00400000,
    0x00488AF0 => increment_death_scores(@edi *mut Unit, @edx u8);
    0x004465C0 => choose_placement_position(u32, u32, *mut Point, u32, @ecx *mut Unit) -> u32;
    0x00473FB0 => update_building_placement_state_hook(*mut Unit, u8, u32, u32, u16, u8, u8, u8, u8) -> u32;
    0x004A13C0 => ai_spellcast(bool, @eax *mut Unit) -> u32;
    0x0047B090 => get_unit_name(@ecx u32) -> *const u8;
    0x0043FCF0 => ai_focus_unit_check(@ecx *mut Unit, @edx u32) -> u32;
	0x00467030 => start_building(@eax *mut Unit) -> u32;
	0x004E9AA0 => drone_land(@eax *mut Unit)->u32;
	0x0045D2E0 => start_zerg_building(@eax *mut Unit)->u32;	
	//
	0x00447980 => add_spending_request(u32,*mut libc::c_void,@eax u16, @ecx u32, @edx u8);
	0x00433730 => ai_add_supply_requests(*mut AiTown);	
	//test
	//0x00433DD0 => add_buildtown_ai(@ebx *mut Unit, @edi *mut AiTown);
	//
	//project hydra
	0x00413850 => is_in_creeparea(@eax u32,@ecx u32)->u32;
	0x0047DF90 => update_creep_disappearance(*mut Unit)->u32;//used in creep range hooks
	0x00414180 => does_provide_creep(u32,bool,@edx u32,@ebx u32)->u32;//used in creep range hooks
	0x00413DB0 => creep_spread_func(u32,u32,@eax bool,@ecx *mut Rect32, @edx u32)->u32;
	0x00413870 => does_spread_creep(bool,@eax u16)->u32;
	0x0046ED80 => isUnselectable(@eax u32)->u32;
	0x00435F10 => ai_spending_req_trainunit(u32, *mut Unit,u32)->u32;
	//0x004C36F0 => get_stat_txt_string(@ecx u32)->u32;
	0x004C36F0 => get_stat_txt_str(@ecx u32) -> *const u8;	
	0x0043D5D0 => progress_military_ai(*mut Unit,u32,u32,u32);
	//project hydra - creep range hooks 
	0x00473920 => get_psi_statement(u32,u32,@esi u32,@eax u32)->u32;
	//used for upgrade hook
	0x00414560 => remove_creep_at_unit_hook(
        @ecx u32,
        @eax u32,
        @edx u32,
        unsafe extern "stdcall" fn(u32, u32, *mut Rect32) -> u32
    );
	0x005153FC => GetRightClickOrder(@ecx u32)->u32;
	//00492020 = CanTargetSpell(), arg 1 Unit *unit, arg 2 x, arg 3 y, arg 4 *out_error, edx tech, edi Unit *target, esi fow_unit_id
	0x00492020 => can_target_spell(*mut Unit,u32,u32,*mut u32,@edx u32,@edi *mut Unit,@esi u32)->u32;
	
	0x0047D770 => stop_creep_disappearing(*mut Unit)->u32;
	0x0047DE40 => update_creep_disappearance_2(u32,@eax bool,@edx u32,@ecx u32);
	0x00414680 => unk_creep_func(@ecx u32,@eax u32,@edx u32)->u32;
	0x00433470 => ai_count_economy_units(@eax u32,u32)->u32;

	0x00479930 => do_weapon_damage(u32,u32,u32,*mut Unit,u32,@edi *mut Unit,@eax u32);

	0x0047B5F0 => modify_speed(@edx *mut Unit, @eax u32)->u32;
	0x00475DC0 => attack_cooldown(@esi *mut Unit, @eax u32)->u32;
	0x0048BD50 => bullet_state_init(@ecx *mut Bullet)->u32;
	0x00465480 => reaver_check_movement_distance(@ecx *mut Unit, u32)->u32;
	
	0x00435DB0 => ai_set_finished_unit_ai(@eax *mut Unit,@ecx *mut Unit);
	0x00425A30 => ss_draw_energy(@ebx *mut Control);
	0x0046D610 => check_dat_req(u32,u32,*mut u16,@eax u32,@esi *mut Unit)->u32;
	0x00487C70 => unknown_gas_func(@ecx u32, @edi u32,u32,u32,u32,u32)->u32;
//	0x00425600 => ss_drawarmor(u32,@eax *mut Control);
	0x00498C50 => draw_sprite(@eax *mut Sprite);
	0x00454E00 => irradiate(*mut Unit, u32, @edi *mut Unit);
	//00454E00 = Irradiate(), arg 1 Unit *attacker, arg 2 attacking_player, edi Unit *target
	0x0048BEC0 => initialize_bullet(*mut Bullet,u32,u32,u32,@ecx u32,@edx u32,@eax *mut Unit)->u32;
	//0x0048B770 => do_missile_dmg(*mut Bullet);
	0x00488BF0 => modify_unit_counters1(i32, @eax *mut Unit);
	0x00488D50 => modify_unit_counters2(i32, @edi *mut Unit, @ecx i32);//hook
	0x0046D5A0 => parse_upgrades();
	
	0x004E6E00 => can_load_unit(*mut Unit,@eax *mut Unit)->u32;
	0x00462EA0 => try_return_home(u32, @eax *mut Unit)->u32;
	//004778E0 = Order_ComputerGuard(), eax Unit *unit
	0x004778E0 => order_computerguard(@eax *mut Unit);
	
	0x004548B0 => order_tech(@eax *mut Unit);
	0x004546A0 => order_upgrade(@eax *mut Unit);
	0x004CE770 => set_upgrade(@edx u8, @eax u32, @ecx u16);//dl eax cx
//	0x0042B620 => gen_minitile_walkability_arr()->u32;
	0x0040C3B0 => copy_minitile_to_buf(@ecx u32, @edx u32);
	//0x004A42B0 => minimap64();
	//0x0049C4C0 => progress_creep_disappearance();
	0x004BCEA0 => set_megatiles_and_flags();
	0x004EE110 => single_player_init_game() -> u32;
	0x004D4130 => init_game_network() -> u32;
	0x00472060 => command_startgame(u32);
	
//	0x004865D0 => process_commands(u32,bool,@eax *const u8);


	0x00462960 => add_guard_ai(*mut Unit);
	0x00462670 => pre_create_guard_ai(u32,u32,@edi u32,@esi u32);
	0x00497A10 => move_sprite(@ebx u32, @edi u32, @ecx *mut Sprite);
	0x004D5E70 => iscript_playframe(@edx u32, @eax *mut Image);
	0x004598D0 => cmdbtn_handler(@ecx *mut Control, @edx *mut Event)->u32;
	0x00423D10 => btnsact_placeaddon(@ecx u16);
	0x004797B0 => damage_unit(*mut Unit,u32,u32,@ecx *mut Unit, @eax u32);
	0x00473A10 => update_placement_mapflags(u32,u32,u8, u32,u32,u32,@eax u16)->u32;
	0x0045D500 => order_zergbuildself(@eax *mut Unit);
	0x004E7420 => ai_prepare_movingto(*mut Unit,u32,u32)->u32;
	0x004E7420 => order_ai_pickup(@eax *mut Unit);
	0x004F39A0 => return_resources(@esi *mut Unit);
	0x0046DFC0 => check_upgrade_req_hook(*mut Unit,@ebx u16,@edi u8)->u32;
	0x00434FF0 => ai_spendreq_buildmorph(*mut Unit,*mut BuildingAi,@ebx u32)->u32;//3rd argument isn't from neiv's list
	
	//00434FF0 = AiSpendReq_Build(), arg 1 Unit *unit, arg 2 BuildingAi *ai
	
	0x00432480 => are_same_race_hook(@ecx *mut Unit, @eax u32)->u32;
	0x004361A0 => try_progress_spending_queue_hook(@ecx *mut Unit);
	0x0040B596 => drawimage_detected(@ecx u32, @edx u32, *mut GrpFrameHeader, *mut Rect, *mut u8);
	0x00495CB0 => progress_speed(@esi *mut Flingy);//Flingy originally
	0x00494FE0 => progress_flingy_turning(@ecx *mut Flingy)->u32;//Flingy originally
//	0x004A28B0 => order_computerscript(@eax *mut Unit);
	0x00433640 => ai_finish_unit(@edi *mut Unit);
	0x00475710 => kill_unit_hook(@eax *mut Unit);
	0x00439D60 => remove_from_ai_structs(*mut Unit,u32)->u32;
	0x0045E320 => get_portrait(@eax u32)->u32;
	
	
	0x004347E0 => delete_ai_town_if_needed(@eax *mut AiTown);
	0x00434330 => try_begin_delete_town(@esi *mut AiTown)->u32;
	
	/*
	004347E0 = DeleteAiTownIfNeeded(), eax AiTown *town
	00434330 = TryBeginDeleteTown(), esi AiTown *town (If can, transfer all units to another town and return 1)
	//00427540 = Ss_DrawSupplyUnitStatus(), eax Control *ctrl*/
	0x00427540 => ss_drawsupplyunitstatus(@eax *mut Control);
	0x00425DD0 => ss_drawkills(@ebx *mut Control);
	0x004759C0 => increment_kills(@ecx *mut Unit);
	0x0049FA40 => finish_unit_hook(@eax *mut Unit);
	//0x00492020 => can_target_spell(*mut Unit,u32,u32,*mut u32,@edx u32,@edi *mut Unit,u32)->u32;
	0x00487B00 => find_blocking_resource(u32,u32,u32)->*mut Sprite;
	0x00473490 => is_resource_container_for_blocking(@ecx *mut Unit)->u32;
	0x00473010 => update_creep_building_placementstate(@eax u32,u32,u32,u32,u32,u32,u32)->u32;

	0x00473720 => update_building_placement_units(*mut Unit,u32,u32,u32,u32,u32,u32,u32,u32,@eax u32)->u32;
/*00473720 = UpdateBuildingPlacementState_Units(), arg 1 Unit *builder, arg 2 x_tile, arg 3 player, arg 4 unit_id, arg 5 size_wh,
    arg 6 placement_state_entry, arg 7 dont_ignore_reacting, arg 8 also_invisible, arg 9 without_vision, eax y_tile
    arg 7 is 0 for addons
	*/	
	0x0043F320 => ai_reacttohit(u32,u32,@ecx *mut Unit,@edx *mut Unit);
	0x004765B0 => get_miss_chance(@ecx *mut Unit, @eax *mut Unit)->u32;
	0x0047B2E0 => has_rally_hook(@eax *mut Unit)->u32;
	//# Ai_WorkerAi(), arg 1 Unit *unit
	0x00435210 => ai_workerai(*mut Unit);
	
	//Ai_TrainUnit(), arg 1 unit_id, arg 2 ai_request_type, arg 3 void *value, eax Unit *parent
	0x004672F0 => ai_train_unit_hook(u16,u32,*mut libc::c_void,@eax *mut Unit)->u32;
	
	//SetBuildHpGain(), edi Unit *unit
	0x0047B180 => set_build_hp_gain_hook(@edi *mut Unit);

	//BTNSCOND
	//BTNSCOND_TerranBasic()
	0x00428A10 => btnscond_terran_basic_hook(*mut Unit,@edx u32,@ecx u32)->u32;
	//BTNSCOND_TerranAdvanced()
	0x00428990 => btnscond_terran_adv_hook(*mut Unit,@edx u32,@ecx u32)->u32;

	0x00473300 => is_tile_blocked_by_hook(*mut Unit, u32,u32,bool,bool,@eax *mut *mut Unit)->u32;
	0x00473410 => does_building_block_hook(@eax *mut Unit, @edx u32,@ecx u16)->u32;
	//004348C0 = Ai_TryProgressSpendingQueue_Worker(), arg 1 AiTown *town, arg 2 Unit *worker

	
	//signed int __usercall AI_MakeDetectorBehaviour@<eax>(CUNIT *a1@<eax>, int a2@<edi>)
	0x00435130 => ai_make_detector_behavior(@eax *mut Unit, @edi u32)->u32;
	0x004F6760 => maelstrom_proc(@ecx *mut Unit,@edx *mut Unit)->u32;
	0x004348C0 => ai_tryprogressspendingqueue_worker_hook(*mut AiTown, *mut Unit)->u32;
	0x0045D0D0 => order_building_morph_hook(*mut Unit);
	
	
	
	0x0046FA40 => get_box_selection(*mut Rect);
	0x0046F0F0 => multi_select_units_hook(*mut *mut Unit, *mut *mut Unit, *mut Unit)->u32;
	//signed int __stdcall increaseUpgradeLevel(char a1, unsigned __int16 upgradeID, unsigned __int8 upgradeLevel, unsigned __int8 playerID)
	0x0047b340 => increase_upgrade_level(u32,u32,u32,u32)->u32;
	0x00447040 => ai_remove_from_attack(@eax u32,@edx u32);
	0x00447230 => ai_add_to_attackforce(@edi u32,@eax u32,@ecx u32);
	0x00447ED0 => save_player_ai_data(*mut File)->u32;
	0x00437F20 => has_units_for_attack_force_hook(@edx u32)->u32;
	0x0043ABB0 => ai_attack_to_hook(u32,i32,i32,bool,bool)->u32;
	0x00438050 => get_missing_attack_force_units_hook(*mut AiRegion,*mut u32,bool)->u32;
	0x0043E670 => train_attack_force(@eax *mut AiRegion)->u32;
	0x0043EEC0 => grouping_for_attack(*mut AiRegion);
	0x004EEE00 => init_game()->u32;
	0x004550A0 => order_defensive_matrix_hook(*mut Unit);
	0x00442DA0 => pick_attack_target(@ecx *mut Unit, @edx *mut Unit)->u32;
	0x004D2E80 => load_game_data(@eax u32, @edi *const u8, *const libc::c_char)->u32;
	
	0x00473150 => update_nydus_placement(@eax u8,u16,u16,u32,u32,u32)->u32;
	//cancel_building_morph_hook
	0x0045D410 => cancel_building_morph_hook(@eax *mut Unit);
	0x004CE7A0 => get_upgrade_level_hook(@eax u32, @ecx u32)->u32;
	
	0x004A09D0 => create_unit_hook(u32,u32,@eax u32,@ecx u32)->*mut Unit;
	
	0x00497110 => building_vision_sync()->u32;
	//add_buildtown_ai
);

whack_funcs!(stdcall, init_funcs, 0x00400000,
    0x00473FB0 => update_building_placement_state(*mut Unit, u8, u32, u32, u16, u8, u8, u8, u8) -> u32;
    0x004936B0 => is_powered(u32, u32, u8, @eax u32) -> u32;
    0x004A34C0 => ping_minimap(u32, u32, u8);
    0x00433DD0 => add_town_unit_ai(@ebx *mut Unit, @edi *mut AiTown);
    0x004A1E50 => remove_unit_ai(@ecx *mut Unit, @edx u32);
    0x00414560 => remove_creep_at_unit(
        @ecx u32,
        @eax u32,
        @edx u32,
        unsafe extern "stdcall" fn(u32, u32, *mut Rect32) -> u32
    );
	0x004E97C0 => unburrow(@eax *mut Unit);	
	0x004EB9C0 => change_movement_target_movetarget2(@edx *mut Unit, @ecx u32, @eax u32);
	0x0046E1C0 => check_unit_dat_requirements(u32,@eax u32, @esi *mut Unit)->u32;
	0x004487B0 => Ai_DoesHaveResourcesForUnit(u32, @ecx u32)->u32;
	0x00467250 => prepare_build_unit(u32,@edi *mut Unit)->u32;
	0x00462960 => add_guard_ai_to_unit_func(*mut Unit);
	0x0043DA20 => add_military_ai(bool,@eax *mut AiRegion,@ebx *mut Unit);
	0x004672F0 => AiTrainUnit(u32,u32,*mut libc::c_void,@eax *mut Unit)->u32;
	0x00495300 => get_angle(u32,@edx u32,@ecx u32,@eax u32)->u32;
	0x00438BF0 => get_ai_region(@esi *mut Unit)->*mut AiRegion;

	0x0043aBB0 => ai_attack_to(i32,i32,i32,i32,i32)->bool;
	0x00438B30 => ai_get_own_regions(@ebx *mut u8,u32)->*mut Sprite;
	0x00439B50 => ai_get_enemy_regions(u32,*mut u8);
	0x00436D80 => ai_get_neighbor_region_types(@ebx *mut u8)->u32;
	0x00436CF0 => ai_finish_region_access(@ebx *mut u8);
	0x004399D0 => ai_set_attack_prep_region(u32,*mut u8,i32,i32,bool,@ecx bool,@edx bool)->i32;

	0x004A09D0 => create_unit(u32,u32,@eax u32,@ecx u32)->*mut Unit;
	0x004A01F0 => finish_unit_pre(@eax *mut Unit);
	0x0049FA40 => finish_unit(@eax *mut Unit);
	0x00475710 => kill_unit(@eax *mut Unit);
	0x00467340 => set_hp(@eax *mut Unit, @ecx u32);

	//
	0x00435770 => inherit_ai(@ecx *mut Unit,@eax *mut Unit);
	0x004A2830 => inherit_ai2(@ecx *mut Unit, @eax *mut Unit);
	
	0x00433470 => ai_count_economy_units_func(@eax u32,u32)->u32;
	0x00454310 => update_speed(@eax *mut Unit);
	
	0x00491460 => reduce_energy(u32,@eax *mut Unit);

	0x004E6C40 => get_loaded_unit(@eax *mut Unit, @ecx u32)->*mut Unit;
	
	0x004A99C0 => GetFirstFreeHumanPlayerId() -> u32;
	0x00431F90 => ai_addbuildrequest(u32,u32,u32,u32,u32,@esi *mut AiTown);
	//0x004E6C40 = GetLoadedUnit(), eax Unit *transport, ecx index
	0x004C36C0 => update_ui();
	0x004CDE70 => add_to_replay_data(u32,@eax *mut ReplayData, @edi u32, @ebx *mut libc::c_void);
	0x00479930 => send_aise_command_func(u32,u32,u32,*mut Unit,u32,@edi *mut Unit,@eax u32);
	
	//
	0x0048CD30 => print_text_raw(u32,u32,u32, @eax *const c_char);
	0x0048CCB0 => print_info_message(@eax *const c_char);

	//0048CD30 = PrintTextRaw?(), arg 1 init_color?, arg 2 disappear_tickcount, arg 3 flags, eax char *string
	0x004A2CA0 => get_game_seconds()->u32;
	
	0x004196A0 => register_event_handler(@esi u32, @edi *mut Control);
	0x004E1970 => update_button_state(@ecx *mut Event, @esi *mut Control);
	0x004E19F0 => button_up(@esi *mut Control);
	0x004D1810 => get_modifier_key_states()->u32;
	0x00489FC0 => neutralize(@eax u32);
	0x0042CF70 => check_supply_for_building(u32,u32,u32)->u32;
	0x0048EE30 => show_info_msg(@ebx u32, @edi u32, @esi u32);
	0x0048E730 => issue_placebuilding_order(@esi u16, u32);
	0x0049CB60 => are_in_same_region_group(u32,u32,@eax *mut Unit)->u32;
	0x00438E70 => are_on_connected_regions(u32,u32,@eax *mut Unit)->u32;
	
	0x004E6EF0 => ai_findtransport(@ebx *mut Unit,*mut *mut Unit)->u32;
	0x00474540 => insert_order_before(Point,*mut Unit,@edx u32,@edi *mut Order,@ebx u32,@esi *mut Unit);
	0x00474CF0 => append_order_simple2(u32,Point,*mut Unit,u32,@edx *mut Unit);
	0x00474790 => insert_order_targeting_ground_before(u32,u32,@esi *mut Unit,@ebx u32,@edi *mut Order);
	0x004747E0 => insert_order_targeting_unit_before(@ecx *mut Unit,@esi *mut Unit,@ebx u32,@edi *mut Order);
	0x00475310 => issue_order_targ_nothing(@esi *mut Unit,@ecx u32);
	0x00475420 => force_order_done(@eax *mut Unit);
	0x004026A0 => set_attackmove_holdpos_flag(@eax *mut Unit);
	0x00401D40 => issubunit(@eax *mut Unit)->u32;
	0x00477160 => ai_update_attack_target(u32,u32,u32,@ecx *mut Unit)->u32;
	0x00474A70 => prepend_order_targeting_unit(@ecx u32,@edx *mut Unit,@eax *mut Unit);
	0x00475000 => do_next_queued_order(@ecx *mut Unit);
	0x0046DFC0 => check_upgrade_req(*mut Unit,@ebx u32,@edi u32)->u32;
	0x004281B0 => upgrade_in_progress(@ecx u32,@edx u32)->u32;
	0x00488210 => create_lone_sprite(u32,u32,u32,@edi u32)->*mut LoneSprite;
	0x004878F0 => update_ls_visibility(@esi *mut LoneSprite);
	0x00499D00 => play_iscript_anim(u32,u32,@ecx *mut Sprite);
    0x0048C260 => create_bullet(u32,u32,u32,u32,@eax *mut Unit,@ecx u32);
	0x00497B40 => delete_sprite(@edi *mut Sprite);
	0x004EB9F0 => move_unit(@edx *mut Unit, @eax u32,@ecx u32);
	0x00493CA0 => hide_unit_partial(@edi *mut Unit,u32);
	0x004EBAE0 => move_unit2(@edx *mut Unit, @eax u32,@ecx u32);
	0x0049D3E0 => get_fitting_pos(*mut Unit,*mut Point,*mut Point, u32, u32, @eax *mut Rect)->u32;
	0x00494160 => finish_move_unit(@eax *mut Unit);
	0x00401FA0 => fix_target_location(@eax u32, @edx *mut Point);
	0x0049FED0 => transform_unit(u32,@eax *mut Unit);
	0x00447D00 => ai_popspendingreq_resourceneeds(u32,@ecx u32);
	0x004476D0 => pop_spending_request(@eax u32);
	0x00432480 => are_same_race(@ecx *mut Unit, @eax u32)->u32;
	0x004361A0 => ai_tryprogress_spendingqueue(@eax *mut Unit);
	0x00498EA0 => add_top_overlay(u32,u32,u32,@eax *mut Sprite,@esi u32);
    0x004465C0 => choose_placement_position_func(u32, Point, *mut Point, u32, @ecx *mut Unit) -> u32;
	0x00492CC0 => disable_unit(@eax *mut Unit);
	0x0048ED50 => playsound_unit(u32,u32,@ebx u32,@esi *mut Unit);
	0x0049E530 => show_last_error(u32);
	0x00466F50 => rally_unit(@ecx *mut Unit, @eax *mut Unit);
	
	0x00498D70 => imgul(@eax *mut Sprite, @esi u32, u32, u32, u32);
	0x004E5CF0 => removeoverlay(@eax *mut Unit,@edx u32,@edi u32);
	0x004D74C0 => progress_iscript_frame(*mut Iscript,u32,u32,@ecx *mut Image);
	0x004EB290 => stop_moving(@eax *mut Unit);
	0x004EB5B0 => halt(@esi *mut Flingy);
	0x0046BF00 => instant_stop(@eax *mut Unit);
	0x004E6140 => show_shield_overlay(@eax u32, @ecx *mut Unit);
	0x00498E00 => add_overlay_above_main(u32,u32,u32,@eax *mut Sprite,@esi u32);
	0x00454A80 => begin_upgrade(@eax u32, @ecx *mut Unit)->u32;
	0x0042D140 => check_resources_and_supply(u32,@edx u32, @esi u32, @ebx u32)->u32;
	0x00433DD0 => add_unit_ai(@ebx *mut Unit, @esi *mut AiTown);
	0x0042D560 => is_walkable(@esi u32, @ecx u32)->u32;
	0x0042D740 => can_walk_here(u32,u32,u32,u32)->u32;
	0x0042FA00 => can_walk_simple(u32, @edx u32, @eax *mut Unit)->u32;//y, x, Unit
	
	
	0x004E76C0 => get_unload_position(*mut Point,@eax *mut Unit,@esi *mut Unit)->u32;
	0x0042FA30 => pathing_distance(u32,@eax u32,@ecx *mut Unit)->u32;
	0x00401500 => is_training_or_building_addon(@ecx *mut Unit)->u32;
	0x004E5EA0 => get_rclick_action(@eax *mut Unit)->u32;
	0x00476180 => has_way_of_attacking(@ecx *mut Unit)->u32;
	0x0047B2E0 => has_rally(@eax *mut Unit)->u32;
	
	0x00426F50 => draw_generic_unit_status();
	0x004246D0 => unk_func246d0();
	0x00457310 => hide_optional_screen_ctrl(@eax *mut Control);
	0x00457250 => show_control_group(@eax *mut Control, @ecx i32, i32);
	0x0047B090 => get_name(@ecx u32)->*const c_char;

	0x004258B0 => set_label(*const u8, @ecx i32, @eax *mut Control);
	0x00425510 => ss_draw_shield_upg(u32,@eax *mut Control);
	0x00425600 => ss_draw_armor(u32,@eax *mut Control);
	0x00418700 => hide_control(@esi *mut Control);
	0x00488900 => get_supply(@eax u32, @ecx u32)->u32;
	0x00466300 => fighter_finished(@esi *mut Unit, @edi *mut Unit);
	0x00468200 => begin_train(u32,@esi *mut Unit,u32)->*mut Unit;
	0x004679A0 => progress_build(u32,@eax *mut Unit,u32);
	0x004C8A30 => add_to_hangar(*mut Unit,u32);
	0x00468830 => update_mineral_amount_animation(@ecx *mut Unit);
//	0x004634E0 => set_ground_building_pathing(@eax *mut Unit)->u8;
	0x00469F60 => set_building_tile_flag(u32,u32,@eax *mut Unit);
	0x004CE770 => set_upgrade_func(@edx u8, @eax u32, @ecx u16);
	0x00433C70 => ai_repairsomething(*mut Unit,*mut AiTown)->u32;
	0x004348C0 => ai_tryprogressspendingqueue_worker(*mut AiTown, *mut Unit)->u32;
	0x00401CF0 => is_resource_depot(@edx *mut Unit)->u32;
	0x00468890 => is_mineral_field(@eax *mut Unit)->u32;
	0x00433B90 => ai_harvest(*mut Unit,@ecx *mut Unit,@eax *mut Unit);
	0x004432D0 => find_free_resource(*mut Unit, u32, @eax u32)->*mut Unit;
	//00432B90 = Ai_GetTownGas(), arg 1 gas_state, arg 2 index, esi AiTown *town
	0x00432B90 => get_town_gas(u32,u32,*mut AiTown)->*mut Unit;
	//004669B0 = GetEmptyBuildQueueSlot(), edx Unit *unit
	0x004669B0 => get_empty_build_queue_slot(@edx *mut Unit)->u32;
	//0047B180 = SetBuildHpGain(), edi Unit *unit
	0x0047B180 => set_build_hp_gain(@edi *mut Unit)->u32;
//	CheckUnitDatRequirements(), arg 1 player_id, eax unit_id, esi Unit *parent
//	00473410 = DoesBuildingBlock(), eax Unit *unit, edx x_tile, ecx y_tile
	0x0042FF80 => find_unit_borders_rect(@eax *mut Rect)->*mut *mut Unit;
//	00473410 = DoesBuildingBlock(), eax Unit *unit, edx x_tile, ecx y_tile
	0x00473410 => does_building_block(@eax *mut Unit, @edx u32,@ecx u16)->u32;
	0x00473300 => is_tile_blocked_by(*mut Unit, i16,i16,bool,bool,@eax *mut *mut Unit)->u32;
//	00473990 = CheckResourceBlocking(), eax check_vision, ecx x_tile, edx y_tile
	0x00473990 => check_resource_blocking(@eax u32,@edx u16,@ecx u16)->u32;
	0x004320D0 => get_gas_building_id(@eax u32)->u32;
	0x00444EB0 => do_area_fixups_for_enemy(u32,u32,u32);
	0x004376F0 => get_bunker_position(@ebx u32, @esi *mut Point)->u32;
	0x004553F0 => set_maelstrom_timer(u32,@edi *mut Unit);
	0x004797B0 => damage_unit_func(*mut Unit,u32,u32,@ecx *mut Unit, @eax u32);
	0x004F6350 => create_sprite_overlay(u32,@ebx *mut Unit);//based on sprite id
	0x004C8040 => give_unit(*mut Unit,u32);
	0x004E7F70 => unload_unit(@eax *mut Unit)->u32;
	0x004BBFD0 => play_sound_from_source(u32,u32,@ebx u32,@esi *mut Unit)->u32;
	0x00498170 => show_images_hidden_by_cloak(@eax *mut Sprite);
	0x00497E80 => remove_cloak_draw_funcs(@eax *mut Sprite);
	0x00498A90 => update_sprite_images_for_cloak(@edi *mut Sprite);
	0x004DBBE0 => clear_melee_computer_slots();
	0x004DBB70 => set_slot_to_computer(u32)->u32;
	0x0049AE40 => update_selection_overlays(u32,@eax *mut Unit)->u32;
	0x004C0860 => send_change_selection_command(u32,*mut Unit)->u32;
	0x00474810 => append_order(u32,*mut Unit,u32,@ecx u32,@edx *mut Unit,@eax u32);
	0x004E65E0 => set_construction_graphic(u32,@edi *mut Unit)->*mut Image;
    0x004742D0 => remove_order_from_queue(@eax *mut Order,@ecx *mut Unit)->u32;
	0x004745F0 => perform_another_order(@ebx u32, @edi *mut Order, @esi *mut Unit,@edx u32,u32,*mut Unit)->u32;	
	0x0043FF00 => focus_unit(@eax *mut Unit)->*mut Unit;
	0x0043FF90 => focus_unit2(@eax *mut Unit)->u32;
	0x00443080 => get_auto_target(@eax *mut Unit)->*mut Unit;
	0x00443F40 => reserve_static_building_on_placemap(@eax *mut Unit, u8, u16, u32, u32);
	0x0049A8C0 => prepare_formation_movement(@edi i32,u8);
	0x00487FD0 => find_fow_unit(@eax i16,@ecx i16,u16)->i32;
	0x0049A850 => get_next_commanded_unit()->*mut Unit;
	//0x005153FC => get_right_click_order[0x7]();
	0x004756E0 => targeted_order_unit(@edx i32,@esi *mut Unit,i32,u8);
	0x0046DC20 => can_issue_order(@eax *mut Unit,@ebx u16,i32)->i32;
	0x0049A500 => get_formation_movement_target(*mut Unit,i32);
	0x004756B0 => targeted_order_ground(@edx u8,@ebx i32,@esi *mut Unit,i32,i16,i16);
	0x00401DA0 => clear_target(@eax *mut Unit);
	0x00476FC0 => attack_unit(@eax u32, @edi *mut Unit, u32, u32);
	0x00474C70 => append_order_targeting_ground(*mut Unit, u8, u16, u16, u32);
	0x004753A0 => order_done(*mut Unit);
	0x00475260 => issue_order_targeting_ground(@esi *mut Unit, u8, u16, u16);

	0x004E4F40 => order_protoss_build_self(@eax *mut Unit);
	0x004E4C70 => order_warp_in(*mut Unit);
	0x00479480 => order_die(*mut Unit);
	0x00464D10 => order_nuke_track(*mut Unit)->u8;
	0x004777F0 => order_subunit_guard(@eax *mut Unit);
	0x00477980 => order_subunit_attack(@eax *mut Unit);
	0x0045E090 => order_drone_build(*mut Unit);
	0x00467FD0 => order_scv_build(*mut Unit);
	0x004E4D00 => order_probe_build(@edi *mut Unit);
	0x00467A70 => order_construct_building(@eax *mut Unit);
	0x004673D0 => order_repair(@eax *mut Unit);
	0x0045DD60 => order_zerg_birth(@eax *mut Unit);
	0x0045DEA0 => order_unit_morph(@eax *mut Unit);
	0x00467760 => order_terran_build_self(@eax *mut Unit);
	0x0045D500 => order_zerg_build_self(@eax *mut Unit);
	0x00465E00 => order_scarab(@eax *mut Unit);
	0x00493DD0 => order_recharge_shields(@edi *mut Unit);
	0x00464360 => order_land(@eax *mut Unit);
	0x004649B0 => order_liftoff(@esi *mut Unit,@edx u32);
	0x004548B0 => order_tech_func(@eax *mut Unit);
	0x004546A0 => order_upgrade_func(@eax *mut Unit);
	0x00468E80 => order_ore_interrupt(@esi *mut Unit);
	0x00468ED0 => order_harvest_wtf(@eax *mut Unit);
	0x00493920 => order_harvest_interrupt(@edi *mut Unit);
	0x00464BD0 => order_siege_mode(@edi *mut Unit);
	0x00464AE0 => order_tank_mode(@eax *mut Unit);
	0x004948B0 => order_warping_archon(@eax *mut Unit);
	0x00493B10 => order_complete_archon_summon(@esi *mut Unit);
	0x004E6700 => order_nuke_train(@eax *mut Unit);
	0x00493A80 => order_cloak_nearby_units(@esi *mut Unit);
	0x004671B0 => order_reset_collision1(@eax *mut Unit);
	0x0042E320 => order_reset_harvester_collision(@eax *mut Unit);
	0x004E3FB0 => order_ctf_cop2(@eax *mut Unit);
	0x0047C3C0 => order_critter_idle(@eax *mut Unit);
	0x00464180 => order_heal(@eax *mut Unit);
	0x004637B0 => order_heal_move(*mut Unit);
	0x00464050 => order_medic_holdpos(@esi *mut Unit);
	0x00463740 => order_heal2(@edi *mut Unit);
	0x00494690 => order_warping_dark_archon(@eax *mut Unit);
	0x004A1D80 => order_ai_patrol(@esi *mut Unit)->u8;
	0x00477D00 => order_attack_fixed_range(@eax *mut Unit,@ecx *mut Unit);
	0x00479040 => order_attack_move(@esi *mut Unit);
	0x00478DE0 => order_ai_attack_move(@eax *mut Unit);
	0x00479BD0 => order_attack_unit(@esi *mut Unit,@ecx *mut Unit);
	0x00493990 => order_recharge_shields2(@eax *mut Unit);
	0x004EA4C0 => order_infest_mine1(@eax *mut Unit,@ecx *mut Unit);
	0x004E9E60 => order_burrow(@eax *mut Unit);
	0x004E9860 => order_zerg_unkown(@eax *mut Unit);
	0x004666A0 => order_carrier(@edi *mut Unit);
	0x00466720 => order_carrier_ignore(@eax *mut Unit);
	0x00465950 => order_carrier_attack1(@eax *mut Unit);
	0x00465910 => order_carrier_idle(@eax *mut Unit);
	0x004F6C40 => order_hallucination(@edi *mut Unit);
	0x004654B0 => order_reaver_stop(*mut Unit);
	0x004A28B0 => order_computer_script(@eax *mut Unit);
	0x00478490 => order_computer_return(@eax *mut Unit);
	0x004E4210 => order_cop_create_flag(@eax *mut Unit);
	0x004E41A0 => order_ctf_cop1(@esi *mut Unit);
	0x004EA670 => order_unburrow(@eax *mut Unit);
	0x004550A0 => order_defensive_matrix(*mut Unit);
	0x0047BCD0 => order_open_door(@esi *mut Unit);
	0x0047BC50 => order_close_door(@esi *mut Unit)->u8;
	0x0047BD60 => order_disable_doodad(@eax *mut Unit);
	0x0047BE80 => order_unk_iscript(@eax *mut Unit);
	0x004E9AA0 => order_drone_land(@eax *mut Unit)->u32;
	0x0045CF80 => order_drone_start_build(@eax *mut Unit)->u8;
	0x004EA3E0 => order_enter_nydus(@eax *mut Unit);
	0x004E7CF0 => order_enter_transport(@eax *mut Unit);
	0x004E75D0 => order_transport_idle_open(@edi *mut Unit);
	0x004F3EA0 => order_unused_powerup(@eax *mut Unit);
	0x004E73B0 => order_pickup3(@edi *mut Unit);
	0x00478EC0 => order_harass_move(@eax *mut Unit)->u8;
	0x0047BF80 => order_trap_attack(@eax *mut Unit);
	0x0047C0A0 => order_trap_hide(@eax *mut Unit,@ecx *mut Unit);
	0x0047C1B0 => order_returntoidle_unk(@eax *mut Unit);
	0x0047C7B0 => order_follow(@eax *mut Unit);
	0x00475B90 => order_guard(@esi *mut Unit);
	0x004774A0 => order_player_guard(@eax *mut Unit);
	0x004778E0 => order_computer_guard(@eax *mut Unit);
	0x00469500 => order_move_to_harvest(@eax *mut Unit);
	0x00469000 => order_enter_gas(@eax *mut Unit);
	0x00469980 => order_harvest_gas(@eax *mut Unit);
	0x00469240 => order_move_to_minerals(@eax *mut Unit);
	0x00468F60 => order_wait_minerals(@eax *mut Unit);
	0x004697C0 => order_harvest_minerals(@eax *mut Unit);
	0x00478D10 => order_hold_position(@eax *mut Unit);
	0x0047C950 => order_move(@eax *mut Unit);
	0x004E96D0 => order_init_creep_growth(@eax *mut Unit)->u8;
	0x00493F70 => order_init_pylon(@eax *mut Unit);
	0x004E9F90 => order_larva(@eax *mut Unit);
	0x00463AC0 => order_lifting_off(@eax *mut Unit);
	0x004E7700 => order_move_unload(*mut Unit);
	0x004E7B70 => order_pickup4(@eax *mut Unit);
	0x004A1C20 => order_neutral(@edi *mut Unit);
	0x00479200 => order_nuke_ground(*mut Unit);
	0x00464730 => order_nuke_launch(@eax *mut Unit);
	0x00463610 => order_nuke_attack(@eax *mut Unit);
	0x00479410 => order_nuke_ground2(@eax *mut Unit);
	0x0045DC20 => order_nydus_exit(@ebx *mut Unit);
	0x004780F0 => order_patrol(@eax *mut Unit);
	0x004E7300 => order_transport_idle(@eax *mut Unit);
	0x004E6880 => order_place_addon(@esi *mut Unit,u8);
	0x00464FD0 => order_place_mine(@eax *mut Unit);
	0x004F3E10 => order_powerup_idle(@eax *mut Unit)->*mut Sprite;
	0x004E4F20 => order_protoss_prebuild(@eax *mut Unit);
	0x004E98E0 => order_infest(@eax *mut Unit);
	0x004EA290 => order_infest_mine4(@eax *mut Unit,@ecx *mut Unit);
	0x004F6EF0 => order_right_click_action(@eax *mut Unit);
	0x004A1EF0 => order_rescue_passive(@eax *mut Unit)->u8;
	0x00466350 => order_interceptor_return(@eax *mut Unit);
	0x004690C0 => order_return_resources(@eax *mut Unit);
	0x00478A40 => order_sap_location(@eax *mut Unit);
	0x004788E0 => order_sap_unit(@eax *mut Unit);
	0x00464E40 => order_place_scanner(@eax *mut Unit);
	0x00463D30 => order_zerg_birth_kill(@eax *mut Unit);
	0x0047C4F0 => order_stay_in_range(@eax *mut Unit);
	0x0047BBA0 => order_returntoidle(@eax *mut Unit);
	0x004E95E0 => order_stop_creep_growth(@eax *mut Unit)->u8;
	0x00465F60 => order_interceptor(@eax *mut Unit);
	0x00494470 => order_recall(*mut Unit);
	0x00476F50 => order_building_attack(@eax *mut Unit);
	0x00479150 => order_tower_attack(@eax *mut Unit);
	0x004665D0 => order_reaver(@edi *mut Unit);
	0x00465690 => order_reaver_attack_unk(@eax *mut Unit);
	0x004E80D0 => order_unload(@eax *mut Unit);
	0x00463DF0 => order_spider_mine(@eax *mut Unit);
	0x0047BAB0 => order_watch_target(@eax *mut Unit);
	0x0045D0D0 => order_building_morph(*mut Unit);
	0x00492850 => order_spell(@eax *mut Unit);
	0x004F6FA0 => order_obscured(@eax *mut Unit);
	0x00463900 => order_medic_idle(@eax *mut Unit);
	0x004F6950 => order_mind_control(*mut Unit);
	0x004F6D40 => order_feedback(*mut Unit);
	0x0047C210 => order_junk_yard_dog(@eax *mut Unit);
	0x004308A0 => find_units_rect(*mut Rect)->*mut *mut Unit;
	0x0046F0F0 => multi_select_units_func(*mut *mut Unit, *mut *mut Unit, *mut Unit)->u32;
	0x0046F290 => add_to_selection_buffer(*mut *mut Unit,*mut *mut Unit,u32,*mut Unit)->u32;
	0x0046FA00 => select_units(*mut *mut Unit,u32,u32,@edi u32);
	0x0047B1D0 => unit_to_index(@esi *mut Unit)->u32;
	0x00496D30 => try_select_recent_hotkey_group(u32)->u32;
	
	0x0046ED80 => is_unselectable_func(@eax u32)->u32;
	0x0046F040 => replace_lower_draw_order_unit(@ecx *mut *mut Unit,*mut Unit,*mut Unit,@eax u32);

	0x0040C360 => fast_distance(i32,i32,i32,i32)->u32;
	0x00476930 => is_threat(@eax *mut Unit,@ecx *mut Unit)->bool;
	0x0043CD40 => set_suicide_target(@eax *mut Unit);

	0x004E6BA0 => is_transport(*mut Unit)->bool;

	0x0049CB40 => get_region1(@eax *mut Unit)->i16;
	0x0049C9F0 => get_region2(@edi i32, @ecx i32)->i16;

	0x00439500 => ai_attack_clear(u32)->u32;
	0x0043E050 => ai_send_suicide(@eax u8,u32);
	0x004c3450 => write_compressed(@eax *mut libc::c_void, u32, *mut File)->u32;
	0x00438050 => get_missing_attack_force_units_func(*mut AiRegion,*mut u32,bool)->u32;
	0x0043E2E0 => ai_add_to_region_military(u32,u32,@eax *mut AiRegion)->u32;
	0x00447DC0 => add_cost_to_ai_needs(u32,@eax u32,@ebx u32);
	0x004484A0 => add_base_cost_to_ai_needs(u32,@esi *mut AiRegion, @ecx u32, @eax u32);
	0x0043A390 => ai_recalculate_region_strength(@esi *mut AiRegion);
	0x0043CC40 => ai_target_enemy_in_region(@edi *mut AiRegion);
	0x00447980 => add_spending_request_func(u32,*mut libc::c_void,@eax u16, @ecx u32, @edx u8);
	0x0043DE40 => move_military_to_defend(@edi *mut AiRegion)->u32;
//	0x0043A790 = Ai_CanTravelToRegion(u32, *mut AiRegion)->u32;
	0x0043A790 => ai_can_travel_to_region(*mut AiRegion, *mut AiRegion)->u32;
	0x00438FC0 => ai_get_region_military_types(@ebx *mut u32, @edi *mut u32, @esi *mut u32, @eax *mut AiRegion);
//	0x004390A0 => change_ai_region_state(@ebx u32, @esi *mut Region);
	0x0043A510 => ai_pickattacktarget_nobuildings(*mut AiRegion)->*mut Unit;
	0x0043E580 => ai_move_military_to_otherregion(@esi *mut AiRegion, @edi *mut AiRegion, u32,u32,u32);
	0x0043dd20 => func_3dd20(@esi *mut AiRegion);
	0x00447090 => is_attack_timed_out(@ecx u32)->u32;
	0x0043A2E0 => has_forces_moving_to_attack(@eax u32)->u32;

	0x00442460 => has_target_in_range(@ecx u32, *mut Unit)->bool;
	0x004381D0 => ai_is_military(*mut Unit)->u32;
	0x00437000 => ai_is_pure_spellcaster(@eax *mut Unit)->u32;
	0x00437E70 => ai_are_connected(@eax u32, @ebx u32, @esi u32)->bool;
	0x0043D785 => ai_region_center_from_id(@eax u16, u32, u32)->u32;
	0x0043C980 => ai_unk_order(@eax u32, @ecx *mut Unit);
	
	0x004D8470 => set_iscript_anim(@ecx *mut Image, u8);
	0x0042D8C0 => update_iscript_speed(@esi *mut Unit);
	0x004D6F90 => flingy_subfunc1(@eax *mut Sprite, u16, i32, u16)->*mut Image;

	0x004DC4A0 => rand_short(@eax u32)->u16;
	0x0047B150 => get_race(@eax *mut Unit)->u8;
	0x0045DA40 => cancel_zerg_building(*mut Unit);
	0x0042CE70 => refund_fourth_of_cost(@eax u16, @ecx u8)->u32;
	0x0042CEC0 => refund_full_cost(@eax u8, @ecx u16)->u32;

	0x0047B5F0 => modify_speed_func(@edx *mut Unit, @eax u32)->u32;
	0x00499BB0 => replace_sprite(@eax *mut Sprite, u32, u32);
	0x00499860 => switch_sprite_id(@ecx *mut Sprite, @eax u32)->u8;

	0x0049B0E0 => get_sprite_color(@eax u32)->u32;
	0x0040ABBE => draw_grp(u32, u32, *mut GrpFrame, *mut Rect, u32);

	0x00437670 => get_turret_position(@ebx u32, @esi u32)->u32;
	0x004E5B40 => get_sight_range(@edx *mut Unit,u32)->u32;
	0x004926D0 => spell_order(u32,u32,*mut u16,@eax u32,@ebx *mut Unit)->u32;
	0x00454F90 => create_defensive_matrix(@edi *mut Unit)->u32;
	0x00491870 => get_max_energy(@eax *mut Unit)->u32;
	

	0x00459030 => draw_tooltip_text(u8)->u16;
	0x00481480 => hide_tooltip()->u8;
	0x0041F1B0 => snprintf(@edi u8, @esi u32, u8);

	0x0047B6A0 => set_build_shield_gain(@ebx *mut Unit);
	0x00454370 => set_upgrade_speed(@esi *mut Unit);
	0x00497920 => progress_sprite_frame(@esi *mut Sprite);
	0x00496360 => init_flingy(@eax u32, @edx u16, @ecx u32, *mut Bullet, u8, u8)->*mut Sprite;

	0x0047B910 => are_enemies(@eax u32, @ecx u32)->u32;
	0x00430F10 => is_in_area(*mut Unit, u32, *mut Unit)->u32;
	0x00475BE0 => check_firing_angle(@esi *mut Unit, @ecx u32, @eax u32, u8)->u32;
	0x004A1140 => is_unk_target(@ecx *mut Unit, @esi *mut Unit)->u32;
	0x00442160 => get_threat_level(@ebx *mut Unit, @edi *mut Unit)->u32;
	0x00440160 => add_unit_to_possible_targets(@ecx u32, @esi *mut Unit)->u32;

	0x0041C400 => mark_control_dirty(@eax *mut Control);
	0x00459360 => clear_cmdbtn_tooltip();
	0x00458800 => set_tooltip_resources(@eax u16, @edx u16, @ecx u16, u16, u16, u16, u16, u16)->u16;

	0x004CE7A0 => get_upgrade_level(@eax u32, @ecx u16)->u8;
	0x00459150 => draw_cmdbtn_tooltip_text(u32)->u8;
	0x00418830 => highlight(@edi *mut Control);
	
	0x00476730 => can_attack_unit(u32,@esi *mut Unit,@ebx *mut Unit)->u32;
	0x00453ED0 => get_next_upgrade_gas_cost(@eax u32, @ebx u8)->u32;
	
	/*
	0x0041B410 => list_update(@eax *mut Control);
	0x0041B760 => listbox_add(@eax u8, @ecx *mut Control, u8)->u8;
	0x0041A600 => set_selected_dialog(@ebx u8, @esi *mut Control);
	*/
	
	0x00495240 => change_move_pos(@eax *mut Bullet, @ecx u32, @esi u32)->*mut Bullet;
	0x0048B250 => progress_bullet_movement(@eax *mut Bullet);
	0x0048B1E0 => choose_bounce_target(@eax *mut Bullet)->*mut Unit;
	0x004E8280 => for_each_unit_borders_in_area(@eax *mut Rect, @ebx u32, *mut Unit)->u32;
	0x0048AFD0 => find_next_bounce_target(*mut Unit)->u8;
	
	0x0047B8A0 => calculate_acceleration_func_first_byte(*mut Unit)->u32;
	0x0047B850 => calculate_turn_speed_func_first_byte(*mut Unit)->u32;

	
	0x004D8470 => play_image_iscript(@ecx *mut Image, u8);
	/*
	0x004020B0 => unit_is_frozen(@eax *mut Unit)->u8;
	0x00401E70 => is_constructing_building(*mut Unit, u32)->u8;
	0x0046E100 => gen_unit_req_offsets()->u32;
	0x004E66B0 => is_building_addon(@eax *mut Unit)->u8;
	0x004E4C40 => is_building_protoss(@eax *mut Unit)->u8;
	0x00465270 => get_train_queue_size(u32, u32)->u8;
	0x004653D0 => get_hangar_capacity(*mut Unit)->u8;
	0x004CE850 => is_tech_researched(@eax u32, @ecx u16)->u8;
	*/
	
	0x00488D50 => modify_unit_counters2_func(i32, @edi *mut Unit, @ecx i32);//func
	0x00476000 => get_target_acquisition_range(@edx *mut Unit)->u8;
	0x004E87E0 => find_nearest_unit_box(@eax u16, @ecx *mut Unit, u32, *mut Unit)->*mut Unit;
	0x00440EC0 => spidermine_enemy_proc(*mut Unit, *mut Unit)->u8;
	0x0043ABB0 => ai_attack_prepare_func(u32,u32,u32,u32,u32)->u32;
	0x004AAF23 => gptp_save()->*mut GptpSaveState;
	0x004AAE94 => gptp_load(@eax *const u8, @ecx u32);
	0x004AAFE3 => gptp_flingyids(@ecx u32)->u32;
	0x004AB045 => gptp_flingyid_copy();
	0x004AB147 => gptp_set_flingy_id(@eax u32,@ecx u32);
	0x004C36F0 => get_stat_txt_str_func(@ecx u32) -> *const u8;
	0x00474D10 => append_order_simple(*mut Unit,u32,u32,*mut Unit);
	0x004754F0 => targeted_order(u32,u32,u32,*mut Unit,u32,*mut Unit,@edx u32,@eax u32,@ebx u32,@esi *mut Unit);
	0x004E5CF0 => remove_overlays(@eax *mut Unit,@edx u32,@edi u32);
	
	0x004c04d0 => modify_alliance_status(@esi u32)->*mut Unit;
	0x0049b870 => set_alliance()->u8;
);

pub const TooltipSurfaceHeight: usize = 0x00481359;
pub const TooltipSurfaceBytes: usize = 0x0048133F;
pub const TooltipTextSurfaceHeight: usize = 0x0048137C;
pub const TooltipTextSurfaceBytes: usize = 0x0048136C;  

whack_vars!(init_vars, 0x00400000,
    0x0057EE9C => player_name: [u8; 0x19];
	0x005820B4 => score_factories_owned: [u32; 0xc];	
	0x00581E14 => score_units_produced: [u32; 0xc];
	0x00581F64 => score_structures_constructed: [u32; 0xc];	
	0x00581F34 => score_structures_total: [u32; 0xc];	
	0x00581ED4 => score_men_total: [u32; 0xc];
	0x00582024 => score_buildings_total: [u32; 0xc];
	0x00581F94 => score_structures_owned: [u32; 0xc];
	0x00581E44 => score_men_owned: [u32; 0xc];
	0x0057EEE0 => player_list: [Player; 0xc];
	0x006509A4 => active_players: [u8; 0x8];
	0x006284E8 => player_selections: [[*mut Unit; 0xc]; 0x8];
	0x0057F1EC => visions: [u32; 0xc];
    0x0057F0B4 => is_multiplayer: u8;
	0x005993C4 => tile_ids: *mut u16;
    0x006D1260 => tile_flags: *mut u32;
	//0x005873F0
	0x005873F0 => dark_swarm_value: u32;
	0x0059C190 => minimap_colors: *mut u8;
	0x00628494 => megatiles: *mut u32;//extended megatiles .cv5ex
	0x005993CC => unk_flags: *mut u8;
	//0x00628494 => megatiles_old: *mut u16;
	0x00628458 => megatile_data: *mut MegatileVx4ex;
	0x006D5EC8 => tile_groups: *mut TileGroupCv5Ex;
	0x005993D0 => minitile_data: *mut Minitile;
	0x0059CB60 => terrain_to_minimap_color: [u8; 0x100];//in 59cb58-59cca8 space
	0x006558C0 => upgrades_dat_req: [u16; 0xe4];
    0x00597208 => client_selection: [*mut Unit; 0xc];
	0x005128F8 => remap_data: *mut PaletteRemap;
	0x0051290C => color_shift_data: [ColorShiftData; 0x7];
	0x0066FF60 => last_error: u32;
	0x00597248 => active_sel: *mut Unit;
	0x00512684 => local_nation_id: u32;
	0x005998E0 => megatile_count: u16;
	0x00654868 => first_active_fow_sprite: *mut LoneSprite;
	//reference: ResourceAreaArray
	0x00692688 => resource_areas: ResourceAreaArray;
	0x0059CC64 => minimap_image_width: u16;
	0x0059CC74 => minimap_image_height: u16;
	0x0059C18C => minimap_width: u16;
	0x0059C18E => minimap_height: u16;
	0x00512680 => lobby_command_user: u32;
	0x0051267C => active_player_id: u32;
	0x00596BBC => replay_data: *mut ReplayData;
	0x0068C14C => current_button_set: u16;
	0x0051CE94 => next_frame_tick: u32;
	0x00596A28 => shift_down: u8;
	//00650974 = byte player_victory_status2[0x8], 0x2 = Lost, 0x3 = Won, 0x4 = Not in game?, set 4 if trig flag 0x2
	0x00650974 => victory_status2: [u8; 0x8];
	0x0058D700 => victory_status: [u8; 0x8];
	0x006CA51C => player_build_minecost: [u32; 0x8];
	0x006CA4EC => player_build_gascost: [u32; 0x8];
	0x0057F1E2 => game_player_race: u8;
	0x00656380 => energy_cost_old: [u16; 0x2c];
	0x0057F23C => elapsed_frames: u32;
	0x0063FF4C => pylon_x_rad: u32;
	0x0063FF58 => pylon_y_rad: u32;
	0x0058D720 => start_positions: [[u16; 0x2]; 0x8];
	0x006D0534 => unk_unused: u16;
	0x006D0F14 => is_replay: u32;
	0x0068C1E5 => status_screen_func: u8;
	0x0059723D => client_selection_amount: u8;
	0x00597238 => game_screen_info: *mut libc::c_void;
	
	0x006408F8 => building_placement_state: [u8;0x60];//0x8 * 0x6 * ? dependent on placement state
	0x0068C1B0 => force_button_refresh: u32;
	0x0068AC74 => portrait_redraw: u8;
	0x0068C1F8 => status_screen_redraw: u8;
	0x0068C1E8 => control_under_mouse: u32;
	0x0068C1EC => control_under_mouse_val: u32;
	0x0057F1E3 => custom_singleplayer: u8;
	0x0057F266 => selected_singleplayer_race: u8;
	
	//orig dat entries
	//units
	
	0x006644F8 => units_dat_flingy_old: [u8;0xe4];
	0x006635D0 => units_dat_armor_upg: [u8;0xe4];
	//weapons
	0x00656CA8 => weapons_dat_flingy: [u32;0x82];
	0x006571D0 => weapons_dat_dmg_upg: [u8;0x82];
//
//	0x006BEE6C => position_search_results_count: u32;
//	0x006BEE70 => position_search_results_offsets: [u32;0x4];
//006BEE64 = dword position_search_result_units_count
	0x006BEE64 => position_search_result_units_count: u32; 
	0x006BEE6C => position_search_results_data: [u32;0x5];//
	//0x0 - data
	//0x4 - offsets [0x4]
	0x0066FF78 => unit_positions_x: [UnitPositions;0xd48];
	0x006769B8 => unit_positions_y: [UnitPositions;0xd48];
	0x00596865 => game_template: GameTemplate;//0x005967F8+6d
	0x0057F267 => selected_singleplayer_races: [u8;0x8];
	0x006284B8 => client_select3: [u32;0xc];
	0x0059723C => client_selection_changed: u8;

	0x00663888 => units_dat_ore_cost: [u16;0xe4];
	0x0065FD00 => units_dat_gas_cost: [u16;0xe4];
	0x00660428 => units_dat_time_cost: [u16;0xe4];
	0x00662350 => units_dat_health: [u32;0xe4];
	0x00664898 => units_dat_return_order: [u8;0xe4];
	0x00663320 => units_dat_attack_order: [u8;0xe4];
	0x00664080 => units_dat_special_ability_flags: [u32;0xe4];
	0x00662098 => units_dat_rclick_action: [u8;0xe4];
	0x00663EB8 => units_dat_kill_score: [u16;0xe4];
	0x006644F8 => units_dat_flingy: [u8;0xe4];
	0x00662F88 => units_dat_portrait: [u16;0xe4];
	0x00662860 => units_dat_placement_box: [u16;0x2*0xe4];
//	0x00661518 => units_dat_staredit_availability_flags: [u16;0xe4];

	0x00664B00 => orders_dat_use_weapon_targeting: [u8;0xbd];

	0x0057F0F0 => player_ore: [u32;0xc];
	0x0057F120 => player_gas: [u32;0xc];

	0x00665040 => orders_dat_can_interrupt: [u8;0xbd];
	0x006283F8 => first_player_unit: [*mut Unit;0xc];
	0x00628430 => first_active_unit_var: *mut Unit;
	
	0x0057F1D4 => map_width_tiles: u16;
	0x0057F1D6 => map_height_tiles: u16;
	0x00628450 => map_width_pixels: u16;
	0x006284B4 => map_height_pixels: u16;

	0x00512678 => command_user: u32;
	0x0068C300 => unk_order: [u8;8];
	0x0064088D => placement_order: u8;
	
	0x00596A18 => keystate: [u8; 0x100];
	0x0057F0B0 => player_visions: u32;
	0x006BEE6C => position_search_results: [u32;0x5];////0x006BEE6C is count, 0x006BEE70 is first offset

	0x006637A0 => units_dat_group_flags: [u8;0xe4]; //0x1 = Zerg, 0x2 = Terran, 0x4 = Protoss, 0x8 = Men, 0x10 = Building, 0x20 = Factory, 0x40 = Independent, 0x80 = Neutral
	0x00664080 => units_dat_flags: [u32;0xe4];
	0x00660178 => units_dat_ai_flags: [u8; 0xe4]; // 0x1 = ?, 0x2 = Non-military
	
	0x006610B0 => units_dat_construction_image: [u32;0xe4];
		
	0x00661FC0 => units_dat_snd_ready: [u16;0xe4];
	0x00663B38 => units_dat_snd_annoyed_begin: [u16;0xe4];
	0x00661EE8 => units_dat_snd_annoyed_end: [u16;0xe4];
	0x0065FFB0 => units_dat_snd_what_begin: [u16;0xe4];
	0x00662BF0 => units_dat_snd_what_end: [u16;0xe4];
	0x00663C10 => units_dat_snd_yes_begin: [u16;0xe4];
	0x00661440 => units_dat_snd_yes_end: [u16;0xe4];

	0x006BB210 => unit_strength: [u32; 0xe4 * 2];
	0x0058D634 => alliances: [u8;0xc * 0xc]; // 0 = enemy, 1 = self, 2 = ally

	0x0058D6F8 => elapsed_seconds_var: u32;
	0x0068C100 => first_active_ai_script: *mut AiScript;
	0x0068C0FC => active_ai_scripts: [*mut AiScript;0x64];
	//ext	
	0x006C9EF8 => flingy_dat_top_speed: [u32;0xd1];
	0x006C9C78 => flingy_dat_acceleration: [u16;0xd1];
	0x006C9E20 => flingy_dat_turn_radius: [u8;0xd1];
	0x006C9858 => flingy_dat_movement_type: [u8;0xd1];
	0x006CA318 => flingy_dat_sprite_id: [u16;0xd1];
	0x006C9930 => flingy_dat_halt_distance: [u32;0xd1];
	0x006CA240 => flingy_dat_iscript_mask: [u8;0xd1];


	0x00629A88 => sprites_dat_vision: [u8; 0x205];
	0x00665C48 => sprites_dat_as_visible: [u8; 0x205];
	0x00666160 => sprites_dat_image_id: [u16;0x205];
	//
	0x00665A3E => sprites_dat_selection_circle: [u8;0x205];
	0x00665F56 => sprites_dat_selection_imagepos: [u8;0x205];
	0x00665DCE => sprites_dat_healthbar: [u8;0x205];
	//
	0x00665AC0  => sprites_dat_selection_circle_off: [u8;0x205];//+130 offset
	0x00665FD8  => sprites_dat_selection_imagepos_off: [u8;0x205];//+130 offset
	0x00665E50  => sprites_dat_healthbar_off: [u8;0x205];//+130 offset
	//
	0x00668AA0 => images_dat_grp_id: [u32;0x3e7];
	0x0066E860 => images_dat_graphic_turns: [u8;0x3e7];
	0x0066C150 => images_dat_clickable: [u8;0x3e7];
	0x0066D4D8 => images_dat_use_full_iscript: [u8;0x3e7];
	0x00667718 => images_dat_draw_if_cloaked: [u8;0x3e7];
	0x00669E28 => images_dat_draw_func: [u8;0x3e7];
	0x00669A40 => images_dat_draw_remap: [u8;0x3e7];
	0x0066EC48 => images_dat_iscript_id: [u32;0x3e7];
	0x0066D8C0 => images_dat_overlay_shield: [u32;0x3e7];
	0x0066B1B0 => images_dat_overlay_attack: [u32;0x3e7];
	0x0066A210 => images_dat_overlay_damage: [u32;0x3e7];
	0x00667B00 => images_dat_overlay_special: [u32;0x3e7];
	0x00666778 => images_dat_overlay_landing: [u32;0x3e7];
	0x0066D8C0 => images_dat_overlay_liftoff: [u32;0x3e7];
	//pointer values
	0x0051F2A8 => overlay_ptr_attack: [u32;0x3e7];
	0x0051F2A8 => overlay_ptr_damage: [u32;0x3e7];
	0x005211E0 => overlay_ptr_special: [u32;0x3e7];
	0x0052217C => overlay_ptr_landing: [u32;0x3e7];
	0x00523118 => overlay_ptr_liftoff: [u32;0x3e7];
	0x0052E5C8 => overlay_ptr_shield: [u32;0x3e7];
	0x0051CED0 => ptr_image: [u32;0x3e7];
	0x005240D0 => building_overlay_state_max: [u32;0x3e7];
	

	0x0051CED0 => image_grps: [*mut GrpFrame;0x3e7];
	0x00581D76 => player_color_palette: [u8;0x8*0xc];
	0x006D5BD4 => current_draw_player: u32;
	0x0050CDC1 => default_grp_remap: [u8;0x100];
	
	
//	0x00512D28 => retreat_array: [u32,u32;0x100]; // no idea how to define this
	0x006D5BFC => pathing_var: [u32;0x97a20];
	0x0069A604 => ai_region_var: [*mut AiRegion;0x8];
	0x006D5A6C => cheat_flags: u32;
	0x006D5C20 => unused_order_type_4_position_xy: u32;
	0x006D5C24 => unused_order_type_4_target: *mut Unit;
/*
	0x006636B8 => units_dat_ground_weapon: [u8;0xe4];

	0x006D1238 => stat_txt_tbl_buffer: u16;
	0x0063FD30 => current_visibility_hash: [u32;0x40];
	0x00629D90 => want_visibility_hash: u32;
	0x0063FE38 => region_visibility_hash: u8;
	0x0063FD28 => current_hash_offset: u32;
	0x00629A88 => sprite_include_in_vision_sync: [u8;0x205];
*/
	0x006957A4 => max_range_px: u32;
	0x006955DC => min_range_px: u32;
	
	/*
	0x0068C1D0 => tooltip_minerals: u16;
	0x0068C1D2 => tooltip_gas: u16;
	0x0068C1D4 => tooltip_energy_p: u16;
	0x0068C1D6 => tooltip_energy_t: u16;
	0x0068C1D8 => tooltip_energy_z: u16;
	0x0068C1DA => tooltip_supply_p: u16;
	0x0068C1DC => tooltip_supply_t: u16;
	0x0068C1DE => tooltip_supply_z: u16;
	0x0068C1CC => tooltip_upgrade_repeat: u16;

	0x00655700 => upgrades_dat_repeat_count: [u8;0x3d];

	0x00656248 => tech_dat_ore_cost: [u16;0x2c];
	0x006561F0 => tech_dat_gas_cost: [u16;0x2c];

	0x0051AAA0 => singleplayer_race_select: u8;
	0x006D1220 => network_tbl: *mut Tbl;
	*/
	/*
	0x0064EECC => bullet_bounce_missile_source: *mut Unit;
	0x0064EED0 => bullet_bounce_missile_target: *mut Unit;
	0x0064EED4 => bullet_bounce_missile_target_next: *mut Unit;

	0x0057F27C => unit_availability: [u8;0xe4];
	0x00660A70 => unit_req_offset: [u16;0xe4];
	0x00514178 => requirement_table: [u16;0xe4]; // neiv's list doesn't define length of array, left is as 0xe4 for now but it might be something else\
	0x00584DE4 => completed_unit_count: [u32;0xe4*0xc];
	0x00582324 => all_unit_count: [u32;0xe4*0xc];
	0x0058F440 => is_bw: u8;
	*/
);
//pub const mineral_test_offset: usize = 0x00453F5a;
//pub const creep_range_instruction: usize = 0x004147BC;
//pub const creep_range_value: usize = 0x004147BE;
//
//pub const asm_call: usize = 0xe8;