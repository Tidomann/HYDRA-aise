#![allow(dead_code)]
#![allow(non_snake_case)]
use failure::{Context,Error};
use bw_dat::{order, TechId, UnitId, OrderId};
use fxhash::{FxHashMap,FxHashSet};
use std::collections::hash_map::Entry;
use unit::{self, HashableUnit, Unit};
use unit_search::{self, UnitSearch, PriorityType};
use config;
use crate::ai::{self, has_resources};
use samase;
use toolbox::{self, UnitSetup, Route};
use build_queue;
use list::ListIter;
use pathfinding::{self, InfluenceZone, InfluenceShape, InfluenceFind, InfluenceType, InfluenceFlags};
use std::cmp;
use std::ptr::null_mut;
use swap_retain::SwapRetain;
use windows;	
use aiscript::{
    self, PlayerMatch, Town, UnitMatch, Position, ReadModifier, ReadModifierType, ScriptData, ModifierAction, BoolModifier, Script, WriteModifier,
	LocalModifier,
};

use globals::{
    self, Globals, BaseLayout, NewGameState, HydraEffect, UpgradeQueue, VarType, TownStateValue, BuildQueue
};
use bw;
use game::Game;
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum PlaceMorphStage {
	Init,
	CheckForCompletion,
	WaitForResources1,
	SendMorpher,
	WaitForMorpher,
	WaitForResources2,
	Morph,
	WaitEnd,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct PlaceMorphStatus {
	pub delay: u32,
	pub stage: PlaceMorphStage,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct PlaceMorph {
	pub player: u8,
	pub area_gather: bw::Rect,
	pub area_place: bw::Rect,
	pub unit_morpher: UnitId,
	pub morph_to: UnitId,
	pub number: u32,
	pub max_delay: u32,
	pub status: Option<PlaceMorphStatus>,
}


#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct WarpRelayAi {
	pub player: u8,
	pub targets: UnitMatch,
	pub seek_range: u32,
	pub delay: u32,
	pub max_delay: u32,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct NydusPlacementAi {
	pub player: u8,
	pub delay: u32,
	pub max_delay: u32,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum KitingState {
	Init,
	KiteBack,
}
#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum KitingAttackState {
	Init,
	Cooldown,
}

impl Default for KitingState {
    fn default() -> KitingState {
        KitingState::Init
    }
}

impl Default for KitingAttackState {
    fn default() -> KitingAttackState {
        KitingAttackState::Init
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RunningKite {
	pub state: KitingState,
	pub delay: u32,
	pub attack_state: KitingAttackState,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct KiteSetting {
	pub kiting_units: UnitMatch,
	pub limit: u32,
	pub player: u8,
	pub kite_state: FxHashMap<HashableUnit,RunningKite>,
}


#[derive(Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum Action {
	SafetyOff,
	SafetyOn,
	ActivateMedstims,
	DeactivateMedstims,
	//
//	Conduit
	ApertureOn,
	ApertureOff,	
	PlanetCracker,
	Observance,
	KhaydarinControlOn,
	KhaydarinControlOff,
	PhaseLinkOn,
	PhaseLinkOff,
	ReverseThrustOn,
	ReverseThrustOff,
	SublimeShepherdOn,
	SublimeShepherdOff,
	TempoChangeOn,
	TempoChangeOff,
	DisruptionWebOn,
	DisruptionWebOff,
	//
	TurbomodeOn,
	TurbomodeOff,
	ExpelWasteOn,
	ExpelWasteOff,
	AltitudeUp,
	AltitudeDown,
	MissilesOn,
	MissilesOff,
	BasiliskOn,
	BasiliskOff,
	AggressiveMiningOn,
	AggressiveMiningOff,
	CustomStim,
	//
	None,
}

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub struct ITAction {
	pub action_data: IdleTacticParsingData,
	pub action: Action,
	pub unit_match: UnitMatch,
	pub player: u8,
	pub wait: u32,
	pub max_wait: u32,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum HarassState {
	Default,
	Home,
	Attacking
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum TransportState {
	Default,
	CanPickEnemy,
	MustPickReaver,
	MustFlyToReaver,
	MustKeepDistance,
	ReturnHome,
	UnloadReaver,
}

#[derive(Copy, Debug, Serialize, Deserialize, Clone)]
pub struct HarassAi {
	pub player: u8,
	pub unit: Unit,
	pub delay: u16,
	pub max_delay: u16,
	pub state: HarassState,
	pub pair: Option<Unit>,
}


#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum PatrolState {
	Default,
	Home,
	Attacking
}
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct HunterKillerAi {
	pub player: u8,
	pub unit: Unit,
	pub delay: u16,
	pub max_delay: u16,
	pub state: PatrolState,
}
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct HunterKillerFlock {
	pub flock: Vec<HunterKillerAi>,
	pub leader: HunterKillerAi,
	pub initial_count: u32,
}

#[derive(Copy, Debug, Serialize, Deserialize, Clone)]
pub struct HarassAiTransport {
	pub player: u8,
	pub unit: Unit,
	pub delay: u16,
	pub max_delay: u16,	
	pub state: TransportState,
	pub designated_target: bw::Point,
	pub pair: Option<Unit>,
	pub attack_wait: u32,
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct IdleTactics {
	pub placed_morphs: Vec<PlaceMorph>,
	pub actions: Vec<ITAction>,
	pub warp_relay_ai: Vec<WarpRelayAi>,
	pub nydus_placement_ai: Vec<NydusPlacementAi>,
	pub harass_ai: Vec<HarassAi>,
	pub hunter_killer_ai: Vec<HunterKillerFlock>,
	//
	pub harass_ai_transports: Vec<HarassAiTransport>,
	pub kite_settings: Vec<KiteSetting>,
	//setups for toolbox
	pub toolbox_setup: Vec<UnitSetup>,
	pub splash_factors: FxHashMap<u32,f64>,
	pub critter_routes: Vec<Route>,
}

impl IdleTactics {
	pub fn in_reaver_micro(&mut self, unit: Unit)->bool{
		for u in self.harass_ai.iter() {
			if u.unit==unit {
				return true;
			}
		}
		return false;
	}
	pub fn refresh_pairs(&mut self){
	//must be replaced from &mut x to x.iter()
		'har: for h in &mut self.harass_ai {
			if h.pair==None {
				for t in &mut self.harass_ai_transports {
					if t.pair==None{
						h.pair = Some(t.unit);
						t.pair = Some(h.unit);
						continue 'har;
					}
				}
			}
		}
		'tr: for t in &mut self.harass_ai_transports {
			if t.pair==None {
				for h in &mut self.harass_ai {
					if h.pair==None {
						h.pair = Some(t.unit);
						t.pair = Some(h.unit);
						continue 'tr;						
					}
				}
			}
		}
	}
	pub fn unit_removed(&mut self, unit: Unit){
		for k in &mut self.kite_settings {
			k.kite_state.retain(|&k,_| k.0 == unit);
		}
		for r in &mut self.critter_routes {
			if let Some(current) = r.current_unit {
				if current==unit {
					r.current_unit = None;
					r.state = 0;
				}
			}
		}
		for h in &mut self.harass_ai {
			if let Some(pair) = h.pair {
				if pair.0==unit.0 {
					h.pair = None;
					h.state = HarassState::Default;
				}
			}
		}
		for h in &mut self.harass_ai_transports {
			if let Some(pair) = h.pair {
				if pair.0==unit.0 {
					h.pair = None;
					h.state = TransportState::Default;
				}
			}
		}	
		self.harass_ai.swap_retain(|x| x.unit.0 != unit.0);
		self.harass_ai_transports.swap_retain(|x| x.unit.0 != unit.0);

	}
	pub fn try_add_relayai(&mut self, ai: WarpRelayAi){
		if !self.warp_relay_ai.iter().any(|i| i == &ai) {
            let insert_pos = self
                .warp_relay_ai
                .binary_search_by_key(&ai.seek_range, |x| x.seek_range)
                .unwrap_or_else(|e| e);
            self.warp_relay_ai.insert(insert_pos, ai);
        }
	}
	pub fn try_remove_relayai(&mut self, ai: &WarpRelayAi) {	
        if let Some(pos) = self.warp_relay_ai.iter().position(|i| i == ai) {//status don't counts
            self.warp_relay_ai.remove(pos);
        }
    }
	pub fn try_add_nydusai(&mut self, ai: NydusPlacementAi){
		if !self.nydus_placement_ai.iter().any(|i| i == &ai) {
            let insert_pos = self
                .nydus_placement_ai
                .binary_search_by_key(&ai.player, |x| x.player)
                .unwrap_or_else(|e| e);
            self.nydus_placement_ai.insert(insert_pos, ai);
        }
	}
	pub fn try_remove_nydusai(&mut self, ai: &NydusPlacementAi) {	
        if let Some(pos) = self.nydus_placement_ai.iter().position(|i| i == ai) {
            self.nydus_placement_ai.remove(pos);
        }
    }	
	
    pub fn try_add_placemorph(&mut self, morph: PlaceMorph) {
		let mut value = morph.clone();
		value.status = Some(PlaceMorphStatus{delay: morph.max_delay, stage: PlaceMorphStage::Init});
        fn area(x: &bw::Rect) -> u32 {
            (x.right.saturating_sub(x.left) as u32) * (x.bottom.saturating_sub(x.top) as u32)
        }
        if !self.placed_morphs.iter().any(|i| i == &value) {
            let area_size = area(&value.area_place);
            let insert_pos = self
                .placed_morphs
                .binary_search_by_key(&area_size, |x| area(&x.area_place))
                .unwrap_or_else(|e| e);
            self.placed_morphs.insert(insert_pos, value)
        }
    }
	pub fn try_add_action(&mut self, action: ITAction){
	   if !self.actions.iter().any(|i| i == &action) {
            self.actions.push(action)
        }
	}
	pub fn try_remove_action(&mut self, action: ITAction){
		self.actions.swap_retain(|x| x!=&action);
	}

    pub fn try_remove_placemorph(&mut self, value: &PlaceMorph) {	
        if let Some(pos) = self.placed_morphs.iter().position(|i| i.player == value.player &&
			i.area_gather==value.area_gather && i.area_place==value.area_place && i.unit_morpher==value.unit_morpher &&
			i.morph_to==value.morph_to && i.number==value.number && i.max_delay==value.max_delay) {//status don't counts
            self.placed_morphs.remove(pos);
        }
    }
}


unsafe fn parse_upg_num(upgrade: &str)->u32 {
	if upgrade.parse::<u32>().is_ok() {
		//debug!("upg num unwrap");
		return upgrade.parse().unwrap();
	}
	0
}
pub unsafe fn parse_upg_id(upgrade: &str)->u32{
	let (data, len) = match samase::read_file("samase\\unitdef.txt") {
		Some(s) => s,
		None => { bw_print!("No unitdef found!"); 
				  return parse_upg_num(upgrade);},
	};
	let slice = std::slice::from_raw_parts(data, len);
	for line in slice.split(|&x| x == b'\n') 
	{
		let line = String::from_utf8_lossy(line);
		let line = line.trim();
		let mut comment = line.starts_with("#");
		if comment {
			if line.starts_with("#upgrade"){
				comment=false;
			}
		}	
		if comment || line.starts_with(";") || line.starts_with("@") || line.is_empty() {
			continue;
		}
		if !(line.starts_with("upgrade") || line.starts_with("#upgrade")){
			continue;
		}
		let cmd: Vec<&str> = line.split('=').collect();
		if cmd.len()==2 {
			let header = cmd[0];
			let header_break: Vec<&str> = header.trim().split(' ').collect();	
			let value: u32 = parse_upg_num(cmd[1].trim());
			if header_break.len()==2 {
				//debug!("Upgrade from unitdef: {}",header_break[1]);
				if header_break[1] == upgrade {
					//bw_print!("Match found! {}",upgrade);
					return value;
				}
			}
			else {
				//bw_print!("size of unitdef upgrade line is incorrect"); 
			}
		}
	}
	//bw_print!("Upgrade ids found: {}",header_debug);
	parse_upg_num(upgrade)
}





fn read_word(text: String)->Option<(String,String)>{
	/*let mut list: Vec<&str> = text.split(|c| c == '(' || c == ')').collect();
	let function = list[0];*/
	#[derive(Debug, Clone, PartialEq)]
	enum WordReadAct {
		ReadFunction,
		ReadCore,
	}
	//bw_print!("Input: {}",text);
	//bw_print!("Read word");
	let mut string = Vec::new();
	let mut braces = 0;
	let mut word_read = WordReadAct::ReadFunction;
	let mut function =  String::new();
	let mut check = false;
	for c in text.chars(){
		match c {
			'(' => {
				braces+=1;
				if word_read==WordReadAct::ReadFunction {
					word_read = WordReadAct::ReadCore;
					//bw_print!("Read core");
					function = string.iter().cloned().collect();
					string.clear();
				}
				else {
					if !check {
						string.push(' ');
					}
					else {
						string.push(c);
					}
				}
			},
			')' => {
				braces-=1;
				/*
				if braces == 0 {
					let rest: String = string.iter().cloned().collect();
					return Some((function,rest));
				}*/
				string.push(' ');
				if !check {
					string.push(' ');
				}
				else {
					string.push(c);
				}
			},
			_=>{
				string.push(c);
			},
		}
		if word_read==WordReadAct::ReadCore && braces==0 {
			check=true;
		}
	}
	if function.len()==0{
		return None;
	}
	let rest: String = string.iter().cloned().collect();
	//bw_print!("Output: {}",rest);
	return Some((function,rest));
}
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum OrderFunction {
	None,
	IfSelf,
	IfInRange,
}

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub struct IdleTacticParser {
	pub func: OrderFunction,
	//
	pub players: PlayerMatch,
	pub units: UnitMatch,
	pub count: u32,
	pub modifier: ExtModifierType,
	
	pub range: u32,
	//
	pub unit_op: UnitOp,
	pub value: u32,
	pub ext_modifier: ExtModifierType,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum Operation {
	None,
	And,
	Or,
}

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub struct IdleTacticParsingData {
	pub parsing_list: Vec<(IdleTacticParser,Operation)>,
}

impl IdleTacticParser {
	fn new()->IdleTacticParser {
		IdleTacticParser {
			players: Default::default(),
			units: Default::default(),
			count: 0,
			modifier: ExtModifierType::Exactly,
			ext_modifier: ExtModifierType::Exactly,
			range: 0,
			value: 0,
			unit_op: UnitOp::None,
			func: OrderFunction::None,
		}
	}
}

pub unsafe fn parse_players(player: u8, current_player: u8)->PlayerMatch{
	let mut result = PlayerMatch {
            players: [false; 12],
	};
	let game = Game::get();
	match player {
		0..=11 => { result.players[player as usize]=true; },
		13 => { result.players[current_player as usize]=true; },
		14 | 15 => {
			let allies = player == 15;
			let players = (0..12)
				.filter(|&x| x != current_player)
				.filter(|&x| game.allied(current_player, x) == allies);
			for player in players {
				result.players[player as usize] = true;
			}		
		},
		17 => result.players = [true; 12],
		18 | 19 | 20 | 21 => {
			let force = 1 + player - 18;
			for player in (0..8).filter(|&x| (*game.0).player_forces[x as usize] == force) {
				result.players[player as usize] = true;
			}
		}
		x => {
			bw_print!("Unsupported player: {:x}", x);
		}		
	}
	result
}

fn parse_read_modifier(line: String)->ReadModifierType{
	let result = match line.to_lowercase().trim() {
		"atleast"=>ReadModifierType::AtLeast,
		"atmost"=>ReadModifierType::AtMost,
		"exactly"=>ReadModifierType::Exactly,
		"greater"|"greaterthan"=>ReadModifierType::Greater,
		"less"|"lessthan"=>ReadModifierType::Less,
		_=>{
			bw_print!("Unknown modifier: {}",line);
			return ReadModifierType::Exactly;
		},
	};
	result
}

//#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
#[derive(Debug, Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]//buildq
pub enum ExtModifierType {
	Exactly,GreaterThan,LessThan,GreaterThanPercent,LessThanPercent,AtLeast,AtMost,
}

fn parse_ext_modifier(line: String)->ExtModifierType{
	let result = match line.to_lowercase().trim() {
		"greaterthan"=>ExtModifierType::GreaterThan,
		"lessthan"=>ExtModifierType::LessThan,
		"greaterthanpercent"=>ExtModifierType::GreaterThanPercent,
		"lessthanpercent"=>ExtModifierType::LessThanPercent,	
		"exactly"=>ExtModifierType::Exactly,
		"atleast"=>ExtModifierType::AtLeast,
		"atmost"=>	ExtModifierType::AtMost,
		_=>{
			bw_print!("Unknown extmodifier: {}",line);
			return ExtModifierType::Exactly;
		},
	};
	result
}
pub unsafe fn parse_unit_match(line: String)->UnitMatch{
	let unit_list: Vec<&str> = line.split('|').collect();
	let mut units = Vec::new();
	if unit_list.len()>0 {	
		for u in unit_list {
			match u.as_ref(){
			"Group_Men"|"Group_Buildings"|"Group_Factories"|"Group_Zerg"|"Group_Terran"|"Group_Protoss"|"Any"=>{
				let flag = match u.as_ref() {
					"Group_Zerg"=>0x1,
					"Group_Terran"=>0x2,
					"Group_Protoss"=>0x4,
					"Group_Men"=>0x8,
					"Group_Buildings"=>0x10,
					"Group_Factories"=>0x20,
					_=>0xffff,
				};		
				for i in 0..228 {
					if UnitId(i).group_flags() & flag != 0 {
						units.push(UnitId(i));
					}
				}
			}
			_=> {
				units.push(config::parse_unit_id(u));
			},
			}
		}
		return UnitMatch{units: units};
	}
	UnitMatch{units: vec![UnitId(0xe4)]}
}


unsafe fn parse_if_in_range(text: String, player: u8) -> Option<(IdleTacticParser,String)> {
	let mut parser = IdleTacticParser::new();
	parser.func = OrderFunction::IfInRange;
	let mut list: Vec<&str> = text.split(' ').filter(|x| x.len()>0).collect();
	//bw_print!("In range: {:?}",list);
	//IfInRange(14 AtLeast 3 zergling 64)
	//IfInRange 14 AtLeast 3 zergling 64 Hp GreaterThanPercent 
	//Hp(GreaterThanPercent 30))
	
	if list.len()>=5 {
		//parsing stuff
		let mut arguments = 5;
		parser.players = parse_players(list[0].trim().parse().unwrap(),player);
		
		parser.modifier = parse_ext_modifier(list[1].to_string());
		parser.count = list[2].trim().parse().unwrap();
		parser.units = parse_unit_match(list[3].to_string());
		//debug!("range unwrap: {:?}",list);
		parser.range = list[4].trim().parse().unwrap();
		//bw_print!("If range list: {:?}",list);
		if list.len()>5 {
			if vital_check(list[5].to_string()) {
				arguments += 3;
				parser.unit_op = parse_unit_op(list[5].trim().to_string());
				
				parser.ext_modifier = parse_ext_modifier(list[6].trim().to_string());
				//debug!("parse 7 value unwrap");
				parser.value = list[7].trim().parse().unwrap();			
			}
		}
		list.drain(0..arguments);
	}
	else {
		panic!("idle_tactics, Incorrect parsing data: {}",text);
	}
	//bw_print!("After drain: {:?}",list);
	Some((parser,list.join(" ")))
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum UnitOp {
	None,
	Hp,Shields,Energy,Health,Hangar,Cargo,
	//new
	ConduitLinks,
	//town-specific
	WorkerMineralWealth,
}

unsafe fn parse_unit_op(op: String)->UnitOp {
	match op.trim() {
		"Hp"=>UnitOp::Hp,	
		"Shields"=>UnitOp::Shields,
		"Energy"=>UnitOp::Energy,	
		"Health"=>UnitOp::Health,
		"Hangar"=>UnitOp::Hangar,	
		"Cargo"=>UnitOp::Cargo,
		"ConduitLinks"=>UnitOp::ConduitLinks,
		"MineralWealth"=>UnitOp::WorkerMineralWealth,
		_=>UnitOp::None,		
	}
}

unsafe fn vital_check(v: String)->bool{
	match v.as_ref() {
		"Hp"|"Shields"|"Energy"|"Health"|"Hangar"|"Cargo"|"ConduitLinks"|"MineralWealth"=>{
			return true;	
		},
		_=>false,
	}
}
unsafe fn parse_if_self(text: String) -> Option<(IdleTacticParser,String)> {
	let mut parser = IdleTacticParser::new();
	//bw_print!("Text before: {}",text);
	let mut list: Vec<&str> = text.split(' ').filter(|x| x.len()>0).collect();
	parser.func = OrderFunction::IfSelf;
	//bw_print!("Self list: {:?}",list);
//custom_script(idle_tactics action SafetyOff IfInRange(14 AtLeast 3 zergling 64)|IfSelf(Hp(GreaterThanPercent 30)))	
	//bw_print!("If Self");
	//this function has ambiguous number of arguments
	if list.len()>=1 {
		let mut arguments = 1;
		if vital_check(list[0].to_string()) {
			arguments = 3;
			parser.unit_op = parse_unit_op(list[0].trim().to_string());
			parser.ext_modifier = parse_ext_modifier(list[1].trim().to_string());
			//debug!("vital check unwrap");
			parser.value = list[2].trim().parse().unwrap();			
		}
		
		//

		//
		//bw_print!("Arg: {}",arguments);
		//bw_print!("Hp: Before drain: {:?}",list);
		list.drain(0..arguments);
	}
	//bw_print!("Hp: After drain: {:?}",list);
	if list.len()>0{
		list[0] = list[0].trim();
	}
	Some((parser,list.join(" ")))
}

fn read_token(text: String)->Option<(char, String)>{
	if text.len()==0 {
		return None;
	}
	//debug!("parse nth unwrap");
	let token = text.chars().nth(0).unwrap();
	let rest = &text[1..].trim();
	match token {
		'|' | '&' => return Some((token,rest.to_string())),
		_=>(),
	}
	None
}				
unsafe fn parse_function_set(mut text: String, player: u8) -> Result<IdleTacticParsingData, Error> {
	//let mut list: Vec<&str> = text.split(|c| c == '(' || c == ')').filter(|x| x.len()>0).collect();
	//bw_print!("List: {:?}",list);
	/*
	ifinrange
	14 AtLeast 3 zergling|vorvaling|broodling|hydralisk|basilisk 64
	|ifself
	hp
	greaterthanpercent 30
*/
	let mut parsing_data = IdleTacticParsingData{ parsing_list: Vec::new() };
	let remove = false;
	let mut op = Operation::None;
	while let Some((function,rest))=read_word(text){
		text = rest.clone();
		let function: &str = &function[..];
		match function {
			"IfInRange" => {
				//if let Some((expr, rest)) = parse_if_in_range(rest){
					//text = &rest;
					//text = rest;
					//add result
				//}
				//debug!("parse ifrange unwrap");
				let (expr,rest) = parse_if_in_range(text,player).unwrap();
				text = rest;
				parsing_data.parsing_list.push((expr,op));
				//push stuff
				
			},
			"IfSelf" => {
				//debug!("parse ifself unwrap");
				let (expr,rest) = parse_if_self(text).unwrap();
				text = rest;
				parsing_data.parsing_list.push((expr,op));
			},
			_=>{
				return Err(Context::new(format!("Incorrect function set opcode {}",function)).into());
			},
		}
	//	bw_print!("Left text: {}",text
		if let Some((token,rest)) = read_token(text){
			//bw_print!("Token found");
			match token {
				'|'=>{
					//bw_print!("Token | found!");
					text = rest;
					op = Operation::Or;
					//bw_print!("Set token |");
					//bw_print!("Text currently: {}",text);
				},
				'&'=>{
					//bw_print!("Token & found!");
					text = rest;		
					op = Operation::And;
					//bw_print!("Set token &");
				},
				_=>{
					break;
				},
			}
		}
		else {
			break;
		}
	}
    Ok(parsing_data)
}

pub unsafe extern fn set_pos_variable(script: *mut bw::AiScript){
    let mut read = ScriptData::new(script);
	let dest = read.read_jump_pos();
    let msg = read.read_string();
	let mut globals = Globals::get("set_pos_variable");
	globals.triggers.set_jmp(String::from_utf8_lossy(msg).to_string(), dest,parse_players(17,(*script).player as u8));
}
pub unsafe extern fn set_var_local(script: *mut bw::AiScript){
    let mut read = ScriptData::new(script);
	let dest = read.read_jump_pos();
    let msg = read.read_string();
	let game = Game::get();
	let players = read.read_player_match(game);
	let mut globals = Globals::get("set_var_local");
	globals.triggers.set_jmp(String::from_utf8_lossy(msg).to_string(), dest,players);
}

pub extern fn parse_modifier_action(modifier: &str)->(ModifierAction,Option<u32>) {
	let st = modifier.to_lowercase();
	//debug!("1: {}",st);
	if st.to_lowercase().starts_with("waitfor"){
		let mut new_str = st.trim()[7..st.len()].to_string();
		//debug!("2: {}",new_str);
		new_str.retain(|x| x!='(' && x!=')');
		//let new_str = new_str.to_string().chars().filter(|c| *c != '(' && *c!=')').collect();
		//bw_print!("parsed for: {}",new_str);
		//debug!("parsed for: {}",new_str);
		return (ModifierAction::Wait, Some(new_str.parse().unwrap()));
	}
	let result = match st.to_lowercase().as_ref() {
		"jump"=>(ModifierAction::Jump,None),
		"call"=>(ModifierAction::Call,None),
		"wait"=>(ModifierAction::Wait,None),
		"multirun"=>(ModifierAction::Multirun,None),
		_=>(ModifierAction::Jump,None),
	};
	result
}

pub fn parse_bool_modifier(modifier: &str, action: ModifierAction) -> BoolModifier {
	let val = match modifier.to_lowercase().as_ref() {
		"true"=>1,
		_=>0,
	};
	BoolModifier {
		action,
		value: val & 1 != 0,
	}
}

fn cs_parse(s: &str)->u32{
	s.trim().parse().unwrap()
}
pub unsafe fn value_to_player_match(value: u8, current_player: u8, game: Game) -> PlayerMatch {
	let mut cont = true;
	let mut result = PlayerMatch {
		players: [false; 12],
	};
	while cont {
		let byte = value;
		cont = byte & 0x80 != 0;
		let player = byte & 0x7f;
		match player {
			x @ 0..=11 => result.players[x as usize] = true,
			13 => result.players[current_player as usize] = true,
			// Foes, allies
			14 | 15 => {
				let allies = player == 15;
				let players = (0..12)
					.filter(|&x| x != current_player)
					.filter(|&x| game.allied(current_player, x) == allies);
				for player in players {
					result.players[player as usize] = true;
				}
			}
			// All players
			17 => result.players = [true; 12],
			// Forces
			18 | 19 | 20 | 21 => {
				// Forces are 1-based
				let force = 1 + player - 18;
				for player in (0..8).filter(|&x| (*game.0).player_forces[x as usize] == force) {
					result.players[player as usize] = true;
				}
			}
			x => {
				bw_print!("Unsupported player: {:x}", x);
			}
		};
	}
	result
}
#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]//buildq
pub struct MultiModifier {
	pub read: Option<ReadModifier>,
	pub modifier_action: (ModifierAction,Option<u32>),
	pub write: Option<WriteModifier>,
	pub is_write: bool,
}
pub extern fn parse_write_modifier(modifier: &str)-> Option<WriteModifier> {
	let st = modifier.to_lowercase();
	let result = match st.as_ref() {
		"set"=>WriteModifier::Set,
		"add"=>WriteModifier::Add,
		"sub"|"subtract"=>WriteModifier::Subtract,
		"randomize"=>WriteModifier::Randomize,
		_=>return None,
	};
	return Some(result);
}

pub extern fn parse_multimodifier(modifier: &str)->MultiModifier{
	let mut multimodifier = MultiModifier{ read: None, modifier_action: (ModifierAction::Jump,None), write: None, is_write: false };
	if modifier=="0"{
		return multimodifier;
	}
	let modifiers_list: Vec<&str> = modifier.split('|').collect();
	match modifier.as_ref() {
		"set"|"add"|"sub"|"subtract"|"randomize"=>{
			multimodifier.write = parse_write_modifier(modifier);
			multimodifier.is_write = true;
			return multimodifier;
		},
		_=>{			
			for m in modifiers_list {
				let mut get_modifier = match m.as_ref() {
					"wait"|"jump"|"call"|"multirun"=>{						
						true
					},
					_=>false,
				};	
				if !get_modifier && m.starts_with("waitfor") {
					get_modifier = true;
				}
				if get_modifier {
					multimodifier.modifier_action = parse_modifier_action(m); 					
				}
				match m {
					"atleast"|"atmost"|"exactly"|"greater"|"less"|"greaterthan"|"lessthan"=>{
						multimodifier.read = Some(ReadModifier{ty: parse_read_modifier(m.to_string()), action: multimodifier.modifier_action.0});
					},
					_=>{},
				}
			}			
		},
	}
	return multimodifier;
}


pub enum ParseStatus {
	ReadFlag,
	ReadContent,
}

pub enum ParseHeader 
{
	Def,
	Resources,
	Tile,
	IfReq,
	Own,
	IfLayout,
	AnyLayout,
	IfNeeded,
	Type,
	Set,
	Add,
	Subtract,
	Max,
}

unsafe fn parse_buildqueue_header(text: &str) -> Option<ParseHeader>{
	let status = match text.to_lowercase().as_ref() {
		"resources"=>ParseHeader::Resources,
		"tile"=>ParseHeader::Tile,
		"ifreq"=>ParseHeader::IfReq,
		"own"=>ParseHeader::Own,
		"iflayout"=>ParseHeader::IfLayout,
		"anylayout"=>ParseHeader::AnyLayout,
		"ifneeded"=>ParseHeader::IfNeeded,
		"type"=>ParseHeader::Type,
		"add"=>ParseHeader::Add,
		"set"=>ParseHeader::Set,
		"subtract"=>ParseHeader::Subtract,
		"max"=>ParseHeader::Max,
		_=>{
			return None;
		},
	}; 
	Some(status)
}


#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]//buildq
pub enum SpendingType {
	Request,
	Queue
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]//buildq
pub enum ActionType {
	Add,
	Subtract,
	Set,
	Max,
}

#[derive(Debug, Default, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BuildQueueFlagStatus {
	pub status: bool,
	pub creep: bool,//for tiles
	pub power: bool,//for tiles
	pub layouts: Vec<BaseLayout>,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct BuildQueueFlags {
	pub resources: Vec<(PlayerMatch,ExtModifierType,u32,build_queue::ResType)>,//player-dependent
	pub tiles: Vec<(String,BQTileFlags,BoolModifier)>,//player and worker (!)-dependent
	pub ifreqs: Vec<(BoolModifier,Option<UnitId>)>,//player-dependent 
	pub owns: Vec<(PlayerMatch,ExtModifierType,u32,UnitMatch,LocalModifier,aiscript::Position)>,//player-dependent
	pub any_layouts: Vec<String>,//player-dependent
	pub if_layouts: Vec<BoolModifier>,
	pub ifneeded: Vec<((u32,UnitId))>,//player-dependent
	pub spending_type: SpendingType,
	pub action_type: ActionType,
}

impl BuildQueueFlags {
	fn new()->BuildQueueFlags {
		BuildQueueFlags {
			resources: Vec::new(),
			tiles: Vec::new(),
			ifreqs: Vec::new(),
			owns: Vec::new(),
			if_layouts: Vec::new(),
			any_layouts: Vec::new(),
			ifneeded: Vec::new(),
			spending_type: SpendingType::Queue,
			action_type: ActionType::Add,
		}
	}
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]//buildq
pub enum BQTileFlags {
	Creep,
	Power,
}

unsafe fn parse_buildqueueflags(text: String,script: *mut bw::AiScript) -> Result<BuildQueueFlags, Error> {

				/*
%1quantity %2unitId [%3flag] [%4priority]
Queues %1quantity of %2unitid [at %4priority] [comparing %3flags].

%3flags can be:

Resources(%AplayerId %Boperator %CresourceQuantity %DresourceType)
Tile(%AlayoutId [from new_layout] %Boperator [wait or 0] %Cflag [creep or power] %Dboolean)
IfReq(%Aoperator %Bboolean [%CunitId]) | if %CunitId is present, check that unit's tech requirements instead of %2unitId's requirements
Own(%Aplayerid %Boperator %Cquantity %Dunitid [%EtownCheck] [local or global] [%Farea]) | compare %Aplayerid %Cquantity of %Dunitid using %Boperator, taking %EtownCheck and %Farea into account
Layout(%AlayoutId [from new_layout]) | can be combined with | to take multiple identifiers
IfNeeded | like player_need
Type(%Atype [request or queue])
Add | adds %1quantity to existing queues/requests, like add_build
Subtract | subtract %1quantity
Max
ex.
build_new 1 evolution_chamber 0 90
queue one evolution chamber at priority 90 (no flags)

build_new 1 evolution_chamber Own(13 AtLeast|Wait 10 drone)|Own(13 AtLeast|Wait 2 hatchery)|IfReq(Wait True)|Tile(ev1|ev2|ev3 Wait Creep True)|Type(Request)
wait to request one evolution chamber until the player owns 10 drones, 2 hatcheries, and the evolution chamber's tech requirements, and has creep tiles in coordinates matching layouts ev1, ev2, or ev3

build_new 3 evolution_chamber Tile(ev1|ev2|ev3 0 Creep True)|Layout(ev1|ev2|ev3)
queue three evolution chambers at coordinates matching layouts ev1, ev2, and ev3, if those declarations have creep tiles

				*/
				
				/*
build_queue 1 evolution_chamber 0 90
build_queue 1 evolution_chamber WaitOwn(10 drone)|WaitOwn(2 hatchery)|WaitTech|WaitTile(Creep True)|Request
build_queue 3 evolution_chamber WaitTile(Creep True)|MatchLayout(50|49|48)

				*/

	let mut parse_status = ParseStatus::ReadFlag;
	let mut flags = BuildQueueFlags::new();
	if text=="0" {
		return Ok(flags);
	}
	let mut line = String::new();
	let mut header = ParseHeader::Def;
	for c in text.chars() {
		match parse_status {
			ParseStatus::ReadFlag=>{
				if let Some(cmp) = parse_buildqueue_header(&line){
					parse_status = ParseStatus::ReadContent;
					header = cmp;
				}
				else {
					line.push(c);
				}
			},
			ParseStatus::ReadContent=>{
				match c {
					'('=>{
						line.clear();
						parse_status = ParseStatus::ReadFlag;
					},
					')'=>{
						let flag: Vec<&str> = line.trim().split(' ').collect();	
						match header {
							ParseHeader::Resources=>{
								//Resources(%AplayerId %Boperator %CresourceQuantity %DresourceType)
								let player_id = parse_players(flag[0].to_string().parse().unwrap(),(*script).player as u8);
								let operator = parse_ext_modifier(flag[1].trim().to_lowercase());					
								let quantity = flag[2].to_string().parse().unwrap();
								let resource_type = build_queue::parse_resource(flag[3].trim());
								flags.resources.push((player_id,operator,quantity,resource_type));
							},
							ParseHeader::Tile=>{							
								//Tile(%AlayoutId [from new_layout] %Boperator [wait or 0] %Cflag [creep or power] %Dboolean)
								let layout_id = flag[0].to_string();
								
								let tile_flag = match flag[1].to_lowercase().as_ref(){
									"power"=>BQTileFlags::Power,
									_=>BQTileFlags::Creep,
								};
								let operator = parse_bool_modifier(&flag[2].trim().to_lowercase(),ModifierAction::Wait);
								flags.tiles.push((layout_id,tile_flag,operator));
							},
							ParseHeader::IfReq=>{
								//IfReq(%Aoperator %Bboolean [%CunitId]) | if %CunitId is present, check that unit's tech requirements instead of %2unitId's requirements
								let operator = parse_bool_modifier(&flag[0].trim().to_lowercase(),ModifierAction::Wait);
								let unit_id = match (flag.len()>1) {
									true=>Some(config::parse_unit_id(flag[1])),
									false=>None,
								};
								flags.ifreqs.push((operator,unit_id));
							},
							ParseHeader::Own=>{
//Own(%Aplayerid %Boperator %Cquantity %Dunitid [%EtownCheck] [local or global] [%Farea]) | compare %Aplayerid %Cquantity of %Dunitid using %Boperator, taking %EtownCheck and %Farea into account				
								let player_id = parse_players(flag[0].to_string().parse().unwrap(),(*script).player as u8);
								let operator = parse_ext_modifier(flag[1].trim().to_lowercase());
								let quantity: u32 = flag[2].to_string().parse().unwrap();
								let unit_id = parse_unit_match(flag[3].to_string());
								let mut local = LocalModifier::Global;
								let mut area = Position::from_rect32(&bw::location(63).area);
								if flag.len()>4 {
									local = match flag[4].trim().to_lowercase().as_ref() {
										"local" => LocalModifier::Local,
										_ => LocalModifier::Global,
									};
									if flag.len()>5 {
										area = parse_position(flag[5]);
									}
								}
								flags.owns.push((player_id,operator,quantity,unit_id,local,area));
							},
							ParseHeader::AnyLayout=>{
							//Layout(%AlayoutId [from new_layout]) | can be combined with | to take multiple identifiers	
								let line = flag[0];
								let layouts: Vec<&str> = line.split('|').collect();
								//flags.if_layouts.append(&layouts);
								for l in layouts {
									flags.any_layouts.push(l.to_string());
								}
							},
							ParseHeader::IfLayout=>{
								let modifier = parse_bool_modifier(&flag[0].trim().to_lowercase(),ModifierAction::Wait);
								flags.if_layouts.push(modifier);
							},
							ParseHeader::IfNeeded=>{
							//IfNeeded | like player_need
								let quantity: u32 = flag[0].to_string().parse().unwrap();
								let unit_id = config::parse_unit_id(flag[1]);
								flags.ifneeded.push((quantity,unit_id));
							},
							ParseHeader::Type=>{
								let sp_type = match flag[0].to_lowercase().as_ref(){
									"request"=>SpendingType::Request,
									_=>SpendingType::Queue,
								};
								flags.spending_type = sp_type;
							},
							ParseHeader::Set=>{
								flags.action_type = ActionType::Set;
							},
							ParseHeader::Max=>{
								flags.action_type = ActionType::Max;
							},
							ParseHeader::Add=>{
								flags.action_type = ActionType::Add;
							},
							ParseHeader::Subtract=>{
								flags.action_type = ActionType::Subtract;
							},
							_=>{},
						}
						//parse flag contents
					},
					'|'=>{},//ignore
					_=>{
						line.push(c);
					},
				}
			},
		}
	}
	Ok(flags)
}

unsafe fn parse_delimited_position(string: String)->bw::Point{
	let mut string = string.clone();
	string.retain(|x| x!='(' && x!=')');
	let cmd: Vec<&str> = string.split(',').collect();
	if cmd.len()!=2 {
		panic!("Incorrect preparation region: {}",string);
	}
	return bw::Point{x: cmd[0].parse().unwrap(), y: cmd[1].parse().unwrap()};
}

unsafe fn parse_position_set(text: &str)->Vec<aiscript::Position>{
	let mut pos = Position::from_rect32(&bw::location(63).area);
	if text.to_lowercase().starts_with("loc."){
		let mut string = text.to_string();
		string.drain(0..4);
		let loc = string.parse();
		let loc = loc.unwrap();
		let location = bw::location(loc);
		pos = Position::from_rect32(&location.area);
	}
	else if text.parse::<u32>().is_ok(){
		let l =text.parse().unwrap();
		pos = Position::from_rect32(&bw::location(l).area)
	}
	else {
		let mut string = text.to_string();
		string.retain(|x| x!='(' && x!=')' && x!='[' && x!=']');
		let cmd: Vec<&str> = string.split(',').collect();
		if cmd.len()%2 != 0 || cmd.len()==0 {
			panic!("Incorrect string parsing position set {}",text);
		}
		let mut pos_list = Vec::new();
		for i in 0..cmd.len()/2 {
			let first = i*2;
			let second = (i*2)+1;
			let x = cmd[first].trim().parse().unwrap();
			let y = cmd[second].trim().parse().unwrap();
			pos_list.push(Position::from_point(x,y));
//			bw_print!("Add to pos list: {} {}",x,y);
		}
	
		return pos_list;
	}
	vec![pos]
}

pub unsafe fn parse_position(text: &str)->aiscript::Position{
	let mut pos = Position::from_rect32(&bw::location(63).area);
	if text.to_lowercase().starts_with("loc."){
		let mut loc = text.to_string();
		loc.drain(0..4);
		let l = loc.parse().unwrap();
		pos = Position::from_rect32(&bw::location(l).area);
	}
	else if text.parse::<u32>().is_ok(){
		let l =text.parse().unwrap();
		pos = Position::from_rect32(&bw::location(l).area)
	}
	else {
		//bw_print!("Parse Delimited: {}",text);
		let center = parse_delimited_position(text.to_string());
		pos = Position::from_point(center.x,center.y);
		//bw_print!("Result: {:?}",pos.center);
	}
	pos
}


bitflags::bitflags! {
    pub struct AttackCustomFlags: u32 {
        const POSITION = 0x1;
		const MAINTOWN = 0x2;
		const WEAKEST_TOWN = 0x4;
		const STRONGEST_TOWN = 0x8;
		const NEAREST_TOWN = 0x10;
		const FARTHEST_TOWN = 0x20;
		const LEAST_RESOURCE_TOWN = 0x40;
		const MOST_RESOURCE_TOWN = 0x80;		
    }
}

pub enum ScoreType {
	Min,
	Max,
}
pub fn get_score_type(flags: AttackCustomFlags)->ScoreType{
	let stype = match flags {
		AttackCustomFlags::WEAKEST_TOWN|AttackCustomFlags::NEAREST_TOWN|AttackCustomFlags::LEAST_RESOURCE_TOWN=>{	
			ScoreType::Min
		},
		_=>ScoreType::Max,
	};
	stype
}

pub unsafe fn adjust_score(flags: AttackCustomFlags, score: u32, max: u32)->u32{
	let result = match get_score_type(flags){
		ScoreType::Min=>{
			max.saturating_sub(score)
		},
		_=>score,
	};
	result
}

pub unsafe fn calculate_attackcustom_score(game: Game, search: &UnitSearch, town: *mut bw::AiTown, 
									main_pos: bw::Point, flags: AttackCustomFlags)->u32{
	
	let mut score = 0;
	let mut units = FxHashSet::default();
	for ai_reg in ListIter((*town).buildings).map(|x| bw::get_ai_region((*x).parent)){
		if ai_reg==null_mut(){
			continue;
		}
		let pathing = bw::pathing();
		let region = &(*pathing).regions[(*ai_reg).id as usize];
		match flags {
			AttackCustomFlags::WEAKEST_TOWN|AttackCustomFlags::STRONGEST_TOWN=>{//Strength/score	
				
				let mut region_sum = search
					.search_iter(&region.area)
					.filter(|x| !units.contains(&HashableUnit(*x)))
					.filter(|x| x.player()==(*ai_reg).player && 
								x.has_way_of_attacking())
					.collect::<Vec<_>>();
				//bw_print!("weakest/strongest, sum: {}, area: {:?}, ai reg player {}",region_sum.len(),&region.area,(*ai_reg).player);
				for u in region_sum {
					score += u.id().build_score();
					units.insert(HashableUnit(u));
				}
			},			
			_=>(),
		}
	}
	match flags {
		AttackCustomFlags::NEAREST_TOWN|AttackCustomFlags::FARTHEST_TOWN=>{
			let distance = (bw::distance((*town).position,main_pos) as f64)/4.0;
			score = distance as u32;
		},
		AttackCustomFlags::LEAST_RESOURCE_TOWN|AttackCustomFlags::MOST_RESOURCE_TOWN=>{
			let resarea = (*town).resource_area;
			let resareas = &bw::resource_areas;
			score = (((resareas.areas[resarea as usize].total_mineral_count+
					resareas.areas[resarea as usize].total_gas_count) as f64)/5.0) as u32;
		},
		_=>(),
	}
	//bw_print!("Result: {}, units in town {}",score,units.len());
	0
}

//ids,relative ids,index

pub unsafe fn attack_add_relative(script: *mut bw::AiScript, game: Game, globals: &mut Globals, ids: UnitMatch, 
							relative_ids: UnitMatch, relative_index: f64){
	let mut count = 0;
	for rel_id in relative_ids.units {
		count += ((game.safe_unit_completed_count((*script).player as u8, rel_id) as f64)*relative_index) as u32;
	}
	for id in ids.units {
		let unit_id = globals.unit_replace.replace_check(id);
		aiscript::add_to_attack_force(globals, (*script).player as u8, unit_id, count);
	}
}

pub unsafe extern fn custom_script(script: *mut bw::AiScript) {
	let old_pos = (*script).pos - 1;
    let mut read = ScriptData::new(script);
    let msg = read.read_string();
	let msg = String::from_utf8_lossy(msg);
	//let mut debug=false;
	//bw_print!("CustomScript Start {}",msg);
	debug!("Custom script {}, position {}",msg,(*script).pos);
	let mut cmd: Vec<&str> = msg.split(' ').collect();
	let mut globals = Globals::get("custom_script");
	let search = UnitSearch::from_bw();
	if cmd.len() >= 2 {
		let header = cmd[0];	
		
		match header {
			"idle_tactics" => {
				debug!("idle tactics: {:?}",cmd);
				let arg_len = cmd.len()-2;
				let args = match cmd[1] {
					"place_morphs" => 5,
					"kite_micro" => 2,//arg1 - unit id, arg2 - frequency, [arg3 - remove check]
					"harass_add" => 3,
					"harass_add_transport" => 3,
					"warp_relay_ai" => 4, 
					"nydus_placement_ai" =>2,
				//	"recall_ai" =>0, //define later
				//	"drop_priority" => 3,
				//	"cloak_energy" => 2,
				//	"cloak_range" => 2,
				//	"safe_nuke" => 3,
				//	"exit_range" => 3,
					"action" => 4,
					_=>0,
				};
				
				if arg_len >= args {
					match cmd[1] {
						"harass_add" => {
							let mut count = cmd[2].trim().parse().unwrap();
							let unit_id = config::parse_unit_id(cmd[3].trim());
							let rate = cmd[4].parse().unwrap();					
							let max: u32 = match arg_len {
								3=>10,
								_=>cmd[5].parse().unwrap(),
							};
							//let saturate = count+
							let current_count = globals.idle_tactics.harass_ai.iter().filter(|x| x.player==(*script).player as u8 && x.unit.id()==unit_id).count();
							let free = max.saturating_sub(current_count as u32);
							count = cmp::min(count,free);
							if count==0 {
								return;
							}													
							let targets = search
								.search_iter(&aiscript::anywhere_rect())
								.filter(|u| u.player()==(*script).player as u8 &&  u.id()==unit_id && u.order().0!=0 && (*u.0).sprite!=null_mut() &&
											u.is_completed() && !u.is_disabled())
								.take(count as usize)
								.collect::<Vec<_>>();							
							for target in targets {
								let harass_ai = HarassAi {
									player: (*script).player as u8,
									unit: target,
									delay: rate,
									max_delay: rate,
									pair: None,
									state: HarassState::Default,
								};
								let cargo_amount = target.cargo_count();
								if cargo_amount == 0 {
									globals.idle_tactics.harass_ai.push(harass_ai);
									bw::remove_unit_ai(target.0, 0);								
								}
							}
						},
						"harass_add_transport" => {
							let mut count = cmd[2].trim().parse().unwrap();
							let unit_id = config::parse_unit_id(cmd[3].trim());
							let rate = cmp::max(1,cmd[4].parse().unwrap());
							let max: u32 = match arg_len {
								3=>10,
								_=>cmd[5].parse().unwrap(),
							};
							//let saturate = count+
							let current_count = globals.idle_tactics.harass_ai_transports.iter().filter(|x| x.player==(*script).player as u8																								 && x.unit.id()==unit_id).count();
							let free = max.saturating_sub(current_count as u32);
							count = cmp::min(count,free);
							if count==0 {
								return;
							}
							let targets = search
								.search_iter(&aiscript::anywhere_rect())
								.filter(|u| u.player()==(*script).player as u8 && u.id()==unit_id && u.order().0!=0 && (*u.0).sprite!=null_mut() &&
											u.is_completed() && !u.is_disabled())
								.take(count as usize)
								.collect::<Vec<_>>();							
							//bw_print!("Harass transports add targets: {}",targets.len());
							for target in targets {
								let harass_ai = HarassAiTransport {
									player: (*script).player as u8,
									unit: target,
									delay: rate,
									max_delay: rate,
									pair: None,
									designated_target: bw::Point{x: 0, y: 0},
									state: TransportState::Default,
									attack_wait: 0,
								};
								globals.idle_tactics.harass_ai_transports.push(harass_ai);
								bw::remove_unit_ai(target.0, 0);
							}							
						},						
						"nydus_placement_ai" => {
							let delay = cmd[2].trim().parse().unwrap();
							let nydus_ai = NydusPlacementAi {
								player: (*script).player as u8,
								delay: delay,
								max_delay: delay,
							};
							
							match cmd[3] {
								"Remove" => globals.idle_tactics.try_remove_nydusai(&nydus_ai),
								_=> globals.idle_tactics.try_add_nydusai(nydus_ai),
							}
						},
						"warp_relay_ai" => {
							let conduit_seek_range = cmd[2].trim().parse().unwrap();
							let target_production = parse_unit_match(cmd[3].trim().to_string());
							let delay = cmd[4].trim().parse().unwrap();
							//bw_print!("warp relay ai: {} {:?} {}",conduit_seek_range,target_production,cmd[5]);
							let relay_ai = WarpRelayAi {
								player: (*script).player as u8,
								seek_range: conduit_seek_range,
								targets: target_production,
								delay: delay,
								max_delay: delay,
							};			
							match cmd[5] {
								"Remove" => globals.idle_tactics.try_remove_relayai(&relay_ai),
								_=> globals.idle_tactics.try_add_relayai(relay_ai),
							}
						},
						"action"|"action_remove" => {
							//let string = cmd[3].trim();
							let remove = "action_remove"==cmd[1];
							let mut temp = Vec::new();
							let mut wait = 24;
							for i in 4..cmd.len() {
								if cmd[i].to_lowercase().starts_with("rate"){
									let mut new_str = cmd[i].trim()[4..cmd[i].len()].to_string();
									new_str.retain(|x| x!='(' && x!=')');
									wait = new_str.parse().unwrap();
								}
								else {
									temp.push(cmd[i]);
								}
							}
							let string = temp.join(" ");
							let action = match cmd[2].trim(){
								"SafetyOff" => Action::SafetyOff,
								"SafetyOn" => Action::SafetyOn,
								"ActivateMedstims" => Action::ActivateMedstims,
								"DeactivateMedstims" => Action::DeactivateMedstims,
								"ApertureOn"=>Action::ApertureOn,
								"ApertureOff"=>Action::ApertureOff,
								"KhaydarinControlOn"=>Action::KhaydarinControlOn,
								"KhaydarinControlOff"=>Action::KhaydarinControlOff,
								"PhaseLinkOn"=>Action::PhaseLinkOn,
								"PhaseLinkOff"=>Action::PhaseLinkOff,
								
								//
								"ReverseThrustOn"=>Action::ReverseThrustOn,
								"ReverseThrustOff"=>Action::ReverseThrustOff,
								"SublimeShepherdOn"=>Action::SublimeShepherdOn,
								"SublimeShepherdOff"=>Action::SublimeShepherdOff,	
								"TempoChangeOn"=>Action::TempoChangeOn,
								"TempoChangeOff"=>Action::TempoChangeOff,
								"DisruptionWebOn"=>Action::DisruptionWebOn,
								"DisruptionWebOff"=>Action::DisruptionWebOff,
								//
								"TurbomodeOn"=>Action::TurbomodeOn,
								"TurbomodeOff"=>Action::TurbomodeOff,
								"ExpelWasteOn"=>Action::ExpelWasteOn,
								"ExpelWasteOff"=>Action::ExpelWasteOff,
								
								"AltitudeUp"=>Action::AltitudeUp,
								"AltitudeDown"=>Action::AltitudeDown,
								"MissilesOn"=>Action::MissilesOn,
								"MissilesOff"=>Action::MissilesOff,
								"BasiliskOn"=>Action::BasiliskOn,
								"BasiliskOff"=>Action::BasiliskOff,
								"AggressiveMiningOn"=>Action::AggressiveMiningOn,
								"AggressiveMiningOff"=>Action::AggressiveMiningOff,
								"CustomStim"=>Action::CustomStim,
								"PlanetCracker"=>Action::PlanetCracker,
								"Observance"=>Action::Observance,
								
								_=> {
									bw_print!("Unknown action {}",cmd[2]);
									return;}
							};
							
							let unit_match = parse_unit_match(cmd[3].to_string());
							
							let result = parse_function_set(string,(*script).player as u8);
							let data = match result {
								Ok(o)=>o,
								Err(e)=>{
									bw_print!("Incorrect action parsing: {}",e);
									return;
								},
							};
							let action_data = ITAction { action_data: data, action: action, unit_match: unit_match, 
								player: (*script).player as u8, wait: wait, max_wait: wait };
							if remove {
								globals.idle_tactics.try_remove_action(action_data);
							}
							else {
								globals.idle_tactics.try_add_action(action_data);
							}
						},
						"kite_micro"=>{
							let unit_id = parse_unit_match(cmd[2].trim().to_string());
							let mut limit = cmd[3].trim().parse().unwrap();
							if limit == 0 {
								limit = 255;
							}
							//bw_print!("Unit id {:?} limit {}",unit_id,limit); 
/*							if cmd.len()>=4 && cmd[4].trim().to_lowercase()=="remove"{
								globals.idle_tactics.kite_settings.swap_retain(|x| x.kiting_units != unit_id && 
																			x.player!=(*script).player as u8 && x.limit!=limit);
							}
							else {*/		
								//bw_print!("Add kite setting");
								globals
								.idle_tactics
								.kite_settings.push(KiteSetting{kiting_units: unit_id, limit: limit, player: (*script).player as u8,
													kite_state: FxHashMap::default()});
							//}
							
						},
						"place_morph_auto"=>{
							let unit_morpher = config::parse_unit_id(cmd[2].trim());
							let morph_to = config::parse_unit_id(cmd[3].trim());
							let number: u32 = cmd[4].trim().parse().unwrap();
							//let mut area_gather = parse_position(cmd[5].trim());
							let mut area_gather = Position::from_point((*script).center.x as i16,(*script).center.y as i16);
							area_gather.area.left = area_gather.area.left-512;
							area_gather.area.right = area_gather.area.right+512;
							area_gather.area.top = area_gather.area.top-512;
							area_gather.area.bottom = area_gather.area.bottom+512;
							//bw_print!("Try auto");
							let pathing = bw::pathing();
							let region_count = (*pathing).region_count;							
							for i in 0..number {
								let ai_regions = bw::ai_regions((*script).player);
								'rloop: for j in 1..region_count {
									let region = ai_regions.add(j as usize);//ai region
									if (*region).flags & 0x1 != 0 || (*region).flags & 0x40 != 0 {//has town
										
										let pathing = bw::pathing();
										let area = (*pathing).regions[(*region).id as usize].area;
										let mut can = true;
										let area_center = bw::point32_from_rect(area);
										if bw::distance32(area_center,(*script).center)>512 {
											can = false;
											//bw_print!("Can't, Distance: {}", bw::distance32(bw::point32_from_rect(area),(*script).center));
										}
										else {
											//bw_print!("Flag found, can place morph");
										}
										if can {
											for i in globals.idle_tactics.placed_morphs.iter(){
												if i.area_place == area {
													continue 'rloop;
												}
											}
											let place_morph = PlaceMorph {
												player: (*script).player as u8,
												area_gather: area_gather.area,
												area_place: area,
												unit_morpher,
												morph_to,
												number: 1,
												max_delay: 500,
												status: None,
											};			
											//bw_print!("Add placemorph auto");
											globals.idle_tactics.try_add_placemorph(place_morph);
										}
									}
								}
							}
						},
						"place_morphs" => {	
							//let area_gather = bw::location(cmd[2].trim().parse().unwrap());
							//let area_place = bw::location(cmd[3].trim().parse().unwrap());	
							let mut area_gather = parse_position(cmd[2].trim());
							let mut area_place = parse_position(cmd[3].trim());
							if area_place.area.right-area_place.area.left <= 2  {
								area_place.area.left = area_place.area.left-32;
								area_place.area.right = area_place.area.right+32;
								area_place.area.top = area_place.area.top-32;
								area_place.area.bottom = area_place.area.bottom+32;
							}
							let unit_morpher = config::parse_unit_id(cmd[4].trim());
							let morph_to = config::parse_unit_id(cmd[5].trim());
							//debug!("placemorph unwrap");
							let number: u32 = cmd[6].trim().parse().unwrap();
							/*bw_print!("Place morphs, {} {} u {} {}, n {}",area_gather.area.left,
							
							area_place.area.left,unit_morpher.0,morph_to.0,number);		*/			
							let mut max_delay = 500;
							if arg_len==6 {
								debug!("maxdelay unwrap");
								max_delay = cmd[7].trim().parse().unwrap();
								
							}
							let place_morph = PlaceMorph {
								player: (*script).player as u8,
//								area_gather: Position::from_rect32(&area_gather.area).area,
//								area_place: Position::from_rect32(&area_place.area).area,
								area_gather: area_gather.area,
								area_place: area_place.area,
								unit_morpher,
								morph_to,
								number,
								max_delay,
								status: None,
							};			
							match cmd[6] {
								"Remove" => globals.idle_tactics.try_remove_placemorph(&place_morph),
								_=> globals.idle_tactics.try_add_placemorph(place_morph),
							}
						},
						_=>(),
					}
				}		
			},
			"comparison" => {			
				let arg_len = cmd.len()-2;				
				let args = match cmd[1] {
					"player_combat" => 7,
					"unit_check" => 8,
					_=>0,
				};
				if arg_len >= args {
					match cmd[1] {
					"unit_check"=>{
					/*
					%1playerId1 %2playerId2 %3area %4operator %5unit1Offset %6unit2Offset %7unitId1 %8unitId2 [%9block] [%10player]
		using %4operator, compare (%1playerId1's %7unitId1 counts + %5unit1Offset) in %3area to (%2playerId2's %8unitId2 counts + %6unit2Offset) in %3area				
					*/
						let game = Game::get();
						let player_1 = value_to_player_match(cs_parse(cmd[2]) as u8,(*script).player as u8,game);
						let player_2 = value_to_player_match(cs_parse(cmd[3]) as u8,(*script).player as u8,game);
						let area = parse_position(cmd[4]);
						let operator = parse_multimodifier(&cmd[5].trim().to_lowercase());
						let offset_1: i32 = cmd[6].parse().unwrap();
						let offset_2: i32 = cmd[7].parse().unwrap();
						let unit_id_1 = parse_unit_match(cmd[8].trim().to_string());
						let unit_id_2 = parse_unit_match(cmd[9].trim().to_string());
						/*
						unit_check 13 13 63 AtLeast|Wait 1 0 engineer tactical_center
						*/
						//9 is block, 10 is player
						let last_arg = match operator.modifier_action.0!=ModifierAction::Wait {
							true=>11,
							_=>10,
						};
						let player_string = match (arg_len>=last_arg){
							true=>value_to_player_match(cs_parse(cmd[last_arg]) as u8,(*script).player as u8,game),
							false=>player_1,
						};
						let search = aiscript::aiscript_unit_search(game);
						let mut count1 = search
							.search_iter(&area.area)
							.filter(|u| player_1.matches(u.player()) && unit_id_1.matches(u))
							.count() as i32;
						let mut count2 = search
							.search_iter(&area.area)
							.filter(|u| player_2.matches(u.player()) && unit_id_2.matches(u))
							.count() as i32;
						//bw_print!("Compare {}+{} to {}+{}",count1,offset_1,count2,offset_2);
						count1 += offset_1;
						count2 += offset_2;
						
						//bw_print!("CMP: {} {}, {:?}",count1,count2,operator);
						if operator.is_write {
							bw_print!("unit_check does not allow write modifier");
							return;
						}
						else {
							let (jump_type, wait_for) = operator.modifier_action;
							let block = match jump_type {
								ModifierAction::Wait=>{ 0 },
								_=>{ 
									if let Some(pos) = globals.triggers.get_jump_pos(cmd[10].trim().to_string(),player_string){
										pos
									}
									else {
										bw_print!("Incorrect string/player comparison: {}",msg);
										return;
									}
								},
							};				
							if let Some(wait) = wait_for {
								(*Script::ptr_from_bw(script)).waitfor_action(wait);
								if (*Script::ptr_from_bw(script)).waitfor_check(){
									return;
								}
							}	
							operator.read.unwrap().compare_and_act_ext(count1,count2, script, block, old_pos, &mut globals, jump_type);
						}
						
					},					
					"player_combat"=>{
							//let player_1 = cs_parse(cmd[2]);
							//let player_2 = cs_parse(cmd[3]);
							let game=Game::get();
							let player_1 = value_to_player_match(cs_parse(cmd[2]) as u8,(*script).player as u8,game);
							let player_2 = value_to_player_match(cs_parse(cmd[3]) as u8,(*script).player as u8,game);							
							let area = bw::location(cs_parse(cmd[4]) as u8).area;
							let mut always_skip = false;	
							let (jump_type, wait_for) = parse_modifier_action(cmd[6].trim());
							if let Some(wait) = wait_for {
								//bw_print!("WFA");
								(*Script::ptr_from_bw(script)).waitfor_action(wait);
								if (*Script::ptr_from_bw(script)).waitfor_check(){
									//bw_print!("always do");
									always_skip = true;
								}
							}	
							let modifier = parse_bool_modifier(cmd[5].trim(), jump_type);							
							let unit_id = parse_unit_match(cmd[7].trim().to_string());
							let unit_id2 = parse_unit_match(cmd[8].trim().to_string());
							let last_arg = match modifier.action!=ModifierAction::Wait {
								true=>10,
								_=>9,
							};
							let player_string = match (arg_len>=last_arg){
								true=>cmd[last_arg].trim().parse().unwrap(),
								false=>13,
							};
							let block = match modifier.action {
								ModifierAction::Wait=>{ Some(0) },
								_=>globals.triggers.get_jump_pos(cmd[9].trim().to_string(), parse_players(player_string as u8,(*script).player as u8)),
							};			
							if block==None {
								panic!("Panic: Tried to unwrap comparison player_combat subcommand with incorrect block {}",cmd[9].trim());
							}
							let block = block.unwrap();
							let r_compare = (globals.events.combat_detected(player_1,player_2,
											area,unit_id.clone(),unit_id2.clone())) == modifier.value;
							if r_compare == modifier.action.get_read_req() && !always_skip {
								modifier.action.do_action_ext(script, block, old_pos, &mut globals, modifier.action);
							}		
						},
						_=>{},
					}
					
				}
			},
			/*
			script(comparison player_combat %1playerid %2playerid %3area %4operator %5jumpType %6block [%7 unitId])
			*/
			"diplomacy" => {
				let game = Game::get();
				let player_1: u32 = cmd[1].parse().unwrap();
				let player_2: u32 = cmd[2].parse().unwrap();
				let game=Game::get();
				let mut always_skip = false;	
				let (jump_type, wait_for) = parse_modifier_action(cmd[3].trim());
				if let Some(wait) = wait_for {
					(*Script::ptr_from_bw(script)).waitfor_action(wait);
					if (*Script::ptr_from_bw(script)).waitfor_check(){
						always_skip = true;
					}
				}	
				let modifier = parse_bool_modifier(cmd[4].trim(), jump_type);	
				let last_arg = match modifier.action!=ModifierAction::Wait {
					true=>6,
					_=>5,
				};
				let block = match modifier.action {
					ModifierAction::Wait=>{ Some(0) },
					_=>globals.triggers.get_jump_pos(cmd[5].trim().to_string(), parse_players(13,(*script).player as u8)),
				};			
				if block==None {
					panic!("Panic: Tried to unwrap comparison player_combat subcommand with incorrect block {}",cmd[5].trim());
				}
				let block = block.unwrap();
				let r_compare = globals.ngs.diplomacy[player_1 as usize].alliance_status[player_2 as usize] == modifier.value;				
				if r_compare == modifier.action.get_read_req() && !always_skip {
					modifier.action.do_action_ext(script, block, old_pos, &mut globals, modifier.action);
				}
			},			
			"listening" => {
				let game = Game::get();
				let player_1: u32 = cmd[1].parse().unwrap();
				let player_2: u32 = cmd[2].parse().unwrap();
				let game=Game::get();
				let mut always_skip = false;	
				let (jump_type, wait_for) = parse_modifier_action(cmd[3].trim());
				if let Some(wait) = wait_for {
					(*Script::ptr_from_bw(script)).waitfor_action(wait);
					if (*Script::ptr_from_bw(script)).waitfor_check(){
						always_skip = true;
					}
				}	
				let modifier = parse_bool_modifier(cmd[4].trim(), jump_type);	
				let last_arg = match modifier.action!=ModifierAction::Wait {
					true=>6,
					_=>5,
				};
				let block = match modifier.action {
					ModifierAction::Wait=>{ Some(0) },
					_=>globals.triggers.get_jump_pos(cmd[5].trim().to_string(), parse_players(13,(*script).player as u8)),
				};			
				if block==None {
					panic!("Panic: Tried to unwrap comparison player_combat subcommand with incorrect block {}",cmd[5].trim());
				}
				let block = block.unwrap();
				let r_compare = globals.ngs.diplomacy[player_1 as usize].listening[player_2 as usize] == modifier.value;				
				if r_compare == modifier.action.get_read_req() && !always_skip {
					modifier.action.do_action_ext(script, block, old_pos, &mut globals, modifier.action);
				}
			},
			"upgrade_jump" => {
				let arg_len = cmd.len()-1;
				let game = Game::get();
				let upgrade = parse_upg_id(cmd[1].trim());
				let level = cmd[2].trim().parse().unwrap();
				let operator = parse_multimodifier(&cmd[3].trim().to_lowercase());
				let mut count1 = aiscript::get_new_upgrade_level_upg(game, &mut globals.upgrades, upgrade as u16, 
						(*script).player as u8);
				let mut count2 = level;
				let last_arg = match operator.modifier_action.0!=ModifierAction::Wait {
					true=>5,
					_=>4,
				};
				let player_string = match (arg_len>=last_arg){
					true=>value_to_player_match(cs_parse(cmd[last_arg]) as u8,(*script).player as u8,game),
					false=>parse_players(13,(*script).player as u8),
				};
				if operator.is_write {
					bw_print!("upgrade_jump does not allow write modifier");
					return;
				}
				else {
					let (jump_type, wait_for) = operator.modifier_action;
					let block = match jump_type {
						ModifierAction::Wait=>{ 0 },
						_=>{ 
							if let Some(pos) = globals.triggers.get_jump_pos(cmd[4].trim().to_string(),player_string){
								pos
							}
							else {
								bw_print!("Incorrect string/player comparison: {}",msg);
								return;
							}
						},
					};				
					if let Some(wait) = wait_for {
						(*Script::ptr_from_bw(script)).waitfor_action(wait);
						if (*Script::ptr_from_bw(script)).waitfor_check(){
							return;
						}
					}	
					operator.read.unwrap().compare_and_act_ext(count1 as i32,count2, script, block, old_pos, &mut globals, jump_type);
				}

			},
			"idle_orders" => {
				
			},
			"bring_jump" => {
			
			},
			
/*			"set_upgrade" => {
				let arg_len = cmd.len()-1;
				if arg_len == 2 {
					debug!("set upgrade unwrap");
					let upgrade_id = parse_upg_id(cmd[1].trim());
					let level: u32 = cmd[2].trim().parse().unwrap();
				}
			},*/
			"escalation_jump" => {
				//syntax: %1 %2 %3: upgrade id, unit_id, jump/call/wait
				/*
				let game = Game::get();
				let arg_len = cmd.len()-1;
				if arg_len == 3 || arg_len==4 {
					let config = config::config();
					let upgrade_id = parse_upg_id(cmd[1].trim());
					let unit_id = config::parse_unit_id(cmd[2].trim());	
					let modifier_action = parse_modifier_action(cmd[3].trim());
					if arg_len==4 && modifier_action==ModifierAction::Wait {
						bw_print!("Can't wait to block");
						return;
					}
					let block = match arg_len {
						4=>cmd[4].trim().parse().unwrap(),
						_=>0,
					};
					
					let escalation = match config.escalations.esc_list.clone().iter_mut().find(|x| x.upg_id==upgrade_id as u32){
							Some(_s)=>true,
							None=>false,
					};			
					if escalation {
						let current_escalation_id = globals.ngs.get_escalation_from_upgrade(upgrade_id as u16,(*script).player as u32);
						
						
					}			
				}*/
			},
			"set_escalation" => {
				let game = Game::get();
				let arg_len = cmd.len()-1;
				if arg_len == 2 {
					let config = config::config();
					let upgrade_id = parse_upg_id(cmd[1].trim());
					let unit_id = config::parse_unit_id(cmd[2].trim());
					let escalation = match config.escalations.esc_list.clone().iter_mut().find(|x| x.upg_id==upgrade_id as u32){
						Some(_s)=>true,
						None=>false,
					};
					let old_uid = globals.ngs.get_escalation_from_upgrade(upgrade_id as u16,(*script).player as u32);
					let new_uid = globals::reverse_escalation(old_uid.0);
					if escalation && old_uid.0!=unit_id.0 && new_uid==unit_id.0 {
						aiscript::escalate(game,&mut globals,(*script).player as u8,upgrade_id as u16);
					}
					else {
						bw_print!("set_escalation is incorrect: {:?}, uid: {}, old: {}, new: {}",cmd,unit_id.0,old_uid.0,new_uid);
					}
					
				}				
			},
			"upgrade" => {
				let arg_len = cmd.len()-1;
				if arg_len == 3 {
					let level: u32 = cmd[1].trim().parse().unwrap();
					let upgrade_id = parse_upg_id(cmd[2].trim());
					let priority: u32 =  cmd[3].trim().parse().unwrap();
					let town = Town::from_ptr((*script).town);
					if let Some(town) = town {	
						
						bw::ai_addbuildrequest(1,level,upgrade_id,priority,0,town.0);
					}
				}
			},/*
			"tile_jump" => {
				let arg_len = cmd.len()-1;
				if arg_len == 3 {
					let area = parse_delimited_area(cmd[1].trim());
					let tile_flags = parse_tile_flags(cmd[2].trim());
					/*
					let tile_flag =
						(*bw::tile_flags).offset(i as isize + (map_width * j as u16) as isize);*/
					let setting = parse_multimodifier(&cmd[1].trim().to_lowercase());
					let (jump_type, wait_for) = setting.modifier_action;
							let block = match jump_type {
								ModifierAction::Wait=>{ 0 },
								_=>globals.triggers.get_jump_pos(cmd[4].trim().to_string()).unwrap(),
					};
					//
					//
					//
				}
			},*/
			"attack_custom" => {
				let arg_len = cmd.len()-1;
				if arg_len >= 2 {
						enum PosType {
							Prepare,
							Target
						}
						//bw_print!("Attack Custom...");
						let mut prepare: Option<bw::Point> = None;
						let mut target: Option<bw::Point> = None;
						let game = Game::get();
						let full_scroll_list = vec![
							AttackCustomFlags::WEAKEST_TOWN,AttackCustomFlags::STRONGEST_TOWN,
							AttackCustomFlags::NEAREST_TOWN,AttackCustomFlags::FARTHEST_TOWN,
							AttackCustomFlags::LEAST_RESOURCE_TOWN,AttackCustomFlags::MOST_RESOURCE_TOWN];	
						for pos in 1..=2 {
							let pos_type = match pos {
								1=>PosType::Prepare,
								_=>PosType::Target,
							};
							let list: Vec<&str> = cmd[pos].split('|').collect();
							//list flags
							let mut picked_position = None;
							let mut flags = AttackCustomFlags::empty();
							'inner: for i in list 
							{
								let mut flag = i.to_string().to_lowercase();
								flag.retain(|x| x!='(' && x!=')');
								let args: Vec<&str> = flag.split(',').collect();
								if args[0].starts_with("loc."){
									picked_position = Some(parse_position(args[0]).center);
									continue;
								}

								match args[0].as_ref(){
									"position"=>{
										//must be pushed to list of positions
										picked_position = 
										Some(bw::Point{x: args[1].parse().unwrap(), y: args[2].parse().unwrap()});
									},
									"location"=>{
										//must be pushed to list of position
										let location = bw::location(args[1].parse().unwrap());
										picked_position = Some(aiscript::Position::from_rect32(&location.area).center);
									},
									"main_town"=>{
										//flags
										flags |= AttackCustomFlags::MAINTOWN;
									},
									"weakest_town"=>{
										//flags
										flags |= AttackCustomFlags::WEAKEST_TOWN;
										flags &= !AttackCustomFlags::STRONGEST_TOWN;
									},
									"strongest_town"=>{
										//flags
										flags |= AttackCustomFlags::STRONGEST_TOWN;
										flags &= !AttackCustomFlags::WEAKEST_TOWN;
									},
									"nearest_town"=>{
										//flags
										flags |= AttackCustomFlags::NEAREST_TOWN;
										flags &= !AttackCustomFlags::STRONGEST_TOWN;
									},
									"farthest_town"=>{
										//flags
										flags |= AttackCustomFlags::FARTHEST_TOWN;
										flags &= !AttackCustomFlags::NEAREST_TOWN;
									},
									"most_resource_town"=>{
										flags |= AttackCustomFlags::MOST_RESOURCE_TOWN;
										flags &= !AttackCustomFlags::LEAST_RESOURCE_TOWN;
									},
									"least_resource_town"=>{
										flags |= AttackCustomFlags::LEAST_RESOURCE_TOWN;
										flags &= !AttackCustomFlags::MOST_RESOURCE_TOWN;
									},
									_=>{
										continue;
									},
								}
								
							}
							let town = bw::first_active_ai_town((*script).player as u8);
							let script_player_main_town_pos = (*town).position;
							let mut player_list = Vec::new();
							match pos_type {
								PosType::Prepare=>{
									player_list.push((*script).player);
								},
								PosType::Target=>{
									for i in 0..8 {
										if !game.allied((*script).player as u8,i){
											player_list.push(i as u32);
										}
									}
								
								},
							}
							let mut sorting_data: FxHashMap<u32,Vec<(AttackCustomFlags,u32)>> = FxHashMap::default();
							let mut max_val = 0;							
							for p in player_list.iter() {
								let mut town = bw::first_active_ai_town(*p as u8);
								if town==null_mut(){
									continue;
								}
								if flags.contains(AttackCustomFlags::MAINTOWN){
									match pos_type {
										PosType::Prepare=>{prepare=Some(script_player_main_town_pos);},
										PosType::Target=>{target=Some((*town).position);},
									}
								}
								while town != null_mut() {
									for flag in full_scroll_list.iter(){
										if flags.contains(*flag){
											let mut score = calculate_attackcustom_score(game,&search,town,
																	script_player_main_town_pos,*flag);									
											if score < max_val {
												max_val = score;
											}
											let ptr = town as *mut u32 as u32;
											let elem = match sorting_data.entry(ptr){
											   Entry::Vacant(entry) => entry.insert(Vec::new()),
											   Entry::Occupied(entry) => entry.into_mut(),
											};
											elem.push((*flag,score));
										}
									}
									town = (*town).next;
								}
							}
							let item = sorting_data.iter().max_by_key(|(ptr,v)| v.iter()
												   .max_by_key(|(f,score)| adjust_score(*f,*score,max_val)));
							if let Some(item) = item {
								let town_ptr = *item.0 as *mut u32 as *mut bw::AiTown;
								match pos_type {
									PosType::Prepare=>{
										picked_position=Some((*town_ptr).position);
									
									},
									PosType::Target=>{picked_position=Some((*town_ptr).position);},
								}
							}	
							match pos_type {
								PosType::Prepare=>{prepare=picked_position;},
								PosType::Target=>{target=picked_position;},
							}
						}					
						if arg_len>=3 && cmd[3]=="aiorder" {
							//bw_print!("AiOrder");
							let order_prepare = globals.ngs.diplomacy[(*script).player as usize]
												.order_setting.attack_wave_prepare;
							let order_target = globals.ngs.diplomacy[(*script).player as usize]
												.order_setting.attack_wave_target;
							prepare = order_prepare;
							target = order_target;							
						}
						/*
						if prepare.is_none(){
							prepare = Some(bw::Point{x: (*script).center.x as i16,y: (*script).center.y as i16});
						}*/
						//bw_print!("Condition: {:?} {:?}",prepare,target);
						if prepare.is_none(){
							prepare = Some(bw::Point{x: (*script).center.x as i16,y: (*script).center.y as i16});
						}
						//bw_print!("Condition: {:?} {:?}",prepare,target);
						if target.is_some(){
							//bw_print!("Attack To Pos...");
							aiscript::attack_to_pos((*script).player as u8,prepare.unwrap(),target.unwrap());
						}
						else {
/*							samase::ai_attack_prepare((*script).player,
												(*script).center.x as i16,(*script).center.y as i16,1,0);*/
							//bw_print!("AiAttackPrepare...");
							bw::ai_attack_prepare_func((*script).player,(*script).center.x as u32,(*script).center.y as u32,1,0);
						}
					}
			},
			"function"=>{
				let game = Game::get();
				toolbox::function(script,&mut globals,&search,cmd,old_pos);
			},
			"rectangle"=>{
				let arg_len = cmd.len()-1;
				if arg_len >= 2 {
					let game=Game::get();
					let position = parse_position(cmd[1]);
					let mut always_skip = false;	
					match cmd[2].trim().to_lowercase().as_ref(){
						"add"=>{
							globals.rectangles.push(position.area);
							return;
						},
						"remove"=>{
							globals.rectangles.swap_retain(|x| x!=&position.area);
							return;
						},
						_=>{},
					}
					let mut always_skip = false;	
					let (jump_type, wait_for) = parse_modifier_action(cmd[3].trim());
					if let Some(wait) = wait_for {
						(*Script::ptr_from_bw(script)).waitfor_action(wait);
						if (*Script::ptr_from_bw(script)).waitfor_check(){
							//bw_print!("always do");
							always_skip = true;
						}
					}	
					let modifier = parse_bool_modifier(cmd[2].trim(), jump_type);		
					let last_arg = match modifier.action!=ModifierAction::Wait {
						true=>5,
						_=>4,
					};
					let player_string = match (arg_len>=last_arg){
						true=>cmd[last_arg].trim().parse().unwrap(),
						false=>13,
					};
					let block = match modifier.action {
						ModifierAction::Wait=>{ Some(0) },
						_=>globals.triggers.get_jump_pos(cmd[4].trim().to_string(), 
														parse_players(player_string as u8,(*script).player as u8)),
					};			
					if block==None {
						return;
					}
					let block = block.unwrap();
					let r_compare = globals.rectangles.iter().any(|x| x==&position.area) == modifier.value;				
					if r_compare == modifier.action.get_read_req() && !always_skip {
						modifier.action.do_action_ext(script, block, old_pos, &mut globals, modifier.action);
					}						
					
				}
			},
			"attack_to_choose" => {
				let arg_len = cmd.len()-1;
				if arg_len >=2 {
					cmd.drain(0..1);
					let mut prepare_list = Vec::new();
					let mut target_list = Vec::new();
					let mut paired_style = false;
					let mut atk_from = true;
					let mut end = false;
					for arg in cmd {
						if arg.to_lowercase() == "pair" {
							paired_style=true;
							continue;
						}
						let mut new = arg.clone().to_string();
						if new.len()==0 {
							continue;
						}
						if new.starts_with("["){
							new.drain(0..1);
						}
						if new.ends_with("]"){
							new.drain(new.len()-1..new.len());
							end = true;
						}
						if atk_from {
							prepare_list.push(new);
						}
						else {
							target_list.push(new);
						}
						atk_from = !end;
					}
					if prepare_list.len()>0 && target_list.len()>0 {
						let random_prepare = globals.rng.synced_rand(0..prepare_list.len() as u32);
						let mut random_target = globals.rng.synced_rand(0..target_list.len() as u32);
						if paired_style {
							random_target = random_prepare;
						}
						let prepare = parse_position(&prepare_list[random_prepare as usize]);
						let target = parse_position(&target_list[random_target as usize]);
						
						aiscript::attack_to_pos((*script).player as u8, prepare.center, target.center);
					}
					else {
						bw_print!("Error: Incorrect attack_to_choose subcommand");	
					}
				}
			},
			"attack_add_relative"=>{
				let arg_len = cmd.len()-1;
				let game = Game::get();
				if arg_len == 3 {
					let ids = parse_unit_match(cmd[1].trim().to_string());
					let relative_ids = parse_unit_match(cmd[2].trim().to_string());
					let relative_index = cmd[3].trim().parse::<f64>().unwrap();
					attack_add_relative(script,game,&mut globals,ids,relative_ids,relative_index);
				}
			},
			"reset_listening"=>{
				for i in 0..8 {
						globals.ngs.diplomacy[i as usize].listening_system[(*script).player as usize].generic_timer =
							globals.ngs.diplomacy[i as usize].listening_system[(*script).player as usize].generic_max_const;
				}
			},/*
			"listening_wait"=>{
				let player = (*script).player;
				let arg_len = cmd.len()-1;
				let game = Game::get();
				if arg_len==1 {
					let id: u32 = cmd[1].parse().unwrap();//player id 
					
					
					if globals.ngs.diplomacy[id as usize].listening_system[player as usize].generic_timer > 0 {
						(*script).pos = old_pos;
						(*script).wait = 30;						
					}
				}
			},*/
			"send_transmission"=>{
				//
				//TO BE IMPLEMENTED
				//
				let arg_len = cmd.len()-1;
				//unit_id location wav_id Set/Add/Subtract time [text] {arbitrary}
				if arg_len >= 6 {
					let unit_id = config::parse_unit_id(cmd[1].trim());
					let location = bw::location(cmd[2].trim().parse().unwrap());
					let wav_id: u32 = cmd[3].trim().parse().unwrap();
					let modifier = parse_write_modifier(cmd[4].trim());
					let length: u32 = cmd[5].trim().parse().unwrap();
					let last = cmd[cmd.len()-2];
					let mut list = Vec::new();
					for i in 6..cmd.len() {
						if cmd[i] != last {
							list.push(cmd[i]);
						}
					}
					let transmission_text = list.join("");
					bw_print!("last: {}",last);
					bw_print!("text: {}",transmission_text);
					if transmission_text.starts_with('"') && transmission_text.ends_with('"') {
						//
						//
						//
					}
					//custom_script(send_transmission marine 0 0 Set 5000 "")
				}
			},
			"clear_queues"=>{
				enum QueueType {
					Train,
					Upgrade,
				}
				let arg_len = cmd.len()-1;
				let game = Game::get();
				if arg_len >= 3 {
					let qtype = match cmd[1].trim().to_lowercase().as_ref() {
						"train"=>QueueType::Train,
						"upgrade"=>QueueType::Upgrade,
						_=>{
							bw_print!("Incorrect clear_queues syntax: {}", msg);
							return;
						},
					};
					match qtype {
						QueueType::Train=>{
							let ids = parse_unit_match(cmd[2].trim().to_string());
							let players = value_to_player_match(cs_parse(cmd[3]) as u8,(*script).player as u8,game);
						//	debug!("Compare queue [1]: {}",globals.queues.queue.len());
							globals.queues.clear_units(ids,players);
						//	debug!("Compare queue [2]: {}",globals.queues.queue.len());
						},
						QueueType::Upgrade=>{
							let upgrade_id = parse_upg_id(cmd[2].trim());
							let players = value_to_player_match(cs_parse(cmd[3]) as u8,(*script).player as u8,game);
							globals.queues.clear_upgrades(upgrade_id as u16,players);
						},
						_=>{},
					}
					
				}
			},
			"issue_order"=>{
				let arg_len = cmd.len()-1;
				let game = Game::get();
				let value = cmd[1].trim();
				let orderId = OrderId(cmd[2].trim().parse().unwrap());
				let vartype = cmd[3].trim().to_lowercase();
				let modifier = parse_players(17,(*script).player as u8);
				//issue_order Var 34 point 24 15
				let mut setUnit = false;
				let mut unit_targ_name: String = "".to_string();
				let mut sourceUnit = None;
				if let Some(var) = globals.triggers.get_var(value.to_string(),modifier){
					if var.var_type==VarType::Unit {
						sourceUnit = var.unit;
						if let Some(unit) = var.unit {
							if vartype=="unit" {
								unit_targ_name = cmd[4].trim().to_string();
								setUnit=true;
							}
							else if vartype == "point" {
								let x = cmd[4].trim().parse().unwrap();
								let y = cmd[5].trim().parse().unwrap();
								unit.issue_order_ground(orderId,bw::Point {x,y});
							}
							else {
								bw::issue_order_targ_nothing(unit.0,orderId.0 as u32);
							}
						}
					}
				}
				if setUnit {
					if let Some(targVar) = globals.triggers.get_var(unit_targ_name,modifier){
						if let Some(target_unit) = targVar.unit {
							if let Some(source_unit) = sourceUnit {
								source_unit.issue_order_unit(orderId,target_unit);
							}
						}
					}
				}
			},
			"unitvar"=>{
				let arg_len = cmd.len()-1;
				if arg_len >= 4 {
					let game = Game::get();
					let string = cmd[1].trim();
					let unit_match = parse_unit_match(cmd[2].to_string());
					let mut area = parse_position(cmd[3].trim());
					let players = value_to_player_match(cs_parse(cmd[4]) as u8,(*script).player as u8,game);
					let mut offset = 0;
					if area.area.right-area.area.left <= 2  {
						let extension: i16 = cmd[5].trim().parse().unwrap();
						area.area.left = area.area.left-extension;
						area.area.right = area.area.right+extension;
						area.area.top = area.area.top-extension;
						area.area.bottom = area.area.bottom+extension;
						offset = 1;
					}
					let last_arg = 6+offset;
					let modifier = match (arg_len>=last_arg){
						true=> parse_players(cmd[last_arg].trim().parse().unwrap(),(*script).player as u8),
						_=> parse_players(17,(*script).player as u8),
					};
					let var_pick = search
						.search_iter(&area.area)
						.find(|x| players.matches(x.player()) && unit_match.matches(x));
					if let Some(var_pick) = var_pick {
						bw_print!("Unit found, x{} y{}",var_pick.position().x,var_pick.position().y);
						globals.triggers.set_unit(string.to_string(), var_pick, modifier);
					}
				}
			},
			
			"var"=>{
				let arg_len = cmd.len()-1;
				if arg_len >= 3 {
					let setting = parse_multimodifier(&cmd[1].trim().to_lowercase());
					let string = cmd[2].trim();
					let value = cmd[3].trim();
					let player = (*script).player as u8;
					let last_arg = match (!setting.is_write && setting.modifier_action.0!=ModifierAction::Wait)  {
						true=>5,
						_=>4,
					};
					//debug!("Last arg: {}",last_arg);
					let modifier = match (arg_len>=last_arg){
						true=> parse_players(cmd[last_arg].trim().parse().unwrap(),player),
						_=> parse_players(17,player),
					};
					//
					
					let mut rand = 0;
					if value.parse::<f64>().is_ok(){
						let v = value.parse().unwrap();
						if v > 0 {
							rand = globals.rng.synced_rand(0..v);
						}
					}
					if setting.is_write {
						if value.parse::<f64>().is_ok(){
//							bw_print!("Set signed integer of var {} to {}",string,value);
							globals.triggers.set_signed_integer(string.to_string(), value.parse().unwrap(), 
								setting.write.unwrap(), modifier,rand);
						}
						else {
							match value.as_ref(){
								"true"=>{
									globals.triggers.set_boolean(string.to_string(), true, modifier);
								},
								"false"=>{
									globals.triggers.set_boolean(string.to_string(), false, modifier);
								},
								_=>{
									//variable (?)
									if let Some(var) = globals.triggers.get_var(value.to_string(),modifier){
										if var.var_type==VarType::I32 {
											globals.triggers.set_signed_integer(string.to_string(), 
												var.int32.unwrap(), setting.write.unwrap(), modifier,0);
										}
									}
								},
							}
						}						
					}
					else {
//						bw_print!("Check signed string {} for {}");
						let (jump_type, wait_for) = setting.modifier_action;
						let block = match jump_type {
							ModifierAction::Wait=>{ 0 },
							_=>{ 
								if let Some(pos) = globals.triggers.get_jump_pos(cmd[4].trim().to_string(),modifier){
									pos
								}
								else {
									bw_print!("Incorrect string/player comparison: {}",msg);
									return;
								}
							},
							//one-directional check
						};				
						if let Some(wait) = wait_for {
							(*Script::ptr_from_bw(script)).waitfor_action(wait);
							if (*Script::ptr_from_bw(script)).waitfor_check(){
								return;
							}
						}	
						if let Some(var) = globals.triggers.get_var(string.to_string(),modifier){//unlike other, reads Players status in one direction
							//bw_print!("Checked integer is {}",var.int32.unwrap());
							let convert = globals::TriggerState::to_variable(value.to_string());
							if var.var_type == convert.var_type {
								match convert.var_type {
									VarType::Boolean=>{
										setting.read.unwrap().compare_and_act_ext(var.boolean.unwrap() as i32, convert.boolean.unwrap() as i32,  script, block, old_pos, &mut globals, jump_type);
									},
									VarType::I32=>{
										setting.read.unwrap().compare_and_act_ext(var.int32.unwrap(), convert.int32.unwrap(), script, block, old_pos, &mut globals, jump_type);
									},
									_=>{
										//bw_print!("Unk type");
									},
								}
								
							}
						}
						else {
							bw_print!("Incorrect string/player comparison (read) {}",msg);
						}
						
					}
				}
			},
			"new_layout" => {
				let arg_len = cmd.len()-1;
				if arg_len >= 6 {
					let unit_id = config::parse_unit_id(cmd[1].trim());	
					let layout_modifier = match cmd[2]{
						"Remove"=>1,
						_=>0,
					};
					let pos = parse_position_set(cmd[3]);
					let amount: u8 = cmd[4].parse().unwrap();
					let town_id: u8 = cmd[5].trim().parse().unwrap();
					let priority: u8 = cmd[6].trim().parse().unwrap();
					let unique_id = match (arg_len>=7){
						true=>cmd[7].trim(),
						false=>"",
					};
					let unique_id = unique_id.to_string();
					drop(globals);
					aiscript::add_layout(
						script,
						unit_id,
						layout_modifier,
						pos,
						amount,
						town_id,
						priority,
						unique_id,
					);
				}
			},
			"build_new" => {
				// build_new causes global panics, should not be used until finished
				let arg_len = cmd.len()-1;
				if arg_len >= 2 {
					let quantity: u32 = cmd[1].trim().parse().unwrap();//remove :u32 later
					let unit_id = config::parse_unit_id(cmd[2].trim());
					let priority = match (arg_len >=4){
						false=>80,
						true=>{
							let last_arg = cmd[arg_len as usize].parse::<f64>();
							if last_arg.is_ok(){//priority exist
								last_arg.unwrap() as u32
							}
							else {
								80
							}
						},
					};
					let mut flags = BuildQueueFlags::new();
					if arg_len >= 3 {//flags exist
						let wait = 24;
						let mut list: Vec<&str> = Vec::new();
						for i in 2..arg_len {
							list.push(cmd[i]);
						}
						let string = list.join("");
						let result = parse_buildqueueflags(string,script);
						flags = match result {
							Ok(o)=>o,
							Err(e)=>{
								bw_print!("Incorrect build_queue parsing: {}",e);
								return;
							},
						};
					}
					
					for i in 0..quantity {
						globals.queues.build_queue.push(BuildQueue::new(script,unit_id,flags.clone(),priority as u8));
					}
				}
			},
			"upgrade_queue" => {
				let arg_len = cmd.len()-1;
				if arg_len >= 3 {
					let level: u32 = cmd[1].trim().parse().unwrap();
					let upgrade_id = parse_upg_id(cmd[2].trim());
					let priority: u32 =  cmd[3].trim().parse().unwrap();					
					let mut town = Town::from_ptr((*script).town);
					if arg_len >= 4 {
						if cmd[4].trim().to_lowercase()=="global"{
							town=None;
						}
					}
					
					
					let queue = UpgradeQueue {
						player: (*script).player as u8,
						level: level as u8,
						upgrade_id: upgrade_id as u16,
						town,
						priority: priority as u8,
					};
					globals.queues.add_upgrade(queue);					
					
				}			
			},
			"add_build" => {
				let arg_len = cmd.len()-1;
				if arg_len >= 3 {
					let town_id = cmd[1].trim().parse().unwrap();		
					let unit_morpher = config::parse_unit_id(cmd[2].trim());
					let add: u8 = cmd[3].trim().parse().unwrap();
					let max: u8 = match (arg_len >=4) {
						false=>248,
						true=>cmd[4].parse().unwrap(),
					};
					let priority = match (arg_len >=5) {
						true=>cmd[5].trim().parse().unwrap(),
						false=>80,
					};
					let town = aiscript::town_from_id(script, &mut globals, town_id);
					if let Some(town) = town {
						for i in 0..100 {
							let flags_and_count = (*town.0).town_units[i].flags_and_count;
							if flags_and_count == 0 {
								(*town.0).town_units[i].flags_and_count = add<<3;
								(*town.0).town_units[i].id = unit_morpher.0;
								(*town.0).town_units[i].priority = priority;
								//bw_print!("no found, just add");
								
								break;
							}
							else {
								if flags_and_count & 0x4 == 0 && flags_and_count & 0x2 ==0 {
									//build req
									let mut count = ((flags_and_count & 0xf8)>>3)+add;
									count = cmp::min(max,count);
									if (*town.0).town_units[i].id == unit_morpher.0 {//id match
										//bw_print!("Id match found");
										(*town.0).town_units[i].flags_and_count = (flags_and_count & 0x7) | (count << 3);
										break;
									}
								}
							}
						}					
					}	
				}
			},			
			"queue" => {
				let arg_len = cmd.len()-1;
				if arg_len >= 7 {
					//q u f / t m area priority
					
					let quantity = cmd[1].trim().parse().unwrap();
					let unit_id = config::parse_unit_id(cmd[2].trim());
					let factory_id = parse_unit_match(cmd[3].to_string());
					let town_id = cmd[4].trim().parse().unwrap();
					let modifier = cmd[5];
					
					let location = bw::location(cmd[6].trim().parse().unwrap());
					let position = Position::from_rect32(&location.area);
					let priority = cmd[7].trim().parse().unwrap();
					let modifier = match modifier.trim().to_lowercase().as_ref() {
						"local" => LocalModifier::Local,
						"global" => LocalModifier::Global,
						x => {
							bw_print!("Unsupported modifier in queue: {}", x);
							return;
						}
					};
					aiscript::try_add_queue(script,town_id,quantity,unit_id,factory_id,modifier,position,priority,&mut globals);
				}
			},
			"town_state"=> {
				let arg_len = cmd.len()-1;
				if arg_len >= 3 {
					let town_id = cmd[1].trim().parse().unwrap();
					let bool_modifier = match cmd[3].trim().to_lowercase().as_ref() {
						"true" => true,
						_=>false,
					};
					let town = aiscript::town_from_id(script, &mut globals, town_id);
					if let Some(town) = town {
						match cmd[2].trim().to_lowercase().as_ref(){
							"persist"=>{
									
									globals.town_states.add_persist(town,bool_modifier);
								
							},
							_=>{},
						}					
					}
				}
			},
			"queue_relative" => {
				let arg_len = cmd.len()-1;
				if arg_len >= 7 {
					//queue_relative vorvaling hatchery|lair|hive 1 255 larva Global 63 75
					let unit_ids = parse_unit_match(cmd[1].trim().to_string());
					let relative_ids = parse_unit_match(cmd[2].trim().to_string());	
					let relative_index = cmd[3].trim().parse::<f64>().unwrap();
					let factory_id = parse_unit_match(cmd[4].to_string());
					let town_id = cmd[5].trim().parse().unwrap();
					let modifier = cmd[6].trim();
					let location = bw::location(cmd[7].trim().parse().unwrap());
					let position = Position::from_rect32(&location.area);
					let priority = cmd[8].trim().parse().unwrap();
					
					let modifier = match modifier.trim().to_lowercase().as_ref() {
						"local" => LocalModifier::Local,
						"global" => LocalModifier::Global,
						x => {
							bw_print!("Unsupported modifier in queue: {}", x);
							return;
						}
					};
					
					let mut count = 0;
					let game = Game::get();
					for rel_id in relative_ids.units {
						count += ((game.safe_unit_completed_count((*script).player as u8, rel_id) as f64)*relative_index) as u32;
					}
					for id in unit_ids.units {
						let unit_id = globals.unit_replace.replace_check(id);
						aiscript::try_add_queue(script,town_id,count as u8,unit_id,factory_id.clone(),modifier,position.clone(),priority,&mut globals);
					}	
				}
			},
			"set_special_mineral" => {
				let positions = parse_position_set(cmd[1]);
				let mineral_string = cmd[2].trim();	
				let opcode = match mineral_string.trim().to_lowercase().as_ref() {		
					"meskalloid"=>1,//never placed on map, only created via unit behavior 
									//or set_special_mineral (for cutscenes)
					"connorite"=>2,//rename later
					"tricorvid"=>3,//rename later
					"high_regen"=>4,
					x=>0,
				};
				for pos in positions {
					let targets = search
						.search_iter(&pos.area)
						.filter(|u| u.id().0>=176 && u.id().0<=178)
						.collect::<Vec<_>>();
					for res in targets {
						match opcode {
							1=>{
								globals.ngs.set_meskalloid_sprite((*res.0).sprite);
							},//don't works without overlord config
							_=>(),
						}
						globals.ngs.set_special_mineral((*res.0).sprite,opcode);
						
						//globals.ngs.add_single_effect(res.0, HydraEffect::UpdateMineralAnimation);
					}
				}
			},
			_=>{},
		}	
	}/*
	if debug {
		bw_print!("Pos after2: {}",(*script).pos);
	}*/
	//bw_print!("custom_script_success");
}

/*
#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub struct ITAction {
	pub action_data: IdleTacticParsingData,
	pub action: Action,
	pub unit_match: UnitMatch,
}


pub struct IdleTacticParsingData {
	pub parsing_list: Vec<(IdleTacticParser,Operation)>,
}

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub struct IdleTacticParser {
	pub func: OrderFunction,
	//
	pub players: PlayerMatch,
	pub units: UnitMatch,
	pub count: u32,
	pub modifier: ReadModifierType,
	
	pub range: u32,
	//
	pub unit_op: UnitOp,
	pub value: u32,
	pub ext_modifier: ExtModifierType,
}


	None,
	Hp,Shields,Energy,Health,Hangar,Cargo,
}

*/

pub unsafe fn check_unit_op(
	unit_op: UnitOp,
	ext_modifier: ExtModifierType,
	command_value: u32,
	picked_unit: Unit,
	game: Game,
	ngs: &mut NewGameState)->bool{
	
	
	//bw_print!("Try: {:?}",unit_op);
	let (value,max_value) = match unit_op {
		UnitOp::Hp=>{
			(picked_unit.hitpoints(), picked_unit.id().hitpoints())
		},
		UnitOp::Shields=>{
			if !picked_unit.id().has_shields() {
				return false;
			}
			(picked_unit.shields(), picked_unit.id().shields())		
		},
		UnitOp::Energy=>{
			(picked_unit.energy() as i32, 250 * 256)
		},
		UnitOp::Health=>{
		  (picked_unit.hitpoints().saturating_add(picked_unit.shields()),
			picked_unit.id().hitpoints().saturating_add(picked_unit.id().shields()))	
		},
		UnitOp::Hangar=>{
			use bw_dat::unit::*;
			match picked_unit.id() {
				VULTURE | JIM_RAYNOR_VULTURE => (picked_unit.spider_mines(game) as i32, 3),
				NUCLEAR_SILO => (picked_unit.has_nuke() as i32, 1),
				CARRIER | GANTRITHOR => (picked_unit.hangar_count() as i32, 8),
				REAVER | WARBRINGER => (picked_unit.hangar_count() as i32, 10),
				_ => (0, 0),
			}
		},
		UnitOp::Cargo=>(picked_unit.cargo_count() as i32, picked_unit.id().cargo_space_provided() as i32),
		UnitOp::ConduitLinks=>(ngs.relay_conduit_count(picked_unit) as i32,1),
		UnitOp::WorkerMineralWealth=>{
			let mut wealth = 0;
			if let Some(worker_ai) = picked_unit.worker_ai(){
				let town = (*worker_ai).town;
				let resarea = (*town).resource_area;
				let resareas = &bw::resource_areas;
				wealth = resareas.areas[resarea as usize].total_mineral_count;			
			}
			(wealth as i32,1000000)
		},
		_=>{
			//bw_print!("Unk UnitOpcode {:?}",unit_op);
			return true;
		},
	};
	//bw_print!("Try 2: {}",max_value);
	if max_value==0 {
		//bw_print!("Max val is 0 ret false");
		return false;
	}
	let percent=((value as f64/max_value as f64)*100.0) as u32;
	//bw_print!("comparison {:?} {}~{} {}%",ext_modifier,command_value,value,percent);
	/*
	if unit_op==UnitOp::Energy {
		bw_print!("Energy: {} {} {}%",value,max_value,percent);
	}*/
	let result = match ext_modifier {
		ExtModifierType::GreaterThan=>value>command_value as i32,
		ExtModifierType::LessThan=>value<command_value as i32,
		ExtModifierType::AtLeast=>value>=command_value as i32,
		ExtModifierType::AtMost=>value<=command_value as i32,		
		ExtModifierType::GreaterThanPercent=> percent > command_value as u32,
		ExtModifierType::LessThanPercent=> percent < command_value as u32,
		ExtModifierType::Exactly=>command_value==value as u32,
	};
	//bw_print!("RESULT: {}",result);
	result
}

pub unsafe fn check_modifier(
	modifier: ExtModifierType,
	current: i32,
	required: i32)->bool{
	let result = match modifier {
		ExtModifierType::AtLeast => current>=required,
		ExtModifierType::AtMost => current<=required,
		ExtModifierType::Exactly => current == required,
		ExtModifierType::GreaterThan => current>required,
		ExtModifierType::LessThan => current<required,
		_=>{
			panic!("Illegal modifier {:p}");
		},
		
	};
	result
}


pub unsafe fn action_can_be_performed(ngs: &mut NewGameState, action: Action, unit: Unit)->bool {
	let game=Game::get();
	match action {
		Action::SafetyOff => {
			let status = ngs.get_cyprian_value(unit.0);
			if game.tech_researched(unit.player(),TechId(26)) && status==0 {
				return true;
			}
			
		},
		
		Action::SafetyOn => {
			return true;
		},
		Action::ReverseThrustOn=>{
			if ngs.get_generic_value(unit.0,13)==0 {
				return true;
			}
		},
		Action::ReverseThrustOff=>{
			if ngs.get_generic_value(unit.0,13)==1 {
				return true;
			}
		},
		
		Action::SublimeShepherdOn=>{
			if ngs.get_generic_value(unit.0,14)==0 && unit.energy()>=(bw::energy_cost_old[40] as u16)<<8{
				return true;
			}
		},
		Action::SublimeShepherdOff=>{
			if ngs.get_generic_value(unit.0,14)==0 {
				return true;
			}
		},
		Action::TempoChangeOn=>{
			if ngs.get_generic_value(unit.0,45)==0 {
				return true;
			}
		},
		Action::TempoChangeOff=>{
			if ngs.get_generic_value(unit.0,45)==1 {
				return true;
			}
		},
		Action::DisruptionWebOn=>{
			if ngs.get_generic_value(unit.0,15)==0 && unit.energy()>=(bw::energy_cost_old[25] as u16)<<8 {
				return true;
			}
		},
		Action::DisruptionWebOff=>{
			if ngs.get_generic_value(unit.0,15)==1 {
				return true;
			}
		},
		Action::ActivateMedstims => {
			if ((*unit.0)._dc132 & 0x8) == 0 && (*unit.0).energy as u32 >= bw_dat::tech::RESTORATION.energy_cost()<<8{
				return true;
			}
		},
		Action::DeactivateMedstims=>{
			if (*unit.0)._dc132 & 0x8 != 0 {
				return true;
			}
			
		},
		
		Action::ApertureOn=>{
//			bw_print!("action check: {} {}",(*unit.0).flingy_flags&0x2!=0,ngs.relay_deployed(unit));
			if !((*unit.0).flingy_flags & 0x2 !=0 ) && !ngs.relay_deployed(unit){
				return true;
			}
		},
		Action::ApertureOff=> {
			if ngs.relay_deployed(unit){
				return true;
			}		
		},
		Action::Observance=>{
			if unit.energy()>=(bw::energy_cost_old[39] as u16) << 8{
				return true;
			}		
		},
		Action::PlanetCracker=>{
			if !ngs.is_cracking(unit) && unit.energy()>=(bw::energy_cost_old[37] as u16) << 8{
				return true;
			}
		},
		Action::PhaseLinkOn=>{
			if !ngs.clarion_deployed(unit){
				return true;
			}
		},
		Action::PhaseLinkOff=> {
			if ngs.clarion_deployed(unit){
				return true;
			}		
		},		
		Action::TurbomodeOn=>{
			if ngs.get_generic_value(unit.0,5)==0 {
				return true;
			}
		},
		Action::TurbomodeOff=>{
			if ngs.get_generic_value(unit.0,5)==1 {
				return true;
			}
		},
		
		Action::AltitudeUp=>{
			if ngs.get_generic_value(unit.0,20000)==0 {
				return true;
			}		
		},
		Action::AltitudeDown=>{
			if ngs.get_generic_value(unit.0,20000)==1 {
				return true;
			}			
		},
		Action::MissilesOn=>{
			if ngs.get_generic_value(unit.0,20001)==0 {
				return true;
			}			
		},
		Action::MissilesOff=>{
			if ngs.get_generic_value(unit.0,20001)==1 {
				return true;
			}			
		},
		Action::BasiliskOn=>{
			if ngs.get_generic_value(unit.0,20002)==0 && (*unit.0).order!=155 {
				return true;
			}			
		},
		Action::BasiliskOff=>{
			if ngs.get_generic_value(unit.0,20002)==1 && (*unit.0).order!=155  {
				return true;
			}			
		},
		Action::AggressiveMiningOn=>{
			if ngs.get_generic_value(unit.0,6)==0 {
				return true;
			}			
		},
		Action::AggressiveMiningOff=>{
			if ngs.get_generic_value(unit.0,6)==1  {
				return true;
			}			
		},
		Action::CustomStim=>{
			return true;
		},
		Action::ExpelWasteOn=>{
			if ngs.get_waste_status(unit.0)!=1 {
				return true;
			}
		},
		Action::ExpelWasteOff=>{
			if ngs.get_waste_status(unit.0)==1 {
				return true;
			}
		},		
		Action::KhaydarinControlOn=>{
			if ngs.get_generic_value(unit.0, 3) == 0 {
				return true;
			}
		},
		Action::KhaydarinControlOff=>{
			if ngs.get_generic_value(unit.0, 3) != 0 {
				return true;
			}			
		},		
		
		_=>{
			bw_print!("Unknown action");
		},
	}
	false
}

/*]-keep the reaver positioned between ranged enemies and the shuttle if possible
]-order shuttle to load reaver after it acquires an in-range target
]-order reaver to enter shuttle after it fires a shot
]-do not load if the shuttle is under attack & less than 25% HP and 35% shields
[-do not unload if reaver has no scarabs, and isn't about to finish any (e.g. in the next 3 seconds)
-if loaded and reaver is out of scarabs + shuttle is being attacked, retreat home

]++if at home - train scarabs
++do not pick if < 4 scarabs


#[derive(Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum TransportState {
	Default,
	CanPickEnemy,+
	MustPickReaver,+
	MustFlyToReaver,+
	MustKeepDistance,+
	ReturnHome,+
	UnloadReaver,+
}

*/
pub fn build_scarabs_if_possible(unit: Unit){
	let game = Game::get();		
	use bw_dat::order::*;
	unsafe{
	
	
	if unit.can_train_hangar(game){
		if has_resources(game,unit.player(),&ai::unit_cost(UnitId(85))){
			if unit.secondary_order() != order::TRAIN_FIGHTER {
				
				(*unit.0).currently_building = null_mut();	
				bw_print!("try");
				if bw::prepare_build_unit(85, unit.0)!=0{
					bw_print!("Issue scarab order {:p}",(*unit.0).currently_building);
					unit.issue_secondary_order(order::TRAIN_FIGHTER);
					(*unit.0).build_queue[(*unit.0).current_build_slot as usize] = 85;
					//(*unit.0).secondary_order_wait = 0;
					(*unit.0).secondary_order_state = 0;
				}

				
			}		
			
		}

	
//		unit.issue_secondary_order(order::TRAIN_FIGHTER);
//		(*unit.0).build_queue[(*unit.0).current_build_slot as usize] = 85;
	}/*
	else if unit.secondary_order() == order::TRAIN_FIGHTER 
	{
		unit.issue_secondary_order(order::REAVER_IDLE);
	}
	//}*/
	}
}
fn reaver_inside(reaver: Unit)->bool{
	unsafe {
	
	
	if (*reaver.0).flags & 0x40 != 0 {
		return true;
	}
	if reaver.is_completed() && (*(*reaver.0).sprite).flags & 0x20 != 0 {
		return true;
	}
	false
	
	
//	return (*reaver.0).related != null_mut();
	}
}


fn find_home(shuttle:Unit, search: &UnitSearch)->Option<(bw::Point,u32)>{
	if let Some((unit,distance)) = search.find_nearest(shuttle.position(), |x| {
											x.id().is_factory() && x.player()==shuttle.player()
										}){
		return Some((unit.position(),distance));				
	}
		
	return None;
}

fn reaver_is_taking_severe_damage(reaver: Unit)->bool {
	if reaver.hp_percent()<25 && reaver.shield_percent()<35 {
		return true;
	}
	false
	
	
}

fn reaver_can_fight(reaver: Unit)->bool{

	if reaver.hp_percent()<25 && reaver.shield_percent()<35 {
		return false;
	}
	if reaver.hangar_count()<3 {
		
		return false;
	}
	true
	
}

fn not_loading(shuttle: Unit)->bool {
	if shuttle.order().0==0x5e {
		return false;
	}
	true
}
fn not_unloading(shuttle: Unit)->bool {

	if shuttle.order().0 == 0x6f || shuttle.order().0==0x70 {
		return false;
	}
	true
}

fn load_reaver(shuttle: Unit, reaver: Unit){
	shuttle.issue_order_unit(OrderId(0x5d), reaver);//Pickup1
}

fn unload_shuttle(shuttle: Unit, home: bw::Point){
	shuttle.issue_order_ground(OrderId(0x70), home);
}

fn is_near_enemy(shuttle: Unit, range: u32, game: Game, search: &UnitSearch)->bool {
	let area = bw::Rect {
		left: (shuttle.position().x).saturating_sub(range as i16),
		top: (shuttle.position().y).saturating_sub(range as i16),
		right: (shuttle.position().x) + range as i16,
		bottom: (shuttle.position().y) + range as i16,
	};						
	let count = search
		.search_iter(&area)
		.filter(|u| !game.allied(shuttle.player(),u.player()))
		.count() as u32;
	return count > 0;
}

fn find_harass_micro_targets(shuttle: Unit, game: Game, search: &UnitSearch)->Option<Unit>{
	if let Some((unit,_dist)) = search.find_best_nearest(shuttle.position(), PriorityType::ReaverMicro, |x| {
		!game.allied(shuttle.player(),x.player()) && x.player()!=shuttle.player() && x.player()!=11 && !x.is_air()}){
		//debug!("Picked target: curp{} id{} p{}, pos {} {}, targ {} {}",shuttle.player(),unit.id().0,unit.player(),shuttle.position().x,shuttle.position().y,unit.position().x,unit.position().y);
		//bw_print!("Picked target: sp{} id{} p{}",shuttle.player(),unit.id().0,unit.player());
		return Some(unit);				
	}
	return None;
}
fn reaver_acquired_target(reaver: Unit)->bool {
	unsafe {
		if (*reaver.0).target != null_mut(){
			if bw::distance(reaver.position(),(*(*reaver.0).target).position) < 256 {
				return true;
			}
		}
		false
	}
}
pub fn get_point_on_circle_closest(pos: bw::Point, target: bw::Point, range: u32)->bw::Point{
	let vector_x = pos.x as f64 - target.x as f64;
	let vector_y = pos.y as f64 - target.y as f64;	
	let root = (vector_x.powf(2.0)+vector_y.powf(2.0) as f64).sqrt();
	//bw_print!("vector: {} {}, root {}",vector_x,vector_y,root);
	let div_x = (vector_x/root)*(range as f64);
	let div_y = (vector_y/root)*(range as f64);
	
	let result = bw::Point{x: target.x+div_x as i16, y: target.y as i16+div_y as i16};		
	//bw_print!("result: {} {} [{} {}]",result.x,result.y,target.x,target.y);
	result
}


fn proximity_enemy(shuttle: Unit, game: Game, search: &UnitSearch)->Option<bw::Point>{
	if let Some((target,_dist)) = search.find_nearest(shuttle.position(), |x| {
		x.id().air_weapon().is_some() && !game.allied(shuttle.player(),x.player()) && x.player()!=shuttle.player()}){
	
		let drop = get_point_on_circle_closest(shuttle.position(), target.position(), target.id().air_weapon().unwrap().max_range());
		//bw_print!("Points: dp {} {}, up {} {}",drop.x,drop.y,target.x,target.y);
		return Some(drop);
	}
	return None;
}
fn get_drop_point(reaver: Unit, shuttle: Unit, game: Game, search: &UnitSearch)->Option<bw::Point>{
	if let Some(target) = find_harass_micro_targets(shuttle,game,search){
		let mut area: InfluenceZone = InfluenceZone::new();
		let answer = area.generate(shuttle.position(), target.position(), reaver, target, 128, 32, 
					InfluenceShape::Circle, InfluenceType::ForEnemy, InfluenceFind::FindMaxBestRangeSpotReaver, 
					InfluenceFlags::GROUND_UNIT, search);//improve for kiters 
		return answer;
					
//		return Some(get_point_on_circle_closest(shuttle.position(), target, 128));
	}
	return None;
}
pub unsafe fn idle_tactics_unit_hook(
	idle_tactics: &mut IdleTactics,
    ngs: &mut NewGameState,
    picked_unit: Unit,
    search: &UnitSearch,
	game: Game,
) {
	//reaver-shuttle micro start
	//reaver-shuttle micro end
	toolbox::critter_route_hook(idle_tactics,ngs,picked_unit,search,game);
	for act in &mut idle_tactics.actions {
		act.wait = act.wait.saturating_sub(1);
		if act.wait == 0 {
			act.wait = act.max_wait;
		}
	}
	
	for act in idle_tactics.actions.iter() {
		/*bw_print!("IdleTactics, w: {} {}, p: {} {}, check: {}",act.wait,act.max_wait,act.player,picked_unit.player(),
				action_can_be_performed(ngs,act.action,picked_unit));*/
		
		 if act.wait==act.max_wait && act.unit_match.matches(&picked_unit) && act.player==picked_unit.player() &&
			action_can_be_performed(ngs,act.action,picked_unit)	{	
			//bw_print!("Matches {}",act.action_data.parsing_list.len());
			let mut logic_set = Vec::new();
			let mut bool_test = Vec::new();

			for (parser, op) in act.action_data.parsing_list.iter() {
				match parser.func {
					OrderFunction::IfSelf=>{
						if check_unit_op(parser.unit_op,parser.ext_modifier,parser.value,picked_unit,game,ngs){	
							logic_set.push((true,op.clone()));
							bool_test.push(true);
						}
						else {
							logic_set.push((false,op.clone()));
							bool_test.push(false);
						}
						
					},
					OrderFunction::IfInRange=>{
						//custom_script(idle_tactics action SafetyOff cyprian 
						//IfSelf(Hp(GreaterThanPercent 30))
						//|IfInRange(+14 AtLeast 3 +zergling +64))
						let area = bw::Rect {
							left: (picked_unit.position().x).saturating_sub(parser.range as i16),
							top: (picked_unit.position().y).saturating_sub(parser.range as i16),
							right: (picked_unit.position().x) + parser.range as i16,
							bottom: (picked_unit.position().y) + parser.range as i16,
						};						
						let targets = search
							.search_iter(&area)
							.filter(|u| parser.units.matches(u) && parser.players.matches(u.player())
								&& check_unit_op(parser.unit_op.clone(),parser.ext_modifier.clone(),parser.value,*u,game,ngs))
							.count() as u32;
							/*
						bw_print!("match count: {}, range: {}, area: {:?}",targets, range, area);
						if targets==0 {
							let test = search.search_iter(&area).filter(|u| parser.units.matches(u) && parser.players.matches(u.player())).count() as u32;
							bw_print!("test: {}", test);
						}*/
						if check_modifier(parser.modifier, parser.count as i32, targets as i32){
							logic_set.push((true,op.clone()));
							bool_test.push(true);
						}
						else {
							logic_set.push((false,op.clone()));
							bool_test.push(false);
						}
					},
					_=>{
					},
				}		
			}
			let mut answer = false;
//			bw_print!("Try answer");
			for (result, operation) in logic_set {
				/*
				if act.action.clone()==Action::AggressiveMiningOn {
					bw_print!("Logic set: {} {:?}",result,operation);
				}*/
				match operation {
					Operation::None|Operation::Or=>{
						answer=result;
					},
					Operation::And=>{
						if answer {
							answer=result;
						}
					}
				}
			}
			/*
			if act.action.clone()==Action::ActivateMedstims {
				bw_print!("Medstims: {}  , {:?}",answer,bool_test.clone());
			}*/
		//	bw_print!("Answer: {}",answer);
			if answer {
//				bw_print!("unit match: {:?} {:?}",picked_unit.id().0,act.unit_match);
				match act.action.clone() {
					Action::SafetyOff=>{
						//bw_print!("Safety off!");
						ngs.add_single_effect(picked_unit.0,HydraEffect::SafetyOff);
					},
					Action::SafetyOn=>{
						//bw_print!("Safety on!");
						ngs.add_single_effect(picked_unit.0,HydraEffect::SafetyOn);
					},
					Action::ActivateMedstims=>{
						//bw_print!("Activate medstims");
						ngs.add_single_effect(picked_unit.0,HydraEffect::ActivateMedstims);
					},
					Action::DeactivateMedstims=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::DeactivateMedstims);
					},
					Action::ApertureOn=>{
//						bw_print!("Addeffect aperture on");
						ngs.add_single_effect(picked_unit.0,HydraEffect::ApertureOn);
					},
					Action::ApertureOff=>{
//						bw_print!("Addeffect aperture off");
						ngs.add_single_effect(picked_unit.0,HydraEffect::ApertureOff);
					},
					Action::PhaseLinkOn=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::PhaseLinkOn);
					},
					Action::PhaseLinkOff=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::PhaseLinkOff);
					},
					Action::ExpelWasteOn=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::ExpelWasteOn);
					},
					Action::ExpelWasteOff=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::ExpelWasteOff);
					},
					Action::TurbomodeOn=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::TurbomodeOn);
					},
					Action::TurbomodeOff=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::TurbomodeOff);
					},
					Action::AltitudeUp=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::AltitudeUp);
					},
					Action::AltitudeDown=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::AltitudeDown);
					},
					Action::MissilesOn=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::MissilesOn);
					},
					Action::MissilesOff=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::MissilesOff);
					},
					Action::BasiliskOn=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::BasiliskOn);					
					},
					Action::BasiliskOff=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::BasiliskOff);
					},
					Action::AggressiveMiningOn=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::AggressiveMiningOn);
					},
					Action::AggressiveMiningOff=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::AggressiveMiningOff);
					},
					Action::CustomStim=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::CustomStim);
					},
					Action::KhaydarinControlOn=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::KhaydarinControlOn);
					},
					Action::KhaydarinControlOff=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::KhaydarinControlOff);
					},
					Action::Observance=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::Observance);
					},
					Action::PlanetCracker=>{
						ngs.add_single_effect(picked_unit.0,HydraEffect::ActivatePlanetcracker);
					},
					_=>{},
				}
			}
		 }
	}
}
pub unsafe fn building_in_different_town(unit1: Unit, unit2: Unit) -> bool {
	let b_ai1 = unit1.building_ai();
	let b_ai2 = unit2.building_ai();
	if b_ai1.is_some() && b_ai2.is_some(){
		return (*b_ai1.unwrap()).town != (*b_ai2.unwrap()).town;
	}
	true
}

/*
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct RunningKite {
	pub picked_unit: Option<Unit>,
	pub state: KitingState,
	pub attack_state: KitingAttackState,
	pub delay: u32,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct KiteSetting {
	pub kiting_units: UnitMatch,
	pub limit: u32,
	pub kite_state: Vec<RunningKite>,
}
*/
pub unsafe fn get_kiting_pos(kiter: Unit, target: Unit, search: &UnitSearch)->Option<bw::Point>{
	let flag = match kiter.is_air(){
		true=>InfluenceFlags::AIR_UNIT,
		_=>InfluenceFlags::GROUND_UNIT,
	};
	let mut area: InfluenceZone = InfluenceZone::new();
	let range = cmp::min(256,(kiter.target_range(target) as f64 * 1.5) as u32);
	let answer = area.generate(kiter.position(), target.position(), kiter, target, range, 32, 
		InfluenceShape::Circle, InfluenceType::ForEnemy, InfluenceFind::FindMaxBestRangeSpot, 
		flag, search);//improve for kiters 	
	answer
}
pub unsafe fn kiting_good(kiter: Unit, target: Unit)->bool{
	/*
	// If unit can kite:
	//		if(speed_a > speed_b && range_a>range_b+kiting_time)
	//	kiting time = (acceleration_a+deceleration_a+2*turn speed)	
	*/
//	let factor = 0.744;
	if !target.has_way_of_attacking() {
		return false;
	}
	let factor = 1.0/343.0;
	
	
	let kiter_speed = (*kiter.0).flingy_top_speed as f64;
	let target_speed = (*target.0).flingy_top_speed as f64;
	let mut kiting_time=(((*kiter.0).acceleration as u32*2)+2*(*kiter.0).flingy_turn_speed as u32) as f64;
	kiting_time *= factor;
	/*bw_print!("Kiting check: speed {} {}, range {} {} [{}], kiting time {}",
		(*kiter.0).flingy_top_speed,
		(*target.0).flingy_top_speed,
		kiter.target_range(target),
		target.target_range(kiter),
		target.target_range(kiter)+kiting_time as u32,
		kiting_time);*/
	if kiter_speed > target_speed
		&& kiter.target_range(target)>target.target_range(kiter)+kiting_time as u32 {
		//bw_print!("good");
		return true;
	}
	
	false
}
pub unsafe fn kiting_frame(idle_tactics: &mut IdleTactics, unit_search: &UnitSearch, ngs: &mut NewGameState, game: Game) {
	
	for kiting in &mut idle_tactics.kite_settings {
		//bw_print!("compare kite {} {}",kiting.kite_state.len(),kiting.limit);
		if (kiting.kite_state.len() as u32)<kiting.limit {
			
			let diff = kiting.limit-kiting.kite_state.len() as u32;
			//bw_print!("Diff: {}",diff);
			let mut kiters = unit_search
				.search_iter(&aiscript::anywhere_rect())
				.filter(|x| x.player()==kiting.player && kiting.kiting_units.matches(x) && x.is_completed() && !x.is_disabled()
							&& ((*x.0).flags & 0x40==0) && !x.is_burrowed() && x.has_way_of_attacking())
				.take(diff as usize)
				.collect::<Vec<_>>();
//			bw_print!("Kiters: {}",kiters.len());
			for k in kiters.iter() {
				kiting.kite_state.insert(HashableUnit(*k),RunningKite{delay: 8, state: KitingState::Init, attack_state:
																					  KitingAttackState::Init});
			}
		}
		//bw_print!("Kite state len {}",kiting.kite_state.len());
		for (u,st) in &mut kiting.kite_state {
			if u.0.is_disabled() || u.0.is_burrowed() || (*(u.0).0).flags & 0x40!=0 || !u.0.is_completed(){
				continue;
			}
			match st.state {
			    KitingState::Init=>{
//					bw_print!("S: Init");
					match st.attack_state {
						KitingAttackState::Init=>{
//							bw_print!("A: Init");
							if u.0.has_cooldown() {
								if let Some(target) = u.0.target(){
									if kiting_good(u.0,target){
										let kiting_pos = get_kiting_pos(u.0,target,unit_search);
										//bw_print!("Find pos...");
										if let Some(pos) = kiting_pos {
											//bw_print!("Kite back: {:?}",pos);
											st.state = KitingState::KiteBack;
//											ngs.add_single_effect_point_spec((u.0).0,HydraEffect::AppendOrder,pos,6);
											ngs.add_single_effect_point_spec((u.0).0,HydraEffect::IssueOrder,pos,6);
											//ngs.add_single_effect_unit_spec((u.0).0,HydraEffect::AppendOrder,target.0,8);
											//ngs.add_single_effect_unit_spec((u.0).0,HydraEffect::AppendOrder,target.0,10);
											ngs.add_single_effect_unit_spec((u.0).0,HydraEffect::AppendOrder,target.0,107);
										}
									}
								}
								st.attack_state = KitingAttackState::Cooldown;
							}
						},
						KitingAttackState::Cooldown=> {
							//bw_print!("A: Cooldown");
							if !u.0.has_cooldown() {
								st.attack_state = KitingAttackState::Init;
								ngs.add_single_effect_point_spec((u.0).0,HydraEffect::IssueOrder,(u.0).position(),1);
							}
						},
						_=>(),
					}
				},
				KitingState::KiteBack=>{
					//bw_print!("S: Kiteback");
					match u.0.order().0 {
						10|11=>{
							if u.0.has_cooldown(){
								//bw_print!("Kited");
								st.attack_state = KitingAttackState::Init;
								st.state = KitingState::KiteBack;
							}
						},
						_=>(),
					}
				},
				_=>(),
			}
		}	
	}
	
	
	// algorithm description:
	// Call after attack / fire
	// If Kiting Behavior is On	&& 2 spare orders in order queue
	// If Unit Has Weapon and Can move
	//	Calculate n value (retreat point)
	//	Prepend attack order(unit,current target)
	//	Prepend move order(unit,n)
	//	Do not allow military ai to control unit during the operation w/ timeout delay
	
	// If unit can kite:
	//		if(speed_a > speed_b && range_a>range_b+kiting_time)
	//	kiting time = (acceleration_a+deceleration_a+2*turn speed)
	
	//	If kiting behavior is on:
	//		Calculate n value (retreat point):
	//		Attach influence field to each enemy in sight
	//		field size = enemy range + k + enemy speed*kiting_time
	//		k = 1, can be adjusted
	//
	//		Define influence map:
	//		I(u1,u2,d) = { if d<=field_size = i }
	//					 { if D>field_size = 0 }
	
	//	Influence area setup:
	//		Considered factors: impassable terrain, enemies, moving in group
	//		n(a,b)=?
	//		1. Calculate range = max(range+speed_b*kiting_time,1)
	//		2. Set walls to max value
	//		3. Remove cells outside of range and high unaccessible ground from calculation
	//		4. Set enemy danger value =(calculate by DPS*a+hardcode spell count*b+energy*c+shields*d)
	//			(a,b,c,d are to be defined)
	//		5. Set positions behind the units and behind the walls to (x) to (x+1)
	//		6. set influence map
	//		6.1. set cell value to "value-(distance to u1*0.2)" 
	//
	//		{
	//		7 (OPTIONAL - functionality for group kiting)
	//		1. Before loop, clear kiting list
	//		2. Find all units of same type w/ kiting AI within (range/2)
	//		3. Find their optimal points via influence areas
	//		4. Points must be closer to each other as closer as possible but not the same.
	//		4.1. (Set "good" points to 0, set already picked points to 0.5)
	//		4.2. Spread -0.1 loop from picked points. Do not interfere with already set values
	//		5. Now, add picked points to kiting list
	//		6. Clear kiting list
	//		}
	//
	//		get n value
}
	

	
pub unsafe fn idle_tactics_frame(idle_tactics: &mut IdleTactics, unit_search: &UnitSearch, ngs: &mut NewGameState, game: Game) {
	
	kiting_frame(idle_tactics,unit_search,ngs,game);
	idle_tactics.refresh_pairs();//for reavers
	for reaver in &mut idle_tactics.harass_ai {
		if !reaver_inside(reaver.unit) && reaver.unit.secondary_order() != order::TRAIN_FIGHTER {		
			build_scarabs_if_possible(reaver.unit);			
		}		
	}
	for shuttle in &mut idle_tactics.harass_ai_transports {
		//bw_print!("Shuttle: {} {}, state: {:?}",shuttle.unit.position().x,shuttle.unit.position().y,shuttle.state);
		//bw_print!("Shuttle: {:?}",shuttle.state);
		if shuttle.pair.is_some(){
		//	bw_print!("State w/ reaver: {:?}",shuttle.state);
		}
		else {
			//bw_print!("No reaver");
		}
		/*
		if shuttle.unit.player()==1 {
			bw_print!("Shuttle {} {}, state {:?}",shuttle.unit.position().x,shuttle.unit.position().y,shuttle.state);
		}*/
		
		match shuttle.state {
			TransportState::Default=>{
				//bw_print!("[-> RETURN HOME]");
				shuttle.state = TransportState::ReturnHome;
			},			
			TransportState::CanPickEnemy=>{
				if let Some(reaver) = shuttle.pair {
					if !reaver_inside(reaver){
						if shuttle.attack_wait > 0 {
							shuttle.attack_wait-=1;
							if shuttle.attack_wait == 0 {
								shuttle.unit.issue_order_ground(order::MOVE, reaver.position());
								shuttle.state = TransportState::MustFlyToReaver;
							//	bw_print!("-> MustFlyToReaver");
								continue;
							}
						}
						//bw_print!("Reaver not inside");
						if reaver_acquired_target(reaver){
							//bw_print!("State [MUST FLY TO REAVER]");
							if bw::distance(shuttle.unit.position(), reaver.position())>48 {
								shuttle.unit.issue_order_ground(order::MOVE, reaver.position());
							}
							
							//bw_print!("[-> FLY TO REAVER]");
//							bw_print!("-> MustFlyToReaver");
							shuttle.state = TransportState::MustFlyToReaver;
						}
						else {
							//
							//MUST BE READDED ONCE FIXED
							
							if let Some(proximity_point) = proximity_enemy(shuttle.unit, game, unit_search){//must use is_near_enemy func		
								//bw_print!("Must Fly To Proximity Point");
								shuttle.unit.issue_order_ground(order::MOVE, proximity_point);
							}					
						}

					}
					else {
						//bw_print!("Reaver inside ][");
						if reaver_can_fight(reaver) {
						//
							//bw_print!("--GDP-- ][");
							if let Some(drop_point) = get_drop_point(reaver, shuttle.unit, game, unit_search){
								/*if !aiscript::check_placement(game, unit_search, reaver, drop_point.x/32,drop_point.y/32, reaver.id(), false, drop_point){
									bw_print!("Bad collision");
								}*/
								shuttle.unit.issue_order_ground(OrderId(0x70), drop_point);
								unload_shuttle(shuttle.unit, drop_point);
								shuttle.designated_target = drop_point;
								//bw_print!("State [UNLOAD REAVER] {} {} -> {} {}",shuttle.unit.position().x,shuttle.unit.position().y,drop_point.x,drop_point.y);
								//debug!("State [UNLOAD REAVER] {} {}",drop_point.x,drop_point.y);
								//bw_print!("-> Unload");
								shuttle.state = TransportState::UnloadReaver;
							}
						}					
						else {
							//bw_print!("Reaver can't fight ][");
						}
					}				
				}
				else {
					shuttle.state = TransportState::Default;
				}

			},
			TransportState::UnloadReaver=>{
				/*if not_unloading(shuttle.unit,reaver.unit){
					unload_reaver_at(shuttle.unit, drop_point);
				}*/
				if let Some(reaver) = shuttle.pair {
					if !reaver_inside(reaver){
						//bw_print!("[-> CAN PICK ENEMY]");
						shuttle.attack_wait = 600;
						shuttle.state = TransportState::CanPickEnemy;
						if (*reaver.0).order == 58 && reaver.hangar_count()>0 {
							let target = unit_search.find_nearest(reaver.position(), |x| {
											x.0 != reaver.0 && x.player()!=reaver.player() && !x.is_invincible() && !x.is_air()
									//add relocator later
									//add region check to influence zones later
							});
							if let Some((target,_distance)) = target {
								reaver.issue_order_unit(OrderId(59), target);
							}
						}
						
					}
//					else if not_unloading(shuttle.unit) {
					else if not_unloading(shuttle.unit) {
						//bw_print!("U");
//						unload_shuttle(shuttle.unit, shuttle.designated_target);
//						shuttle.issue_order_ground(OrderId(0x70), shuttle.position());
//004E76C0 = GetUnloadPosition(), arg 1 word *position, eax Unit *transport, esi Unit *unit
						let mut pos = bw::Point{x: 0, y: 0};
						if bw::get_unload_position(&mut pos as *mut bw::Point,shuttle.unit.0,reaver.0)==1 {
						//	bw_print!("TryU");
							unload_shuttle(shuttle.unit, pos);
						}
						else {
							//bw_print!("--> CanPickEnemy");
							shuttle.attack_wait = 600;
							shuttle.state = TransportState::CanPickEnemy;
						}
					}
				}
				else {
					//bw_print!("NO REAVER PAIR");
					shuttle.state = TransportState::Default;
				}
			},
			TransportState::MustPickReaver=>{
				if let Some(reaver) = shuttle.pair {
					if !reaver_inside(reaver){
					//	bw_print!("Reaver Not Inside");
						if not_loading(shuttle.unit) && reaver.secondary_order() != order::TRAIN_FIGHTER {
							load_reaver(shuttle.unit,reaver);
						}
					}
					else {
						//bw_print!("Reaver Inside");
						if reaver_can_fight(reaver) {
							//bw_print!("Reaver can fight");
							if let Some(target) = find_harass_micro_targets(shuttle.unit,game,&unit_search){
								//bw_print!("Move to enemy [STATE: CAN PICK ENEMY]");
								shuttle.unit.issue_order_ground(order::MOVE, target.position());
								//bw_print!("[-> CAN PICK ENEMY]");
								shuttle.attack_wait = 600;
								shuttle.state = TransportState::CanPickEnemy;								
							}
						}		
						else {
						//	bw_print!("Reaver can't fight, go home");
							if is_near_enemy(shuttle.unit, 256, game, unit_search){//must be used in proximity func
								if let Some((home,_dist)) = find_home(shuttle.unit, unit_search){
								//	bw_print!("[STATE: RETURN HOME]");
								//	bw_print!("[-> HOME]");
									shuttle.unit.issue_order_ground(order::MOVE, home);
									shuttle.state = TransportState::ReturnHome;
								}
							}							
						}
					}
				}
				else {
					//bw_print!("No pair");
					shuttle.state = TransportState::Default;
				}
				
			},
			
			TransportState::MustFlyToReaver=>{//probably not required
				if let Some(reaver) = shuttle.pair {
					if (*shuttle.unit.0).target != reaver.0 && (*shuttle.unit.0).move_target_unit != reaver.0 {
						shuttle.unit.issue_order_ground(order::MOVE, reaver.position());
					}					
					if bw::distance(shuttle.unit.position(),reaver.position())<48 && reaver.scarabs_outside()>0{
						if !reaver_is_taking_severe_damage(reaver)  {
							load_reaver(shuttle.unit,reaver);
						//	bw_print!("[-> MUST PICK REAVER [1]");
							shuttle.state = TransportState::MustPickReaver;							
						}
					}
					else if !is_near_enemy(reaver, 128, game, unit_search) && reaver.secondary_order() != order::TRAIN_FIGHTER {
						load_reaver(shuttle.unit,reaver);
					//	bw_print!("[-> MUST PICK REAVER [2]");
						shuttle.state = TransportState::MustPickReaver;						
					}				
				}
				else {
					shuttle.state = TransportState::Default;
				}

			},
			TransportState::ReturnHome=>{
				if let Some((home,distance)) = find_home(shuttle.unit, unit_search){
					//bw_print!("Home found");
					if distance <= 256 {
						//bw_print!("distance good");
						if let Some(reaver) = shuttle.pair {
							//bw_print!("reaver found");
							if !reaver_can_fight(reaver) && reaver_inside(reaver){
								//bw_print!("reaver can't fight");
								if not_unloading(shuttle.unit){
									//bw_print!("shuttle is not unloading, UNLOAD!");
									
									unload_shuttle(shuttle.unit, home);
								}
								else {
									//bw_print!("shuttle is loading(?)");
								}
							}
							else {
								if reaver_inside(reaver) {
								//	bw_print!("reaver inside [STATE: MUST PICK REAVER]");
									//bw_print!("[-> MUST PICK REAVER]");
									shuttle.state = TransportState::MustPickReaver;	
								}
								else {
									//bw_print!("reaver outside");
									if reaver_can_fight(reaver) {
										if not_loading(shuttle.unit) && reaver.secondary_order() != order::TRAIN_FIGHTER  {
											//bw_print!("not loading load reaver");
											load_reaver(shuttle.unit,reaver);
										}
										else {
											//bw_print!("loading (?)");
										}
									}
									else {
									//	bw_print!("++reaver can't fight++ x{} y{}, count {}",reaver.position().x,reaver.position().y,reaver.hangar_count());
									}
								}
								
							}
						}
					}
					else {
						//bw_print!("distance bad");
						/*
						if let Some(reaver) = shuttle.pair {
							if !reaver_inside(reaver){
								if !(shuttle.unit.order() == order::MOVE || shuttle.unit.order()==OrderId(0x5d)) {
									shuttle.unit.issue_order_ground(order::MOVE, home);
								}						
							}
						}*/
						if (!(shuttle.unit.order() == order::MOVE || shuttle.unit.order()==OrderId(0x5d))) || shuttle.pair.is_none(){
							shuttle.unit.issue_order_ground(order::MOVE, home);
						}
					}
				}
				
			},
			_=>{},
		}
	}	
	let anywhere = aiscript::anywhere_rect();
	'outer: for nydus_ai in &mut idle_tactics.nydus_placement_ai {
		if nydus_ai.delay > 0 {
			nydus_ai.delay = nydus_ai.delay.saturating_sub(1);		
		}
		else {
			nydus_ai.delay = nydus_ai.max_delay;
			let nyduses = unit_search
						.search_iter(&anywhere)
						.filter(|x| x.player()==nydus_ai.player && x.can_issue_order(OrderId(46)) && x.id().0==134 && !x.has_nydus_exit() &&
									x.is_completed())
						.collect::<Vec<_>>();
			'inner: for nydus in nyduses {
				if nydus.has_nydus_exit(){
					continue 'inner;
				}
				let target = unit_search
					.search_iter(&anywhere)
					.find(|x| x.player()==nydus_ai.player && x.id().is_building() && building_in_different_town(nydus, *x)
						&& unit_search.search_iter(&bw::Rect{ left: x.position().x-512, top: x.position().y-512,
															right: x.position().x+512, bottom: x.position().y+512})
															.find(|y| y.id()==UnitId(134)).is_none());
				if let Some(target) = target {
//					bw_print!("target found");
					for i in -5..=5 {
						for j in -5..=5 {
//							bw_print!("Try nydus");
							let x = target.position().x+(i*32);
							let y = target.position().y+(j*32);
							let position = bw::Point{x: x, y: y};
							if aiscript::check_placement(game,unit_search,nydus,x/32,y/32,UnitId(134),false,position){
								//bw_print!("Location found");
								nydus.issue_order_ground(OrderId(46), position);                
								(*nydus.0).build_queue[(*nydus.0).current_build_slot as usize] = 134;
								continue 'inner;
							}
						}
					}
				}		
			}
		}	
	}
	for warp_ai in &mut idle_tactics.warp_relay_ai {
		//let mutate = warp_ai.clone();
		if warp_ai.delay > 0 {
			warp_ai.delay = warp_ai.delay.saturating_sub(1);
		}
		else {
			warp_ai.delay = warp_ai.max_delay;
			let area = aiscript::anywhere_rect();
			let relays = unit_search
						.search_iter(&area)
						.filter(|x| x.id().0==29 && x.player()==warp_ai.player)
						.collect::<Vec<_>>();
			for rel in relays {
				let structure = unit_search
					.search_iter(&area)
					.filter(|x| warp_ai.targets.matches(x) && x.id().is_factory() && x.is_completed() && x.player()==warp_ai.player 
							&& bw::distance(rel.position(),x.position())<=warp_ai.seek_range
							&& ngs.if_linked_to_relay(*x)==0 && !ngs.relay_deployed(*x))
					.min_by_key(|x| bw::distance(rel.position(),x.position()));
				if let Some(structure) = structure {
					//bw_print!("Conduit!");
					rel.issue_order_unit(OrderId(154),structure);
				}
			}	
		}
	}

    for morph in &mut idle_tactics.placed_morphs {
		//if let Some
		let mutate = morph.clone();
		//area_place
		let mut area_place = mutate.area_place;
		if let Some(ref mut status) = morph.status {
			match status.stage {
				PlaceMorphStage::Init=>{
					status.stage = PlaceMorphStage::CheckForCompletion;
				},
				PlaceMorphStage::CheckForCompletion=>{
					
					let units = unit_search
						.search_iter(&area_place)
						.filter(|x| x.id() == mutate.morph_to && x.player()==mutate.player)
						.collect::<Vec<_>>();	
					if units.len()<mutate.number as usize {
						status.stage=PlaceMorphStage::WaitForResources1;
					}
					else {
						status.stage=PlaceMorphStage::WaitEnd;
					}
				},
				PlaceMorphStage::WaitForResources1=>{
					if has_resources(game, mutate.player, &ai::unit_cost(mutate.morph_to)) {
						status.stage=PlaceMorphStage::SendMorpher;
					}
					else {
					}
				},
				PlaceMorphStage::SendMorpher=>{
					let unit_source = unit_search.search_iter(&mutate.area_gather).find(|x| 
						x.id() == mutate.unit_morpher && !x.is_burrowed() &&
						x.is_completed() && x.order()!=order::MOVE && x.player()==mutate.player);	
					
					if let Some(unit_source) = unit_source {
						unit_source.issue_order_ground(order::MOVE, bw::point_from_rect(area_place));
						status.stage = PlaceMorphStage::WaitForMorpher;
					}
				},
				PlaceMorphStage::WaitForMorpher=>{
					let unit_source = unit_search.search_iter(&area_place).find(|x| x.id() == mutate.unit_morpher &&
							x.order() != order::MOVE && x.player()==mutate.player);	
							
					match unit_source {
						Some(_s)=>{
							status.stage = PlaceMorphStage::WaitForResources2;
							status.delay = mutate.max_delay;						
						},
						None=>{
							status.delay = status.delay.saturating_sub(1);
							if status.delay == 0 {
								status.delay = mutate.max_delay;
								status.stage = PlaceMorphStage::Init;
							}							
						},
					}		
				},
				PlaceMorphStage::WaitForResources2=>{
					let unit_source = unit_search.search_iter(&area_place).find(|x| x.id() == mutate.unit_morpher &&
							x.order() != order::MOVE && x.player()==mutate.player);	
					if let Some(unit_source) = unit_source {
						if has_resources(game, mutate.player, &ai::unit_cost(mutate.morph_to)) {
							if (*unit_source.0).ai!=null_mut() {
								bw::remove_unit_ai(unit_source.0, 0);//new
							}
							unit_source.issue_order_ground(OrderId(42), bw::point_from_rect(mutate.area_gather));
							(*unit_source.0).build_queue[(*unit_source.0).current_build_slot as usize] = mutate.morph_to.0;
							status.delay = mutate.max_delay;
							status.stage=PlaceMorphStage::WaitEnd;
						}	
						else {
							status.delay = status.delay.saturating_sub(1);
							if status.delay == 0 {
								status.delay = mutate.max_delay;
								status.stage = PlaceMorphStage::Init;
							}					
						}
					}
					else {
						status.stage = PlaceMorphStage::Init;
					}
				},
				PlaceMorphStage::WaitEnd=>{
					status.delay = status.delay.saturating_sub(1);
					if status.delay == 0 {
						status.delay = mutate.max_delay;
						status.stage = PlaceMorphStage::Init;
					}				
				},
				_=>{},
			}
		}
		//i+=1;
	}
}