#![allow(dead_code)]
#![allow(non_snake_case)]
use config::{self, Config};
use globals::{
    Globals,
};
use bw_dat::{UnitId};
use std::ptr::null_mut;
use bw;
use game::Game;
use aiscript;
use unit::{Unit};
use std::ptr;

// 00454310 = UpdateSpeed(), eax Unit *unit, Takes speed upg, ensnare and stim into account -- https://pastebin.com/raw/GGYGWtiB

pub unsafe fn update_speed_hook(
    unit: *mut bw::Unit,
    orig: &dyn Fn(*mut bw::Unit)) {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(unit);
				return;
			},
        };
        let mut flingy = bw::units_dat_flingy[unit.id().0 as usize];//u8
        let mut moveControl = bw::flingy_dat_movement_type[flingy as usize];
        if moveControl >= 2 {
            bw::update_iscript_speed(unit.0);
        }
        else {
            (*unit.0).flingy_top_speed = bw::modify_speed_func(unit.0, bw::flingy_dat_top_speed[flingy as usize]);
            (*unit.0).acceleration = bw::calculate_acceleration_func_first_byte(unit.0) as u16;
            (*unit.0).flingy_turn_speed = bw::calculate_turn_speed_func_first_byte(unit.0) as u8;
        }
    }

// 0x00463A10 unknown flingy hook -- https://pastebin.com/raw/B6ij7NQJ

pub unsafe fn flingy_unk_hook(
    arg_id: u16,
    unit: *mut bw::Unit,
    orig: &dyn Fn(u16, *mut bw::Unit,)->bool)->bool {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(arg_id, unit);
			},
        };
		//ebx/bx - u16, ebp+8 - unit
		let v5 = 0;
		let mut pos_box: i32 = 0;
		let unit_id = unit.id();
		let arg_id = UnitId(arg_id);
		let hit = UnitId(unit_id.0+122).hitpoints() as u16;
		let x = ((arg_id.placement().width/2)-(unit_id.placement().width/2)-unit_id.addon_position().width) as i32;
		let y = ((arg_id.placement().height/2)-(unit_id.placement().height/2)-unit_id.addon_position().height) as i32;
		pos_box |= x;
		pos_box |= (y << 16);
		let graphics = bw::units_dat_flingy[arg_id.0 as usize];
		let sprite = bw::sprites_dat_image_id[bw::flingy_dat_sprite_id[graphics as usize] as usize];
		return bw::flingy_subfunc1((*unit.0).sprite, sprite, pos_box, 0)!=null_mut(); 
		
    }

// 00468280 = CancelUnit(), ecx Unit *unit -- https://pastebin.com/raw/BcZbwF9U
/*
pub unsafe fn cancel_unit_hook(
    unit: *mut bw::Unit,
    orig: &dyn Fn(*mut Unit)->u8)->u8 {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit);
			},
        };
        let mut unitCopy = unit.0;
        let mut result = (*unitCopy.0).sprite;
        if result != null_mut() {
            let mut unitOrder = (*unit.0).order;
            if unitOrder != null_mut() {
                result = (*unit.0).flags;
                if (result & 1) != 0 { // Completed
                    let mut unitId = (*unit.0).unit_id;
                    if unitId != 0x2C && // Guardian
                    unitId != 0x67 && // Lurker
                    unitId != 0x3E && // Devourer
                    (unitId != 0x86 || unit::has_nydus_exit(unit.0) = 0) && // Nydus Canal
                    unitId != 0x2B && // Mutalisk
                    unitId != 0x26 { // Hydralisk
                        if (result & 2) = 1 { // Landed
                            if bw::get_race(unit.0) = 0 {
                                bw::cancel_zerg_building(unitCopy.0);
                                return result as u8;
                            }
                            bw::refund_fourth_of_cost(v6, (*unitCopy.0).player); // v6 is NOT defined in function
                        }
                        else {
                            let mut morphUnit: u16 = 0;
                            if unitId == 0x24 || // Egg
                                unitId == 0x3B || // Cocoon
                                unitId == 0x61 { // Lurker egg
                                    morphUnit = (*unit.0).build_queue[(*unit.0).current_build_slot];
                                }
                                else {
                                    morphUnit = (*unit.0).unit_id;
                                }
                                bw::refund_full_cost((*unit.0).player, morphUnit);
                        }
                        let mut returnOrder: u32 = 0;
                        let mut unitId2 = (*unit.0).unit_id;
                        if unitId2 == 0x3B { // Cocoon
                            returnOrder = 0x2B; // 
                        }
                        else {
                            if unitid2 != 0x61 { // Lurker egg
                                if unitId2 == 0xe { // Nuclear missile
                                    let mut conUnit = (*unit.0).related;
                                    if conUnit != null_mut() {
                                        (*conUnit.0).flags = 0;
                                        (*conUnit.0).order_state = 0;
                                    }
                                    bw::update_ui();
                                }
                                bw::kill_unit(unitCopy.0);
                                return result as u8;
                            }
                            returnOrder = 0x26; // Train
                        }
                        bw::transform_unit(unitCopy.0, returnOrder);
                        let mut buildUnit = (*unitCopy.0).current_build_slot;
                        (*unitCopy.0).remaining_build_time = 0;
                        (*unitCopy.0).build_queue[buildUnit.0] = 0xE4; // None
                        let mut PARTYTIME = bw::units_dat_flingy[(*unit.0).previous_unit_id];
                        let mut HEREWEGO = bw::flingy_sprite_id[PARTYTIME];
                        let mut FUCK = bw::sprites_dat_image_id[HEREWEGO];
                        bw::replace_sprite((*unitCopy.0).sprite, FUCK, 0);
                        let mut sprite = (*unitCopy.0).sprite;
                        (*unitCopy.0).order_signal = 0xFB;
                        let mut i = (*sprite.0).overlay;
                        for i in 1..14 {
                            if i == null_mut() {
                                break;
                            }
                            bw::set_iscript_anim(i, 14);
                            i += 1;
                        }
                        bw::issue_order_targ_nothing(unitCopy.0, 0x29); // Zerg birth
                    }
                }
            }
        }
    }
*/
// 0047B8F0 = CalculateSpeedChange_Flingy(), edx Unit *unit (If unit uses flingy movement) -- https://pastebin.com/raw/cYKaJ8VL
/*
pub unsafe fn calculate_speedchange_flingy_hook(
    unit: *mut bw::Unit,
    orig: &dyn Fn(*mut Unit,)->u32)->u32 {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit);
			},
        };
        let mut HAHA = bw::units_dat_flingy[unit.0 + 100];
        let mut OHNO = bw::flingy_dat_top_speed[HAHA];
        return bw::modify_speed(OHNO, unit.0);
    }
*/
// getUnitPlaceboxSize 0048D750 0000005D -- https://pastebin.com/raw/Dy10XY3b
/*
pub unsafe fn get_unit_placebox_size_hook(
    unitId: u32,
    arg_width: u16,
    arg_height: u16,
    orig: &dyn Fn(u32, u16, u16,)) {
        let mut HOHO = bw::units_dat_flingy[unitId];
        let mut HIHI = bw::flingy_sprite_id[HOHO];
        let mut image = bw::image_grps[HIHI];
        let mut placeboxX = bw::units_dat_placement_box[unitId];
        let mut placeboxY = bw::units_dat_placement_box[unitId] + 8;
        let mut frameWidth = (*image.0).width;
        if frameWidth <= placeboxX {
            frameWidth |= placeboxX & 0x0000_ffff;
        }
        arg_width = frameWidth;
        let mut frameHeight = (*image.0).height;
        if frameHeight <= placeboxY {
            arg_height = placeboxY;
        }
        else {
            arg_height = frameHeight;
        }
    }
*/
// 0048DA30 -- https://pastebin.com/raw/099LyZ9D
/*
pub unsafe fn draw_player_color_hook(
    orderId: u16,
    orig: &dyn Fn(u16,)) {
        let mut orderId2 = arg1;
        let mut player: u32 = 0;
        if bw::placement_order != 71 && (bw::placement_order != 36 || orderId) {
            player = (*bw::active_sel.0).player;
        }
        else {
            player = (*bw::active_sel.0).sprite.player;
        }
        let mut color = bw::get_sprite_color(player);
        let mut palette1 = bw::player_color_palette[8 * color];
        let mut palette2 = bw::player_color_palette[8 * color + 4];
        bw::current_draw_player = color;
        bw::default_grp_remap[8] = palette1;
        let mut flingy = bw::units_dat_flingy[unitCopy.0];
        bw::default_grp_remap[12] = palette2;
        let mut NOTAGAIN = bw::flingy_sprite_id[flingy];
        let mut grpFrame = bw::image_grps[NOTAGAIN];
        let mut leftFrame = (*grpFrame.0);
        let mut topFrame = (*grpFrame.0)+1;
        // SetRect((LPRECT)&rc, 0, 0, v7->frames[0].right, v7->frames[0].bottom); // Should define 'rect' variable (I think)
        bw::draw_grp(leftFrame, topFrame, (*grpFrame.0).frames, rect, 0);
    }
*/
/*
// 00497110 = InitSpriteVisionSync() -- https://pastebin.com/raw/dRnw5zh5
pub unsafe fn init_sprite_vision_sync(
    orig: &dyn Fn()->u32)->u32 {
        let mut result: u32 = 0;
        ptr::memset(bw::current_visibility_hash, 0, sizeof(bw::current_visibility_hash));
        bw::want_visibility_hash = 0;
        bw::region_visibility_hash = 0;
        bw::current_hash_offset = 0;
        while result < 228 {
            if bw::units_dat_special_ability_flags[result] & 1 == 0 {
                if bw::units_dat_staredit_availability_flags[result] & 1 >= 1 {
                    bw::sprite_include_in_vision_sync[bw::flingy_sprite_id[bw::units_dat_flingy[result]]] = 1;
                }
                result += 1;
            }
        }
        return result;
    }
*/
/*
// 0049ECF0 = InitializeUnit(), arg 1 unit_id, arg 2 x, arg 3 player, eax Unit *unit, edx y | Called also when transformed -- Needs ASM pseudocode
pub unsafe fn init_unit_hook(
    unitid: u32,
    arg_x: u32,
    player: u32,
    unit: *mut bw::Unit,
    arg_y: u32,
    orig: &dyn Fn(u32, u32, u32, *mut Unit u32,)->u32)->u32 {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unitid, arg_x, player, unit, arg_y);
			},
        };
        let mut result: u32 = 0;
        let mut active_unit = bw::active_iscript_unit;
        let mut flingy = bw::units_dat_flingy[unitid];

        if bw::init_flingy(flingy, arg_y as u16, arg_x, unit, player as u8, 0) == 0 {
            //blockA
            result = 0;
            return result;
        }
        (*unit.0).player = player;
        (*unit.0).order = 0xBC; // Fatal
        (*unit.0).order_state = 0;
        (*unit.0).order_signal = 0;
        (*unit.0).order_fow_unit = 0;
        (*unit.0).order_timer = 0;
        (*unit.0).target = 0;
        (*unit.0).order_target_pos = 0;
        let first_child = (*unit.0).unit_specific; // assumed to be first_inside_child
        (*first_child.0).hitpoints = 0;
        (*first_child.0).sprite = 0;
        (*unit.0).unit_id = unitid;
        let mut sprite = (*unit.0).sprite;
        (*unit.0).carried_powerup_flags = 0;
        (*unit.0).secondary_order_wait = 0;
        if sprite != null_mut() {
            //blockB
            bw::progress_sprite_frame(sprite);
            (*unit.0).last_attacking_player = 0;
            (*unit.0).shields = bw::units_dat_shields[unitid] << 8;
            if unitid != 0xAC { // Shield Battery
                //blockC
                (*unit.0).energy = (bw::get_max_energy(unit.0) + (active_unit & 3)) << 2; // active_unit might also be player_id, or have defaulted to arg_y
            }
            else {
                (*unit.0).energy = 6400;
            }
            //blockD
            (*unit.0).ai_spell_flags = 0;
            (*sprite.0).elevation_level = bw::units_dat_elevation_level[unitid];
            let mut datFlags = bw::units_dat_flags[unitid] >> 1;
            let mut unitFlags = (*unit.0).flags;
            datFlags ^= unitFlags;
            unitFlags ^= unitFlags & 2; // Landed building
            datFlags ^= unitFlags;
            unitFlags = datFlags & 4 ^= unitFlags; // Air

            datFlags = bw::units_dat_flags[unitid] >> 0xC;
            datFlags ^= unitFlags;
            datFlags &= 0x10000; // Organic
            datFlags ^= unitFlags;
            unitFlags = datFlags;

            datFlags = bw::units_dat_flags[unitid] >> 0xA;
            datFlags ^= unitFlags;
            datFlags &= 0x20000; // Requires creep?
            datFlags ^= unitFlags;
            unitFlags = datFlags;
            unitFlags >> 2;
            unitFlags << 14;
            !unitFlags;
            unitFlags ^= datFlags;
            unitFlags &= 0x100000; // Burrowable
            unitFlags ^= datFlags;
            

            (*unit.0).flags = unitFlags;
            let mut pathFlags = (*unit.0).pathing_flags;
            let mut eax: u8 = 0;
            if bw::units_dat_elevation_level[unitid] < 0xC {
                eax = 1;
            }
            else {
                eax = 0;
            }
            eax ^= pathFlags;
            pathFlags = 1;
            eax ^= pathFlags;
            (*unit.0).pathing_flags = eax;
            if unitid < 0xC2 { // Zerg Beacon
                //blockE
                if unitid != 0xCC { // Floor Hatch
                    //blockG
                    if (bw::units_dat_flags[unitid] & 1) == 0 { // Building
                        (*unit.0).tech = 0x2C;
                        (*unit.0).upgrade = 0x3D;
                    }
                }
                //blockF
                (*unit.0).flags |= 0x200000; // No collision
            }
            if unitid <= 0xC7 { // Protoss Flag Beacon
                //blockF
                (*unit.0).flags |= 0x200000; // No collision
                //blockG
                if (bw::units_dat_flags[unitid] & 1) == 0 { // Building
                    (*unit.0).tech = 0x2C;
                    (*unit.0).upgrade = 0x3D;
                }
            }
            //blockH
            eax = 0;
            (*unit.0).path = 0;
            (*unit.0).movement_state = 0;
            (*unit.0).move_target_update_timer = 0;
            let mut imageid = (*sprite.0).image.image_id;
            let mut buildTime = bw::units_dat_build_time[unit];
            if (bw::units_dat_flags[unitid] & 0x20000000) == 1 { // Invincible
                //blockI
                if (bw::units_dat_flags[unitid] & 0x20000000) == 0 { // Invincible
                    //blockK
                    (*unit.0).building_overlay_state = bw::building_overlay_state_max[sprite];
                    (*unit.0).remaining_build_time = buildTime;
                    if buildTime == 0 {
                        (*unit.0).remaining_build_time = 1;
                    }
                    //blockL
                    bw::set_build_hp_gain(unit.0);
                    bw::set_build_shield_gain(unit.0);
                    bw::set_upgrade_speed(unit.0);
                    bw::update_speed(unit.0);
                    result = 1;
                    return result;
                }
                eax = (*unit.0).flags;
                eax &= 0xFBFF_FFFF
            }
            else {
                eax |= 4000000; // Invincible
            }
            //blockJ
            (*unit.0).flags = eax;
            //blockK
            (*unit.0).building_overlay_state = bw::building_overlay_state_max[sprite];
            (*unit.0).remaining_build_time = buildTime;
            if buildTime == 0 {
                (*unit.0).remaining_build_time = 1;
            }
            //blockL
            bw::set_build_hp_gain(unit.0);
            bw::set_build_shield_gain(unit.0);
            bw::set_upgrade_speed(unit.0);
            bw::update_speed(unit.0);
            result = 1;
            return result;
        }
    }
*/
// 004E99D0 = Unburrow_Generic(), eax Unit *unit | Both zerg units and spider mines -- https://pastebin.com/raw/TuhDBZ94
/*
pub unsafe fn unburrow_gen_hook(
    unit: *mut bw::Unit,
    orig: &dyn Fn(*mut unit)->u8)->u8 {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit);
			},
        };
        let mut unitCopy = unit.0;

        let mut sprite = (*unitCopy.0).sprite;
        let mut pathFlags = (*unitCopy.0).pathing_flags;
        (*unitCopy.0).flags = 0;
        let mut flags = (*unitCopy.0).flags;
        (*unitCopy.0).pathing_flags = (pathFlags ^ ((*sprite.0).elevation_level < 0xC)) & 1 ^ pathFlags;
        let mut unitId = (*unitCopy.0).unit_id;
        (*unitCopy.0).flags = flags & 0xFFFF_F7EF;
        let mut XD = bw::units_dat_flingy[unitId];
        return bw::switch_sprite_id(sprite, bw::flingy_sprite_id[XD]);
    }
*/
// 0047B850 = CalculateTurnSpeed(), ecx Unit *unit -- https://pastebin.com/raw/3zmABM9K
/*
pub unsafe fn calculate_turn_speed_hook(
    unit: *mut bw::Unit,
    orig: &dyn Fn(*mut Unit)->u32,)->u32 {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit);
			},
        };
        let mut result: u32 = bw::flingy_dat_turn_radius[bw::units_dat_flingy[(*unit.0).unit_id]];
        let mut var2: u32 = 0;
        if (*unit.0).stim_timer > 0 {
            var2 = 1;
        }
        if ((*unit.0).flags & 0x10000000) == 1 { // Has speed upgrade
            var2 += 1;
        }
        if (*unit.0).ensnare_timer > 0 {
            v2 -= 1;
        }
        if v2 <= 0 {
            if v2 < 0 {
                result -= result >> 2;
            }
            else {
                result *= 2;
            }
        }
        return result;
    }
*/
// 0047B8A0 = CalculateAcceleration(), ecx Unit *unit -- https://pastebin.com/raw/eHvdTnPt
/*
pub unsafe fn calculate_accel(
    unit: *mut bw::Unit,
    orig: &dyn Fn(*mut Unit)->u32,)->u32 {
        let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit);
			},
        };
        let mut result: u32 = bw::flingy_dat_acceleration[units_dat_flingy[(*unit.0).unit_id]];
        let mut var2L u32 = 0;
        if (*unit.0).stim_timer > 0 {
            var2 = 1;
        }
        if ((*unit.0).flags & 0x10000000) == 1 { // Has speed upgrade
            var2 += 1;
        }
        if (*unit.0).ensnare_timer > 0 {
            v2 -= 1;
        }
        if v2 <= 0 {
            if v2 < 0 {
                result -= result >> 2;
            }
            else {
                result *= 2;
            }
        }
        return result;
    }
*/