
fn cdq(
	_i64: i64)->{
		if _i64.signum()>=0 {
			0
		}
		-1
}





/*
pub unsafe fn ai_workerai(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
	
	
//	orig(unit);
//	return;
// 1
	
	let worker = match Unit::from_ptr(unit){
			Some(s)=>s,
			None=>{
				orig(unit);	
				return;
			},
     };
     let mut v2: u32 = 0;
	 let mut v7: u32 = 0;
	 let mut v15 = 0;
	 let mut v17 = 0;
	 let mut v13: *mut bw::Unit = null_mut();
	 if let Some(worker_ai) = worker.worker_ai(){
		let ai = bw::player_ai(worker.player() as u32);
		if (*ai).request_count == 0 || (*ai).requests[0 as usize].ty!=3 {//3 is ConstructMorph
			return 0;
		}
		let v4 = (*worker_ai).town;
		if (*worker_ai).wait_timer != 0 {
			(*worker_ai).wait_timer += 1;
			return;
		}
		(*worker_ai).last_update_second = (*game.0).elapsed_seconds;
		let id = worker.id();
		
		if id.group_flags() & 0x5 != 0 || !id.is_terran() || bw::ai_repairsomething(worker.0,v4)==0 {//0x5 - zerg and protoss
			let v6 = (*v4).main_building;
			if v6 != null_mut(){
				v7 = bw::is_resource_depot(v6);
				if v7 != 0 {
					let v8 = (*worker_ai).target_resource;//0x1 - minerals, 0x2 - gas, 0x2 - gas 2, 0x3 - gas 3
					if v8 == 1 {
						if let Some(current_targ) = worker.current_harvest_target_opt(){
							bw::ai_harvest(null_mut(),unit.0,target);
						}
						else {
							let mut v11 = Unit::from_ptr((*v4).mineral);
							if v11.is_none() || v11.unwrap().used_resource_area()!=0 {
								//
								let sprite = (*worker.0).sprite;
								let pos_xy = (*sprite).position.x as u32 + ((*sprite).position.y<<16) as u32;
								let res = bw::find_free_resource(worker.0,pos_xy,index as u32);
								if res == null_mut(){
									v11 = Unit::from_ptr((*v4).mineral);
								}
								else {
									v11 = Unit::from_ptr(res);
								}
							}
							let mut v12 =  bw::get_town_gas(3,1,v4);
							if v12 == null_mut(){
								v12 = bw::get_town_gas(2,1,v4);
							}
							bw::ai_harvest(v12,worker.0,v11.0);
						}
						return;
					}
					//
					if v8 == 2 {
						v13 = bw::get_town_gas(3,1,v4);
						//
						//GOTO LABEL 44
						//
					}
					v15 = 0;
					v17 = 0;
					v14 = (*v4).gas_buildings[0];
					
				}
			}
		}
	}

		  //
          if ( v8 == 2 )
          {
            v13 = GetAIRefinery(v4, 3, 1);
            goto LABEL_44;
          }
          v15 = 0;
          v17 = v8 == 3;
          v14 = v4->gas_0;
          if ( v17 )
          {
            if ( v14 )
            {
              if ( AI_getVespeneType(v14, (unsigned __int8)v4->ubOwner) == 3 )
                v15 = 1;
            }
            v16 = v4->gas_1;
            if ( !v16 || AI_getVespeneType(v4->gas_1, (unsigned __int8)v4->ubOwner) != 3 || (++v15, v15 != 2) )
            {
              v16 = v4->gas_2;
              if ( !v16 || AI_getVespeneType(v4->gas_2, (unsigned __int8)v4->ubOwner) != 3 )
                goto LABEL_45;
              v17 = v15 == 1;
LABEL_42:
              if ( !v17 )
                goto LABEL_45;
              goto LABEL_43;
            }
          }
          else
          {
            if ( v14 )
            {
              if ( AI_getVespeneType(v14, (unsigned __int8)v4->ubOwner) == 3 )
                v15 = 1;
            }
            v16 = v4->gas_1;
            if ( !v16 || AI_getVespeneType(v4->gas_1, (unsigned __int8)v4->ubOwner) != 3 || (++v15, v15 != 3) )
            {
              v16 = v4->gas_2;
              if ( !v16 || AI_getVespeneType(v4->gas_2, (unsigned __int8)v4->ubOwner) != 3 )
                goto LABEL_45;
              v17 = v15 == 2;
              goto LABEL_42;
            }
          }
LABEL_43:
          v13 = v16;
LABEL_44:
          if ( v13 )
          {
LABEL_88:
            AI_AutoHarvest((int)v13, (int)unitMem, (int)v4->field_2C);
            return;
          }
LABEL_45:
          v13 = GetAIRefinery(v4, 3, 1);
          if ( !v13 )
          {
            v18 = v4->gas_0;
            v19 = 0;
            if ( v18 )
            {
              if ( AI_getVespeneType(v18, (unsigned __int8)v4->ubOwner) == 3 )
                v19 = 1;
            }
            v20 = v4->gas_1;
            if ( (!v20 || AI_getVespeneType(v4->gas_1, (unsigned __int8)v4->ubOwner) != 3 || (++v19, v19 != 2))
              && ((v20 = v4->gas_2) == 0 || AI_getVespeneType(v4->gas_2, (unsigned __int8)v4->ubOwner) != 3 || v19 != 1)
              || (v13 = v20) == 0 )
            {
              v21 = v4->gas_0;
              v22 = 0;
              if ( v21 )
              {
                if ( AI_getVespeneType(v21, (unsigned __int8)v4->ubOwner) == 3 )
                  v22 = 1;
              }
              v23 = v4->gas_1;
              if ( (!v23 || AI_getVespeneType(v4->gas_1, (unsigned __int8)v4->ubOwner) != 3 || (++v22, v22 != 3))
                && ((v23 = v4->gas_2) == 0 || AI_getVespeneType(v4->gas_2, (unsigned __int8)v4->ubOwner) != 3
                                           || v22 != 2)
                || (v13 = v23) == 0 )
              {
                v13 = GetAIRefinery(v4, 2, 1);
                if ( !v13 )
                {
                  v24 = v4->gas_0;
                  v28 = 0;
                  if ( v24 )
                  {
                    if ( AI_getVespeneType(v24, (unsigned __int8)v4->ubOwner) == 2 )
                      v28 = 1;
                  }
                  v25 = v4->gas_1;
                  if ( (!v25 || AI_getVespeneType(v4->gas_1, (unsigned __int8)v4->ubOwner) != 2 || (++v28, v28 != 2))
                    && ((v25 = v4->gas_2) == 0
                     || AI_getVespeneType(v4->gas_2, (unsigned __int8)v4->ubOwner) != 2
                     || v28 != 1)
                    || (v13 = v25) == 0 )
                  {
                    v29 = 0;
                    if ( v24 )
                    {
                      if ( AI_getVespeneType(v24, (unsigned __int8)v4->ubOwner) == 2 )
                        v29 = 1;
                    }
                    v26 = v4->gas_1;
                    if ( v26 && AI_getVespeneType(v4->gas_1, (unsigned __int8)v4->ubOwner) == 2 && (++v29, v29 == 3)
                      || (v26 = v4->gas_2) != 0
                      && AI_getVespeneType(v4->gas_2, (unsigned __int8)v4->ubOwner) == 2
                      && v29 == 2 )
                    {
                      v13 = v26;
                    }
                    else
                    {
                      v13 = 0;
                    }
                  }
                }
              }
            }
          }
          goto LABEL_88;
        }
      }
    }
  }
}
*/	






pub unsafe fn ai_tryprogressspendingqueue_worker_hook(

	town: *mut bw::AiTown
	worker: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::AiTown,*mut bw::Unit)->u32,)->u32 {
	
		orig(town,worker)
	}
	
	
	
	


// 2
//0043D5D0 = ProgressMilitaryAi?(), arg 1 Unit *unit, arg 2 move_order?, arg 3 x, arg 4 y
pub unsafe fn progress_military_ai(
	unit: *mut bw::Unit,
	move_order: u32,
	x: u32,
	y: u32,
	orig: &dyn Fn(*mut bw::Unit,u32,u32,u32){
	
	}
// 2
void __stdcall AI_OrderToDestination(CUNIT *unit, int move_order, int x, int y)
{
  CAIController *v4; // eax
  CUNIT *v5; // esi
  DWORD v6; // edx
  unsigned __int8 v7; // cl
  bool v8; // zf
  int v9; // eax
  int v10; // ecx
  CUNIT *v11; // edi
  aiCaptain *v12; // edx
  aiCaptain *v13; // edx
  int v14; // eax
  DWORD v15; // eax
  int v16; // ebx
  WORD v17; // ax
  int v18; // eax
  int v19; // esi
  unsigned __int16 v20; // ax
  int v21; // edx
  int v22; // eax
  int v23; // edi
  int v24; // ebx
  char v25; // al
  int v26; // edx
  signed int v27; // ebx
  int v28; // esi
  WORD v29; // ax
  int x1; // [esp+4h] [ebp-10h]
  int v31; // [esp+8h] [ebp-Ch]
  int y1; // [esp+Ch] [ebp-8h]
  unsigned __int8 v33; // [esp+13h] [ebp-1h]


  let v5 = Unit::from_ptr(unit).unwrap();
  let v4 = v5.military_ai();
  
  if let Some(v4) = Unit::from_ptr(unit).military_ai()
  {
    if (*v4).parent == unit && !v5.id().is_building()
    {
      let v6 = (*v5.0).flags;
      if v5.is_completed()
      {
        let v7 = (*v5.0).order;
        if v7 != 0x6a && v7 != 96 )//archon summon, reset collision
        {
          if v6 & 0x20 != 0 //in building
          {
            Unload(unit);
          }
          else if ( v7 == ORD_SIEGE || v7 == ORDERID_Ignore )
          {
            return;
          }
          v8 = unit->type == UNITID_TerranSiegeTankSiegeMode;
          v31 = 0;
          if ( v8 )
          {
            if ( unit->mainOrderType == ORD_UNSIEGE )
              return;
            LOBYTE(v9) = AI_SiegedTankCanAttackUnitNear(v7, unit);
            if ( v9 )
            {
              if ( !BurrowIdle_Normal(unit, 0, 1, 0) )
              {
                v11 = sub_443080((int)unit, v10);
                if ( v11 )
                {
                  resetSubunitTarget(unit);
                  sub_476FC0((int)v11, unit, 0, 0);
                }
              }
              return;
            }
            if ( unit->type == UNITID_TerranSiegeTankSiegeMode )
            {
              orderComputer_cl(unit, ORD_UNSIEGE);
              v5 = unit;
              v31 = 1;
            }
          }
          if ( isAIControllerNotABuilding(v5) )
          {
            v13 = v5->CAIControl->cptn;
            if ( !isUnknownSpellcaster((int)v5) )
            {
              if ( v12->captainType == 1 || (LOBYTE(v14) = sub_4369F0(v12), v14) )
                move_order = v5->type != UNI_T_MEDIC ? 158 : 177;
            }
            v15 = v5->statusFlags;
            if ( !(v15 & USFlag_InTransport) || v15 & USFlag_InBuilding )
            {
              v33 = v5->owner;
              v16 = SAI_GetRegionIdFromPxEx(x, y);
              v17 = getRegionIdFromUnit(v5);
              if ( sub_437E70(v33, v16, v17) )
              {
                v20 = getRegionIdFromUnit(unit);
                getRegionCenterFromId2(v20, (DWORD *)&x1, (DWORD *)&y1);
                v19 = x1;
                getDirectionFromPoints(y, x1, x, y1);
                RandomizeShort(56);
                v22 = RandomizeShort(56) & 0x7F;
                v23 = x + (v22 * retreatArray[v21].x >> 8);
                v24 = y + (v22 * retreatArray[v21].y >> 8);
                x1 = getDirectionFromPoints(y + (v22 * retreatArray[v21].y >> 8), v19, v23, y1);
                getDistanceFast(v19, v23, y1, v24);
                v25 = RandomizeShort(56);
                v18 = v26 + (v25 & 0x7F) - 64;
                if ( v18 < 0 )
                  v18 = 0;
                v27 = v19 + (v18 * retreatArray[x1].x >> 8);
                v28 = y1 + (v18 * retreatArray[x1].y >> 8);
                if ( v27 >= 0 && v27 < (unsigned __int16)gwMapPixWidth && v28 >= 0 && v28 < gwMapPixHeight )
                {
                  x1 = SAI_GetRegionIdFromPxEx(v27, y1 + (v18 * retreatArray[x1].y >> 8)) << 6;
                  v29 = SAI_GetRegionIdFromPxEx(x, y);
                  if ( *(__int16 *)((char *)&SAI_Paths->regions[0].groupIndex + x1) == SAI_Paths->regions[v29].groupIndex )
                  {
                    x = v27;
                    y = v28;
                  }
                }
              }
              if ( v31 )
              {
                if ( !unit->orderQueueHead )
                {
                  doOrder(unit, move_order, x, y, 0);
                  toIdle(unit);
                }
              }
              else
              {
                orderTarget(unit, move_order, x, y);
              }
            }
            else
            {
              sub_43C980(ORDERID_ComputerAI, v5);
            }
          }
        }
      }
    }
  }
}









//0043D5D0 = ProgressMilitaryAi?(), arg 1 Unit *unit, arg 2 move_order?, arg 3 x, arg 4 y

void __stdcall AI_OrderToDestination(CUNIT *a1, int a3, int x, int y)
{
  CAIController *v4; // eax
  CUNIT *v5; // esi
  DWORD v6; // edx
  unsigned __int8 v7; // cl
  bool v8; // zf
  int v9; // eax
  int v10; // ecx
  CUNIT *v11; // edi
  aiCaptain *v12; // edx
  aiCaptain *v13; // edx
  int v14; // eax
  DWORD v15; // eax
  int v16; // ebx
  WORD v17; // ax
  int v18; // eax
  int v19; // esi
  unsigned __int16 v20; // ax
  int v21; // edx
  int v22; // eax
  int v23; // edi
  int v24; // ebx
  char v25; // al
  int v26; // edx
  signed int v27; // ebx
  int v28; // esi
  WORD v29; // ax
  int x1; // [esp+4h] [ebp-10h]
  int v31; // [esp+8h] [ebp-Ch]
  int y1; // [esp+Ch] [ebp-8h]
  unsigned __int8 v33; // [esp+13h] [ebp-1h]

  v5 = a1;
  v4 = a1->CAIControl;
  if ( v4 )
  {
    if ( v4->controlType == 4 && v4->unit == a1 && !(unitsdat_SpecialAbilityFlags[(unsigned __int16)a1->type] & 1) )
    {
      v6 = a1->statusFlags;
      if ( v6 & 1 )
      {
        v7 = a1->mainOrderType;
        if ( v7 != ORDERID_CompletingArchonsummon && v7 != ORDERID_ResetCollision1 )
        {
          if ( v6 & USFlag_InBuilding )
          {
            Unload(a1);
          }
          else if ( v7 == ORD_SIEGE || v7 == ORDERID_Ignore )
          {
            return;
          }
          v8 = a1->type == UNITID_TerranSiegeTankSiegeMode;
          v31 = 0;
          if ( v8 )
          {
            if ( a1->mainOrderType == ORD_UNSIEGE )
              return;
            LOBYTE(v9) = AI_SiegedTankCanAttackUnitNear(v7, a1);
            if ( v9 )
            {
              if ( !BurrowIdle_Normal(a1, 0, 1, 0) )
              {
                v11 = sub_443080((int)a1, v10);
                if ( v11 )
                {
                  resetSubunitTarget(a1);
                  sub_476FC0((int)v11, a1, 0, 0);
                }
              }
              return;
            }
            if ( a1->type == UNITID_TerranSiegeTankSiegeMode )
            {
              orderComputer_cl(a1, ORD_UNSIEGE);
              v5 = a1;
              v31 = 1;
            }
          }
          if ( isAIControllerNotABuilding(v5) )
          {
            v13 = v5->CAIControl->cptn;
            if ( !isUnknownSpellcaster((int)v5) )
            {
              if ( v12->captainType == 1 || (LOBYTE(v14) = sub_4369F0(v12), v14) )
                a3 = v5->type != UNI_T_MEDIC ? 158 : 177;
            }
            v15 = v5->statusFlags;
            if ( !(v15 & USFlag_InTransport) || v15 & USFlag_InBuilding )
            {
              v33 = v5->owner;
              v16 = SAI_GetRegionIdFromPxEx(x, y);
              v17 = getRegionIdFromUnit(v5);
              if ( sub_437E70(v33, v16, v17) )
              {
                v20 = getRegionIdFromUnit(a1);
                getRegionCenterFromId2(v20, (DWORD *)&x1, (DWORD *)&y1);
                v19 = x1;
                getDirectionFromPoints(y, x1, x, y1);
                RandomizeShort(56);
                v22 = RandomizeShort(56) & 0x7F;
                v23 = x + (v22 * retreatArray[v21].x >> 8);
                v24 = y + (v22 * retreatArray[v21].y >> 8);
                x1 = getDirectionFromPoints(y + (v22 * retreatArray[v21].y >> 8), v19, v23, y1);
                getDistanceFast(v19, v23, y1, v24);
                v25 = RandomizeShort(56);
                v18 = v26 + (v25 & 0x7F) - 64;
                if ( v18 < 0 )
                  v18 = 0;
                v27 = v19 + (v18 * retreatArray[x1].x >> 8);
                v28 = y1 + (v18 * retreatArray[x1].y >> 8);
                if ( v27 >= 0 && v27 < (unsigned __int16)gwMapPixWidth && v28 >= 0 && v28 < gwMapPixHeight )
                {
                  x1 = SAI_GetRegionIdFromPxEx(v27, y1 + (v18 * retreatArray[x1].y >> 8)) << 6;
                  v29 = SAI_GetRegionIdFromPxEx(x, y);
                  if ( *(__int16 *)((char *)&SAI_Paths->regions[0].groupIndex + x1) == SAI_Paths->regions[v29].groupIndex )
                  {
                    x = v27;
                    y = v28;
                  }
                }
              }
              if ( v31 )
              {
                if ( !a1->orderQueueHead )
                {
                  doOrder(a1, a3, x, y, 0);
                  toIdle(a1);
                }
              }
              else
              {
                orderTarget(a1, a3, x, y);
              }
            }
            else
            {
              sub_43C980(ORDERID_ComputerAI, v5);
            }
          }
        }
      }
    }
  }
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
// 1
//Ai_
signed int __stdcall Ai_TryProgressSpendingQueue_Worker(town *pTown, CUNIT *unitMem)
{
  int v2; // eax
  CUNIT *pUnit; // edi
  CAIController *aiCtrl; // edx
  int ownr; // ecx
  unsigned int _buildType; // ebx
  unsigned __int16 buildType; // ax
  int v9; // edx
  town *_pTown; // esi
  CUNIT *v11; // eax
  CSPRITE *v12; // eax
  __int16 v13; // dx
  __int16 _ptwnPos_Y; // dx
  town *__pTown; // esi
  unsigned __int16 rgnIdAtPos; // ax
  int v17; // edi
  aiCaptain *v18; // esi
  pathsPrime *v19; // edx
  int v20; // ecx
  __int16 v21; // dx
  __int16 v22; // ax
  __int16 v23; // cx
  unsigned __int16 v24; // ax
  signed int v25; // eax
  int v26; // eax
  DWORD v27; // esi
  unsigned __int8 v28; // al
  unsigned int v29; // [esp-18h] [ebp-28h]
  town *v30; // [esp-14h] [ebp-24h]
  signed int v31; // [esp-Ch] [ebp-1Ch]
  int nTownOwner; // [esp+4h] [ebp-Ch]
  unsigned int __buildType; // [esp+8h] [ebp-8h]
  dwPosition _pos; // [esp+Ch] [ebp-4h]

	let pUnit: *mut bw::Unit = worker;
	let unit = Unit::from_ptr
	let mut v2 = (*worker).player;
	let ai = bw::player_ai(v2 as u32);
	if (*ai).request_count == 0 || (*ai).requests[0 as usize].ty!=3 {//3 is ConstructMorph
		return 0;
	}
	
	let worker = match Unit::from_ptr(worker){
			Some(s)=>s,
			None=>{
				return orig(town,worker);			
			},
     };
	let worker_ai = worker.worker_ai().unwrap();
	if (*worker_ai).town != (*ai).requests[0 as usize].val as *mut bw::AiTown && (*ai).requests[0 as usize].id!=109 {//supply depot
		return 0;
	}
	if (*worker_ai).reassign_count >= 3 {
		return 0;
	}
	let request_id = (*ai).requests[0 as usize].id;
	let mut _buildType = request_id;
	let mut __buildType = request_id;
	if bw::are_same_race(request_id, worker.0)==0 || bw::check_unit_dat_requirements(worker.player(),request_id,worker.0)!=1 
		|| bw::Ai_DoesHaveResourcesForUnit(worker.player(),request_id)==0 {
		return 0;
	}
	let mut _pos = bw::Point{x: 0, y: 0};
	let _pTown = town;
	let nTownOwner = (*town).player;
	let mut v9 = 0;
	if _buildType != bw::get_gas_building_id(nTownOwner) {
		if _buildType == 125 {//bunker
			let ptr = &mut _pos as *mut bw::Point;
			if bw::get_bunker_position((*town).player,ptr) == 0 {//is EBP-4, not sure how to hook that
				_pos.x = (*town).position.x;
				_pos.y = (*town).position.y;
			}
			let rngIdAtPos = bw::get_region(_pos);
			let __pTown = town;
			bw::do_area_fixups_for_enemy((*town).player,rngIdAtPos,1);
			pUnit = worker.0;
			if (*ai).currently_building_bunkers==0 {
				
			}
			
		//
	
	   //
	   
      if ( !AIScript_Controller[(unsigned __int8)unitMem->owner].builtSomething )
      {
        if ( !AIManageBuildingPlacement(unitMem, UNI_T_PILLBOX, _pos, &_pos, 64) )
        {
          AIScript_Controller[(unsigned __int8)pUnit->owner].builtSomething = 1;
          return AI_FinishUnit(pUnit);
        }
        v17 = SAI_GetRegionIdFromPxEx(_pos.x, _pos.y);
        v18 = &AI_RegionCaptains[(unsigned __int8)__pTown->ubOwner][v17];
        if ( !v18->captainType )
        {
          AI_AssignMilitary(4, v18);
          v19 = SAI_Paths;
          v20 = (unsigned __int16)v17;
          pUnit = unitMem;
          v18->captainFlags |= 0x41u;
          LOWORD(_buildType) = __buildType;
          if ( v19->regions[v20].accessabilityFlags != 0x1FFD )
            v18->field_C = 1000;
          goto LABEL_44;
        }
        pUnit = unitMem;
LABEL_43:
        LOWORD(_buildType) = __buildType;
LABEL_44:
        _pTown = pTown;
        goto LABEL_45;
      }
    }
    else if ( _buildType == UNI_T_TURRET )
    {
      if ( !GetTurretPosition(v9, (dwPosition *)&unitMem) )
      {
        v21 = pTown->position.y;
        LOWORD(unitMem) = pTown->position.x;
        HIWORD(unitMem) = v21;
      }
      if ( !AIScript_Controller[(unsigned __int8)pUnit->owner].builtSomething )
      {
        if ( !AIManageBuildingPlacement(pUnit, UNI_T_TURRET, (dwPosition)unitMem, &_pos, 40) )
        {
          AIScript_Controller[(unsigned __int8)pUnit->owner].builtSomething = 1;
          return AI_FinishUnit(pUnit);
        }
        goto LABEL_43;
      }
    }
    else
    {
      if ( _buildType == UNI_Z_HATCHERY && _pTown->field_24 )
      {
        sub_444630((unsigned __int8)_pTown->baseID, (WORD *)&pTown, (WORD *)&pTown + 1);
        if ( AIScript_Controller[(unsigned __int8)pUnit->owner].builtSomething )
          return AI_FinishUnit(pUnit);
        v31 = 64;
        v30 = pTown;
        v29 = UNI_Z_HATCHERY;
      }
      else
      {
        v22 = _pTown->position.x;
        v23 = _pTown->position.y;
        _pos.x = _pTown->position.x;
        _pos.y = v23;
        if ( _buildType == UNI_Z_COLON || _buildType == UNI_P_CANNON )
        {
          v24 = SAI_GetRegionIdFromPxEx(v22, v23);
          DoAreaFixupsForEnemy(nTownOwner, v24, 4u);
          pUnit = unitMem;
          _buildType = __buildType;
          _pTown = pTown;
        }
        v25 = 64;
        if ( AIScript_Controller[(unsigned __int8)pUnit->owner].builtSomething )
          return AI_FinishUnit(pUnit);
        if ( (_WORD)_buildType == UNI_T_TURRET )
          v25 = 40;
        v31 = v25;
        v30 = (town *)_pos;
        v29 = _buildType;
      }
      if ( AIManageBuildingPlacement(pUnit, v29, (dwPosition)v30, &_pos, v31) )
        goto LABEL_45;
      AIScript_Controller[(unsigned __int8)pUnit->owner].builtSomething = 1;
    }
    return AI_FinishUnit(pUnit);
  }
  v11 = GetAIRefinery(_pTown, 1, 1);
  if ( !v11 )
    return 0;
  v12 = v11->sprite;
  v13 = v12->position.x;
  LOWORD(v12) = v12->position.y;
  _pos.x = v13;
  _pos.y = (signed __int16)v12;
LABEL_45:
  v27 = (unsigned __int8)_pTown->ubOwner;
  AI_Reserve(v27, 1);
  sub_4476D0(v27);
  v26 = (unsigned __int16)pUnit->type;
  switch ( v26 )
  {
    case UNI_T_SCV:
      v28 = ORDERID_SCVBuild1;
      goto doBuild;
    case UNI_Z_DRONE:
      v28 = ORDERID_DroneStartBuild;
      goto doBuild;
    case UNI_P_PROBE:
      v28 = ORDERID_ProbeBuild1;
doBuild:
      orderTarget(pUnit, v28, _pos.x, _pos.y);
      refundAllQueueSlots(pUnit);
      HasMoneyCanMake(pUnit, _buildType);
      return 1;
  }
  return 0;
}







/*
00473300 = IsTileBlockedBy(), arg 1 Unit *builder, arg 2 x_tile, arg 3 y_tile, arg 4 dont_ignore_reacting,
    arg 5 also_invisible, eax Unit **arr
*/

	//0x00473300 => is_tile_blocked_by(*mut Unit, i16,i16,bool,bool,@eax *mut *mut Unit)->u32;
	
	
pub unsafe fn does_building_block(
	unit: *mut bw::Unit,
	x_tile: u32,
	y_tile: u32,
	orig: &Fn(*mut bw::Unit,u32,u32)->u32,)->u32 {
	
		//orig(unit,x_tile,y_tile)
	
	

  let mut x_pos = 0;
  let mut v4 = 0;
  let mut result = 1;
 //unit - a1, x_tile - a2, y_tile - a3

  result = 1;
  if unit != null_mut()
  {
    if (*unit).flags & 0x2 != 0
    {
      if (*unit).unit_id != 134 && !(a1->sprite->flags & 0x20)//nydus
      {
        x_pos = 32 * x_tile;
        if bw::unit_positions_x[(*unit).position_search_left as usize].key < x_pos + 32 && 
			unitFinderX[(*unit).position_search_right as usize].key > x_pos
        {
          y_pos = 32 * y_tile;
          if bw::unit_positions_y[(*unit).position_search_top as usize].key < y_pos + 32 && 
			bw::unit_positions_y[(*unit).position_search_bottom as usize].key > y_pos {
            result = 0;
		   }
        }
      }
    }
  }
  return result;
}
*/














pub unsafe fn update_building_placement_units_newhook(
	builder: *mut bw::Unit,
	x_tile: u32,
	player: u32,
	unit_id: u32,
	size_wh: u32,
	placement_state_entry: u32,
	dont_ignore_reacting: u32,
	also_invisible: u32,
	without_vision: u32,
	y_tile: u32,
	orig: &Fn(*mut bw::Unit,u32,u32,u32,u32,u32,u32,u32,u32,u32)->u32)->u32{



	//eax - y_tile
	
	//a2 - builder
	//a3 - x_tile
	//a4 - player
	//a5 - unit_id
	//a6 - size_wh
	//a7 - placement_state_entry
	//a8 - dont_ignore_reacting
	//a9 - also_invisible
	//a10 - without_vision
	
//0x00473720 => update_building_placement_units(*mut Unit,u32,u32,u32,u32,u32,u32,u32,u32,@eax u32)->u32;

  let builder = match Unit::from_ptr(builder){
			Some(s)=>s,
			None=>{
				return orig(builder,x_tile,player,unit_id,size_wh,placement_state_entry,dont_ignore_reacting,also_invisible,
						without_vision,y_tile);			
		},
  };
  let mut x_offset = x_tile;
  let mut y_offset = y_tile;
  let map_width = (*game.0).map_width_tiles as i16;
  let mut position_tile = *(*bw::tile_flags).offset(x_tile as i16 + (y_tile as i16)*map_width);
  let is_human = (bw::player_list[player as usize].ty==2);//bool
  
  let mut player_shift = 0;
  let mut return_value = 0;//name to be determined
  if is_human {
	 player_shift = 1 << player;
  }
  let builder_id = builder.id();
  let id_built = UnitId(unit_id);
  let mut placement_w = id_built.placement().width as i64;
  let mut placement_h = id_built.placement().height as i64;
  let placement_off_x = (32*x_tile + (placement_w - cdq(placement_w)))/2;
  let placement_off_y = (32*y_tile + placement_h)/2;
  //v14 = 32 * a3 + (((signed int)v13 - HIDWORD(v13)) >> 1);
  let collision_rect = builder_id.dimensions();
  

  let rect = bw::Rect {
		left: (placement_off_x - collision_rect.left) as i16,
		top: (placement_off_y - collision_rect.top) as i16,
		right: (placement_w + collision_rect.right) as i16,
		bottom: (placement_h + collision_rect.bottom) as i16,
  };	
  let rect_ptr = &mut rect as *mut bw::Rect;
  let units = bw::find_unit_borders_rect(rect_ptr);

  let mut itr = 0;
  let mut y_itr = y_offset;
  //0xff
  let size_width = size_wh & 0x0000_ffff;
  let size_height = (size_wh & 0xffff_0000)>>32;
  let mut x_offset_mut = 0;
  let mut position_tile_offset = 0;
  if size_height > 0 
  {
	let mut width_mut = size_width;
    let mut y_iterator = 0;//y_iterator
    let width_diff = 4 * (map_width - width);
    let width_diff2 = width_diff;
    'outer: loop
    {
      let mut placement_x_offset_modified = 0;
      x_offset_mut = x_offset;
      if width_mut > 0
      {
        let mut placement_y_offset = 6 * (y_iterator + 8 * placement_state_entry);
        let mut placement_y_default = placement_y_offset;
        let mut placement_x_offset = 0;
        'inner: loop
        {
          let mut placement_error = 0;
		  if !bw::building_placement_state[(placement_y_offset+placement_x_offset) as usize] && (without_vision || !((player_shift & position_tile) != 0))
          {
//			
			//00473410 = DoesBuildingBlock(), eax Unit *unit, edx x_tile, ecx y_tile
			//eax, edx, ecx
			let mut does_block = bw::does_building_block(builder,x_offset_mut,y_itr as u16);
			does_block &= 0xff;
            if (position_tile & 0x8000000)!=0 && does_block!=0 
            {
              placement_error = 4;
            }
            else
            {
			/*
			00473300 = IsTileBlockedBy(), arg 1 Unit *builder, arg 2 x_tile, arg 3 y_tile, arg 4 dont_ignore_reacting,
    arg 5 also_invisible, eax Unit **arr

			*/
			//signed int __userpurge sub_473300@<eax>(CUNIT **a1@<eax>, CUNIT *a2, int a3, int a4, char a5, char a6)
			//0x00473300 => is_tile_blocked_by(*mut Unit, u32,u32,bool,bool,@eax *mut *mut Unit)->u32;
			  let mut tile_blocked = bw::is_tile_blocked_by(builder,x_offset_mut,y_itr as i16,dont_ignore_reacting,also_invisible,units);
              if tile_blocked != 0 {
                placement_error = tile_blocked;
			  }
            }
            if return_value <= placement_error {
              return_value = placement_error;
			}
            placement_y_offset = v37;
			bw::building_placement_state[(placement_y_offset + placement_x_offset) as usize] = placement_error;
          }
		  position_tile_offset += 1;
		  position_tile = *(*bw::tile_flags).offset(x_tile as i16 + position_tile_offset + (y_tile as i16)*map_width);
		  placement_x_offset_modified += 1;
		  placement_x_offset = placement_x_offset_modified;
          width_mut = size_width;
		  x_offset_mut += 1;
        }
		
        if !(placement_x_offset_modified as i16 < size_width){
			break inner;
		}
        x_offset = x_tile;
        width_diff = width_diff2;
		
      }
	  position_tile_offset += width_diff;
	  position_tile = *(*bw::tile_flags).offset(x_tile as i16 + position_tile_offset + (y_tile as i16)*map_width);
      y_iterator = itr + 1;
      itr = y_iterator;
      y_itr += 1;
      y_iterator = y_iterator & 0x0000ffff;
	  if y_iterator >= size_height {
		break outer;
	  }
    }
    
  }
  bw::position_search_results_data[0] -=1;
  let search_results = bw::position_search_results_data[bw::position_search_results_data[0]];
  *bw::position_search_result_units_count = search_results;
  return return_value;
}